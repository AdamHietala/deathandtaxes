grp medieval city = {
	grp animations = {
		str city medieval = {yes}
	}
	str minimum size = {0.0}
	str maximum size = {max}
	grp requirements = {
		grp infrastructure levels = {
		}
		grp tech levels = {
		}
		grp technologies = {
			str farming = {yes}
		}
	}
	grp forbidden = {
		grp infrastructure levels = {
		}
		grp tech levels = {
		}
		grp technologies = {
		}
	}
	
}