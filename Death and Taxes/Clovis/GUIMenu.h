#pragma once
#include <vector>
#include "Widget.h"
#include "AnimationInstance.h"
namespace dat
{
	class Animation;
	class GUIMenu : public Widget
	{
	private:
		std::vector<Widget *> widgets;
		AnimationInstance *background;
		Vector2D<float> size, position;
		bool within(Vector2D<float> pos);
		void updateBackgroundSizeAndPos();
	public:
		GUIMenu(Vector2D<float> size, Vector2D<float> position);
		virtual ~GUIMenu() override;
		virtual void setSize(Vector2D<float> size);
		virtual void setPosition(Vector2D<float> position);
		virtual void mouseMoved(bool &eventClaimed, Vector2D<int> mousePos) override;
		virtual void mouseButtonPressed(bool &eventClaimed, Vector2D<int> mousePos, sf::Mouse::Button button) override;
		virtual void mouseButtonReleased(bool &eventClaimed, Vector2D<int> mousePos, sf::Mouse::Button button) override;
		virtual void mouseScrolled(bool &eventClaimed, Vector2D<int> mousePos, int delta) override;
		virtual void keyPressed(bool &eventClaimed, sf::Keyboard::Key key) override;
		virtual void keyReleased(bool &eventClaimed, sf::Keyboard::Key key) override;
		virtual void draw(sf::RenderTarget &renderTarget, sf::Time time, bool respectAlpha = false) override;
		void setBackGround(Animation *newBackground); //Sets the background of this menu, will be copied from the argument. If the background is set to nullptr the background will be treated as transparrant. The background is nullptr by default.

		void addWidget(Widget *widget); //Adds this widget to the menu
		void removeWidget(Widget *widget); //Removes this widget from the menu and deletes it.
	};
}