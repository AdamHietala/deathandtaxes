#pragma once
#include "LoadsOnce.h"
#include <map>
#include <vector>
#include "ResourceType.h"
#include "IDType.h"
#include <string>
namespace dat
{
	class ModuleLoader;
	class ResourceTypeLoader : public LoadsOnce //loads the different types of resources
	{
	private:
		std::map<std::string, IDType<ResourceType>> stringIDToID; //Maps the ResourceType string ID to the ResourceTypeID
		std::vector<std::string> idToStringID;
		std::vector<ResourceType *> idToResourceType;
		void addResourceType(std::string stringID, ResourceType *resourceType); //Adds the ResourceType to the datastructure.
		static IDType<ResourceType> const CURRENCY_ID;
	public:
		ResourceTypeLoader();
		~ResourceTypeLoader();

		void load(ModuleLoader &moduleLoader); //Needs nothing to be loaded.
		IDType<ResourceType> getID(std::string stringID) const; //Returns the tileset ID.
		std::string getStringID(IDType<ResourceType> id) const; //Returns the stringID so that it can be referred to in a savefile.
		ResourceType *getResource(IDType<ResourceType> id) const; //Returns the resource.
		int getNumberOfResourceTypes() const;//returns the amount of resource types that there are.
		static IDType<ResourceType> getCurrencyID();//returns the ID for the currency resource.
	};

}