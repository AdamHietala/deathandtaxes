#include "InfrastructureTypeLoader.h"
#include "Exceptions.h"
#include "FileStructure.h"
#include "StringManipulation.h"
#include "DataFileIOFunctions.h"
#include <vector>
#include "IDType.h"
#include "TechType.h"
#include "Technology.h"
#include "ResourceType.h"
#include "TechTreeLoader.h"
#include "ResourceTypeLoader.h"
#include "ModuleLoader.h"
namespace dat
{
	void InfrastructureTypeLoader::addInfrastructureType(std::string stringID, InfrastructureType *infrastructureType)
	{

		testException(stringIDToID.count(stringID) == 0, KeyAlreadyExistsException("The InfrastructureType with string id \"" + stringID + "\" was added twice."));
		assert(stringIDToID.size() == infrastructureType->id.getID());
		stringIDToID[stringID] = infrastructureType->id;
		idToStringID.push_back(stringID);
		idToInfrastructureType.push_back(infrastructureType);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	InfrastructureTypeLoader::InfrastructureTypeLoader()
	{
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	InfrastructureTypeLoader::~InfrastructureTypeLoader()
	{
		for (auto infrastructureType : idToInfrastructureType)
		{
			delete infrastructureType;
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void InfrastructureTypeLoader::load(ModuleLoader &moduleLoader, TechTreeLoader const &techTreeLoader, ResourceTypeLoader const &resourceTypeloader)
	{
		LoadsOnce::load();
		assert(techTreeLoader.hasLoaded());
		assert(resourceTypeloader.hasLoaded());




		int freeID = 0;


		DataFileIO &infrastructureTypesFile = moduleLoader.loadDataFileIO(filestructure::INFRASTRUCTURE_TYPE_FOLDER_PATH);


		for (auto infrastructureTypeKey : sortStrings(infrastructureTypesFile.getGroupKeys())) //Sort them to make sure the number id's are always the same so that they can be sent between different clients without problem.
		{
			infrastructureTypesFile.openGroup(infrastructureTypeKey); //a1

			std::vector<std::pair<IDType<TechType>, double>> neededTechLevels;
			infrastructureTypesFile.openGroup("needed tech levels"); //b1
			for (auto techTypeKey : infrastructureTypesFile.getStringKeys())
			{
				double neededTechLevel = getDouble(infrastructureTypesFile, techTypeKey);
				IDType<TechType> techTypeID = techTreeLoader.getTechTypeID(techTypeKey);
				neededTechLevels.push_back(std::pair<IDType<TechType>, double>(techTypeID, neededTechLevel));
			}
			infrastructureTypesFile.closeGroup(); //b1

			std::vector<std::pair<IDType<Technology>, double>> technologyLimits;
			infrastructureTypesFile.openGroup("needed technologies"); //b2
			for (auto technologyKey : infrastructureTypesFile.getStringKeys())
			{
				double technologyLevelLimit = getDouble(infrastructureTypesFile, technologyKey);
				IDType<Technology> technologyID = techTreeLoader.getTechnologyID(technologyKey);
				technologyLimits.push_back(std::pair<IDType<Technology>, double>(technologyID, technologyLevelLimit));
			}
			infrastructureTypesFile.closeGroup(); //b2

			std::vector<std::pair<IDType<ResourceType>, double>> resourceCosts;
			infrastructureTypesFile.openGroup("resource cost"); //b3
			for (auto resourceTypeKey : infrastructureTypesFile.getStringKeys())
			{
				double resourceCost = getDouble(infrastructureTypesFile, resourceTypeKey);
				IDType<ResourceType> resourceTypeID = resourceTypeloader.getID(resourceTypeKey);
				resourceCosts.push_back(std::pair<IDType<ResourceType>, double>(resourceTypeID, resourceCost));
			}
			infrastructureTypesFile.closeGroup(); //b3

			infrastructureTypesFile.closeGroup(); //a1

			IDType<InfrastructureType> id(freeID++);
			addInfrastructureType(infrastructureTypeKey, new InfrastructureType(id, neededTechLevels, resourceCosts, technologyLimits));

		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	IDType<InfrastructureType> InfrastructureTypeLoader::getID(std::string stringID) const
	{
		return stringIDToID.at(stringID);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::string InfrastructureTypeLoader::getStringID(IDType<InfrastructureType> id) const
	{
		return idToStringID[id.getID()];
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	int InfrastructureTypeLoader::getAmountOfInfrastructureTypes() const
	{
		return (int)idToStringID.size();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}