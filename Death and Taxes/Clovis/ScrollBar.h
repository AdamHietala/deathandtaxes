#pragma once
#include "Widget.h"
#include "VectorXD.h"
namespace dat
{
	class AnimationInstance;
	class Animation;
	class ScrollableList;
	class ScrollBar : public Widget
	{
	private:
		float angle;
		Vector2D<float> topLeft, size;
		AnimationInstance *bar, *indicator;
		float value;
		bool mouseClicked;
		void calcValue(Vector2D<int> pos);
		ScrollableList &scrollableList;
	public:
		ScrollBar(ScrollableList &scrollableList, Vector2D<int> topLeft = Vector2D<int>(0, 0), Vector2D<int> size = Vector2D<int>(1, 1), float angle = 0.0, Animation *bar = nullptr, Animation *indicator = nullptr, float startValue = 1.0);
		virtual ~ScrollBar() override;
		bool withinScroller(Vector2D<int> pos);

		virtual void mouseMoved(bool &eventClaimed, Vector2D<int> mousePos) override;
		virtual void mouseButtonPressed(bool &eventClaimed, Vector2D<int> mousePos, sf::Mouse::Button button) override;
		virtual void mouseButtonReleased(bool &eventClaimed, Vector2D<int> mousePos, sf::Mouse::Button button) override;
		virtual void mouseScrolled(bool &eventClaimed, Vector2D<int> mousePos, int delta) override;
		virtual void keyPressed(bool &eventClaimed, sf::Keyboard::Key key) override;
		virtual void keyReleased(bool &eventClaimed, sf::Keyboard::Key key) override;
		virtual void draw(sf::RenderTarget &renderTarget, sf::Time time, bool respectAlpha = false) override;

		void setAnimations(Animation &bar, Animation &indicator);
		void setAngle(float newAngle);
		float getAngle() const;
		void setTopLeft(Vector2D<int> newTopLeft);
		Vector2D<int> getTopLeft() const;
		void setSize(Vector2D<int> newSize);
		Vector2D<int> getSize() const;
		void setValue(float value);
		float getValue() const;
	};
}