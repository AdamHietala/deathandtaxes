#pragma once
namespace dat
{
	class NonCopyable
	{
	private:
	public:
		NonCopyable(void) {}
		~NonCopyable(void) {}
		NonCopyable(const NonCopyable&) = delete;
		NonCopyable& operator=(const NonCopyable&) = delete;
	};

}