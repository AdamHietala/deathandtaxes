#include "TextBox.h"

namespace dat
{
	TextBox::TextBox(Rectangle<int> boundingBox, float textScale, sf::Color textColour, sf::Font *font, bool writeable, std::string text, std::string heightAlignmentString)
	{
		setBoundingBox(boundingBox);
		setTextScale(textScale);
		setTextColour(textColour);
		setFont(font);
		setWriteable(writeable);
		setText(text);
		setHeightAlignmentString(heightAlignmentString);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	TextBox::~TextBox()
	{
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void TextBox::mouseMoved(bool &eventClaimed, Vector2D<int> mousePos)
	{
		if (!isActive())
		{
			return;
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void TextBox::mouseButtonPressed(bool &eventClaimed, Vector2D<int> mousePos, sf::Mouse::Button button)
	{
		if (!isActive())
		{
			return;
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void TextBox::mouseButtonReleased(bool &eventClaimed, Vector2D<int> mousePos, sf::Mouse::Button button)
	{
		if (!isActive())
		{
			return;
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void TextBox::mouseScrolled(bool &eventClaimed, Vector2D<int> mousePos, int delta)
	{
		if (!isActive())
		{
			return;
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void TextBox::keyPressed(bool &eventClaimed, sf::Keyboard::Key key)
	{
		if (!isActive())
		{
			return;
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void TextBox::keyReleased(bool &eventClaimed, sf::Keyboard::Key key)
	{
		if (!isActive())
		{
			return;
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void TextBox::draw(sf::RenderTarget &renderTarget, sf::Time time, bool respectAlpha)
	{
		if (!isActive())
		{
			return;
		}

		prepareDrawingAreaAlpha(renderTarget, boundingBox.typecast<float>(), respectAlpha);
		sf::Text sfText, heightAlignmentText;
		sfText.setOutlineColor(textColour);
		sfText.setFillColor(textColour);
		sfText.setString(text);
		heightAlignmentText.setString(heightAlignmentString);
		sfText.setFont(*font);
		heightAlignmentText.setFont(*font);

		sfText.setScale(textScale, textScale);
		heightAlignmentText.setScale(textScale, textScale);
		sfText.setOrigin(sf::Vector2f(sfText.getLocalBounds().width / 2, heightAlignmentText.getLocalBounds().height));
		sfText.setPosition(sf::Vector2f((float)boundingBox.topLeft.x + (float)boundingBox.dimensions.x / 2.0f, (float)boundingBox.topLeft.y + (float)boundingBox.dimensions.y / 2.0f));

		renderTarget.draw(sfText, renderStatesFilterAlphaThroughAlpha);
		renderTarget.draw(sfText, renderStatesFilterThroughAlpha);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void TextBox::setWriteable(bool writeable)
	{
		TextBox::writeable = writeable;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void TextBox::setText(std::string text)
	{
		TextBox::text = text;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void TextBox::setFont(sf::Font *font)
	{
		TextBox::font = font;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void TextBox::setTextColour(sf::Color colour)
	{
		textColour = colour;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void TextBox::setBoundingBox(Rectangle<int> boundingBox)
	{
		TextBox::boundingBox = boundingBox;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Rectangle<int> TextBox::getBoundingBox() const
	{
		return boundingBox;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void TextBox::setTextScale(float textScale)
	{
		TextBox::textScale = textScale;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void TextBox::setHeightAlignmentString(std::string string)
	{
		heightAlignmentString = string;
	}
}