#pragma once
namespace dat
{
	namespace filestructure
	{
		//base
		const std::string MOD_FOLDER_NAME = "Mods";
		const std::string MOD_FOLDER_PATH = "" + MOD_FOLDER_NAME;
		
		const std::string LOAD_ORDER_NAME = "Load Order.txt";
		const std::string LOAD_ORDER_PATH = "" + LOAD_ORDER_NAME;

		const std::string OPTIONS_FILE_NAME = "options.txt";
		const std::string OPTIONS_FILE_PATH = "" + OPTIONS_FILE_NAME;



		


		//graphics
		const std::string GRAPHICS_FOLDER_NAME = "Graphics";
		const std::string GRAPHICS_FOLDER_PATH = "" + GRAPHICS_FOLDER_NAME;

		const std::string IMAGES_FOLDER_NAME = "Images";
		const std::string IMAGES_FOLDER_PATH = GRAPHICS_FOLDER_NAME + "\\" + IMAGES_FOLDER_NAME;

		const std::string FONT_FOLDER_NAME = "Fonts";
		const std::string FONT_FOLDER_PATH = GRAPHICS_FOLDER_PATH + "\\" + FONT_FOLDER_NAME;

		const std::string GUI_CONFIG_FOLDER_NAME = "GUI Config";
		const std::string GUI_CONFIG_FOLDER_PATH = GRAPHICS_FOLDER_PATH + "\\" + GUI_CONFIG_FOLDER_NAME;

		const std::string GAME_LOADER_FILE_NAME = "Loading Config.txt";
		const std::string GAME_LOADER_FILE_PATH = GRAPHICS_FOLDER_PATH + "\\" + GAME_LOADER_FILE_NAME;

		const std::string ANIMATION_FOLDER_NAME = "Animations";
		const std::string ANIMATION_FOLDER_PATH = GRAPHICS_FOLDER_PATH + "\\" + ANIMATION_FOLDER_NAME;

		const std::string ANIMATION_ATTACHMENT_POINT_FOLDER_NAME = "Attachment Points";
		const std::string ANIMATION_ATTACHMENT_POINT_FOLDER_PATH = GRAPHICS_FOLDER_PATH + "\\" + ANIMATION_ATTACHMENT_POINT_FOLDER_NAME;

		const std::string TILESETS_FOLDER_NAME = "Tilesets";
		const std::string TILESETS_FOLDER_PATH = GRAPHICS_FOLDER_PATH + "\\" + TILESETS_FOLDER_NAME;

		const std::string SPRITES_FOLDER_NAME = "Sprites";
		const std::string SPRITES_FOLDER_PATH = GRAPHICS_FOLDER_PATH + "\\" + SPRITES_FOLDER_NAME;

		const std::string CITY_SPRITES_FOLDER_NAME = "Cities";
		const std::string CITY_SPRITES_FOLDER_PATH = SPRITES_FOLDER_PATH + "\\" + CITY_SPRITES_FOLDER_NAME;

		const std::string POP_TYPES_IMAGES_FOLDER_NAME = "Pop Types";
		const std::string POP_TYPES_IMAGES_FOLDER_PATH = GRAPHICS_FOLDER_PATH + "\\" + POP_TYPES_IMAGES_FOLDER_NAME;

		const std::string RESOURCE_TYPES_IMAGES_FOLDER_NAME = "Resource Types";
		const std::string RESOURCE_TYPES_IMAGES_FOLDER_PATH = GRAPHICS_FOLDER_PATH + "\\" + RESOURCE_TYPES_IMAGES_FOLDER_NAME;
		


		//localisation
		const std::string LOCALISATION_FOLDER_NAME = "Localisation";
		const std::string LOCALISATION_FOLDER_PATH = "" + LOCALISATION_FOLDER_NAME;




		//common
		const std::string COMMON_FOLDER_NAME = "Common";
		const std::string COMMON_FOLDER_PATH = "" + COMMON_FOLDER_NAME;


		const std::string DEFINES_FOLDER_NAME = "Defines";
		const std::string DEFINES_FOLDER_PATH = COMMON_FOLDER_PATH + "\\" + DEFINES_FOLDER_NAME;

		const std::string TERRAIN_TYPES_FOLDER_NAME = "Terrain Types";
		const std::string TERRAIN_TYPES_FOLDER_PATH = COMMON_FOLDER_PATH + "\\" + TERRAIN_TYPES_FOLDER_NAME;

		const std::string POP_TYPES_FOLDER_NAME = "Pop Types";
		const std::string POP_TYPES_FOLDER_PATH = COMMON_FOLDER_PATH + "\\" + POP_TYPES_FOLDER_NAME;

		const std::string RESOURCE_TYPES_FOLDER_NAME = "Resource Types";
		const std::string RESOURCE_TYPES_FOLDER_PATH = COMMON_FOLDER_PATH + "\\" + RESOURCE_TYPES_FOLDER_NAME;

		const std::string TECHNOLOGY_FOLDER_NAME = "Technology";
		const std::string TECHNOLOGY_FOLDER_PATH = COMMON_FOLDER_PATH + "\\" + TECHNOLOGY_FOLDER_NAME;

		const std::string TECHNOLOGIES_FOLDER_NAME = "Technologies";
		const std::string TECHNOLOGIES_FOLDER_PATH = TECHNOLOGY_FOLDER_PATH + "\\" + TECHNOLOGIES_FOLDER_NAME;

		const std::string TECH_TYPE_FOLDER_NAME = "Tech Types";
		const std::string TECH_TYPE_FOLDER_PATH = TECHNOLOGY_FOLDER_PATH + "\\" + TECH_TYPE_FOLDER_NAME;

		const std::string INFRASTRUCTURE_TYPE_FOLDER_NAME = "Infrastructure";
		const std::string INFRASTRUCTURE_TYPE_FOLDER_PATH = COMMON_FOLDER_PATH + "\\" + INFRASTRUCTURE_TYPE_FOLDER_NAME;

		const std::string RECIPIES_FOLDER_NAME = "Recipies";
		const std::string RECIPIES_FOLDER_PATH = COMMON_FOLDER_PATH + "\\" + RECIPIES_FOLDER_NAME;
	}
}