#include "Widget.h"
#include "GUI.h"

namespace dat
{
	const sf::RenderStates Widget::renderStatesFilterAlphaThroughAlpha = sf::BlendMode(sf::BlendMode::Zero, sf::BlendMode::One, sf::BlendMode::Add, sf::BlendMode::DstAlpha, sf::BlendMode::Zero, sf::BlendMode::Add);
	const sf::RenderStates Widget::renderStatesFilterThroughAlpha = sf::BlendMode(sf::BlendMode::DstAlpha, sf::BlendMode::OneMinusDstAlpha, sf::BlendMode::Add, sf::BlendMode::Zero, sf::BlendMode::One, sf::BlendMode::Add);


	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void Widget::resetDrawingAreaAlpha(sf::RenderTarget &renderTarget)
	{
		sf::RectangleShape rect;
		rect.setFillColor(sf::Color(0, 0, 0, 0));
		rect.setPosition(sf::Vector2f(0, 0));
		rect.setSize(sf::Vector2f((float)renderTarget.getSize().x, (float)renderTarget.getSize().y));
		renderTarget.draw(rect, sf::BlendMode(sf::BlendMode::Zero, sf::BlendMode::One, sf::BlendMode::Add, sf::BlendMode::One, sf::BlendMode::Zero, sf::BlendMode::Add));
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	void Widget::prepareDrawingAreaAlpha(sf::RenderTarget &renderTarget, Rectangle<float> drawingArea, bool respectAlpha)
	{
		sf::RectangleShape rect;
		rect.setFillColor(sf::Color(255, 255, 255, 255));
		rect.setPosition(sf::Vector2f((float)drawingArea.topLeft.x, (float)drawingArea.topLeft.y));
		rect.setSize(sf::Vector2f((float)drawingArea.dimensions.x, (float)drawingArea.dimensions.y));
		if (respectAlpha)
		{
			renderTarget.draw(rect, renderStatesFilterAlphaThroughAlpha);
		}
		else
		{
			resetDrawingAreaAlpha(renderTarget);
			renderTarget.draw(rect, sf::BlendMode(sf::BlendMode::Zero, sf::BlendMode::One, sf::BlendMode::Add, sf::BlendMode::One, sf::BlendMode::Zero, sf::BlendMode::Add));
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Widget::Widget() : active(true)
	{
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Widget::~Widget()
	{

	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	bool Widget::isActive() const
	{
		return active;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void Widget::setActive(bool active)
	{
		Widget::active = active;
	}
}