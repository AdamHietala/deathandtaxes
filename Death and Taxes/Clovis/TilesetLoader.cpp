#include "TilesetLoader.h"
#include "FileStructure.h"
#include "DataFileIOFunctions.h"
#include "StringManipulation.h"
#include "Exceptions.h"
#include <assert.h>
#include "Tileset.h"
#include "TerrainType.h"
#include "ModuleLoader.h"
#include "AnimationLoader.h"
#include "TerrainTypeLoader.h"
namespace dat
{
	void TilesetLoader::addTileset(std::string stringID, Tileset *tileset)
	{
		testException(stringIDToID.count(stringID) == 0, KeyAlreadyExistsException("The type with string id \"" + stringID + "\" was added twice."));
		assert(stringIDToID.size() == tileset->id.getID());
		stringIDToID[stringID] = tileset->id;
		iDToTileset.push_back(tileset);
		idToStringID.push_back(stringID);

		while (tileset->terrainTypeID.getID() >= (int)terrainTypeIDToTileset.size())
		{
			terrainTypeIDToTileset.push_back(nullptr);
		}
		terrainTypeIDToTileset[tileset->terrainTypeID.getID()] = tileset;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	TilesetLoader::TilesetLoader()
	{
		highestPriority = 0;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	TilesetLoader::~TilesetLoader()
	{
		for (auto tileSet : iDToTileset)
		{
			delete tileSet;
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void TilesetLoader::load(ModuleLoader &moduleLoader, AnimationLoader const &animationLoader, TerrainTypeLoader const &terrainTypeLoader)
	{
		LoadsOnce::load();

		assert(animationLoader.hasLoaded());
		assert(terrainTypeLoader.hasLoaded());



		int freeID = 0;
		DataFileIO &tilesetsFile = moduleLoader.loadDataFileIO(filestructure::TILESETS_FOLDER_PATH);

		for (auto tileSetKey : sortStrings(tilesetsFile.getGroupKeys())) //Sort them to make sure the number id's are always the same so that they can be sent between different clients without problem.
		{
			std::vector<IDType<Animation>> baseTextures, firstMasks, secondMasks;
			tilesetsFile.openGroup(tileSetKey); //a1

			int priority = getInt(tilesetsFile, "priority");
			if (highestPriority < priority)
			{
				highestPriority = priority;
			}
			IDType<TerrainType> terrainTypeID(terrainTypeLoader.getID(tilesetsFile.getString("terrain type")));
			tilesetsFile.openGroup("tiles"); //b1
			std::vector<std::string> tileKeys = tilesetsFile.getGroupKeys();
			for (auto tileKey : tileKeys)
			{
				tilesetsFile.openGroup(tileKey); //c1

				baseTextures.push_back(animationLoader.getID(tilesetsFile.getString("base texture")));
				firstMasks.push_back(animationLoader.getID(tilesetsFile.getString("first mask")));
				secondMasks.push_back(animationLoader.getID(tilesetsFile.getString("second mask")));

				tilesetsFile.closeGroup(); //c1
			}
			tilesetsFile.closeGroup(); //b1
			tilesetsFile.closeGroup(); //a1

			IDType<Tileset> id(freeID++);

			Tileset *tileset = new Tileset(id, terrainTypeID, priority, baseTextures, firstMasks, secondMasks);
			addTileset(tileSetKey, tileset);
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	IDType<Tileset> TilesetLoader::getID(std::string stringID) const
	{
		return stringIDToID.at(stringID);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Tileset *TilesetLoader::getTileset(IDType<Tileset> id) const
	{
		assert(id.getID() < (int)iDToTileset.size() && id.getID() >= 0);
		return iDToTileset[id.getID()];
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Tileset *TilesetLoader::getTileset(IDType<TerrainType> id) const
	{
		assert(id.getID() < (int)terrainTypeIDToTileset.size() && id.getID() >= 0);
		return terrainTypeIDToTileset[id.getID()];
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Tileset *TilesetLoader::getTileset(TerrainType *terrainType) const
	{
		assert(terrainType != nullptr);
		return getTileset(terrainType->id);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::string TilesetLoader::getStringID(IDType<Tileset> id) const
	{
		return idToStringID[id.getID()];
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	int TilesetLoader::getHighestPriority() const
	{
		return highestPriority;
	}
}