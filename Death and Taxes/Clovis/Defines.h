#pragma once
#include "LoadsOnce.h"
namespace dat
{
	class ModuleLoader;

	class Defines : LoadsOnce
	{
	private:
		double popFearLossFactor;
		double popSatisfactionHappinessFactor;
		double popBuyExtraFactor;
	public:
		Defines();
		~Defines();

		void load(ModuleLoader &moduleLoader);

		double getPopFearLossFactor() const;
		double getPopSatisfactionHappinessFactor() const;
		double getPopBuyExtraFactor() const;
	};

}