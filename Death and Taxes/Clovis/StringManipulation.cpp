#include "StringManipulation.h"
#include <assert.h>
#include <set>
#include <functional>
#include <sstream>
#include <iomanip>
namespace dat
{
	std::string multiplyString(std::string str, int times)
	{
		std::string newStr;
		for(int i = 0; i < times; ++i)
		{
			newStr.append(str);
		}
		return newStr;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	bool isSpace(std::string str)
	{
		for(int i = 0; i < (int) str.size(); ++i)
		{
			if(!isSpace(str[i]))
			{
				return false;
			}
		}
		return true;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	bool isSpace(char character)
	{
		return character == ' ' || character == '\t' || character == '\n';
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::string join(std::vector<std::string> &lines, std::string glue)
	{
		if(lines.size() == 0)
		{
			return "";
		}

		std::string joinedLine;
		for(int i = 0; i < (int) lines.size() - 1; ++i)
		{
			joinedLine.append(lines[i]);
			joinedLine.append(glue);
		}
		joinedLine.append(lines[lines.size() - 1]);

		return joinedLine;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::string trimWhiteSpaces(std::string str)
	{
		if(str == "")
		{
			return "";
		}

		int i = 0;
		while (i < (int)str.size() && isSpace(str[i]))
		{
			++i;
		}

		int q = (int) str.size();
		while (q > i && isSpace(str[q - 1]))
		{
			--q;
		}
		return getSubString(str, i, q);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::vector<std::string> trimWhiteSpaces(std::vector<std::string> &lines)
	{
		std::vector<std::string> newLines;
		for(int i = 0; i < int(lines.size()); ++i)
		{
			newLines.push_back(trimWhiteSpaces(lines[i]));
		}
		return newLines;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::string getSubString(std::string str, int from, int to)
	{
		std::string newStr;
		for(auto i = from; i < to; ++i)
		{
			newStr.push_back(str[i]);
		}
		return newStr;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	bool isInt(char chr)
	{
		for (char &c : std::string("0123456789"))
		{
			if (c == chr)
			{
				return true;
			}
		}
		return false;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	bool isInt(std::string str)
	{
		int iStart;
		if(str == "")
		{
			return false;
		}
		else if(str[0] == '-')	//might be a negative number
		{
			if(str == "-")		//negative nothing
			{
				return false;
			}
			iStart = 1;		//start after the '-'
		}
		else
		{
			iStart = 0;			//start at the beginning
		}

		for(int i = iStart; i < (int) str.size(); ++i)
		{
			if(!isInt(str[i]))
			{
				return false;
			}
		}
		return true;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	bool isDouble(std::string str)
	{
		std::string firstPart;
		bool decimalFound = false;
		int i;	//we need to remember i, the position of the point
		for(i = 0; i < (int) str.size(); ++i)
		{
			if(str[i] == '.')
			{
				decimalFound = true;
				break;
			}
			else
			{
				firstPart.push_back(str[i]);
			}
		}

		if(!decimalFound)	//This would be an integer
		{
			assert(str == firstPart);
			return isInt(firstPart);
		}
		else
		{
			if( !(firstPart == "" || isInt(firstPart)) )	//The part before the '.' is not an int
			{
				return false;
			}

			std::string secondPart;	//secondPart will be the rest of the string
			for(i = i + 1; i < (int) str.size(); ++i)
			{
				secondPart.push_back(str[i]);
			}

			if(secondPart[0] == '-')	//secondPart may not be a negative number
			{
				return false;
			}
			else
			{
				return isInt(secondPart);
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::vector<std::string> split(std::string str, std::string splitter)
	{
		std::vector<std::string> parts;

		std::string thisPart;
		for(int i = 0; i < (int) str.size(); ++i)
		{
			bool split = true;
			for(int q = 0; q < (int) splitter.size() && q + i < (int) str.size(); ++q)
			{
				if(str[i + q] != splitter[q])
				{
					split = false;
					break;
				}
			}
			if(split)
			{
				if(thisPart != "")
				{
					parts.push_back(thisPart);
					thisPart.clear();
				}
				i += (int) splitter.size() - 1;
			}
			else
			{
				thisPart.push_back(str[i]);
			}
		}
		if(thisPart != "")
		{
			parts.push_back(thisPart);
		}

		return parts;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::wstring stringToWstring(std::string str)
	{
		std::wstring widestr;
		for(int i = 0; i < int(str.size()); ++i)
		{
			widestr += wchar_t( str[i] );
		}

		return widestr;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::string replaceChar(std::string string, char oldChar, char newChar)
	{
		std::string newStr;
		for (auto chr : string)
		{
			if (chr == oldChar)
			{
				newStr += newChar;
			}
			else
			{
				newStr += chr;
			}
		}
		return newStr;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	std::string replaceStr(std::string string, std::string oldstr, std::string newStr) //searches through string and replaces all instances of oldstr with newStr.
	{
		std::string retval;
		for (unsigned i = 0; i < string.size(); ++i)
		{
			bool found = true;
			for (unsigned q = 0; q < oldstr.size(); ++q)
			{
				if (i + q >= string.size() || string[i + q] != oldstr[q])
				{
					found = false;
					break;
				}
			}

			if (found)
			{
				retval += newStr;
				i += oldstr.size() - 1;
			}
			else
			{
				retval += string[i];
			}
		}
		return retval;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::string lower(std::string str)
	{
		std::string newstr;
		for (auto chr : str)
		{
			switch (chr)
			{
			case 'A': newstr += 'a'; break; 
			case 'B': newstr += 'b'; break; 
			case 'C': newstr += 'c'; break; 
			case 'D': newstr += 'd'; break; 
			case 'E': newstr += 'e'; break; 
			case 'F': newstr += 'f'; break; 
			case 'G': newstr += 'g'; break; 
			case 'H': newstr += 'h'; break; 
			case 'I': newstr += 'i'; break; 
			case 'J': newstr += 'j'; break; 
			case 'K': newstr += 'k'; break; 
			case 'L': newstr += 'l'; break; 
			case 'M': newstr += 'm'; break; 
			case 'N': newstr += 'n'; break; 
			case 'O': newstr += 'o'; break; 
			case 'P': newstr += 'p'; break; 
			case 'Q': newstr += 'q'; break; 
			case 'R': newstr += 'r'; break; 
			case 'S': newstr += 's'; break; 
			case 'T': newstr += 't'; break; 
			case 'U': newstr += 'u'; break; 
			case 'V': newstr += 'v'; break; 
			case 'W': newstr += 'w'; break; 
			case 'X': newstr += 'x'; break;
			case 'Y': newstr += 'y'; break; 
			case 'Z': newstr += 'z'; break;
			default: newstr += chr;
			}
		}
		return newstr;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::string upper(std::string str)
	{
		std::string newstr;
		for (auto chr : str)
		{
			switch (chr)
			{
			case 'a': newstr += 'A'; break;
			case 'b': newstr += 'B'; break;
			case 'c': newstr += 'C'; break;
			case 'd': newstr += 'D'; break;
			case 'e': newstr += 'E'; break;
			case 'f': newstr += 'F'; break;
			case 'g': newstr += 'G'; break;
			case 'h': newstr += 'H'; break;
			case 'i': newstr += 'I'; break;
			case 'j': newstr += 'J'; break; 
			case 'k': newstr += 'K'; break;
			case 'l': newstr += 'L'; break; 
			case 'm': newstr += 'M'; break; 
			case 'n': newstr += 'N'; break; 
			case 'o': newstr += 'O'; break; 
			case 'p': newstr += 'P'; break; 
			case 'q': newstr += 'Q'; break; 
			case 'r': newstr += 'R'; break;
			case 's': newstr += 'S'; break; 
			case 't': newstr += 'T'; break; 
			case 'u': newstr += 'U'; break; 
			case 'v': newstr += 'V'; break;
			case 'w': newstr += 'W'; break; 
			case 'x': newstr += 'X'; break; 
			case 'y': newstr += 'Y'; break;
			case 'z': newstr += 'Z'; break;
			default: newstr += chr;
			}
		}
		return newstr;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	bool charInStr(std::string str, char chr)
	{
		for (auto c : str)
		{
			if (c == chr)
			{
				return true;
			}
		}
		return false;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	int findFirstOccurrence(std::string str, std::string subStr, int start)
	{
		for (int i = start; i < (int)str.size(); ++i)
		{
			int q = 0;
			while (i + q < (int) str.size())
			{
				if (q == subStr.size())
				{
					return (int) i;
				}
				else if (str[i + q] != subStr[q])
				{
					break;
				}
				++q;
			}
		}
		return (int) str.size();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::vector<std::string> sortStrings(std::vector<std::string> strings)
	{
		std::function<bool(std::string , std::string )> compare = [&](std::string str1, std::string str2){
			if (str1 == "")
			{
				return true;
			}
			if (str2 == "")
			{
				return false;
			}
			if (isInt(str1[0]) && !isInt(str2[0]))
			{
				return true;
			}
			if (!isInt(str1[0]) && isInt(str2[0]))
			{
				return false;
			}
			if (!isInt(str1[0]))
			{
				assert(!isInt(str2[0]));
				if (str1[0] == str2[0])
				{
					return compare(getSubString(str1, 1, str1.size()), getSubString(str2, 1, str2.size()));
				}
				else
				{
					return str1[0] < str2[0];
				}
			}
			else if (isInt(str1[0]))
			{
				assert(isInt(str2[0]));
				std::string nr1, nr2;
				int i = 0;
				while (i < (int)str1.size() && isInt(str1[i]))
				{
					nr1 += str1[i];
					++i;
				}
				i = 0;
				while (i < (int)str2.size() && isInt(str2[i]))
				{
					nr2 += str2[i];
					++i;
				}
				if (nr1 == nr2)
				{
					return compare(getSubString(str1, nr1.size(), str1.size()), getSubString(str2, nr2.size(), str2.size()));
				}
				else
				{
					return std::atoi(nr1.c_str()) < std::atoi(nr2.c_str());
				}
			}

			assert(false); //intellisense wants something here, but the code should never reach here.
			return false;
		};

		std::set<std::string, std::function<bool(std::string, std::string)>> sortedStrings(compare);

		for (auto str : strings)
		{
			sortedStrings.insert(str);
		}

		std::vector<std::string> sortedStringVector;
		for (auto str : sortedStrings)
		{
			sortedStringVector.push_back(str);
		}
		return sortedStringVector;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::string toString(double number, int decimals)
	{
		std::stringstream ss;
		ss << std::fixed << std::setprecision(decimals) << number;
		return ss.str();
	}
}