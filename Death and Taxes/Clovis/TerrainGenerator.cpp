#include "TerrainGenerator.h"
#include "GameMain.h"
#include "GeneralFunctions.h"
#include "TerrainTypeLoader.h"
#include "TerrainType.h"
#include "Exceptions.h"
#include <iostream>
namespace dat
{
#ifdef SHOW_TERRAIN_GENERATION
	void TerrainGenerator::renderState()
	{
		if (!renderWindow.isOpen())
		{
			return;
		}
		sf::Event evnt;
		while (renderWindow.pollEvent(evnt))
		{
			if (evnt.type == sf::Event::Closed)
			{
				renderWindow.close();
			}
			else if (evnt.type == sf::Event::KeyPressed)
			{
				if (evnt.key.code == sf::Keyboard::F1)
				{
					drawGround = !drawGround;
				}
				else if (evnt.key.code == sf::Keyboard::F2)
				{
					drawWater = !drawWater;
				}
				else if (evnt.key.code == sf::Keyboard::F3)
				{
					drawHumidity = !drawHumidity;
				}
				else if (evnt.key.code == sf::Keyboard::F4)
				{
					drawTemp = !drawTemp;
				}
				else if (evnt.key.code == sf::Keyboard::F5)
				{
					drawLight = !drawLight;
				}
				else if (evnt.key.code == sf::Keyboard::F6)
				{
					drawWind = !drawWind;
				}
				else if (evnt.key.code == sf::Keyboard::P)
				{
					draw = !draw;
				}
			}
		}
		if (!draw)
		{
			return;
		}
		renderWindow.clear();
		sf::CircleShape hexagon;
		const float HEX_RADIUS = WINDOW_SIZE_FACTOR;
		hexagon.setRadius(HEX_RADIUS);
		hexagon.setPointCount(6);
		
		for (int i = 0; i < mapSize.x; ++i)
		{
			for (int q = 0; q < mapSize.y; ++q)
			{

				hexagon.setPosition(sf::Vector2f((float(i) + float(q % 2)*0.5f)*HEX_RADIUS*float(hex::HORIZONTAL_OFFSET_FACTOR), float(q)*HEX_RADIUS*float(hex::VERTICAL_OFFSET_FACTOR)));
				MapSquare &mapSquare = (*oldMapSquares)[i][q];

				Vector2D<int> neighbourCoord = hex::getNeighbour(Vector2D<int>(i, q), hex::NeighbourDirection::SouthWest, mapSize, wrapType);
				if (hex::withinMap(neighbourCoord, mapSize))
				{
					MapSquare &neighbourMapSquare = (*oldMapSquares)[neighbourCoord.x][neighbourCoord.y];
					//ground
					if (drawGround)
					{
						Vector2D<int> groundCoords = Vector2D<int>(i, q);
						MapSquare &waterMovedSquare = (*oldMapSquares)[groundCoords.x][groundCoords.y];
						double groundColour = max(min(waterMovedSquare.groundHeight * 0.25, 160.0), 20.0);
						hexagon.setFillColor(sf::Color(int(groundColour*0.9), int(groundColour), int(groundColour*0.5)));
						renderWindow.draw(hexagon);
					}
					//water
					if (drawWater)
					{
						int waterColour1 = max(min(int(mapSquare.waterHeight * 25.0), 60), 0);
						hexagon.setFillColor(sf::Color(0, 0, 255, waterColour1));
						renderWindow.draw(hexagon);

						int waterColour2 = max(min(int(mapSquare.waterHeight * 0.01), 200), 0);
						hexagon.setFillColor(sf::Color(0, 0, 255, waterColour2));
						renderWindow.draw(hexagon);
					}
					//light
					if (drawLight)
					{
						const double MAX_LIGHT = 75.0;
						double heightDifference;
						if (drawWater)
						{
							heightDifference = (neighbourMapSquare.groundHeight + neighbourMapSquare.waterHeight) - (mapSquare.groundHeight + mapSquare.waterHeight);
						}
						else
						{
							heightDifference = neighbourMapSquare.groundHeight - mapSquare.groundHeight;
						}
						double lightStrength = max(min(heightDifference* 3.0 + MAX_LIGHT*0.5, MAX_LIGHT), 0.0);
						hexagon.setFillColor(sf::Color(255, 255, 255, int(lightStrength)));
						renderWindow.draw(hexagon);
					}
					//humidity
					if (drawHumidity)
					{
						int humidityColour = max(min(int(mapSquare.humidity * 1.0), 200), 0);
						hexagon.setFillColor(sf::Color(0, 230, 230, humidityColour));
						renderWindow.draw(hexagon);
					}
					//temperature
					if (drawTemp)
					{
						int temperatureColour = max(min(int((mapSquare.temperature + 30.0) * 3.0), 200), 0);
						hexagon.setFillColor(sf::Color(255, 0, 0, temperatureColour));
						renderWindow.draw(hexagon);
					}
					//wind
					if (drawWind)
					{
						const double WIND_SPEED_FACTOR = 25.0;
						sf::Color windColour(0, 0, 0, 0);
						int horizontalSpeedColour = max(min(int((abs(mapSquare.windSpeed.x)) * WIND_SPEED_FACTOR), 255), 10);
						windColour.a = horizontalSpeedColour;
						if (mapSquare.windSpeed.x > 0)
						{
							windColour.r = horizontalSpeedColour;
							windColour.g = horizontalSpeedColour / 2;
						}
						else
						{
							windColour.b = horizontalSpeedColour;
						}
						hexagon.setFillColor(windColour);
						renderWindow.draw(hexagon);

						int VerticalSpeedColour = max(min(int((abs(mapSquare.windSpeed.y)) * WIND_SPEED_FACTOR), 255), 10);
						windColour.r = 0;
						windColour.g = 0;
						windColour.b = 0;
						windColour.a = VerticalSpeedColour;

						if (mapSquare.windSpeed.y > 0)
						{
							windColour.r = VerticalSpeedColour;
						}
						else
						{
							windColour.g = VerticalSpeedColour;
						}
						hexagon.setFillColor(windColour);
						renderWindow.draw(hexagon);
					}
				}
			}
		}
		renderWindow.display();

	}
#endif



	Vector2D<int> TerrainGenerator::MapSquare::getNeighbourDirectionIndices(Vector2D<int> direction)
	{
		assert(direction.x == 1 || direction.x == -1);
		assert(direction.y >= -1 && direction.y <= 1);
		if (direction.x == 1)
		{
			return Vector2D<int>(direction.x, direction.y + 1);
		}
		else
		{
			return Vector2D<int>(direction.x + 1, direction.y + 1);
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Vector2D<int> TerrainGenerator::MapSquare::getNeighbourDirectionIndices(hex::NeighbourDirection neighbourDirection)
	{
		return getNeighbourDirectionIndices(getNeighbourDirection(neighbourDirection));
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Vector2D<int> TerrainGenerator::MapSquare::getNeighbourDirection(hex::NeighbourDirection neighbourDirection)
	{
		switch (neighbourDirection)
		{
		case hex::NeighbourDirection::NorthEast:
			return Vector2D<int>(1, -1);

		case hex::NeighbourDirection::East:
			return Vector2D<int>(1, 0);

		case hex::NeighbourDirection::SouthEast:
			return Vector2D<int>(1, 1);

		case hex::NeighbourDirection::SouthWest:
			return Vector2D<int>(-1, 1);

		case hex::NeighbourDirection::West:
			return Vector2D<int>(-1, 0);

		case hex::NeighbourDirection::NorthWest:
			return Vector2D<int>(-1, -1);
		}
		assert(false);//there are no other neighbourDirections
		return Vector2D<int>(0, 0);//just to have a return value to make the compiler stop complaining.
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	TerrainGenerator::MapSquare::MapSquare()
	{

		humidity = 0.0;
		waterHeight = 0.0;
		groundHeight = 0.0;
		temperature = 0.0;
		windSpeed = Vector2D<double>(0.0, 0.0);
		spawnedWater = 0.0;
		for (int i = 0; i < hex::NR_OF_DIRECTIONS; ++i)
		{
			Vector2D<int> neighbourDirectionIndices = getNeighbourDirectionIndices(hex::getNeighbourDirection(i));
			waterSpeed[neighbourDirectionIndices.x][neighbourDirectionIndices.y] = 0.0;
			groundSpeed[neighbourDirectionIndices.x][neighbourDirectionIndices.y] = 0.0;
			humiditySpeed[neighbourDirectionIndices.x][neighbourDirectionIndices.y] = 0.0;
		}

	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	const double TerrainGenerator::SEA_HEIGHT = 50.0;

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void TerrainGenerator::swapOldAndNew()
	{

		std::vector<std::vector<MapSquare>> *temp = oldMapSquares;
		oldMapSquares = newMapSquares;
		newMapSquares = temp;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void TerrainGenerator::generateLandmass()
	{
		const int ISLAND_CHAIN_CHANCE = 9;
		const int MOUNTAIN_CHAIN_CHANCE = 500;
		for (int i = 0; i < mapSize.x; ++i)
		{
			for (int q = 0; q < mapSize.y; ++q)
			{
				if (rand() % 1000 < ISLAND_CHAIN_CHANCE && rand() % 1000 < ISLAND_CHAIN_CHANCE)
				{
					double angle = randomAngle();
					setMountainChain(Vector2D<int>(i, q), angle, 50, 70, 5000.0);
					if (rand() % 1000 < MOUNTAIN_CHAIN_CHANCE && rand())
					{
						setMountainChain(Vector2D<int>(i, q), angle, 3, 25, 25000.0);
					}
				}
			}
		}
		Vector2D<int> coord(rand()%mapSize.x, rand()%mapSize.y);
		double angle = randomAngle();
		setMountainChain(Vector2D<int>(coord.x, coord.y), angle, 50, 70, 5000.0);
		if (rand() % 1000 < MOUNTAIN_CHAIN_CHANCE && rand())
		{
			setMountainChain(Vector2D<int>(coord.x, coord.y), angle, 3, 25, 25000.0);
		}


		mapSquares2 = mapSquares1;
		bool unDecided;
		int nrOfThreads = gameMain.threadScheduler.getNumberOfThreads();
		auto calcMapSquares = [&](void *threadID)
		{
			int id = *(int*)threadID;
			int startRow = id*mapSize.x / nrOfThreads;
			int endRow = (id + 1)*mapSize.x / nrOfThreads;
			for (int i = startRow; i < endRow; ++i)
			{
				for (int q = 0; q < mapSize.y; ++q)
				{
					calculateLandMass(Vector2D<int>(i, q), unDecided);
				}
			}
		};


		std::vector<int> threadIDs;
		for (int i = 0; i < nrOfThreads; ++i)
		{
			threadIDs.push_back(i);
		}
		unDecided = true;
		while (unDecided == true)
		{
			unDecided = false;
			for (int q = 0; q < nrOfThreads; ++q)
			{
				gameMain.threadScheduler.scheduleWork(calcMapSquares, (void *)&threadIDs[q]);
			}
			gameMain.threadScheduler.waitForWorkToFinish();
			swapOldAndNew();
#ifdef SHOW_TERRAIN_GENERATION
			renderState();
#endif
		}

	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void TerrainGenerator::setMountainChain(Vector2D<int> position, double direction, int width, int endChance, double height)
	{

		int topChance = 100 / width;
		for (int i = -width; i <= width; ++i)
		{
			for (int q = -width; q <= width; ++q)
			{
				if (rand() % 1000 < topChance)
				{
					int distanceSquared = i*i + q*q;
					if (distanceSquared <= width*width)
					{
						Vector2D<int> thisCoord = wrapCoord(position + Vector2D<int>(i, q), mapSize, wrapType);

						if (0 <= thisCoord.x && thisCoord.x < mapSize.x &&
							0 <= thisCoord.y && thisCoord.y < mapSize.y)
						{
							MapSquare &mapSquare = (*oldMapSquares)[thisCoord.x][thisCoord.y];
							mapSquare.groundHeight += height;
						}
					}
				}
			}
		}

		if (rand() % 1000 < endChance)
		{
			return;
		}
		else
		{
			const double JUMP_DISTANCE = 5;
			setMountainChain(position + (Vector2D<double>(cos(direction), sin(direction))*JUMP_DISTANCE).typecast<int>(), direction + randomAngle()*0.05, width, endChance, height);
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void TerrainGenerator::calculateLandMass(Vector2D<int> position, bool &unDecided)
	{
		MapSquare &newMapSquare = (*newMapSquares)[position.x][position.y];
		MapSquare &oldMapSquare = (*oldMapSquares)[position.x][position.y];

		int groundNeighbours = 0;
		int waterNeighbours = 0;
		double groundHeightSum = 0.0;
		double groundHeightNeighbours = 0.0;


		for (int i = 0; i < hex::NR_OF_DIRECTIONS; ++i)
		{
			hex::NeighbourDirection neighbourDirection = hex::getNeighbourDirection(i);
			Vector2D<int> neighbourDirectionVector = MapSquare::getNeighbourDirection(neighbourDirection);
			Vector2D<int> thisNeighbourCoord = hex::getNeighbour(position, neighbourDirection, mapSize, wrapType);
			if (hex::withinMap(thisNeighbourCoord, mapSize))
			{
				MapSquare &neighbourMapSquare = (*oldMapSquares)[thisNeighbourCoord.x][thisNeighbourCoord.y];

				if (neighbourMapSquare.groundHeight > 0.0)
				{
					groundNeighbours += 1;
				}
				if (neighbourMapSquare.waterHeight > 0.0)
				{
					waterNeighbours += 1;
				}
				groundHeightSum += neighbourMapSquare.groundHeight;
				groundHeightNeighbours += 1.0;

				
			}
		}
		//const int GROUND_SPREAD_CHANCE = 200;
		//const int GROUND_RANDOM_BRITH_CHANCE = 10;
		//const int GROUND_BIRTH_LIMIT = 2;
		//groundHeightSum += oldMapSquare.groundHeight;
		//groundHeightNeighbours += 1.0;
		//double avarageGroundHeight = groundHeightSum / groundHeightNeighbours;
		//double groundGrowthSize = sqrt(sqrt(avarageGroundHeight))*1.0 + 1.0;
		//if ((groundNeighbours >= GROUND_BIRTH_LIMIT && (newMapSquare.groundHeight > 0.0 || rand() % 1000 < GROUND_SPREAD_CHANCE)) || (waterNeighbours == 0 && rand() % 1000 < GROUND_RANDOM_BRITH_CHANCE && rand() % 1000 < GROUND_RANDOM_BRITH_CHANCE))
		//{
		//	newMapSquare.groundHeight = oldMapSquare.groundHeight + groundGrowthSize;
		//}
		//else
		//{
		//	newMapSquare.groundHeight = oldMapSquare.groundHeight;
		//}

		groundHeightSum += oldMapSquare.groundHeight;
		groundHeightNeighbours += 1.0;
		double avarageGroundHeight = groundHeightSum / groundHeightNeighbours;
		newMapSquare.groundHeight = avarageGroundHeight;

		const int WATER_SPREAD_CHANCE = 100;
		const int WATER_BIRTH_LIMIT = 1;
		const int WATER_RANDOM_BRITH_CHANCE = 1;
		if ((waterNeighbours >= WATER_BIRTH_LIMIT && newMapSquare.groundHeight < SEA_HEIGHT && rand() % 1000 < WATER_SPREAD_CHANCE) || oldMapSquare.waterHeight > 0.0 || (groundNeighbours == 0 && rand() % 1000 < WATER_RANDOM_BRITH_CHANCE && rand() % 1000 < WATER_RANDOM_BRITH_CHANCE))
		{
			newMapSquare.waterHeight = SEA_HEIGHT - newMapSquare.groundHeight;
		}
		else
		{
			newMapSquare.waterHeight = oldMapSquare.waterHeight;
		}

		if (newMapSquare.waterHeight == 0.0 && newMapSquare.groundHeight == 0.0)
		{
			unDecided = true;
		}


	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void TerrainGenerator::flattenTerrain(Vector2D<int> position)
	{
		MapSquare &newMapSquare = (*newMapSquares)[position.x][position.y];
		MapSquare &oldMapSquare = (*oldMapSquares)[position.x][position.y];

		double groundHeightSum = 0.0;
		double neighbours = 0.0;
		for (int i = 0; i < hex::NR_OF_DIRECTIONS; ++i)
		{
			hex::NeighbourDirection neighbourDirection = hex::getNeighbourDirection(i);
			Vector2D<int> neighbourDirectionVector = MapSquare::getNeighbourDirection(neighbourDirection);
			Vector2D<int> thisNeighbourCoord = hex::getNeighbour(position, neighbourDirection, mapSize, wrapType);
			Vector2D<int> neighbourDirectionIndices = MapSquare::getNeighbourDirectionIndices(neighbourDirectionVector);
			Vector2D<int> myDirectionIndices = MapSquare::getNeighbourDirectionIndices(-neighbourDirectionVector);
			if (hex::withinMap(thisNeighbourCoord, mapSize))
			{
				Vector2D<int> neighbourCoord = hex::getNeighbour(position, neighbourDirection, mapSize, wrapType); //flats the terrain is if it were a square grid instead of hexagonal
				if (hex::withinMap(neighbourCoord, mapSize, wrapType))
				{
					MapSquare &neighbour = (*oldMapSquares)[neighbourCoord.x][neighbourCoord.y];
					neighbours += 1.0;
					groundHeightSum += neighbour.groundHeight;
				}
			}
		}
		newMapSquare.groundHeight = groundHeightSum / neighbours;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	bool TerrainGenerator::createRiver(Vector2D<int> position, std::vector<Vector2D<int>> &visitedPositions, std::vector<int> searchOrder)
	{
		for (int i = 0; i < int(visitedPositions.size()); ++i)
		{
			if (position == visitedPositions[i])
			{
				return false;
			}
		}

		visitedPositions.push_back(position);
		MapSquare &mapSquare = mapSquares1[position.x][position.y];
		if (mapSquare.groundHeight < SEA_HEIGHT || mapSquare.spawnedWater > 0.0)
		{
			mapSquare.groundHeight = 0.0;
			return true;
		}

		if (searchOrder.size() == 0 || rand()%2 == 0)
		{
			searchOrder.clear();
			std::vector<int> sortedNeighbours;
			for (int i = 0; i < hex::NR_OF_DIRECTIONS; ++i)
			{
				sortedNeighbours.push_back(i);
			}
			while (sortedNeighbours.size() > 0)
			{
				int index = rand() % sortedNeighbours.size();
				searchOrder.push_back(sortedNeighbours[index]);
				sortedNeighbours.erase(sortedNeighbours.begin() + index);
			}
		}
		bool foundSea = false;
		for (int i = 0; i < int(searchOrder.size()); ++i)
		{
			hex::NeighbourDirection neighbourDirection = hex::getNeighbourDirection(searchOrder[i]);
			Vector2D<int> neighbourDirectionVector = MapSquare::getNeighbourDirection(neighbourDirection);
			Vector2D<int> thisNeighbourCoord = hex::getNeighbour(position, neighbourDirection, mapSize, wrapType);
			if (hex::withinMap(thisNeighbourCoord, mapSize))
			{
				MapSquare &neighbourMapSquare = mapSquares1[thisNeighbourCoord.x][thisNeighbourCoord.y];
				if (neighbourMapSquare.groundHeight < mapSquare.groundHeight)
				{
					foundSea = createRiver(thisNeighbourCoord, visitedPositions, searchOrder);
					if (foundSea)
					{
						break;
					}
				}
			}
		}
		if (!foundSea)
		{
			for (int i = 0; i < int(searchOrder.size()); ++i)
			{
				hex::NeighbourDirection neighbourDirection = hex::getNeighbourDirection(searchOrder[i]);
				Vector2D<int> neighbourDirectionVector = MapSquare::getNeighbourDirection(neighbourDirection);
				Vector2D<int> thisNeighbourCoord = hex::getNeighbour(position, neighbourDirection, mapSize, wrapType);
				if (hex::withinMap(thisNeighbourCoord, mapSize))
				{
					MapSquare &neighbourMapSquare = mapSquares1[thisNeighbourCoord.x][thisNeighbourCoord.y];
					foundSea = createRiver(thisNeighbourCoord, visitedPositions, searchOrder);
					if (foundSea)
					{
						break;
					}
				}
			}
		}

		if (!foundSea)
		{
			return false;
		}
		else
		{
			mapSquare.spawnedWater = 5.0;
			mapSquare.groundHeight = 0.0;
			return true;
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void TerrainGenerator::initiateMap()
	{
		//prepare for smoothening terrain
		int flatteningWidth;
		int nrOfThreads = gameMain.threadScheduler.getNumberOfThreads();
		auto flatten = [&](void *threadID)
		{
			int id = *(int*)threadID;
			int startRow = id*mapSize.x / nrOfThreads;
			int endRow = (id + 1)*mapSize.x / nrOfThreads;

			for (int i = startRow; i < endRow; ++i)
			{
				for (int q = 0; q < mapSize.y; ++q)
				{
					flattenTerrain(Vector2D<int>(i, q));
				}
			}
		};

		//generate initial landmasses
		generateLandmass();

		mapSquares2 = mapSquares1;
		flatteningWidth = 1;
		std::vector<int> threadIDs;
		for (int i = 0; i < nrOfThreads; ++i)
		{
			threadIDs.push_back(i);
		}
		for (int i = 0; i < 2; ++i)
		{
			for (int q = 0; q < nrOfThreads; ++q)
			{
				gameMain.threadScheduler.scheduleWork(flatten, (void *)&threadIDs[q]);
			}
			gameMain.threadScheduler.waitForWorkToFinish();
			swapOldAndNew();

		}

		//lower terrain to zero
		double lowestPoint = mapSquares1[0][0].groundHeight;
		for (int i = 0; i < mapSize.x; ++i)
		{
			for (int q = 0; q < mapSize.y; ++q)
			{
				if (mapSquares1[i][q].groundHeight < lowestPoint)
				{
					lowestPoint = mapSquares1[i][q].groundHeight;
				}
			}
		}
		for (int i = 0; i < mapSize.x; ++i)
		{
			for (int q = 0; q < mapSize.y; ++q)
			{
				mapSquares1[i][q].groundHeight -= lowestPoint;
			}
		}

		mapSquares2 = mapSquares1;
		//create river spawners
		for (int i = 0; i < mapSize.x; ++i)
		{
			for (int q = 0; q < mapSize.y; ++q)
			{
				if (mapSquares1[i][q].groundHeight > SEA_HEIGHT && rand() % 750 == 0)
				{
					createRiver(Vector2D<int>(i, q));
				}
			}
		}
		//add random bumpyness
		for (int i = 0; i < mapSize.x; ++i)
		{
			for (int q = 0; q < mapSize.y; ++q)
			{
				mapSquares1[i][q].groundHeight += double(rand() % 100)/100.0*5.0;
			}
		}
		mapSquares2 = mapSquares1;

		for (int i = 0; i < 2; ++i)
		{
			for (int q = 0; q < nrOfThreads; ++q)
			{
				gameMain.threadScheduler.scheduleWork(flatten, (void *)&threadIDs[q]);
			}
			gameMain.threadScheduler.waitForWorkToFinish();
			swapOldAndNew();

#ifdef SHOW_TERRAIN_GENERATION
			renderState();
#endif
		}
		mapSquares2 = mapSquares1;


		//level sea
		for (int i = 0; i < mapSize.x; ++i)
		{
			for (int q = 0; q < mapSize.y; ++q)
			{
				if (mapSquares1[i][q].groundHeight < SEA_HEIGHT)
				{
					mapSquares1[i][q].waterHeight = SEA_HEIGHT - mapSquares1[i][q].groundHeight;
				}
				else
				{
					mapSquares1[i][q].waterHeight = 0.0;
				}
			}
		}

		mapSquares2 = mapSquares1;

	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void TerrainGenerator::simulateMap()
	{
		int nrOfThreads = gameMain.threadScheduler.getNumberOfThreads();
		auto calcMapSquares = [&](void *threadID)
		{
			int id = *(int*)threadID;
			int startRow = id*mapSize.x / nrOfThreads;
			int endRow = (id + 1)*mapSize.x / nrOfThreads;
			for (int i = startRow; i < endRow; ++i)
			{
				for (int q = 0; q < mapSize.y; ++q)
				{
					simulateMapSquare(Vector2D<int>(i, q));
				}
			}
		};

		
		std::vector<int> threadIDs;
		for (int i = 0; i < nrOfThreads; ++i)
		{
			threadIDs.push_back(i);
		}
		for (int i = 0; i < 400; ++i)
		{
			for (int q = 0; q < nrOfThreads; ++q)
			{
				gameMain.threadScheduler.scheduleWork(calcMapSquares, (void *)&threadIDs[q]);
			}
			gameMain.threadScheduler.waitForWorkToFinish();
			swapOldAndNew();
#ifdef SHOW_TERRAIN_GENERATION
			renderState();
#endif
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void TerrainGenerator::simulateMapSquare(Vector2D<int> position)
	{
		assert(0 <= position.x && position.x < mapSize.x && 0 <= position.y && position.y < mapSize.y);
		MapSquare &oldMapSquare = (*oldMapSquares)[position.x][position.y];
		MapSquare &newMapSquare = (*newMapSquares)[position.x][position.y];



		//ground
		double groundSpeedSum = 0.0;
		newMapSquare.groundHeight = oldMapSquare.groundHeight;

		//water
		double waterSpeedSum = 0.0;
		newMapSquare.waterHeight = oldMapSquare.waterHeight;

		//humidity
		double humiditySpeedSum = 0.0;
		newMapSquare.humidity = oldMapSquare.humidity;

		//temperature
		double temperatureNeighbours = 8.0;
		newMapSquare.temperature = oldMapSquare.temperature*temperatureNeighbours;

		//wind
		double windSpeedNeighbours = 25.0;
		newMapSquare.windSpeed = oldMapSquare.windSpeed*windSpeedNeighbours * 0.999;


		for (int i = 0; i < hex::NR_OF_DIRECTIONS; ++i)
		{
			hex::NeighbourDirection neighbourDirection = hex::getNeighbourDirection(i);
			Vector2D<int> neighbourDirectionVector = MapSquare::getNeighbourDirection(neighbourDirection);
			Vector2D<int> thisNeighbourCoord = hex::getNeighbour(position, neighbourDirection, mapSize, wrapType);
			Vector2D<int> neighbourDirectionIndices = MapSquare::getNeighbourDirectionIndices(neighbourDirectionVector);
			Vector2D<int> myDirectionIndices = MapSquare::getNeighbourDirectionIndices(-neighbourDirectionVector);
			if (hex::withinMap(thisNeighbourCoord, mapSize))
			{
				MapSquare &neighbourMapSquare = (*oldMapSquares)[thisNeighbourCoord.x][thisNeighbourCoord.y];

				double windSpeedTowards = getWindSpeedTowards(neighbourDirectionVector, neighbourMapSquare.windSpeed);
				

				//ground
				const double WATER_FACTOR = 1.0;
				double groundFactor;
				groundFactor = 0.0001 * (oldMapSquare.waterHeight);
				double groundPressure = groundFactor*(oldMapSquare.groundHeight - neighbourMapSquare.groundHeight);
				if (oldMapSquare.waterSpeed[neighbourDirectionIndices.x][neighbourDirectionIndices.y] >= 0.0) //calculate using last rounds water speed to simulate water erosion
				{
					groundPressure += oldMapSquare.waterSpeed[neighbourDirectionIndices.x][neighbourDirectionIndices.y] * WATER_FACTOR;
				}
				else
				{
					assert(neighbourMapSquare.waterSpeed[myDirectionIndices.x][myDirectionIndices.y] >= 0.0);
					groundPressure -= neighbourMapSquare.waterSpeed[myDirectionIndices.x][myDirectionIndices.y] * WATER_FACTOR;
				}

				newMapSquare.groundSpeed[neighbourDirectionIndices.x][neighbourDirectionIndices.y] += groundPressure;
				if (newMapSquare.groundSpeed[neighbourDirectionIndices.x][neighbourDirectionIndices.y] >= 0.0)
				{
					groundSpeedSum += newMapSquare.groundSpeed[neighbourDirectionIndices.x][neighbourDirectionIndices.y];
				}


				//water
				const double WATER_ACC_FACTOR = 0.1;
				double waterPressure = (oldMapSquare.groundHeight + oldMapSquare.waterHeight) - (neighbourMapSquare.waterHeight + neighbourMapSquare.groundHeight);
				newMapSquare.waterSpeed[neighbourDirectionIndices.x][neighbourDirectionIndices.y] += waterPressure*WATER_ACC_FACTOR;
				if (newMapSquare.waterSpeed[neighbourDirectionIndices.x][neighbourDirectionIndices.y] >= 0.0)
				{
					waterSpeedSum += newMapSquare.waterSpeed[neighbourDirectionIndices.x][neighbourDirectionIndices.y];
				}

				//humidity
				const double WIND_SPEED_FACTOR = 0.7;
				const double HUMIDITY_ACC_FACTOR = 0.1;
				double humidityPressure = (oldMapSquare.humidity - neighbourMapSquare.humidity);
				humidityPressure += -windSpeedTowards*WIND_SPEED_FACTOR;
				newMapSquare.humiditySpeed[neighbourDirectionIndices.x][neighbourDirectionIndices.y] += humidityPressure*HUMIDITY_ACC_FACTOR;
				if (newMapSquare.humiditySpeed[neighbourDirectionIndices.x][neighbourDirectionIndices.y] >= 0.0)
				{
					humiditySpeedSum += newMapSquare.humiditySpeed[neighbourDirectionIndices.x][neighbourDirectionIndices.y];
				}

				//temperature
				if (windSpeedTowards > 0.0)
				{
					newMapSquare.temperature += neighbourMapSquare.temperature*windSpeedTowards;
					temperatureNeighbours += windSpeedTowards;
				}

				//wind
				if (windSpeedTowards > 0.0)
				{
					newMapSquare.windSpeed += neighbourMapSquare.windSpeed*windSpeedTowards;
					windSpeedNeighbours += windSpeedTowards;
				}
				

			}
		}




		for (int i = 0; i < hex::NR_OF_DIRECTIONS; ++i)
		{
			hex::NeighbourDirection neighbourDirection = hex::getNeighbourDirection(i);
			Vector2D<int> neighbourDirectionVector = MapSquare::getNeighbourDirection(neighbourDirection);
			Vector2D<int> thisNeighbourCoord = hex::getNeighbour(position, neighbourDirection, mapSize, wrapType);
			Vector2D<int> neighbourDirectionIndices = MapSquare::getNeighbourDirectionIndices(neighbourDirectionVector);
			Vector2D<int> myDirectionIndices = MapSquare::getNeighbourDirectionIndices(-neighbourDirectionVector);
			if (hex::withinMap(thisNeighbourCoord, mapSize))
			{
				MapSquare &neighbourMapSquare = (*oldMapSquares)[thisNeighbourCoord.x][thisNeighbourCoord.y];


				//ground
				if (newMapSquare.groundSpeed[neighbourDirectionIndices.x][neighbourDirectionIndices.y] > 0.0)
				{
					const double FRICTION_FACTOR = 0.075;
					if (groundSpeedSum > oldMapSquare.groundHeight)
					{
						newMapSquare.groundSpeed[neighbourDirectionIndices.x][neighbourDirectionIndices.y] = oldMapSquare.groundHeight*newMapSquare.groundSpeed[neighbourDirectionIndices.x][neighbourDirectionIndices.y] / groundSpeedSum;
					}
					newMapSquare.groundSpeed[neighbourDirectionIndices.x][neighbourDirectionIndices.y] *= FRICTION_FACTOR;
					newMapSquare.groundHeight -= newMapSquare.groundSpeed[neighbourDirectionIndices.x][neighbourDirectionIndices.y]; //remove the ground going out to my neighbours
				}
				else
				{
					newMapSquare.groundSpeed[neighbourDirectionIndices.x][neighbourDirectionIndices.y] = 0.0;
				}

				if (neighbourMapSquare.groundSpeed[myDirectionIndices.x][myDirectionIndices.y] > 0.0)
				{
					newMapSquare.groundHeight += neighbourMapSquare.groundSpeed[myDirectionIndices.x][myDirectionIndices.y]; //add the ground that went out from my neighbour
				}


				//water
				if (newMapSquare.waterSpeed[neighbourDirectionIndices.x][neighbourDirectionIndices.y] > 0.0)
				{
					const double FRICTION_FACTOR = 0.6;
					if (waterSpeedSum > oldMapSquare.waterHeight)
					{
						newMapSquare.waterSpeed[neighbourDirectionIndices.x][neighbourDirectionIndices.y] = oldMapSquare.waterHeight*newMapSquare.waterSpeed[neighbourDirectionIndices.x][neighbourDirectionIndices.y] / waterSpeedSum;
					}
					newMapSquare.waterSpeed[neighbourDirectionIndices.x][neighbourDirectionIndices.y] *= FRICTION_FACTOR;
					newMapSquare.waterHeight -= newMapSquare.waterSpeed[neighbourDirectionIndices.x][neighbourDirectionIndices.y]; //remove the water going out to my neighbours
				}
				else
				{
					newMapSquare.waterSpeed[neighbourDirectionIndices.x][neighbourDirectionIndices.y] = 0.0;
				}

				if (neighbourMapSquare.waterSpeed[myDirectionIndices.x][myDirectionIndices.y] > 0.0)
				{
					newMapSquare.waterHeight += neighbourMapSquare.waterSpeed[myDirectionIndices.x][myDirectionIndices.y]; //add the water that went out from my neighbour
				}


				//humidity
				if (newMapSquare.humiditySpeed[neighbourDirectionIndices.x][neighbourDirectionIndices.y] > 0.0)
				{
					const double FRICTION_FACTOR = 0.95;
					if (humiditySpeedSum > oldMapSquare.humidity)
					{
						newMapSquare.humiditySpeed[neighbourDirectionIndices.x][neighbourDirectionIndices.y] = oldMapSquare.humidity*newMapSquare.humiditySpeed[neighbourDirectionIndices.x][neighbourDirectionIndices.y] / humiditySpeedSum;
					}
					newMapSquare.humiditySpeed[neighbourDirectionIndices.x][neighbourDirectionIndices.y] *= FRICTION_FACTOR;
					newMapSquare.humidity -= newMapSquare.humiditySpeed[neighbourDirectionIndices.x][neighbourDirectionIndices.y]; //remove the water going out to my neighbours
				}
				else
				{
					newMapSquare.humiditySpeed[neighbourDirectionIndices.x][neighbourDirectionIndices.y] = 0.0;
				}

				if (neighbourMapSquare.humiditySpeed[myDirectionIndices.x][myDirectionIndices.y] > 0.0)
				{
					newMapSquare.humidity += neighbourMapSquare.humiditySpeed[myDirectionIndices.x][myDirectionIndices.y]; //add the water that went out from my neighbour
				}




			}
		}




		//humidity
		double maxHumidity;
		const double TEMPERATURE_FACTOR = 1.0;
		const double MINIMUM_TEMP = -20;
		maxHumidity = (oldMapSquare.temperature + -MINIMUM_TEMP)*TEMPERATURE_FACTOR;
		if (maxHumidity < 0.0)
		{
			maxHumidity = 0.0;
		}
		if (newMapSquare.humidity > maxHumidity)
		{
			const double RELEASE_FACTOR = 0.0001;
			double waterSurplus = newMapSquare.humidity - maxHumidity;
			double releasedWater = waterSurplus*RELEASE_FACTOR;
			newMapSquare.waterHeight += releasedWater;
			newMapSquare.humidity -= releasedWater;

		}
		else
		{
			const double ABSORBTION_FACTOR = 1.0;
			double waterAbsorbtionCapacity = maxHumidity - newMapSquare.humidity;
			double absorbedwater = min(waterAbsorbtionCapacity*ABSORBTION_FACTOR, newMapSquare.waterHeight);
			//newMapSquare.waterHeight -= absorbedwater;
			newMapSquare.humidity += absorbedwater;
		}
		if (newMapSquare.humidity < 0.0)
		{
			newMapSquare.humidity = 0.0;
		}
		
		//water 
		////add sea water
		//const double WATER_RISE_FACTOR = 0.1;
		//if (newMapSquare.waterHeight + newMapSquare.groundHeight < SEA_HEIGHT)
		//{
		//	double difference = SEA_HEIGHT - (newMapSquare.waterHeight + newMapSquare.groundHeight);
		//	newMapSquare.waterHeight += difference*WATER_RISE_FACTOR;

		//}
		assert(newMapSquare.spawnedWater == oldMapSquare.spawnedWater);
		if (newMapSquare.waterHeight < newMapSquare.spawnedWater)
		{
			newMapSquare.waterHeight = newMapSquare.spawnedWater;
		}
		

		//temperature
		double distanceToEquator = abs(double(position.y) - double(mapSize.y) / 2.0) / (double(mapSize.y) / 2.0);
		double avarageTemperature = (-62.5*distanceToEquator + 50.0) - (oldMapSquare.groundHeight + oldMapSquare.waterHeight - SEA_HEIGHT)*0.05 + double(rand() % 100)*0.1;
		double SUN_FACTOR = 2.5;
		newMapSquare.temperature += avarageTemperature*SUN_FACTOR;
		temperatureNeighbours += SUN_FACTOR;
		newMapSquare.temperature /= temperatureNeighbours;




		//wind
		newMapSquare.windSpeed += (Vector2D<double>(double(rand()%1000), double(rand()%1000))/1000.0*2.0 - 1.0)*100.0;
		windSpeedNeighbours += 1.0;
		newMapSquare.windSpeed /= windSpeedNeighbours;

	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	double TerrainGenerator::getWindSpeedTowards(Vector2D<int> direction, Vector2D<double> windSpeed) const
	{		
		Vector2D<double> windSpeedTowards = windSpeed * (-direction.typecast<double>());

		return windSpeedTowards.x + windSpeedTowards.y;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	double TerrainGenerator::calcHeightDifference(Vector2D<int> position) const
	{
		const int NR_OF_NEIGHBOUR_PAIRS = 4;
		const Vector2D<Vector2D<int>> NEIGHBOUR_PAIRS[NR_OF_NEIGHBOUR_PAIRS] = {
			Vector2D<Vector2D<int>>(Vector2D<int>(-1, -1), Vector2D<int>(1, 1)),
			Vector2D<Vector2D<int>>(Vector2D<int>(0, -1), Vector2D<int>(0, 1)),
			Vector2D<Vector2D<int>>(Vector2D<int>(1, -1), Vector2D<int>(-1, 1)),
			Vector2D<Vector2D<int>>(Vector2D<int>(-1, 0), Vector2D<int>(1, 0))
		};
		double sum = 0.0;
		for (int i = 0; i < NR_OF_NEIGHBOUR_PAIRS; ++i)
		{
			Vector2D<int> neighbourPos1 = wrapCoord(position + NEIGHBOUR_PAIRS[i].x, mapSize, wrapType);
			Vector2D<int> neighbourPos2 = wrapCoord(position + NEIGHBOUR_PAIRS[i].y, mapSize, wrapType);
			if (0 <= neighbourPos1.x && neighbourPos1.x < mapSize.x &&
				0 <= neighbourPos1.y && neighbourPos1.y < mapSize.y &&
				0 <= neighbourPos2.x && neighbourPos2.x < mapSize.x &&
				0 <= neighbourPos2.y && neighbourPos2.y < mapSize.y)
			{
				sum += abs((*oldMapSquares)[neighbourPos1.x][neighbourPos1.y].groundHeight - (*oldMapSquares)[neighbourPos2.x][neighbourPos2.y].groundHeight);
				
			}
		}
		return sum;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	TerrainGenerator::TerrainGenerator(GameMain &gameMain) : gameMain(gameMain)
	{
		mapGenerated = false;


		drawTemp = false;
		drawWater = true;
		drawGround = true;
		drawHumidity = false;
		drawLight = true;
		drawWind = false;
		draw = true;

		oldMapSquares = &mapSquares1;
		newMapSquares = &mapSquares2;

		wrapType = hex::WrapType::Flat;
	}



	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	TerrainGenerator::~TerrainGenerator()
	{

	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void TerrainGenerator::generateMap(Vector2D<int> mapSize, hex::WrapType wrapType)
	{
		mapSquares1.clear();
		mapSquares2.clear();
		TerrainGenerator::mapSize = mapSize;
		TerrainGenerator::wrapType = wrapType;

		std::vector<MapSquare> tempColumn;
		tempColumn.resize(mapSize.y, MapSquare());
		mapSquares1.resize(mapSize.x, tempColumn);
		mapSquares2 = mapSquares1; //the two maps are identical now, both filled with zeroes.


#ifdef SHOW_TERRAIN_GENERATION
		renderWindow.create(sf::VideoMode(int(float(mapSize.x)*WINDOW_SIZE_FACTOR*hex::HORIZONTAL_OFFSET_FACTOR), int(float(mapSize.y)* WINDOW_SIZE_FACTOR*hex::VERTICAL_OFFSET_FACTOR)), "Terrain Generation Debug Window", sf::Style::Default);

		drawTemp = false;
		drawWater = true;
		drawGround = true;
		drawHumidity = false;
		drawLight = true;
		drawWind = false;
		draw = true;
#endif

		initiateMap();
		simulateMap();

#ifdef SHOW_TERRAIN_GENERATION
		renderWindow.close();
#endif

		mapGenerated = true;


	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	IDType<TerrainType> TerrainGenerator::getFittingTerrainType(Vector2D<int> coord) const
	{

		assert(mapGenerated);
		assert(0 <= coord.x && coord.x < mapSize.x && 0 <= coord.y && coord.y < mapSize.y);
		MapSquare &mapSquare = (*oldMapSquares)[coord.x][coord.y];
		std::vector<IDType<TerrainType>> terrainTypes;
		double heightDifference = calcHeightDifference(coord);
		for (int i = 0; i < gameMain.terrainTypeLoader.getNumberOfTerrainTypes(); ++i)
		{
			IDType<TerrainType> id(i);
			TerrainType *terrainType = gameMain.terrainTypeLoader.getTerrainType(id);
			if (terrainType->minHumidity <= mapSquare.humidity && mapSquare.humidity <= terrainType->maxHumidity &&
				terrainType->minTemp <= mapSquare.temperature && mapSquare.temperature <= terrainType->maxTemp &&
				terrainType->minWaterHeight <= mapSquare.waterHeight && mapSquare.waterHeight <= terrainType->maxWaterHeight &&
				terrainType->minHeight <= mapSquare.groundHeight && mapSquare.groundHeight <= terrainType->maxHeight &&
				terrainType->minHeightDifference <= heightDifference && heightDifference <= terrainType->maxHeightDifference)
			{
				terrainTypes.push_back(id);
			}
		}


		testException(terrainTypes.size() != 0, NoFittingTerrainException("There was no terrain type that fits:\nHumidity: " + std::to_string(mapSquare.humidity) + ",\nTemperature: " + std::to_string(mapSquare.temperature) + ",\nWaterHeight: " + std::to_string(mapSquare.waterHeight)
			+ ",\nGroundHeight: " + std::to_string(mapSquare.groundHeight) + ",\nHeightDifference: " + std::to_string(heightDifference) + "."));


		return terrainTypes[rand() % terrainTypes.size()];
	}
}