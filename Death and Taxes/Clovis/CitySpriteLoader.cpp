#include "CitySpriteLoader.h"
#include "GameMain.h"
#include "City.h"
#include "Exceptions.h"
#include "FileStructure.h"
#include "DataFileIOFunctions.h"
#include "StringManipulation.h"
#include "AnimationLoader.h"
#include "TechTreeLoader.h"
#include "InfrastructureTypeLoader.h"
namespace dat
{

	CitySpriteLoader::CitySpriteLoader()
	{
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	CitySpriteLoader::~CitySpriteLoader()
	{
		for (auto citySprite : citySprites)
		{
			delete citySprite;
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void CitySpriteLoader::load(ModuleLoader &moduleLoader, AnimationLoader const &animationLoader, TechTreeLoader const &techTreeLoader, InfrastructureTypeLoader const &infrastructureTypeLoader)
	{
		LoadsOnce::load();
		assert(animationLoader.hasLoaded());
		assert(techTreeLoader.hasLoaded());
		assert(infrastructureTypeLoader.hasLoaded());




		DataFileIO &citySpritesFile = moduleLoader.loadDataFileIO(filestructure::CITY_SPRITES_FOLDER_PATH);

		for (auto citySpriteKey : sortStrings(citySpritesFile.getGroupKeys())) //Sort them to make sure the number id's are always the same so that they can be sent between different clients without problem.
		{
			CitySprite *newCitySprite = new CitySprite();
			citySprites.push_back(newCitySprite);
			citySpritesFile.openGroup(citySpriteKey); //a1


			//animations
			citySpritesFile.openGroup("animations"); //b1
			for (auto animationKey : citySpritesFile.getStringKeys())
			{
				if (getBoolean(citySpritesFile, animationKey))
				{
					newCitySprite->animations.push_back(animationLoader.getAnimation(animationLoader.getID(animationKey)));
				}
			}
			citySpritesFile.closeGroup(); //b1


			//pop size
			newCitySprite->maxSize = getDouble(citySpritesFile, "maximum size");
			newCitySprite->minSize = getDouble(citySpritesFile, "minimum size");


			//requirements
			citySpritesFile.openGroup("requirements"); //b2

			citySpritesFile.openGroup("infrastructure levels"); //c1
			for (auto infrastructureType : citySpritesFile.getStringKeys())
			{
				double neededAmount = getDouble(citySpritesFile, infrastructureType);
				testException(neededAmount >= 0, NegativeNeedException("The citySprite \"" + citySpriteKey + "\" has a negative infrastructure need of \"" + infrastructureType + "\"."));
				IDType<InfrastructureType> infrastructureTypeID = infrastructureTypeLoader.getID(infrastructureType);
				newCitySprite->infrastructureNeed.push_back(std::pair<IDType<InfrastructureType>, double>(infrastructureTypeID, neededAmount));
			}
			citySpritesFile.closeGroup(); //c1

			citySpritesFile.openGroup("tech levels"); //c2
			for (auto techType : citySpritesFile.getStringKeys())
			{
				double neededAmount = getDouble(citySpritesFile, techType);
				testException(neededAmount >= 0, NegativeNeedException("The citySprite \"" + citySpriteKey + "\" has a negative tech need of \"" + techType + "\"."));
				IDType<TechType> techTypeId = techTreeLoader.getTechTypeID(techType);
				newCitySprite->techTypeNeed.push_back(std::pair<IDType<TechType>, double>(techTypeId, neededAmount));
			}
			citySpritesFile.closeGroup(); //c2

			citySpritesFile.openGroup("technologies"); //c3
			for (auto technology : citySpritesFile.getStringKeys())
			{
				if (getBoolean(citySpritesFile, technology))
				{
					IDType<Technology> technologyID = techTreeLoader.getTechnologyID(technology);
					newCitySprite->technologiesNeeded.push_back(technologyID);
				}
			}
			citySpritesFile.closeGroup(); //c3

			citySpritesFile.closeGroup(); //b2



			//forbidden
			citySpritesFile.openGroup("forbidden"); //b3

			citySpritesFile.openGroup("infrastructure levels"); //c4
			for (auto infrastructureType : citySpritesFile.getStringKeys())
			{
				double forbiddenAmount = getDouble(citySpritesFile, infrastructureType);
				testException(forbiddenAmount >= 0, NegativeNeedException("The citySprite \"" + citySpriteKey + "\" has a negative infrastructure prohibition of \"" + infrastructureType + "\"."));
				IDType<InfrastructureType> infrastructureTypeID = infrastructureTypeLoader.getID(infrastructureType);
				newCitySprite->infrastructureForbidden.push_back(std::pair<IDType<InfrastructureType>, double>(infrastructureTypeID, forbiddenAmount));
			}
			citySpritesFile.closeGroup(); //c4

			citySpritesFile.openGroup("tech levels"); //c5
			for (auto techType : citySpritesFile.getStringKeys())
			{
				double forbiddenAmount = getDouble(citySpritesFile, techType);
				testException(forbiddenAmount >= 0, NegativeNeedException("The citySprite \"" + citySpriteKey + "\" has a negative tech prohibition of \"" + techType + "\"."));
				IDType<TechType> techTypeId = techTreeLoader.getTechTypeID(techType);
				newCitySprite->techTypeForbidden.push_back(std::pair<IDType<TechType>, double>(techTypeId, forbiddenAmount));
			}
			citySpritesFile.closeGroup(); //c5

			citySpritesFile.openGroup("technologies"); //c6
			for (auto technology : citySpritesFile.getStringKeys())
			{
				if (getBoolean(citySpritesFile, technology))
				{
					IDType<Technology> technologyID = techTreeLoader.getTechnologyID(technology);
					newCitySprite->technologiesForbidden.push_back(technologyID);
				}
			}
			citySpritesFile.closeGroup(); //c6

			citySpritesFile.closeGroup(); //b3

			citySpritesFile.closeGroup();//a1
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Animation *CitySpriteLoader::getAnimation(City const &city, int choice) const
	{
		std::vector<CitySprite *> validSprites; 
		double cityPopulation = city.getPopulation();
		for (auto citySprite : citySprites)
		{
			if (cityPopulation >= citySprite->minSize && cityPopulation < citySprite->maxSize)
			{
				bool valid = true;
				for (auto techNeed : citySprite->technologiesNeeded)
				{
					if (false)//debug test if city doesn't have the technology needed
					{
						valid = false;
						break;
					}
				}
				if (!valid)
				{
					continue;
				}
				for (auto techNeed : citySprite->technologiesForbidden)
				{
					if (false)//debug test if city does have the technology forbidden
					{
						valid = false;
						break;
					}
				}
				if (!valid)
				{
					continue;
				}
				for (auto techNeed : citySprite->techTypeNeed)
				{
					if (false)//debug test if city doesn't have the TechType level needed
					{
						valid = false;
						break;
					}
				}
				if (!valid)
				{
					continue;
				}
				for (auto techNeed : citySprite->techTypeForbidden)
				{
					if (false)//debug test if city does have the TechType level forbidden
					{
						valid = false;
						break;
					}
				}
				if (!valid)
				{
					continue;
				}
				for (auto infrastructureNeed : citySprite->infrastructureNeed)
				{
					if (false)//debug test if city doesn't have the TechType level needed
					{
						valid = false;
						break;
					}
				}
				if (!valid)
				{
					continue;
				}
				for (auto infrastructureNeed : citySprite->infrastructureForbidden)
				{
					if (false)//debug test if city does have the TechType level forbidden
					{
						valid = false;
						break;
					}
				}
				if (valid)
				{
					validSprites.push_back(citySprite);
				}
			}
		}
		testException(validSprites.size() > 0, NoMatchingSpriteExcepetion("Couldn't find a matching city sprite for a city with a population of " + std::to_string(cityPopulation) + "."));//debug unfinished message, these things need to be added to city before a error message can be produced properly.

		CitySprite *chosenSprite = validSprites[choice%validSprites.size()];
		return chosenSprite->animations[choice%chosenSprite->animations.size()];
	}
}