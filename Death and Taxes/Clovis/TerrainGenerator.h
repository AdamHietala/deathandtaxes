#pragma once
#include <vector>
#include "VectorXD.h"
#include "HexGrid.h"
#include "IDType.h"
#include "TerrainGeneratorDefines.h"
#ifdef SHOW_TERRAIN_GENERATION
#include <SFML\Graphics.hpp>
#endif
namespace dat
{
	class GameMain;
	class TerrainType;
	class TerrainGenerator
	{
	private:
#ifdef SHOW_TERRAIN_GENERATION
		sf::RenderWindow renderWindow;
		bool drawTemp, drawWater, drawGround, drawHumidity, drawLight, drawWind, draw;
		void renderState();
#endif
		class MapSquare
		{
		public:
			static Vector2D<int> getNeighbourDirectionIndices(Vector2D<int> direction);
			static Vector2D<int> getNeighbourDirectionIndices(hex::NeighbourDirection neighbourDirection);
			static Vector2D<int> getNeighbourDirection(hex::NeighbourDirection neighbourDirection);
			double waterHeight, waterSpeed[2][3];
			double groundHeight, groundSpeed[2][3];
			double humidity, humiditySpeed[2][3];
			double temperature;
			Vector2D<double> windSpeed;
			double spawnedWater;
			MapSquare();
		private:
		};
		static const double SEA_HEIGHT;


		bool mapGenerated;
		GameMain &gameMain;
		std::vector<std::vector<MapSquare>> mapSquares1, mapSquares2;
		std::vector<std::vector<MapSquare>> *oldMapSquares, *newMapSquares;
		Vector2D<int> mapSize;
		hex::WrapType wrapType;
		void swapOldAndNew();
		void generateLandmass(); //Creates the base height map.
		void setMountainChain(Vector2D<int> position, double direction, int width, int endChance, double height); //creates a few starting points for islands;
		void calculateLandMass(Vector2D<int> position, bool &unDecided); //calculates whether this mapSquare is supposed to be sea or not, and it's height. Is run it several times until all cells report unDecided = false. unDecided tells whether this cell is still unused or is either water or land.
		void flattenTerrain(Vector2D<int> position); //flattens the terrain
		bool createRiver(Vector2D<int> position, std::vector<Vector2D<int>> &visitedPositions = std::vector<Vector2D<int>>(), std::vector<int> searchOrder = std::vector<int>()); //must be called above sea level, return true if sea was found.
		
		void initiateMap(); //sets the starting point for the map simulation.
		void simulateMap(); //simulates the entire map's climate for a few rounds.
		void simulateMapSquare(Vector2D<int> position); //simulates this mapSquare
		double getWindSpeedTowards(Vector2D<int> direction, Vector2D<double> windSpeed) const; //calculates how much windspeed goes towards the centre depending on the direction it comes or goes from.
		double calcHeightDifference(Vector2D<int> position) const; //gives a higher number the higher the difference in height is between the four neighbour pairs are (the neighbours on opposite sides of position).
	public:
		TerrainGenerator(GameMain &gameMain);
		~TerrainGenerator();
		void generateMap(Vector2D<int> mapSize, hex::WrapType wrapType); //Generates the entire map.
		IDType<TerrainType> getFittingTerrainType(Vector2D<int> coord) const; //returns a fitting terrain type.
	};

}