#pragma once
#include "LoadsOnce.h"
#include <vector>
#include "VectorXD.h"
namespace dat
{
	class InputEventListener;
	class GameMain;
	class InputEventHandler
	{
	private:
		GameMain &gameMain;
		std::vector<InputEventListener *> listeners;
		std::vector<unsigned> listenerPriority;
		bool windowIsInFocus;
	public:
		InputEventHandler(GameMain &gameMain);
		~InputEventHandler();
		void handleEvents(); //Handles all events since last frame.
		void addListener(InputEventListener *listener, unsigned priority); //Adds the new listener as a listener to the events. Will make the InputEventHandler forward all events to the listener as well. Priority tells in what order they want to be in the list. The igher the number, the further back they will be. If items have the same priority they will be put next to each other in an arbitrary order.
		void removeListener(InputEventListener *listener); //Removes the listener from the listener vector. The listener will no longer be forwarded the events.
		void flushEvents(); //Goes through all the events without handling them.
	};


}