#include "PopType.h"
#include "PopTypeLoader.h"
#include "TechLevel.h"
#include "InfrastructureLevel.h"
namespace dat
{

	PopType::PopType(IDType<PopType> id, bool merchant, std::vector<std::pair<IDType<ResourceType>, double>> lifeNeeds, std::vector<std::pair<IDType<ResourceType>, double>> basicNeeds, 
		std::vector<std::pair<IDType<ResourceType>, double>> everyDayNeeds, std::vector<std::pair<IDType<ResourceType>, double>> luxuryNeeds, std::vector<std::pair<IDType<InfrastructureType>, double>> infrastructureNeed, 
		std::vector<std::pair<IDType<TechType>, double>> techTypeNeed, std::vector<IDType<Technology>> technologiesNeeded, std::vector<std::pair<IDType<InfrastructureType>, double>> infrastructureForbidden, 
		std::vector<std::pair<IDType<TechType>, double>> techTypeForbidden, std::vector<IDType<Technology>> technologiesForbidden, std::vector<IDType<PopType>> promotionPaths, 
		std::vector<IDType<PopType>> demotionPaths, std::vector<IDType<Recipe>> recipies, double fulfilledNoNeedsGrowth, double fulfilledLifeNeedsGrowth, double fulfilledBasicNeedsGrowth, double fulfilledEveryDayNeedsGrowth, double fulfilledLuxuryNeedsGrowth) :
		id(id), merchant(merchant), lifeNeeds(lifeNeeds), basicNeeds(basicNeeds), everyDayNeeds(everyDayNeeds), luxuryNeeds(luxuryNeeds), infrastructureNeed(infrastructureNeed), techTypeNeed(techTypeNeed),
		technologiesNeeded(technologiesNeeded), infrastructureForbidden(infrastructureForbidden), techTypeForbidden(techTypeForbidden), technologiesForbidden(technologiesForbidden), 
		promotionPaths(promotionPaths), demotionPaths(demotionPaths), recipies(recipies), fulfilledNoNeedsGrowth(fulfilledNoNeedsGrowth), fulfilledLifeNeedsGrowth(fulfilledLifeNeedsGrowth),
		fulfilledBasicNeedsGrowth(fulfilledBasicNeedsGrowth), fulfilledEveryDayNeedsGrowth(fulfilledEveryDayNeedsGrowth), fulfilledLuxuryNeedsGrowth(fulfilledLuxuryNeedsGrowth)
	{

	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	PopType::~PopType()
	{
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	bool PopType::hasSufficient(TechLevel &techLevel, InfrastructureLevel &infrastructureLevel) const
	{
		for (int i = 0; (int)techTypeNeed.size(); ++i)
		{
			if (techLevel.getTechTypeLevel(techTypeNeed[i].first) < techTypeNeed[i].second)
			{
				return false;
			}
		}

		for (int i = 0; i < (int)technologiesNeeded.size(); ++i)
		{
			if (techLevel.isTechUnlocked(technologiesNeeded[i]))
			{
				return false;
			}
		}

		for (int i = 0; i < (int)infrastructureNeed.size(); ++i)
		{
			if (infrastructureLevel.getInfrastructure(infrastructureNeed[i].first) < infrastructureNeed[i].second)
			{
				return false;
			}
		}


		return true;
	}
}