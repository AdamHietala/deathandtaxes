#include "InputEventHandler.h"
#include <assert.h>
#include <SFML\Graphics.hpp>
#include "InputEventListener.h"
#include "GameMain.h"
#include <iostream>
#include "TerrainType.h"
namespace dat
{
	InputEventHandler::InputEventHandler(GameMain &gameMain) : gameMain(gameMain)
	{
		windowIsInFocus = true;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	InputEventHandler::~InputEventHandler()
	{

	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void InputEventHandler::handleEvents()
	{
		sf::Event evnt;
		if (windowIsInFocus)
		{
			while (gameMain.renderWindow.pollEvent(evnt))
			{
				if (evnt.type == sf::Event::Closed)
				{
					gameMain.closeGame();
				}
				else if (evnt.type == sf::Event::MouseWheelMoved)
				{
					gameMain.camera.accelerateZoomSpeed(evnt.mouseWheelScroll.wheel * 0.05f);
				}
				else if (evnt.type == sf::Event::LostFocus)
				{
					windowIsInFocus = false;
				}

				bool eventClaimed = false;
				for (auto listener : listeners)
				{
					listener->eventHappened(evnt, eventClaimed);
				}
			}




			Vector2D<float> movement(0.0f, 0.0f);
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
			{
				movement += Vector2D<float>(-1.0f, 0.0f);
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
			{
				movement += Vector2D<float>(1.0f, 0.0f);
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
			{
				movement += Vector2D<float>(0.0f, -1.0f);
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
			{
				movement += Vector2D<float>(0.0f, 1.0f);
			}
			gameMain.camera.move(movement);

		}
		else
		{

			while (gameMain.renderWindow.pollEvent(evnt))
			{
				if (evnt.type == sf::Event::Closed)
				{
					gameMain.closeGame();
				}
				else if (evnt.type == sf::Event::GainedFocus)
				{
					windowIsInFocus = true;
				}
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void InputEventHandler::addListener(InputEventListener *listener, unsigned priority)
	{
		assert(listenerPriority.size() == listeners.size());
		for (int i = 0; i < (int)listenerPriority.size(); ++i)
		{
			if (priority <= listenerPriority[i])
			{
				listeners.insert(listeners.begin() + i, listener);
				listenerPriority.insert(listenerPriority.begin() + i, priority);
				return;
			}
		}

		listeners.push_back(listener);
		listenerPriority.push_back(priority);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void InputEventHandler::removeListener(InputEventListener *listener)
	{
		for (int i = 0; i < (int) listeners.size(); ++i)
		{
			if (listeners[i] == listener)
			{
				listeners.erase(listeners.begin() + i);
				return;
			}
		}
		ASSERT_DEAD_CODE
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void InputEventHandler::flushEvents()
	{
		sf::Event evnt;
		while (gameMain.renderWindow.pollEvent(evnt));
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		
	
}