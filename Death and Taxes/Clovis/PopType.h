#pragma once
#include "IDType.h"
#include <vector>
#include "NonCopyable.h"
namespace dat
{
	class InfrastructureType;
	class Technology;
	class TechType;
	class TechLevel;
	class ResourcePack;
	class ResourceType;
	class Recipe;
	class PopTypeLoader;
	class InfrastructureLevel;
	class PopType : NonCopyable 
	{
	private:

	public:
		PopType(IDType<PopType> id, bool merchant, std::vector<std::pair<IDType<ResourceType>, double>> lifeNeeds, std::vector<std::pair<IDType<ResourceType>, double>> basicNeeds,
			std::vector<std::pair<IDType<ResourceType>, double>> everyDayNeeds, std::vector<std::pair<IDType<ResourceType>, double>> luxuryNeeds,
			std::vector<std::pair<IDType<InfrastructureType>, double>> infrastructureNeed, std::vector<std::pair<IDType<TechType>, double>> techTypeNeed,
			std::vector<IDType<Technology>> technologiesNeeded, std::vector<std::pair<IDType<InfrastructureType>, double>> infrastructureForbidden,
			std::vector<std::pair<IDType<TechType>, double>> techTypeForbidden, std::vector<IDType<Technology>> technologiesForbidden, std::vector<IDType<PopType>> promotionPaths,
			std::vector<IDType<PopType>> demotionPaths, std::vector<IDType<Recipe>> recipies, double fulfilledNoNeedsGrowth, double fulfilledLifeNeedsGrowth, double fulfilledBasicNeedsGrowth,
			double fulfilledEveryDayNeedsGrowth, double fulfilledLuxuryNeedsGrowth);
		~PopType();

		const IDType<PopType> id;
		const bool merchant;

		const std::vector<std::pair<IDType<ResourceType>, double>> lifeNeeds; 
		const std::vector<std::pair<IDType<ResourceType>, double>> basicNeeds;
		const std::vector<std::pair<IDType<ResourceType>, double>> everyDayNeeds;
		const std::vector<std::pair<IDType<ResourceType>, double>> luxuryNeeds;

		const std::vector<std::pair<IDType<InfrastructureType>, double>> infrastructureNeed;
		const std::vector<std::pair<IDType<TechType>, double>> techTypeNeed;
		const std::vector<IDType<Technology>> technologiesNeeded;

		const std::vector<std::pair<IDType<InfrastructureType>, double>> infrastructureForbidden;
		const std::vector<std::pair<IDType<TechType>, double>> techTypeForbidden;
		const std::vector<IDType<Technology>> technologiesForbidden;

		const std::vector<IDType<PopType>> promotionPaths;
		const std::vector<IDType<PopType>> demotionPaths;

		const std::vector<IDType<Recipe>> recipies;

		bool hasSufficient(TechLevel &techLevel, InfrastructureLevel &infrastructureLevel) const; //returns true if techlevel and infrastructureLevel is high enough to allow promotion/demotion to this PopType.

		//const double fulfilledNoneGrowth; The percentage of needs not satisfied is the number of pops lost, rather than a factor
		const double fulfilledNoNeedsGrowth;
		const double fulfilledLifeNeedsGrowth;
		const double fulfilledBasicNeedsGrowth;
		const double fulfilledEveryDayNeedsGrowth;
		const double fulfilledLuxuryNeedsGrowth;
	};

}