#pragma once
#include <map>
#include <vector>
#include "IDType.h"
#include "LoadsOnce.h"
#include <string>
namespace dat
{
	class ModuleLoader;
	class TerrainType;
	class TerrainTypeLoader : public LoadsOnce
	{
	private:
		int idGiver;
		std::map<std::string, IDType<TerrainType>> stringIDToID; //Maps the terraintype string ID to the animationID
		std::vector<TerrainType *> iDToTerrainType;
		std::vector<std::string> idToStringID;
		void addType(std::string stringID, TerrainType *terrainType); //Adds the type to the datastructure.
	public:
		TerrainTypeLoader();
		~TerrainTypeLoader();
		void load(ModuleLoader &moduleLoader);
		IDType<TerrainType> getID(std::string stringID) const; //Returns the animation ID.
		TerrainType *getTerrainType(IDType<TerrainType> id) const; //Returns a reference to the loaded terraintype with the particular ID.
		std::string getStringID(IDType<TerrainType> id) const; //Returns the stringID so that it can be referred to in a savefile.
		int getNumberOfTerrainTypes() const; //returns the number of terrain types loaded.
	};

}