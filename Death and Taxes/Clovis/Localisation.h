#pragma once
#include <string>
#include <map>
#include "LoadsOnce.h"
namespace dat
{
	class ModuleLoader;
	class Localisation : public LoadsOnce
	{
	private:
		std::map<std::string, std::string> strings;
	public:
		Localisation();
		~Localisation();

		void load(ModuleLoader &moduleLoader);
		std::string getString(std::string key) const; //Returns the localised string. If the key is not found the string returned will be the same as the one resieved but with "STRING MISSING " attached to it.
	};

}