#include "InfrastructureType.h"

namespace dat
{
	InfrastructureType::InfrastructureType(IDType<InfrastructureType> id, std::vector<std::pair<IDType<TechType>, double>> neededTechLevel, std::vector<std::pair<IDType<ResourceType>, double>> resourceCosts, std::vector<std::pair<IDType<Technology>, double>> technologyLimits) :
		id(id), neededTechLevel(neededTechLevel), resourceCosts(resourceCosts), technologyLimits(technologyLimits)
	{
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	InfrastructureType::~InfrastructureType()
	{
	}
}