#pragma once
#include "IDType.h"
#include <vector>
#include "PointerSafeMemory.h"
namespace dat
{
	template<typename type>
	class IDDealingList
	{
	private:
		PointerSafeMemory<type> psMemory;
		std::vector<IDType<type>> usedIndices;
		std::vector<type *> types;
		std::vector<int> freeIndices;
		int firstFreeIndex;

		void removeIndex(IDType<type> index, int searchStart, int searchEnd)
		{
			int middlePoint = searchStart + (searchEnd - searchStart) / 2;
			if (usedIndices[middlePoint].getID() < index.getID())
			{
				storeIndex(index, middlePoint + 1, searchEnd);
			}
			else if (usedIndices[middlePoint].getID() > index.getID())
			{
				storeIndex(index, searchStart, middlePoint - 1);
			}
			else
			{
				assert(usedIndices[middlePoint] == index);
				usedIndices.erase(usedIndices.begin() + middlePoint);
			}
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		void storeIndex(IDType<type> index, int searchStart, int searchEnd)
		{
			if (searchStart == searchEnd)
			{
				usedIndices.insert(usedIndices.begin() + searchStart, index);
			}
			else
			{
				int middlePoint = searchStart + (searchEnd - searchStart) / 2;
				if (usedIndices[middlePoint].getID() < index.getID())
				{
					storeIndex(index, middlePoint + 1, searchEnd);
				}
				else if (usedIndices[middlePoint].getID() > index.getID())
				{
					storeIndex(index, searchStart, middlePoint - 1);
				}
			}
		}

	public:
		IDDealingList(unsigned expectedMaxCount = 500) : psMemory(expectedMaxCount)
		{
			clear();
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		~IDDealingList()
		{
			clear();
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		type *getItem(IDType<type> id) const //returns the 'type' object with this ID.
		{
			return types[id.getID()];
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		IDType<type> getNextFreeID() const //returns the next id that the next instantiation of a 'type' object should have. This is used in the 'type' object's constructor to set its own ID.
		{
			return IDType<type>(firstFreeIndex);
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		int getSize() const //returns the amount of saved objects in the list
		{
			return (int)usedIndices.size();
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		type * instantiate(type &copyFrom) //Copyfrom is supposed to only be a temporary object with an ID set by calling getNextFreeID(). Creates a new copy of copyFrom, stores it in the list and returns a pointer to it. The index of the item in the list will be the ID of the new object.
		{

			type *pointer = psMemory.instantiate(copyFrom);


			assert(pointer->getID().getID() == firstFreeIndex);
			assert(firstFreeIndex <= int(freeIndices.size()));
			if (firstFreeIndex == int(freeIndices.size()))
			{
				types.push_back(nullptr);
				freeIndices.push_back(firstFreeIndex + 1);
			}

			types[firstFreeIndex] = pointer;
			firstFreeIndex = freeIndices[firstFreeIndex];
			storeIndex(pointer->getID(), 0, (int) usedIndices.size());
			return pointer;
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		void deleteItem(type *pointer) //Deletes a pointed and adds its ID back to the list to be reused once again.
		{
			assert(pointer->getID().getID() < int(types.size())); //this function assumes all types have this function, which should return an IDType<type>

			int index = pointer->getID().getID();
			psMemory.deletePointer(types[index]);
			types[index] = nullptr;
			freeIndices[index] = firstFreeIndex;
			firstFreeIndex = index;
			removeIndex(pointer->getID(), 0, (int)usedIndices.size());
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		void clear() //deletes all the pointers within the list and clears them from the list.
		{
			psMemory.clear();
			usedIndices.clear();
			types.clear();
			freeIndices.clear();
			firstFreeIndex = 0;
		}

		const std::vector<IDType<type>> &getIDList() const //returns all instantiated ids that have not been deleted yet.
		{
			return usedIndices;
		}

	};
}