#pragma once
#include "LoadsOnce.h"
#include <map>
#include <vector>
#include "IDType.h"
#include <string>
namespace dat
{
	class ModuleLoader;
	class AnimationLoader;
	class TerrainTypeLoader;
	class Tileset;
	class TerrainType;
	class TilesetLoader : public LoadsOnce
	{
	private:
		std::map<std::string, IDType<Tileset>> stringIDToID; //Maps the tileset string ID to the animationID
		std::vector<Tileset *> iDToTileset;
		std::vector<std::string> idToStringID;
		void addTileset(std::string stringID, Tileset *tileset); //Adds the tileset to the datastructure.
		std::vector<Tileset *> terrainTypeIDToTileset;
		int highestPriority;
	public:
		TilesetLoader();
		~TilesetLoader();

		void load(ModuleLoader &moduleLoader, AnimationLoader const &animationLoader, TerrainTypeLoader const &terrainTypeLoader); //Needs Animations to be loaded.
		IDType<Tileset> getID(std::string stringID) const; //Returns the tileset ID.
		Tileset *getTileset(IDType<Tileset> id) const; //Returns a reference of the loaded tileset with the particular ID.
		Tileset *getTileset(IDType<TerrainType> id) const; //Returns a reference of the loaded tileset that is for this terraintype.
		Tileset *getTileset(TerrainType *terrainType) const; //Returns a reference of the loaded tileset that is for this terraintype.
		std::string getStringID(IDType<Tileset> id) const; //Returns the stringID so that it can be referred to in a savefile.
		int getHighestPriority() const;//returns highestPriority
	};

}