#pragma once
#include <vector>
#include "Widget.h"
#include "InputEventListener.h"
#include "LoadsOnce.h"
namespace dat
{
	class GameMain;
	class DataFileIO;
	class RectButton;
	class GUIMenu;

	class GUI : public Widget, public InputEventListener, public LoadsOnce
	{
	protected:
		std::vector<Widget *> widgets;
		void addWidget(Widget *widget); //Adds this widget to the gui;
		void removeWidget(Widget *widget); //Removes this widget from the gui and deletes it

		RectButton *loadRectButton(GameMain &gameMain, DataFileIO &dataFileIO, std::string buttonKey, GUIMenu *menu, std::string textKey); //Loads the rectbutton. Returns it so the on event functions can be added.

		GameMain &gameMain;
		GUIMenu *ingameMainMenu;
	public:
		GUI(GameMain &gameMain);
		~GUI();

		virtual void eventHappened(sf::Event &evnt, bool &eventClaimed) override; //Tells all the widgets about the event. It's up to the individual widget to determine if the event was relevant for it or not.
		virtual void draw(sf::RenderTarget &renderTarget, sf::Time time, bool respectAlpha = false) override; //Draws the gui to the rendertarget.

		void load(); //Loads the GUI. Must only ever be called once.
		
	};
}