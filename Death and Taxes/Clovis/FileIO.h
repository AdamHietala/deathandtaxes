#pragma once
#include <string>
#include <vector>
namespace dat
{
	bool fileExists(std::string filePath);
	void writeToFile(std::string lines, std::string filePath); //Throws IOException if the file could not be written to.
	void writeToFile(std::vector<std::string> &lines, std::string filePath); //Throws IOException if the file could not be written to.
	std::string getFileContent(std::string filePath, bool cleanNewline = true); //Throws IOException if the file could not be opened. cleanNewline will make all newlines be standardised to '\n'
	std::vector<std::string> getFileLines(std::string filePath, bool cleanNewline = true); //Throws IOException if the file could not be opened. cleanNewline will make all newlines be standardised to '\n'
	bool folderExists(std::string folderPath);
	std::vector<std::string> listFilesInFolder(std::string folderPath, std::string extension = "");
	std::string getFileType(std::string fileName);	//will return "" if no extension is found
	bool goesThroughFolder(std::string folderPath, std::string subfilePath); //checks if the searchpath of subfilePath goes through folderPath. Does not check if the file or folder actually exists.
	std::string standardiseFilePath(std::string path); //will expand all dots and double dots and make all slashes be backslashes '\\'
}