#include "FontLoader.h"
#include "FileStructure.h"
#include "FileIO.h"
#include "Exceptions.h"
namespace dat
{

	FontLoader::FontLoader(ModuleLoader &moduleLoader) : moduleLoader(moduleLoader)
	{
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	FontLoader::~FontLoader()
	{
		for (auto font : alreadyLoadedFonts)
		{
			delete font.second;
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	sf::Font &FontLoader::loadFont(std::string defaultPath)
	{
		defaultPath = filestructure::FONT_FOLDER_PATH + "\\" + standardiseFilePath(defaultPath);
		if (alreadyLoadedFonts.count(defaultPath) == 0)
		{
			std::string truePath = moduleLoader.getModFilePath(defaultPath);
			sf::Font *font = new sf::Font();
			if (!font->loadFromFile(truePath))
			{
				delete font;
				throw IOException("The font at \"" + defaultPath + "\" could not be opened");
			}
			alreadyLoadedFonts[defaultPath] = font;
		}
		return *alreadyLoadedFonts.at(defaultPath);
	}
}