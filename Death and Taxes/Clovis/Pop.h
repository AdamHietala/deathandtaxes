#pragma once
#include "ResourcePack.h"
#include "PopType.h"
#include "Ideology.h"
#include "NonCopyable.h"
#include "IDDealingList.h"
namespace dat
{
	class Defines;
	class City;
	class Pop //debug missing religion, culture and techLevel
	{
	public:
		enum class NeedType{ None, LifeNeeds, BasicNeeds, EveryDayNeeds, LuxuryNeeds };
	private:
		const Defines &defines;
		const ResourceTypeLoader &resourceTypeloader;
		City &homeCity;
		PopType const &popType;
		IDType<Pop> const ID;
		ResourcePack resourcePack; //the owned resources of this pop, the price they'd put on them and the amount they to sell (negative numbers means they want to buy).
		Ideology ideology; //the avarage ideology of this pop.
		NeedType highestFulfillableNeed;//the currently highest fulfillable need.
		double popSize; //the amount of people inside this pop.
		double happiness;//how happy the population is. Goes between 0.0 and 1.0
		double fear; //how fearful the population is of the government above them and the people around them. Goes between 0.0 and 1.0

		void setPricesAndUpForSale(); //sets the value that each pop would put on each item.
		void checkForBestPrice(City &city, IDType<ResourceType> resourceType, Pop *&bestSeller); //find the seller with the best price. People only buy and sell from merchants, except for merchants who buy and sell from everyone.
		void buyResources();//buys the resources they need according to prices and up for sale. 
		

		void checkHighestFulfillableNeed(); //tells whether this pop can cover the specific need for the PopType
		void tryFulfillingNeeds(); //assumes getFulfillableNeed has been called beforehand to see what need should be fulfilled. The pop will try to fulfill the need one above its unfulfillable need.
	public:
		Pop(Defines &defines, ResourceTypeLoader const &resourceTypeloader, IDType<Pop> id, PopType const &popType, City &homeCity, double popSize, double happiness = 1.0, double fear = 0.0);
		~Pop();

		IDType<Pop> getID() const;
		void transferFrom(Pop &fromPop, double amount);//transfers 'amount' pop from 'otherPop' to 'this', making sure the right amount of resources are transferred and other variables are changed correctly.
		double getPopSize() const;
		City &getHomeCity();
		const PopType &getPopType() const;
		void createCitizens(double amount);//adds 'amount' to the 'popSize'.
		ResourcePack &getResourcePack(); //returns the resource pack, containing the resources this pop collectively has.
		Ideology &getIdeology(); //the avarage ideology of this pop.
		NeedType getHighestFulfillableNeed() const; //the currently highest fulfillable need.
		void calcTick(int tickTime); //calcs the pop's stat changes for the next tick.
	};

}