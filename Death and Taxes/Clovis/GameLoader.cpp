#include "GameLoader.h"
#include "GameMain.h"
#include "FileStructure.h"
#include "DataFileIOFunctions.h"
#include <SFML\Graphics.hpp>
#include <thread>
#include <iostream>
namespace dat
{
	void GameLoader::loadMe(GameMain &gameMain)
	{
		Vector2D<float> screenSize(float(gameMain.options.screenSize.x), float(gameMain.options.screenSize.y));


		DataFileIO &loaderFile = gameMain.moduleLoader.loadDataFileIO(filestructure::GAME_LOADER_FILE_PATH);

		//backgrounds
		loaderFile.openGroup("backgrounds"); //a1
		for (auto backgroundPath : loaderFile.getStringKeys())
		{
			if (getBoolean(loaderFile, backgroundPath))
			{
				backgrounds.push_back(&gameMain.textureLoader.loadTexture(backgroundPath));
			}
		}
		loaderFile.closeGroup(); //a1
		currentBackground.setPosition(0, 0);
		changeImage = float(getDouble(loaderFile, "change background time"));


		//text
		loaderFile.openGroup("text"); //b1
		sf::Font &font = gameMain.fontLoader.loadFont(loaderFile.getString("font"));
		loadingText.setFont(font);
		sf::Color colour = getColor(loaderFile, "colour");
		loadingText.setOutlineColor(colour);
		loadingText.setFillColor(colour);
		loadingText.setString("");
		
		
		Vector2D<double> positionPercent = get2DVectorDouble(loaderFile, "position");
		textPosition = positionPercent.typecast<float>()*screenSize;
		loadingText.setPosition(sf::Vector2f(textPosition.x, textPosition.y));

		textScale = float(getDouble(loaderFile, "scale"));
		loaderFile.closeGroup(); //b1

		loadState = LoadState::Start;

	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void GameLoader::entertainUser(GameMain &gameMain)
	{
		Vector2D<float> screenSize(float(gameMain.options.screenSize.x), float(gameMain.options.screenSize.y));


		currentBackground.setTexture(*backgrounds[rand() % backgrounds.size()]);
		currentBackground.setScale(screenSize.x / currentBackground.getLocalBounds().width, screenSize.y / currentBackground.getLocalBounds().height);

		LoadState oldLoadState = LoadState::Start; //even if the threads have come further than this that will be updated later, with the correct consequences as opposed to checking it here.
		sf::Clock clock;


		while (true)
		{
			loadStateMutex.lock();
			LoadState currentLoadState = loadState;
			loadStateMutex.unlock();

			if (currentLoadState == LoadState::Done)
			{
				break;
			}
			else
			{
				if (oldLoadState != currentLoadState)
				{
					oldLoadState = currentLoadState;


					switch (currentLoadState)
					{
					case LoadState::Defines:
						loadingText.setString(gameMain.localisation.getString("loading defines"));
						break;

					case LoadState::Animations:
						loadingText.setString(gameMain.localisation.getString("loading animations"));
						break;

					case LoadState::GUI:
						loadingText.setString(gameMain.localisation.getString("loading gui"));
						break;

					case LoadState::TerrainTypes:
						loadingText.setString(gameMain.localisation.getString("loading terrain types"));
						break;

					case LoadState::Tilesets:
						loadingText.setString(gameMain.localisation.getString("loading tilesets"));
						break;

					case LoadState::ResourceTypes:
						loadingText.setString(gameMain.localisation.getString("loading resource types"));
						break;

					case LoadState::TechTree:
						loadingText.setString(gameMain.localisation.getString("loading tech tree"));
						break;

					case LoadState::PopTypes:
						loadingText.setString(gameMain.localisation.getString("loading pop types"));
						break;

					case LoadState::Recipies:
						loadingText.setString(gameMain.localisation.getString("loading recipies"));
						break;

					case LoadState::InfrastructureTypes:
						loadingText.setString(gameMain.localisation.getString("loading infrastructure"));
						break;

					case LoadState::CitySprites:
						loadingText.setString(gameMain.localisation.getString("loading city sprites"));
						break;

					case LoadState::TypeImages:
						loadingText.setString(gameMain.localisation.getString("loading type images"));
						break;
					}

					float scaleFactor = (screenSize.y / loadingText.getLocalBounds().height) * textScale;
					loadingText.setScale(scaleFactor, scaleFactor);


					loadingText.setOrigin(loadingText.getLocalBounds().left + loadingText.getLocalBounds().width / 2, loadingText.getLocalBounds().top + loadingText.getLocalBounds().height / 2);
					float scaleX = loadingText.getScale().x / screenSize.x*loadingText.getLocalBounds().width;
					float scaleY = loadingText.getScale().y / screenSize.y*loadingText.getLocalBounds().height;
					loadingText.setPosition(sf::Vector2f(textPosition.x, textPosition.y));

				}
			}

			if (clock.getElapsedTime().asSeconds() > changeImage)
			{
				currentBackground.setTexture(*backgrounds[rand() % backgrounds.size()]);
				currentBackground.setScale(screenSize.x / currentBackground.getLocalBounds().width, screenSize.y / currentBackground.getLocalBounds().height);
				clock.restart();
			}
			gameMain.inputEventHandler.flushEvents();
			gameMain.renderer.drawLoadingScreen(currentBackground, loadingText);
		}

	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	GameLoader::GameLoader()
	{
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	GameLoader::~GameLoader()
	{
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void GameLoader::load(GameMain &gameMain)
	{
		LoadsOnce::load();

		
		gameMain.moduleOrder.load(); 
		gameMain.options.load(); 
		gameMain.localisation.load(gameMain.moduleLoader); 
		gameMain.options.updateOptions(gameMain); 

		loadMe(gameMain); //needs moduleOrder to be loaded and options to be updated.

		auto changeState = [&](LoadState newState){ //Small function to make changing states easier.
			loadStateMutex.lock();
			loadState = newState;
			loadStateMutex.unlock();
		};


		auto loader = [&](void *)
		{
			try //try here beacuse it's another thread
			{

				changeState(LoadState::Defines);
				gameMain.defines.load(gameMain.moduleLoader);
				changeState(LoadState::Animations);
				gameMain.animationLoader.load(gameMain.moduleLoader, gameMain.textureLoader);
				changeState(LoadState::GUI);
				gameMain.gui.load(); //Needs the load order, localisation, animationLoader and options to be done.
				changeState(LoadState::TerrainTypes);
				gameMain.terrainTypeLoader.load(gameMain.moduleLoader); 
				changeState(LoadState::Tilesets);
				gameMain.tilesetLoader.load(gameMain.moduleLoader, gameMain.animationLoader, gameMain.terrainTypeLoader); 
				changeState(LoadState::ResourceTypes);
				gameMain.resourceTypeloader.load(gameMain.moduleLoader);
				changeState(LoadState::TechTree);
				gameMain.techTreeLoader.load(gameMain.moduleLoader);
				changeState(LoadState::InfrastructureTypes);
				gameMain.infrastructureTypeLoader.load(gameMain.moduleLoader, gameMain.techTreeLoader, gameMain.resourceTypeloader);
				changeState(LoadState::Recipies);
				gameMain.recipeLoader.load(gameMain.moduleLoader, gameMain.resourceTypeloader, gameMain.techTreeLoader, gameMain.infrastructureTypeLoader);
				changeState(LoadState::PopTypes);
				gameMain.popTypeLoader.load(gameMain.moduleLoader, gameMain.resourceTypeloader, gameMain.techTreeLoader, gameMain.recipeLoader, gameMain.infrastructureTypeLoader);
				changeState(LoadState::CitySprites);
				gameMain.citySpriteLoader.load(gameMain.moduleLoader, gameMain.animationLoader, gameMain.techTreeLoader, gameMain.infrastructureTypeLoader);
				changeState(LoadState::TypeImages);
				gameMain.typeImageLoader.load(gameMain.moduleLoader, gameMain.animationLoader, gameMain.popTypeLoader, gameMain.resourceTypeloader);
				changeState(LoadState::Done);

			}
			catch (std::exception &ex)
			{
				std::cout << ex.what() << "\n";
				throw;
			}
		};

		gameMain.threadScheduler.scheduleWork(loader, nullptr);


		entertainUser(gameMain);

	}
}