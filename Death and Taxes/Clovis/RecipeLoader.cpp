#include "RecipeLoader.h"
#include "Exceptions.h"
#include "FileStructure.h"
#include "StringManipulation.h"
#include "DataFileIOFunctions.h"
#include "ResourceTypeLoader.h"
#include "TechTreeLoader.h"
#include "InfrastructureTypeLoader.h"
#include "ModuleLoader.h"
namespace dat
{
	void RecipeLoader::addRecipe(std::string stringID, Recipe *recipe)
	{
		testException(stringIDToID.count(stringID) == 0, KeyAlreadyExistsException("The Recipe with string id \"" + stringID + "\" was added twice."));
		assert(stringIDToID.size() == recipe->id.getID());
		stringIDToID[stringID] = recipe->id;
		idToStringID.push_back(stringID);
		idToRecipe.push_back(recipe);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	RecipeLoader::RecipeLoader()
	{
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	RecipeLoader::~RecipeLoader()
	{
		for (auto recipe : idToRecipe)
		{
			delete recipe;
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void RecipeLoader::load(ModuleLoader &moduleLoader, ResourceTypeLoader const &resourceTypeloader, TechTreeLoader const &techTreeLoader, InfrastructureTypeLoader const &infrastructureTypeLoader)
	{
		LoadsOnce::load();
		assert(resourceTypeloader.hasLoaded());
		assert(techTreeLoader.hasLoaded());
		assert(infrastructureTypeLoader.hasLoaded());

		int freeID = 0;


		DataFileIO &recipiesFile = moduleLoader.loadDataFileIO(filestructure::RECIPIES_FOLDER_PATH);


		for (auto recipiesKey : sortStrings(recipiesFile.getGroupKeys())) //Sort them to make sure the number id's are always the same so that they can be sent between different clients without problem.
		{
			recipiesFile.openGroup(recipiesKey); //a1

			std::vector<std::pair<IDType<ResourceType>, double>> neededResources;
			recipiesFile.openGroup("needed resources"); //b1
			for (auto amountKey : recipiesFile.getStringKeys())
			{
				double amount = getDouble(recipiesFile, amountKey);
				IDType<ResourceType> id = resourceTypeloader.getID(amountKey);
				neededResources.push_back(std::pair<IDType<ResourceType>, double>(id, amount));
			}
			recipiesFile.closeGroup(); //b1
			
			std::vector<std::pair<IDType<ResourceType>, double>> yieldedResources;
			recipiesFile.openGroup("yielded resources"); //b2
			for (auto amountKey : recipiesFile.getStringKeys())
			{
				double amount = getDouble(recipiesFile, amountKey);
				IDType<ResourceType> id = resourceTypeloader.getID(amountKey);
				yieldedResources.push_back(std::pair<IDType<ResourceType>, double>(id, amount));
			}
			recipiesFile.closeGroup(); //b2

			

			std::vector<std::pair<IDType<TechType>, double>> minimumTechLevel;
			recipiesFile.openGroup("minimum tech level"); //b3
			for (auto amountKey : recipiesFile.getStringKeys())
			{
				double amount = getDouble(recipiesFile, amountKey);
				IDType<TechType> id = techTreeLoader.getTechTypeID(amountKey);
				minimumTechLevel.push_back(std::pair<IDType<TechType>, double>(id, amount));
			}
			recipiesFile.closeGroup(); //b3

			std::vector<std::pair<IDType<TechType>, double>> maximumTechLevel;
			recipiesFile.openGroup("maximum tech level"); //b4
			for (auto amountKey : recipiesFile.getStringKeys())
			{
				double amount = getDouble(recipiesFile, amountKey);
				IDType<TechType> id = techTreeLoader.getTechTypeID(amountKey);
				maximumTechLevel.push_back(std::pair<IDType<TechType>, double>(id, amount));
			}
			recipiesFile.closeGroup(); //b4

			

			std::vector<IDType<Technology>> neededTechs;
			recipiesFile.openGroup("needed technologies"); //b5
			for (auto technologyKey : recipiesFile.getStringKeys())
			{
				if (getBoolean(recipiesFile, technologyKey))
				{
					IDType<Technology> id = techTreeLoader.getTechnologyID(technologyKey);
					neededTechs.push_back(id);
				}
			}
			recipiesFile.closeGroup(); //b5

			std::vector<IDType<Technology>> forbiddenTechs;
			recipiesFile.openGroup("forbidden technologies"); //b6
			for (auto technologyKey : recipiesFile.getStringKeys())
			{
				if (getBoolean(recipiesFile, technologyKey))
				{
					IDType<Technology> id = techTreeLoader.getTechnologyID(technologyKey);
					forbiddenTechs.push_back(id);
				}
			}
			recipiesFile.closeGroup(); //b6

			

			std::vector<std::pair<IDType<InfrastructureType>, double>> minimumInfrastructureLevel;
			recipiesFile.openGroup("minimum infrastructure level"); //b7
			for (auto amountKey : recipiesFile.getStringKeys())
			{
				double amount = getDouble(recipiesFile, amountKey);
				IDType<InfrastructureType> id = infrastructureTypeLoader.getID(amountKey);
				minimumInfrastructureLevel.push_back(std::pair<IDType<InfrastructureType>, double>(id, amount));
			}
			recipiesFile.closeGroup(); //b7

			std::vector<std::pair<IDType<InfrastructureType>, double>> maximumInfrastructureLevel;
			recipiesFile.openGroup("maximum infrastructure level"); //b8
			for (auto amountKey : recipiesFile.getStringKeys())
			{
				double amount = getDouble(recipiesFile, amountKey);
				IDType<InfrastructureType> id = infrastructureTypeLoader.getID(amountKey);
				maximumInfrastructureLevel.push_back(std::pair<IDType<InfrastructureType>, double>(id, amount));
			}
			recipiesFile.closeGroup(); //b8

			recipiesFile.closeGroup(); //a1


			IDType<Recipe> id(freeID++);
			addRecipe(recipiesKey, new Recipe(id, neededResources, yieldedResources, minimumTechLevel, maximumTechLevel, neededTechs, forbiddenTechs, minimumInfrastructureLevel, maximumInfrastructureLevel));

		}

	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	IDType<Recipe> RecipeLoader::getID(std::string stringID) const
	{
		return stringIDToID.at(stringID);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::string RecipeLoader::getStringID(IDType<Recipe> id) const
	{
		return idToStringID[id.getID()];
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Recipe *RecipeLoader::getRecipe(IDType<Recipe> id) const
	{
		return idToRecipe[id.getID()];
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	int RecipeLoader::getAmountOfRecipies() const
	{
		return (int)idToRecipe.size();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}