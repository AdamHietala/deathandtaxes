#include "TechTreeLoader.h"
#include "Exceptions.h"
#include "DataFileIOFunctions.h"
#include "FileStructure.h"
#include "StringManipulation.h"
#include "ModuleLoader.h"
namespace dat
{
	void TechTreeLoader::addTechnology(std::string stringID, Technology *technology)
	{
		testException(technologyStringIDToID.count(stringID) == 0, KeyAlreadyExistsException("The technology with string id \"" + stringID + "\" was added twice."));
		assert(technologyStringIDToID.size() == technology->getID().getID());
		technologyStringIDToID[stringID] = technology->getID();
		iDToTechnology.push_back(technology);
		technologyIDToStringID.push_back(stringID);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void TechTreeLoader::addTechTypeID(std::string stringID, IDType<TechType> id)
	{
		testException(techTypeStringIDToID.count(stringID) == 0, KeyAlreadyExistsException("The techType with string id \"" + stringID + "\" was added twice."));
		assert(techTypeStringIDToID.size() == id.getID());
		techTypeStringIDToID[stringID] = id;
		techTypeIDToStringID.push_back(stringID);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void TechTreeLoader::loadTechTypes(ModuleLoader &moduleLoader)
	{
		DataFileIO &techTypesFile = moduleLoader.loadDataFileIO(filestructure::TECH_TYPE_FOLDER_PATH);

		int freeID = 0;
		for (auto techTypeKey : sortStrings(techTypesFile.getStringKeys()))//Sort them to make sure the number id's are always the same so that they can be sent between different clients without problem.
		{
			if (getBoolean(techTypesFile, techTypeKey))
			{
				addTechTypeID(techTypeKey, IDType<TechType>(freeID++));
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void TechTreeLoader::loadTechnologies(ModuleLoader &moduleLoader)
	{
		DataFileIO &technologyFile = moduleLoader.loadDataFileIO(filestructure::TECHNOLOGIES_FOLDER_PATH);

		std::vector<std::string> technologyKeys = sortStrings(technologyFile.getGroupKeys());//Sort them to make sure the number id's are always the same so that they can be sent between different clients without problem.
		int freeID = 0;
		for (auto technologyKey : technologyKeys)
		{

			addTechnology(technologyKey, new Technology(IDType<Technology>(freeID++), getNrOfTechTypes()));

		}
		for (auto technologyKey : technologyKeys)
		{
			Technology *tech = getTechnology(getTechnologyID(technologyKey));


			technologyFile.openGroup(technologyKey); //a1

			technologyFile.openGroup("needed tech levels");//b1
			for (auto techType : technologyFile.getStringKeys())
			{
				double level = getDouble(technologyFile, techType);
				testException(level >= 0, NegativeNeedException("The technology \"" + technologyKey + "\" has not a positive tech need of \"" + techType + "\"."));
				tech->addTechTypeLevelNeed(getTechTypeID(techType), level);
			}
			technologyFile.closeGroup();//b1

			technologyFile.openGroup("needed technologies");//b2
			for (auto technology : technologyFile.getStringKeys())
			{
				if (getBoolean(technologyFile, technology))
				{
					tech->addTechnologyDependency(getTechnologyID(technology));
				}
			}

			technologyFile.closeGroup();//b2	


			technologyFile.closeGroup();//a1
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	TechTreeLoader::TechTreeLoader()
	{
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	TechTreeLoader::~TechTreeLoader()
	{
		for (auto tech : iDToTechnology)
		{
			delete tech;
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void TechTreeLoader::load(ModuleLoader &moduleLoader)
	{
		LoadsOnce::load();

		loadTechTypes(moduleLoader);
		loadTechnologies(moduleLoader);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	int TechTreeLoader::getNrOfTechTypes() const
	{
		return (int)techTypeStringIDToID.size();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	int TechTreeLoader::getNrOfTechnologies() const
	{
		return (int)iDToTechnology.size();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Technology* TechTreeLoader::getTechnology(IDType<Technology> techID) const
	{
		return iDToTechnology[techID.getID()];
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	IDType<TechType> TechTreeLoader::getTechTypeID(std::string stringID) const
	{
		return techTypeStringIDToID.at(stringID);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::string TechTreeLoader::getTechTypeStringID(IDType<TechType> id) const
	{
		return techTypeIDToStringID[id.getID()];
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	IDType<Technology> TechTreeLoader::getTechnologyID(std::string stringID) const
	{
		return technologyStringIDToID.at(stringID);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::string TechTreeLoader::getTechnologyStringID(IDType<Technology> id) const
	{
		return technologyIDToStringID[id.getID()];
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}