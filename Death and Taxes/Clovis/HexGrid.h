#pragma once
#include "VectorXD.h"
#include <vector>
namespace dat
{
	namespace hex //using odd row offset
	{
		const double HORIZONTAL_OFFSET_FACTOR = sqrt(3.0); //times the radius
		const double SHORT_RADIUS_FACTOR = HORIZONTAL_OFFSET_FACTOR * 0.5;
		const double VERTICAL_OFFSET_FACTOR = 1.5f;//times the radius
		const int NR_OF_DIRECTIONS = 6; //this is a hexagon grid, duh.
		enum class WrapType{ Flat, Cylinder, Toroid };
		enum class DiagonalDirection{ NorthWest = 0, North = 1, NorthEast = 2, SouthEast = 3, South = 4, SouthWest = 5 };
		enum class NeighbourDirection{ West = 0, NorthWest = 1, NorthEast = 2, East = 3, SouthEast = 4, SouthWest = 5 };
		DiagonalDirection getOpposite(DiagonalDirection direction); //returns the opposite direction of the one entered.
		NeighbourDirection getOpposite(NeighbourDirection direction); //returns the opposite direction of the one entered.
		NeighbourDirection getNeighbourDirection(int index);//returns the direction this index represents. Expects a value between 0 <= index < NR_OF_DIRECTIONS.
		DiagonalDirection getDiagonalDirection(int index);//returns the direction this index represents. Expects a value between 0 <= index < NR_OF_DIRECTIONS.
		NeighbourDirection getNextToNeighbourDirection(NeighbourDirection direction, bool left); //returns the neighbour left or right of the given direction.
		DiagonalDirection getNextToDiagonalDirection(DiagonalDirection direction, bool left); //returns the diagonal left or right of the given direction.
		NeighbourDirection getNeighbourNextToDiagonalDirection(DiagonalDirection direction, bool left); //returns the neighbour left or right of the given direction.
		Vector2D<int> wrapCoord(Vector2D<int> coord, Vector2D<int> worldDimensions, WrapType wrapType); //wraps the coord around according to the world's wrap type.
		Vector2D<double> wrapCoord(Vector2D<double> coord, Vector2D<int> worldDimensions, WrapType wrapType); //wraps the coord around according to the world's wrap type.
		std::vector<Vector2D<int>> getNeighbours(Vector2D<int> coord, Vector2D<int> worldDimensions, WrapType wrapType); //Gives the coordinate list for this tile's neighbours. The list will be shorter than 6 if the tile is at an edge or corner.
		Vector2D<int> getNeighbour(Vector2D<int> coord, NeighbourDirection direction);//returns the coordinate that is in a particular direction of 'coord'. Does not wrap coord. It is not certain this coord is within the map.
		Vector2D<int> getNeighbour(Vector2D<int> coord, NeighbourDirection direction, Vector2D<int> worldDimensions, WrapType wrapType);//returns the neighbouring coordinate that is in a particular direction of 'coord'. Does wrap coord, but it is not certain this coord is within the map.
		Vector2D<int> getDiagonal(Vector2D<int> coord, DiagonalDirection direction);//returns the coordinate that is in a particular direction of 'coord'. Does not wrap coord. It is not certain this coord is within the map.
		Vector2D<int> getDiagonal(Vector2D<int> coord, DiagonalDirection direction, Vector2D<int> worldDimensions, WrapType wrapType);//returns the neighbouring coordinate that is in a particular direction of 'coord'. Does wrap coord, but it is not certain this coord is within the map.
		bool withinMap(Vector2D<int> coord, NeighbourDirection direction, Vector2D<int> worldDimensions, WrapType wrapType);//returns true if a neighbour exists in that direction.
		bool withinMap(Vector2D<int> coord, Vector2D<int> worldDimensions, WrapType wrapType);//returns true if the coordinate is within the map, after first wrapping coordinate 'coord'. 
		bool withinMap(Vector2D<int> coord, Vector2D<int> worldDimensions);//returns true if the coordinate is within the map, without first wrapping the coordinate.
		Vector3D<int> roundHex(Vector3D<double> cubeCoord); //rounds this hex coordinate properly so that x + y + z = 0 still applies.
		Vector2D<int> roundHex(Vector2D<double> hexCoord);//rounds this hex coordinate properly so that x + y + z = 0 still applies for the cube version of this coordinate.
		Vector2D<double> hexCoordToCoord(Vector2D<int> hexCoord, double hexRadius);//maps the hex to a global coordinate.
		Vector2D<double> hexCoordToCoord(Vector2D<double> hexCoord, double hexRadius);//maps the hex to a global coordinate.
		Vector2D<int> coordToHexCoord(Vector2D<double> coord, double hexRadius); //maps the global coordinate to a hex.
		Vector2D<double> coordToDecimalHexCoord(Vector2D<double> coord, double hexRadius); //maps the global coordinate to a hexcoord without rounding.
		Vector3D<double> hexToCube(Vector2D<double> coord); //converts from 2d to 3d coordinate system.
		Vector2D<double> cubeToHex(Vector3D<double> coord); //converts from 3d to 2d coordinate system.
		double getAngle(NeighbourDirection neighbourDirection); //returns the angle between straight east and this direction.
		double getAngle(DiagonalDirection diagonalDirection); //returns the angle between straight east and this direction.
	}
}