#include "Ideology.h"
#include "GeneralFunctions.h"
#include <assert.h>
namespace dat
{

	Ideology::Ideology(double economic, double governmental, double personal)
	{
		Ideology::economic = economic;
		Ideology::governmental = governmental;
		Ideology::personal = personal;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Ideology::Ideology()
	{
		economic = 0.0;
		governmental = 0.0;
		personal = 0.0;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Ideology::~Ideology()
	{
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	double Ideology::getSimilarity(Ideology &ideology) const
	{
		double distance = 0.0;//manhattan distance since they don't actually have much to do with each other, meaning you can't take a shortcut and go diagonally
		distance += abs(ideology.economic - economic);
		distance += abs(ideology.governmental - governmental);
		distance += abs(ideology.personal - personal);
		double similarity = 1.0 - (distance / 6.0);//divide by 3.0 to get an avarage and by 2.0 to have the range go from 0.0 to 1.0 for each individual scale.
		assert(similarity >= 0.0 && similarity <= 1.0);
		return similarity; 
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void Ideology::setIdeologyStance(double newStance, IdeologyScale ideologyScale)
	{
		assert(newStance >= -1.0 && newStance <= 1.0);
		switch (ideologyScale)
		{
		case IdeologyScale::Economic:
			economic = newStance;
			break;
		case IdeologyScale::Governmental:
			governmental = newStance;
			break;
		case IdeologyScale::Personal:
			personal = newStance;
			break;
		default:
			assert(false);//there are no other alternatives.
			return;
		}

	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void Ideology::changeIdeologyStance(double change, IdeologyScale ideologyScale)
	{
		double *scale;
		switch (ideologyScale)
		{
		case IdeologyScale::Economic:
			scale = &economic;
			break;
		case IdeologyScale::Governmental:
			scale = &governmental;
			break;
		case IdeologyScale::Personal:
			scale = &personal;
			break;
		default:
			assert(false);//there are no other alternatives.
			return;
		}

		(*scale) += change;
		if ((*scale) < -1.0)
		{
			(*scale) = -1.0;
		}
		else if ((*scale) > 1.0)
		{
			(*scale) = 1.0;
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	double Ideology::getStance(IdeologyScale ideologyScale) const
	{
		switch (ideologyScale)
		{
		case IdeologyScale::Economic:
			return economic;
		case IdeologyScale::Governmental:
			return governmental;
		case IdeologyScale::Personal:
			return personal;
		default:
			assert(false);//there are no other alternatives.
			return 0.0;
		}
	}
}