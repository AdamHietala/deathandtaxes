#pragma once
namespace dat
{
	class RNGen
	{
	private:
		int currentNum;
		const unsigned factor, addedNumber, divisor;
	public:
		RNGen(unsigned factor, unsigned addedNumber, unsigned divisor, unsigned seed = 0);
		~RNGen();
		void setSeed(unsigned seed);
		void addSeedNumber(unsigned seed);
		int getNextNumber();//a positive number is returned
	};

}