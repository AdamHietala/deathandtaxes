#pragma once
#include <SFML\System.hpp>
#include "LoadsOnce.h"
#include "IDType.h"
#include <map>
#include <vector>
namespace dat
{
	class Animation;
	class ModuleLoader;
	class TextureLoader;
	class AnimationLoader : public LoadsOnce
	{
	private:
		std::map<std::string, IDType<Animation>> stringIDToID; //Maps the animation string ID to the animationID
		std::vector<Animation *> iDToAnimation;
		std::vector<std::string> idToStringID;
		void addAnimation(std::string stringID, Animation *animation); //Adds the animation to the datastructure.
	public:
		AnimationLoader();
		~AnimationLoader();
		void load(ModuleLoader &moduleLoader, TextureLoader &textureLoader);
		IDType<Animation> getID(std::string stringID) const; //Returns the animation ID.
		Animation *getAnimation(IDType<Animation> id) const; //Returns a reference of the loaded animation with the particular ID.
		std::string getStringID(IDType<Animation> id) const; //Returns the stringID so that it can be referred to in a savefile.
	};
}