#pragma once
#include "InputEventHandler.h"
#include "SFML_Renderer.h"
#include "GUI.h"
#include "ModuleOrder.h"
#include "ModuleLoader.h"
#include "TextureLoader.h"
#include "FontLoader.h"
#include "Localisation.h"
#include "Options.h"
#include "ThreadScheduler.h"
#include "GameLoader.h"
#include "AnimationLoader.h"
#include "TerrainTypeLoader.h"
#include "WorldDrawer.h"
#include "GameWorld.h"
#include "TilesetLoader.h"
#include "Camera.h"
#include "ResourceTypeLoader.h"
#include "TechTreeLoader.h"
#include "PopTypeLoader.h"
#include "RecipeLoader.h"
#include "InfrastructureTypeLoader.h"
#include "CitySpriteLoader.h"
#include "PlayerControlState.h"
#include "TypeImageLoader.h"
#include "Defines.h"
namespace dat
{
	class GameMain
	{
	private:
		bool programRunning;
	public:
		sf::RenderWindow renderWindow;
		InputEventHandler inputEventHandler;
		GUI gui;
		SFML_Renderer renderer;
		ModuleOrder moduleOrder;
		ModuleLoader moduleLoader;
		Defines defines;
		TextureLoader textureLoader;
		FontLoader fontLoader;
		Localisation localisation;
		Options options;
		ThreadScheduler threadScheduler;
		AnimationLoader animationLoader;
		TerrainTypeLoader terrainTypeLoader;
		WorldDrawer worldDrawer;
		GameWorld gameWorld;
		TilesetLoader tilesetLoader;
		ResourceTypeLoader resourceTypeloader;
		TechTreeLoader techTreeLoader;
		PopTypeLoader popTypeLoader;
		RecipeLoader recipeLoader;
		InfrastructureTypeLoader infrastructureTypeLoader;
		CitySpriteLoader citySpriteLoader;
		TypeImageLoader typeImageLoader;

		PlayerControlState playerControlState;
		Camera camera;
		GameMain();
		~GameMain();
		void run();


		void closeGame(); //Will close the game after finishing this frame.
	};
}