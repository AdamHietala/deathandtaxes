#pragma once
#include "ResourceTypeLoader.h"
#include <vector>
#include "IDType.h"
namespace dat 
{


	class PopType;
	class ResourcePack //contains the amount of different resources this pop/city has
	{
	private:
		ResourceTypeLoader const &resourceTypeLoader;
		std::vector<double> resourceAmount, incomingResources, prices, upForSale; //negative values on upForSale means they seek to buy that much. Negative values on the rest are not allowed.
		std::vector<int> serviceIndices;//a list of all the indices of 'resourceAmount' that are services
	public:
		ResourcePack(ResourceTypeLoader const &resourceTypeLoader);
		~ResourcePack();

		void transferFrom(ResourcePack &from, IDType<ResourceType> resourceType, double amount); //Transfers amount amount of resources from from to this. A negative amount is not allowed.
		void transferFromSeller(ResourcePack &seller, IDType<ResourceType> resourceType, double amount, double price, bool buyerIsMerchant = false); //Transfers amount amount of resources from seller to this. A negative amount is not allowed. The removal of the resources from from is instant while the incoming resources are delayed until updateDelayedResources is called. Also removes the resources sent from seller's up for sale.

		void addResourceAmountDelayed(IDType<ResourceType> resourceType, double amount); //used when trading, so that resources don't travel several cities in one turn.
		void updateDelayedResources(); //adds all the resources in 'incomingResources' to 'resourceAmount' and sets 'incomingResources' to zero.

		void setResourceAmount(IDType<ResourceType> resourceType, double amount); //sets the amount to a certain number.
		void addResourceAmount(IDType<ResourceType> resourceType, double amount);//if amount is negative the user must have checked that there is this much amount to take away
		double getResourceAmount(IDType<ResourceType> resourceType) const; //returns the resource amount of this type.
		void deleteServiceResources();//sets all service resources to 0.0 //debug this function is currently never used.

		void setPrice(IDType<ResourceType> resourceType, double price);
		double getPrice(IDType<ResourceType> resourceType) const;

		void setUpForSale(IDType<ResourceType> resourceType, double amount);//negative values means they seek to buy that much.
		void addUpForSale(IDType<ResourceType> resourceType, double amount);//used when a pop has bought or been bought from.
		double getUpForSale(IDType<ResourceType> resourceType) const;

		int getNrOfResourceTypes() const;

	};

}