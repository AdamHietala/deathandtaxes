#pragma once
#include "ScrollableList.h"
#include "Shapes.h"
namespace dat
{
	class Pop;
	class PlayerControlState;
	class TypeImageLoader;
	class Localisation;
	class PopTypeLoader;
	class Animation;
	class PopScrollableList : public ScrollableList
	{
	private:
		class PopInfo : public ScrollableListItem
		{
		private:
			PopScrollableList &popScrollableList;
			Pop &pop;
		public:
			PopInfo(PopScrollableList &popScrollableList, Pop &pop);
			virtual ~PopInfo() override;
			virtual void draw(sf::RenderTarget &renderTarget, sf::Time time, bool respectAlpha = false) override;
			virtual void mouseButtonPressed(bool &eventClaimed, Vector2D<int> mousePos, sf::Mouse::Button button) override;
		};
		PopTypeLoader &popTypeLoader;
		PlayerControlState &playerControlState;
		TypeImageLoader &typeImageLoader;
		Localisation &localisation;


		Rectangle<double> professionImageBox, professionBox, populationBox, politicsBox, economicPoliticsBox, governmentalPoliticsBox, personalPoliticsBox;

		virtual bool menuOpen() const override;
		AnimationInstance *selectionOverlay;
	public:
		PopScrollableList(Localisation &localisation, PopTypeLoader &popTypeLoader, PlayerControlState &playerControlState, TypeImageLoader &typeImageLoader);
		virtual ~PopScrollableList() override;
		virtual void draw(sf::RenderTarget &renderTarget, sf::Time time, bool respectAlpha = false) override;
		virtual void drawHeader(sf::RenderTarget &renderTarget, sf::Time time, bool respectAlpha) override;

		void setProfessionImageBox(Rectangle<double> box); //sets the position and scale of the profession image relative to the space alotted to the scrollable list.
		void setProfessionBox(Rectangle<double> box); //sets the position and scale of the profession post relative to the space alotted to the scrollable list.
		void setPopulationBox(Rectangle<double> box); //sets the position and scale of the population post relative to the space alotted to the scrollable list.
		void setPoliticsBox(Rectangle<double> box); //sets the position and scale of the politics post relative to the space alotted to the scrollable list.
		void setEconomicPoliticsBox(Rectangle<double> box); //sets the position and scale of the economic politics post relative to the space alotted to the politics post. Needs the politicsbox to be set first.
		void setGovernmentalPoliticsBox(Rectangle<double> box); //sets the position and scale of the governmental politics post relative to the space alotted to the politics post. Needs the politicsbox to be set first.
		void setPersonalPoliticsBox(Rectangle<double> box); //sets the position and scale of the personal politics post relative to the space alotted to the politics post. Needs the politicsbox to be set first.

		void setAnimations(Animation &newBackground, Animation &scrollBar, Animation &scrollIndicator, Animation &selectionOverlay); //Sets the animations of the list.
	};
}