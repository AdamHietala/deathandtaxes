#include "ResourceTypeImageLoader.h"
#include <assert.h>
#include "AnimationLoader.h"
#include "ResourceTypeLoader.h"
#include "DataFileIO.h"
#include "FileStructure.h"
#include "ModuleLoader.h"
namespace dat
{
	ResourceTypeImageLoader::ResourceTypeImageLoader()
	{
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	ResourceTypeImageLoader::~ResourceTypeImageLoader()
	{
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void ResourceTypeImageLoader::load(ModuleLoader &moduleLoader, AnimationLoader const &animationLoader, ResourceTypeLoader const &resourceTypeLoader)
	{
		LoadsOnce::load();
		assert(animationLoader.hasLoaded());
		assert(resourceTypeLoader.hasLoaded());



		idToAnimation.resize(resourceTypeLoader.getNumberOfResourceTypes(), nullptr);

		DataFileIO &resourceTypeImagesFile = moduleLoader.loadDataFileIO(filestructure::RESOURCE_TYPES_IMAGES_FOLDER_PATH);
		for (auto resourceTypeKey : resourceTypeImagesFile.getStringKeys())
		{
			std::string animationString = resourceTypeImagesFile.getString(resourceTypeKey);
			Animation *animation = animationLoader.getAnimation(animationLoader.getID(animationString));
			IDType<ResourceType> id = resourceTypeLoader.getID(resourceTypeKey);
			idToAnimation[id.getID()] = animation;
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Animation *ResourceTypeImageLoader::getAnimation(IDType<ResourceType> id) const
	{
		assert(idToAnimation[id.getID()] != nullptr);
		return idToAnimation[id.getID()];
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}