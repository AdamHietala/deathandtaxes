#include "Tileset.h"
#include <assert.h>
namespace dat
{

	Tileset::Tileset(IDType<Tileset> id, IDType<TerrainType> terrainTypeID, int priority, std::vector<IDType<Animation>> baseTextures, std::vector<IDType<Animation>> firstMasks, std::vector<IDType<Animation>> secondMasks) : 
		priority(priority), baseTextures(baseTextures), firstMasks(firstMasks), secondMasks(secondMasks), id(id), terrainTypeID(terrainTypeID), nrOfVariations((int)baseTextures.size())
	{
		assert(baseTextures.size() == firstMasks.size() && baseTextures.size() == secondMasks.size());//runtime error checking should be done in the loader.
		assert(nrOfVariations > 0);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Tileset::~Tileset()
	{
	}
}