#include <cstdlib>
#include <iostream>
#include "GameMain.h"
#include <SFML\Graphics.hpp>

int main(int argc, char* argv[])
{
	try
	{
		dat::GameMain game;
		game.run();
	}
	catch (std::exception &ex)
	{
		std::cout << ex.what() << "\n";
		throw;
	}

	return EXIT_SUCCESS;
}