#include "ModuleOrder.h"
#include <assert.h>
#include "FileIO.h"
#include "StringManipulation.h"
#include "FileStructure.h"
namespace dat
{
	ModuleOrder::ModuleOrder()
	{

	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	ModuleOrder::~ModuleOrder()
	{

	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::vector<std::string> ModuleOrder::getLoadOrder() const
	{
		return loadOrder;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void ModuleOrder::setLoadorder(std::vector<std::string> newOrder)
	{
		loadOrder.clear();
		for (auto modName : newOrder)
		{
			loadOrder.push_back(filestructure::MOD_FOLDER_PATH + "\\" + modName);
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void ModuleOrder::load()
	{
		loadOrder.clear();
		for (auto modName : trimWhiteSpaces(getFileLines(filestructure::LOAD_ORDER_PATH)))
		{
			loadOrder.push_back(filestructure::MOD_FOLDER_PATH + "\\" + modName);
		}
		
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void ModuleOrder::save()
	{
		std::vector<std::string> mods;
		for (auto modPath : loadOrder)
		{
			mods.push_back(getSubString(modPath, filestructure::MOD_FOLDER_PATH.size() + 1, modPath.size()));
		}
		writeToFile(mods, filestructure::LOAD_ORDER_PATH);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}