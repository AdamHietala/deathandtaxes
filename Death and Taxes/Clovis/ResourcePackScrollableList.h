#pragma once
#include <functional>
#include "ScrollableList.h"
#include "IDType.h"
namespace dat 
{
	class Localisation;
	class PlayerControlState;
	class ResourcePack;
	class ResourceTypeLoader;
	class ResourceType;
	class TypeImageLoader;
	class ResourcePackScrollableList : public ScrollableList
	{
	private:
		class ResourceInfo : public ScrollableListItem
		{
		private:
		public:
			ResourceInfo(ResourcePackScrollableList &resourcePackScrollableList, IDType<ResourceType> resourceID);
			virtual ~ResourceInfo() override;
			virtual void draw(sf::RenderTarget &renderTarget, sf::Time time, bool respectAlpha = false) override;
			ResourcePackScrollableList &resourcePackScrollableList;
			const IDType<ResourceType> resourceID;
		};
		TypeImageLoader &typeImageLoader;
		Localisation &localisation;
		PlayerControlState &playerControlState;
		ResourceTypeLoader &resourceTypeLoader;
		virtual bool menuOpen() const override;
		const std::function<ResourcePack*(void)> getResourcePack; //The function must return the relevant ResourcePack or a nullptr if the menu should not be shown.
		Rectangle<double> resourceTypeImageBox, resourceTypeBox, amountBox, upForSaleBox, priceBox;
	public:
		ResourcePackScrollableList(Localisation &localisation, TypeImageLoader &typeImageLoader, ResourceTypeLoader &resourceTypeLoader, PlayerControlState &playerControlState, std::function<ResourcePack*(void)> getResourcePack);
		virtual ~ResourcePackScrollableList() override;
		virtual void draw(sf::RenderTarget &renderTarget, sf::Time time, bool respectAlpha = false) override;
		virtual void drawHeader(sf::RenderTarget &renderTarget, sf::Time time, bool respectAlpha) override;


		void setResourceTypeImageBox(Rectangle<double> box); //sets the position and scale of the resource type image relative to the space alotted to the scrollable list.
		void setResourceTypeBox(Rectangle<double> box); //sets the position and scale of the resource name post relative to the space alotted to the scrollable list.
		void setAmountBox(Rectangle<double> box); //sets the position and scale of the amount post relative to the space alotted to the scrollable list.
		void setPriceBox(Rectangle<double> box); //sets the position and scale of the price post relative to the space alotted to the scrollable list.
		void setUpForSaleBox(Rectangle<double> box); //sets the position and scale of the up for sale post relative to the space alotted to the scrollable list.
	
	};
}