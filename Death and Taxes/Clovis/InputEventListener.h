#pragma once
#include <SFML\Graphics.hpp>
namespace dat
{
	class InputEventListener
	{
	private:
	protected:
		InputEventListener();
	public:
		~InputEventListener();
		virtual void eventHappened(sf::Event &evnt, bool &eventClaimed) = 0; //set 'eventClaimed' to true if the eventListener decides the event was ment for them. 'eventClaimed' also tells the eventlistener whether another eventlistener has already claimed ownership of the event. The event listener still gets to know about the event since it might be important anyway.
	};

}