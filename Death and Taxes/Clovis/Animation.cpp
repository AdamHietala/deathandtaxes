#include "Animation.h"
#include "Exceptions.h"
#include <SFML\Graphics.hpp>
namespace dat
{

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Animation::Animation(const IDType<Animation> id, int frameLength) : id(id), frameLength(frameLength)
	{
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Animation::~Animation()
	{

	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void Animation::addFrame(sf::Sprite sprite, Vector2D<float> positionCompensation)
	{
		testException(sprites.size() == 0 || (sprites[0].getLocalBounds().height == sprite.getLocalBounds().height && sprites[0].getLocalBounds().width == sprite.getLocalBounds().width), DifferentSizeException("The frames have different sizes."));
		

		sprites.push_back(sprite);

		positionComps.push_back(positionCompensation);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	int Animation::getFrameLength() const
	{
		return frameLength;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	sf::Sprite Animation::getSprite(int frameIndex) const
	{
		return sprites[frameIndex];
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Vector2D<float> Animation::getPositionCompensation(int frameIndex) const
	{
		return positionComps[frameIndex];
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	int Animation::getNumberOfFrames() const
	{
		return (int) sprites.size();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	int Animation::getRevolutionTime() const
	{
		return getFrameLength()*getNumberOfFrames();
	}
}