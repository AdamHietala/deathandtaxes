#pragma once
namespace dat
{
	class Ideology//a package of political opinions, the scale goes between -1.0 and 1.0
	{
	private:
		double economic;//collectivist -1.0, individualist +1.0 //represents how money should be divided in society
		double governmental;//authoritarian -1.0, libertarian +1.0 //represents the meassures the government should be able to take to get something done
		double personal; //authoritarian -1.0, libertarian +1.0 //represents religion, drugs, etc
	public:
		enum class IdeologyScale { Economic, Governmental, Personal };
		Ideology(double economic, double governmental, double personal);
		Ideology();
		~Ideology();

		double getSimilarity(Ideology &ideology) const;//tells you how similar these two ideologies are.
		void setIdeologyStance(double newStance, IdeologyScale ideologyScale);//sets the ideology stance on one scale.
		void changeIdeologyStance(double change, IdeologyScale ideologyScale); //changes the specified ideology in a direction, if the ideology goes above 1.0 or below -1.0 they will be set to their maximum/minimum
		double getStance(IdeologyScale ideologyScale) const; //returns the position of this pop on a certain scale.
	};
}