#pragma once
#include "LoadsOnce.h"
#include <vector>
#include "SFML\Graphics.hpp"
#include "VectorXD.h"
#include <mutex>
namespace dat
{
	class GameMain;
	class GameLoader : public LoadsOnce
	{
	private:
		std::vector<sf::Texture *> backgrounds;
		void loadMe(GameMain &gameMain); //Loads the GameLoader itself
		void entertainUser(GameMain &gameMain); //Renders the progress of the loading to the user.
		sf::Text loadingText;
		std::mutex loadStateMutex;
		enum class LoadState{ Start, Defines, GUI, TerrainTypes, Animations, Tilesets, ResourceTypes, TechTree, PopTypes, Recipies, InfrastructureTypes, CitySprites, TypeImages, Done };
		LoadState loadState;
		sf::Sprite currentBackground;
		float textScale;
		Vector2D<float> textPosition;
		float changeImage;
	public:
		GameLoader();
		~GameLoader();
		void load(GameMain &gameMain); //Loads all of the game.
	};

}