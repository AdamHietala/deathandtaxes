#include "TechLevel.h"
#include "Technology.h"
#include "TechTreeLoader.h"
namespace dat
{

	TechLevel::TechLevel(TechTreeLoader &techTreeLoader) : techTreeLoader(techTreeLoader)
	{
		techTypeLevels.resize(techTreeLoader.getNrOfTechTypes(), 0.0);
		techUnlocked.resize(techTreeLoader.getNrOfTechnologies(), false);
		updateNextUnlockedTech();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	TechLevel::~TechLevel()
	{

	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void TechLevel::updateNextUnlockedTech()
	{
		if (nextUnlockedTechIsValid)
		{
			if (techTreeLoader.getTechnology(nextUnlockedTech)->isUnlockable(techTypeLevels, techUnlocked))
			{
				return;
			}
		}



		std::vector<IDType<Technology>> unlockableTechs;
		for (int i = 0; i < techTreeLoader.getNrOfTechTypes(); ++i)
		{
			Technology* tech = techTreeLoader.getTechnology(IDType<Technology>(i));
			if (tech->isUnlockable(techTypeLevels, techUnlocked))
			{
				unlockableTechs.push_back(tech->getID());
			}
		}
		if (unlockableTechs.size() > 0)
		{
			nextUnlockedTech = unlockableTechs[rand() % unlockableTechs.size()];
			nextUnlockedTechIsValid = true;
		}
		else
		{
			nextUnlockedTechIsValid = false;
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	bool TechLevel::isTechUnlocked(IDType<Technology> techId) const
	{
		return techUnlocked[techId.getID()];
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	bool TechLevel::hasUnlockableTech() const
	{
		return nextUnlockedTechIsValid;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void TechLevel::unlockNextTech()
	{
		assert(hasUnlockableTech());
		assert(techUnlocked[nextUnlockedTech.getID()] == false);

		techUnlocked[nextUnlockedTech.getID()] = true;
		updateNextUnlockedTech();

	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	double TechLevel::getTechTypeLevel(IDType<TechType> techTypeId) const
	{
		return techTypeLevels[techTypeId.getID()];
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void TechLevel::changeTechLevel(IDType<TechType> techType, double amount)
	{
		techTypeLevels[techType.getID()] += amount;
		if (amount < 0.0)
		{
			bool moreCheckingNeeded;
			do
			{
				moreCheckingNeeded = false;

				for (int i = 0; i < (int)techUnlocked.size(); ++i)
				{
					if (techUnlocked[i] == true && techTreeLoader.getTechnology(i)->isUnlockable(techTypeLevels, techUnlocked) == false)
					{
						techUnlocked[i] = false;
						moreCheckingNeeded = true;
						break;
					}
				}
			} while (moreCheckingNeeded);

			updateNextUnlockedTech();
		}
	}
}