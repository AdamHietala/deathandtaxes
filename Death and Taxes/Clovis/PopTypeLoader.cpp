#include "PopTypeLoader.h"
#include "Exceptions.h"
#include "FileStructure.h"
#include "DataFileIOFunctions.h"
#include "StringManipulation.h"
#include <assert.h>
#include "PopType.h"
#include "TechType.h"
#include "InfrastructureTypeLoader.h"
#include "ModuleLoader.h"
#include "ResourceTypeLoader.h"
#include "TechTreeLoader.h"
#include "RecipeLoader.h"
namespace dat
{
	void PopTypeLoader::addPopType(PopType *popType)
	{
		assert(popType->id.getID() == (int)iDToPopType.size());//this function is called on in the same order as addPopTypeID.
		iDToPopType.push_back(popType);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void PopTypeLoader::addPopTypeID(std::string stringID, IDType<PopType> id)
	{

		testException(stringIDToID.count(stringID) == 0, KeyAlreadyExistsException("The type with string id \"" + stringID + "\" was added twice."));
		assert(stringIDToID.size() == id.getID());
		stringIDToID[stringID] = id;
		idToStringID.push_back(stringID);

	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	PopTypeLoader::PopTypeLoader()
	{
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	PopTypeLoader::~PopTypeLoader()
	{
		for (auto popType : iDToPopType)
		{
			delete popType;
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void PopTypeLoader::load(ModuleLoader &moduleLoader, ResourceTypeLoader const &resourceTypeloader, TechTreeLoader const &techTreeLoader, RecipeLoader const &recipeLoader, InfrastructureTypeLoader const &infrastructureTypeLoader)
	{ 
		LoadsOnce::load();
		assert(resourceTypeloader.hasLoaded());
		assert(techTreeLoader.hasLoaded());
		assert(recipeLoader.hasLoaded());
		assert(infrastructureTypeLoader.hasLoaded());


		DataFileIO &popTypesFile = moduleLoader.loadDataFileIO(filestructure::POP_TYPES_FOLDER_PATH);

		const std::vector<std::string> popTypeKeys = sortStrings(popTypesFile.getGroupKeys());//Sort them to make sure the number id's are always the same so that they can be sent between different clients without problem.

		int freeID = 0;
		for (auto popTypeKey : popTypeKeys)
		{
			IDType<PopType> id(freeID++);
			addPopTypeID(popTypeKey, id);
		}

		freeID = 0;//the ids will be dealt in the same order as before.
		for (auto popTypeKey : popTypeKeys) 
		{

			popTypesFile.openGroup(popTypeKey); //a1
			//merchant
			bool merchant = getBoolean(popTypesFile, "merchant");

			//needs
			std::vector<std::pair<IDType<ResourceType>,double>> lifeNeeds, basicNeeds, everyDayNeeds, luxuryNeeds;

			popTypesFile.openGroup("needs"); //b1

			popTypesFile.openGroup("life needs"); //c1
			for (auto resourceType : popTypesFile.getStringKeys())
			{
				double neededAmount = getDouble(popTypesFile, resourceType);
				testException(neededAmount >= 0, NegativeNeedException("The poptype \"" + popTypeKey + "\" has a negative need of \"" + resourceType + "\"."));
				IDType<ResourceType> resourceId = resourceTypeloader.getID(resourceType);
				lifeNeeds.push_back(std::pair<IDType<ResourceType>, double>(resourceId, neededAmount));
			}
			popTypesFile.closeGroup(); //c1

			popTypesFile.openGroup("basic needs"); //c2
			for (auto resourceType : popTypesFile.getStringKeys())
			{
				double neededAmount = getDouble(popTypesFile, resourceType);
				testException(neededAmount >= 0, NegativeNeedException("The poptype \"" + popTypeKey + "\" has a negative need of \"" + resourceType + "\"."));
				IDType<ResourceType> resourceId = resourceTypeloader.getID(resourceType);
				basicNeeds.push_back(std::pair<IDType<ResourceType>, double>(resourceId, neededAmount));
			}
			popTypesFile.closeGroup(); //c2

			popTypesFile.openGroup("every day needs"); //c3
			for (auto resourceType : popTypesFile.getStringKeys())
			{
				double neededAmount = getDouble(popTypesFile, resourceType);
				testException(neededAmount >= 0, NegativeNeedException("The poptype \"" + popTypeKey + "\" has a negative need of \"" + resourceType + "\"."));
				IDType<ResourceType> resourceId = resourceTypeloader.getID(resourceType);
				everyDayNeeds.push_back(std::pair<IDType<ResourceType>, double>(resourceId, neededAmount));
			}
			popTypesFile.closeGroup(); //c3

			popTypesFile.openGroup("luxury needs"); //c4
			for (auto resourceType : popTypesFile.getStringKeys())
			{
				double neededAmount = getDouble(popTypesFile, resourceType);
				testException(neededAmount >= 0, NegativeNeedException("The poptype \"" + popTypeKey + "\" has a negative need of \"" + resourceType + "\"."));
				IDType<ResourceType> resourceId = resourceTypeloader.getID(resourceType);
				luxuryNeeds.push_back(std::pair<IDType<ResourceType>, double>(resourceId, neededAmount));
			}
			popTypesFile.closeGroup(); //c4
			
			popTypesFile.closeGroup(); //b1


			//requirements
			std::vector<std::pair<IDType<InfrastructureType>, double>> infrastructureTypeLevelsNeeded;
			std::vector<std::pair<IDType<TechType>, double>> techTypeLevelsNeeded;
			std::vector<IDType<Technology>> technologiesNeeded;

			popTypesFile.openGroup("requirements"); //b2

			popTypesFile.openGroup("infrastructure levels"); //c5
			for (auto infrastructureType : popTypesFile.getStringKeys())
			{
				double neededAmount = getDouble(popTypesFile, infrastructureType);
				testException(neededAmount >= 0, NegativeNeedException("The poptype \"" + popTypeKey + "\" has a negative infrastructure need of \"" + infrastructureType + "\"."));
				IDType<InfrastructureType> infrastructureTypeID = infrastructureTypeLoader.getID(infrastructureType);
				infrastructureTypeLevelsNeeded.push_back(std::pair<IDType<InfrastructureType>, double>(infrastructureTypeID, neededAmount));
			}
			popTypesFile.closeGroup(); //c5

			popTypesFile.openGroup("tech levels"); //c6
			for (auto techType : popTypesFile.getStringKeys())
			{
				double neededAmount = getDouble(popTypesFile, techType);
				testException(neededAmount >= 0, NegativeNeedException("The poptype \"" + popTypeKey + "\" has a negative tech need of \"" + techType + "\"."));
				IDType<TechType> techTypeId = techTreeLoader.getTechTypeID(techType);
				techTypeLevelsNeeded.push_back(std::pair<IDType<TechType>, double>(techTypeId, neededAmount));
			}
			popTypesFile.closeGroup(); //c6

			popTypesFile.openGroup("technologies"); //c7
			for (auto technology : popTypesFile.getStringKeys())
			{
				if (getBoolean(popTypesFile, technology))
				{
					IDType<Technology> technologyID = techTreeLoader.getTechnologyID(technology);
					technologiesNeeded.push_back(technologyID);
				}
			}
			popTypesFile.closeGroup(); //c7

			popTypesFile.closeGroup(); //b2



			//forbidden
			std::vector<std::pair<IDType<InfrastructureType>, double>> infrastructureTypeLevelsForbidden;
			std::vector<std::pair<IDType<TechType>, double>> techTypeLevelsForbidden;
			std::vector<IDType<Technology>> technologiesForbidden;
			popTypesFile.openGroup("forbidden"); //b3

			popTypesFile.openGroup("infrastructure levels"); //c8
			for (auto infrastructureType : popTypesFile.getStringKeys())
			{
				double forbiddenAmount = getDouble(popTypesFile, infrastructureType);
				testException(forbiddenAmount >= 0, NegativeNeedException("The poptype \"" + popTypeKey + "\" has a negative infrastructure prohibition of \"" + infrastructureType + "\"."));
				IDType<InfrastructureType> infrastructureTypeID = infrastructureTypeLoader.getID(infrastructureType);
				infrastructureTypeLevelsForbidden.push_back(std::pair<IDType<InfrastructureType>, double>(infrastructureTypeID, forbiddenAmount));
			}
			popTypesFile.closeGroup(); //c8

			popTypesFile.openGroup("tech levels"); //c9
			for (auto techType : popTypesFile.getStringKeys())
			{
				double forbiddenAmount = getDouble(popTypesFile, techType);
				testException(forbiddenAmount >= 0, NegativeNeedException("The poptype \"" + popTypeKey + "\" has a negative tech prohibition of \"" + techType + "\"."));
				IDType<TechType> techTypeId = techTreeLoader.getTechTypeID(techType);
				techTypeLevelsForbidden.push_back(std::pair<IDType<TechType>, double>(techTypeId, forbiddenAmount));
			}
			popTypesFile.closeGroup(); //c9

			popTypesFile.openGroup("technologies"); //c10
			for (auto technology : popTypesFile.getStringKeys())
			{
				if (getBoolean(popTypesFile, technology))
				{
					IDType<Technology> technologyID = techTreeLoader.getTechnologyID(technology);
					technologiesForbidden.push_back(technologyID);
				}
			}
			popTypesFile.closeGroup(); //c10

			popTypesFile.closeGroup(); //b3



			//promotions
			std::vector<IDType<PopType>> promotionPaths;
			popTypesFile.openGroup("promotion paths"); //b4
			for (auto popType : popTypesFile.getStringKeys())
			{
				if (getBoolean(popTypesFile, popType))
				{
					IDType<PopType> promotionPath = getID(popType);
					promotionPaths.push_back(promotionPath);
				}
			}
			popTypesFile.closeGroup(); //b4

			//demotions
			std::vector<IDType<PopType>> demotionPaths;
			popTypesFile.openGroup("demotion paths"); //b5
			for (auto popType : popTypesFile.getStringKeys())
			{
				if (getBoolean(popTypesFile, popType))
				{
					IDType<PopType> promotionPath = getID(popType);
					demotionPaths.push_back(promotionPath);
				}
			}
			popTypesFile.closeGroup(); //b5

			//recipies
			std::vector<IDType<Recipe>> recipies;
			popTypesFile.openGroup("recipies"); //b6
			for (auto recipeString : popTypesFile.getStringKeys())
			{
				if (getBoolean(popTypesFile, recipeString))
				{
					IDType<Recipe> recipe = recipeLoader.getID(recipeString);
					recipies.push_back(recipe);
				}
			}
			popTypesFile.closeGroup(); //b6

			//growth
			double noNeedsFulfilled, lifeNeedsFulfilled, basicNeedsFulfilled, everyDayNeedsFulfilled, luxuryNeedsFulfilled;
			popTypesFile.openGroup("growth");//b7
			noNeedsFulfilled = getDouble(popTypesFile, "no needs");
			lifeNeedsFulfilled = getDouble(popTypesFile, "life needs");
			basicNeedsFulfilled = getDouble(popTypesFile, "basic needs");
			everyDayNeedsFulfilled = getDouble(popTypesFile, "every day needs");
			luxuryNeedsFulfilled = getDouble(popTypesFile, "luxury needs");
			popTypesFile.closeGroup(); //b7

			popTypesFile.closeGroup(); //a1





			//make sure all higher needs also contain the needs of lower needs.
			std::vector<std::pair<IDType<ResourceType>, double>> *needListpairs[3][2] = { { &lifeNeeds, &basicNeeds }, { &basicNeeds, &everyDayNeeds }, { &everyDayNeeds, &luxuryNeeds } };
			for (auto needs : needListpairs)
			{
				auto lowerNeed = needs[0];
				auto higherNeed = needs[1];
				for (int i = 0; i < (int)lowerNeed->size(); ++i)
				{
					IDType<ResourceType> lowerType = lowerNeed->at(i).first;
					double lowerAmount = lowerNeed->at(i).second;
					bool found = false;
					for (int q = 0; q < (int)higherNeed->size(); ++q) //check if it is overwritten already by the higher need
					{
						IDType<ResourceType> higherType = higherNeed->at(q).first;
						double higherAmount = higherNeed->at(q).second;
						if (lowerType == higherType)
						{
							found = true;
							break;
						}
					}
					if (!found)
					{
						higherNeed->push_back(std::pair<IDType<ResourceType>, double>(lowerType, lowerAmount));
					}
				}
			}


			//remove all resources set to zero for some reason (an example of a sensible reason being overwriting a lower need)
			std::vector<std::pair<IDType<ResourceType>, double>> *needLists[] = { &lifeNeeds, &basicNeeds, &everyDayNeeds, &luxuryNeeds };
			for (auto needList : needLists)
			{
				for (int i = 0; i < (int)needList->size(); ++i)
				{
					double needAmount = needList->at(i).second;
					if (needAmount == 0.0)
					{
						needList->erase(needList->begin() + i);
						--i;
					}
				}
			}


			IDType<PopType> id(freeID++);
			assert(idToStringID[id.getID()] == popTypeKey);//the ids come in the same order as before.
			PopType *popType = new PopType(id, merchant, lifeNeeds, basicNeeds, everyDayNeeds, luxuryNeeds, infrastructureTypeLevelsNeeded, techTypeLevelsNeeded, technologiesNeeded, infrastructureTypeLevelsForbidden, techTypeLevelsForbidden, technologiesForbidden, promotionPaths, demotionPaths, recipies, noNeedsFulfilled, lifeNeedsFulfilled, basicNeedsFulfilled, everyDayNeedsFulfilled, luxuryNeedsFulfilled);
			addPopType(popType);


		}
		


	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	IDType<PopType> PopTypeLoader::getID(std::string stringID) const
	{
		return stringIDToID.at(stringID);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	const PopType &PopTypeLoader::getPopType(IDType<PopType> id) const
	{
		assert(id.getID() < (int)iDToPopType.size() && id.getID() >= 0);
		return *iDToPopType[id.getID()];
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::string PopTypeLoader::getStringID(IDType<PopType> id) const
	{
		return idToStringID[id.getID()];
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	int PopTypeLoader::getNrOfPopTypes() const
	{
		return (int)iDToPopType.size();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}