#include "HexGrid.h"
#include <assert.h>
#define _USE_MATH_DEFINES
#include <math.h>
namespace dat
{
	namespace hex
	{




		DiagonalDirection getOpposite(DiagonalDirection direction)
		{
			return (DiagonalDirection)(((int)direction + 3) % NR_OF_DIRECTIONS);
		}
	
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		NeighbourDirection getOpposite(NeighbourDirection direction)
		{
			return (NeighbourDirection)(((int)direction + 3) % NR_OF_DIRECTIONS);
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		NeighbourDirection getNeighbourDirection(int index)
		{
			assert(index >= 0 && index < NR_OF_DIRECTIONS);
			return (NeighbourDirection)index;
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		DiagonalDirection getDiagonalDirection(int index)
		{
			assert(index >= 0 && index < NR_OF_DIRECTIONS);
			return (DiagonalDirection)index;
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		NeighbourDirection getNextToNeighbourDirection(NeighbourDirection direction, bool left)
		{
			if (left)
			{
				return (NeighbourDirection)(((int)direction - 1 + NR_OF_DIRECTIONS) % NR_OF_DIRECTIONS);
			}
			else
			{
				return (NeighbourDirection)(((int)direction + 1) % NR_OF_DIRECTIONS);
			}
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		DiagonalDirection getNextToDiagonalDirection(DiagonalDirection direction, bool left)
		{
			if (left)
			{
				return (DiagonalDirection)(((int)direction - 1 + NR_OF_DIRECTIONS) % NR_OF_DIRECTIONS);
			}
			else
			{
				return (DiagonalDirection)(((int)direction + 1) % NR_OF_DIRECTIONS);
			}
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		NeighbourDirection getNeighbourNextToDiagonalDirection(DiagonalDirection direction, bool left)
		{
			if (left)
			{
				return (NeighbourDirection) direction;
			}
			else
			{
				return (NeighbourDirection)(((int)direction + 1) % NR_OF_DIRECTIONS);
			}
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		Vector2D<int> wrapCoord(Vector2D<int> coord, Vector2D<int> worldDimensions, WrapType wrapType)
		{

			switch (wrapType)
			{
			case WrapType::Toroid:
				assert(worldDimensions.y % 2 == 0);
				coord.y = coord.y % worldDimensions.y;
				if (coord.y < 0)
				{
					coord.y += worldDimensions.y;
				}

			case WrapType::Cylinder:
				coord.x = coord.x % worldDimensions.x;
				if (coord.x < 0)
				{
					coord.x += worldDimensions.x;
				}
			case WrapType::Flat:
				return coord;
			default:
				assert(false);//all enums are accounted for
				return coord;


			}
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		Vector2D<double> wrapCoord(Vector2D<double> coord, Vector2D<int> worldDimensions, WrapType wrapType)
		{
			Vector2D<int> roundedCoord = roundHex(coord);
			Vector2D<double> coordDecimals = coord - roundedCoord.typecast<double>();
			Vector2D<int> wrappedCoord = wrapCoord(roundedCoord, worldDimensions, wrapType);
			return wrappedCoord.typecast<double>() + coordDecimals;
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		std::vector<Vector2D<int>> getNeighbours(Vector2D<int> coord, Vector2D<int> worldDimensions, WrapType wrapType)
		{
			std::vector<Vector2D<int>> neighbours;
			const NeighbourDirection NEIGHBOURS_DIRECTIONS[6] = { NeighbourDirection::NorthEast, NeighbourDirection::East, NeighbourDirection::SouthEast, NeighbourDirection::SouthWest, NeighbourDirection::West, NeighbourDirection::NorthWest };
			for (auto direction : NEIGHBOURS_DIRECTIONS)
			{
				Vector2D<int> newCoord = wrapCoord(getNeighbour(coord, direction), worldDimensions, wrapType);
				if (withinMap(newCoord, worldDimensions))
				{
					neighbours.push_back(newCoord);
				}
			}
			return neighbours;
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		Vector2D<int> getNeighbour(Vector2D<int> coord, NeighbourDirection direction)
		{
			Vector2D<int> addedCoord;
			if ((coord.y & 1) == 0) //even
			{
				switch (direction)
				{
				case NeighbourDirection::NorthEast:
					addedCoord = Vector2D<int>(0, -1);
					break;

				case NeighbourDirection::East:
					addedCoord = Vector2D<int>(1, 0);	
					break;

				case NeighbourDirection::SouthEast:
					addedCoord = Vector2D<int>(0, 1);
					break;

				case NeighbourDirection::SouthWest:
					addedCoord = Vector2D<int>(-1, 1);
					break;

				case NeighbourDirection::West:
					addedCoord = Vector2D<int>(-1, 0);
					break;

				case NeighbourDirection::NorthWest:
					addedCoord = Vector2D<int>(-1, -1);
					break;
				}
			}
			else //odd
			{
				switch (direction)
				{
				case NeighbourDirection::NorthEast:
					addedCoord = Vector2D<int>(1, -1);
					break;

				case NeighbourDirection::East:
					addedCoord = Vector2D<int>(1, 0);
					break;

				case NeighbourDirection::SouthEast:
					addedCoord = Vector2D<int>(1, 1);
					break;

				case NeighbourDirection::SouthWest:
					addedCoord = Vector2D<int>(0, 1);
					break;

				case NeighbourDirection::West:
					addedCoord = Vector2D<int>(-1, 0);
					break;

				case NeighbourDirection::NorthWest:
					addedCoord = Vector2D<int>(0, -1);
					break;
				}
			}
			return coord + addedCoord;
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		Vector2D<int> getNeighbour(Vector2D<int> coord, NeighbourDirection direction, Vector2D<int> worldDimensions, WrapType wrapType)
		{
			Vector2D<int> newCoord = wrapCoord(getNeighbour(coord, direction), worldDimensions, wrapType);
			return newCoord;
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		Vector2D<int> getDiagonal(Vector2D<int> coord, DiagonalDirection direction)
		{
			switch (direction)
			{
			case DiagonalDirection::NorthWest:
				return getNeighbour(getNeighbour(coord, NeighbourDirection::NorthWest), NeighbourDirection::West);
			case DiagonalDirection::North:
				return getNeighbour(getNeighbour(coord, NeighbourDirection::NorthEast), NeighbourDirection::NorthWest);
			case DiagonalDirection::NorthEast:
				return getNeighbour(getNeighbour(coord, NeighbourDirection::NorthEast), NeighbourDirection::East);
			case DiagonalDirection::SouthEast:
				return getNeighbour(getNeighbour(coord, NeighbourDirection::SouthEast), NeighbourDirection::East);
			case DiagonalDirection::South:
				return getNeighbour(getNeighbour(coord, NeighbourDirection::SouthEast), NeighbourDirection::SouthWest);
			case DiagonalDirection::SouthWest:
				return getNeighbour(getNeighbour(coord, NeighbourDirection::SouthWest), NeighbourDirection::West);

			default:
				assert(false);
				return coord;
			}
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		Vector2D<int> getDiagonal(Vector2D<int> coord, DiagonalDirection direction, Vector2D<int> worldDimensions, WrapType wrapType)
		{
			return wrapCoord(getDiagonal(coord, direction), worldDimensions, wrapType);
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		bool withinMap(Vector2D<int> coord, NeighbourDirection direction, Vector2D<int> worldDimensions, WrapType wrapType)
		{
			Vector2D<int> newCoord = getNeighbour(coord, direction);
			return withinMap(newCoord, worldDimensions, wrapType);
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		bool withinMap(Vector2D<int> coord, Vector2D<int> worldDimensions, WrapType wrapType)
		{
			return withinMap(wrapCoord(coord, worldDimensions, wrapType), worldDimensions);
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		bool withinMap(Vector2D<int> coord, Vector2D<int> worldDimensions)
		{
			return coord.x >= 0 && coord.x < worldDimensions.x && coord.y >= 0 && coord.y < worldDimensions.y;
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		Vector3D<int> roundHex(Vector3D<double> cubeCoord)
		{
			Vector3D<double> roundedCoord;
			roundedCoord.x = round(cubeCoord.x);
			roundedCoord.y = round(cubeCoord.y);
			roundedCoord.z = round(cubeCoord.z);

			Vector3D<double> roundDiff = roundedCoord - cubeCoord;
			roundDiff.x = abs(roundDiff.x);
			roundDiff.y = abs(roundDiff.y);
			roundDiff.z = abs(roundDiff.z);

			if (roundDiff.x > roundDiff.y && roundDiff.x > roundDiff.z)
			{
				roundedCoord.x = -roundedCoord.y - roundedCoord.z; //x + y + z = 0
			}
			else if (roundDiff.y > roundDiff.z)
			{
				roundedCoord.y = -roundedCoord.x - roundedCoord.z; //x + y + z = 0
			}
			else
			{
				roundedCoord.z = -roundedCoord.x - roundedCoord.y; //x + y + z = 0
			}

			return roundedCoord.typecast<int>();
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		Vector2D<int> roundHex(Vector2D<double> hexCoord)
		{
			return cubeToHex(roundHex(hexToCube(hexCoord)).typecast<double>()).typecast<int>();
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		Vector2D<double> hexCoordToCoord(Vector2D<int> hexCoord, double hexRadius)
		{
			return hexCoordToCoord(hexCoord.typecast<double>(), hexRadius);
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		Vector2D<double> hexCoordToCoord(Vector2D<double> hexCoord, double hexRadius)
		{
			Vector3D<double> cubeCoord = hexToCube(hexCoord);
			Vector2D<double> coord;
			coord.x = cubeCoord.x*sqrt(3.0) + cubeCoord.z*sqrt(3.0) / 2.0;
			coord.y = cubeCoord.x*0.0 + cubeCoord.z*(3.0 / 2.0);
			coord *= hexRadius;


			return coord;
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		Vector2D<int> coordToHexCoord(Vector2D<double> coord, double hexRadius)
		{
			Vector2D<double> hexCoord = coordToDecimalHexCoord(coord, hexRadius);

			return roundHex(hexCoord);
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		Vector2D<double> coordToDecimalHexCoord(Vector2D<double> coord, double hexRadius)
		{
			Vector3D<double> cubedCoord;
			cubedCoord.x = coord.x*sqrt(3) / 3.0 + coord.y*(-1.0 / 3.0);
			cubedCoord.z = coord.x*0.0 + coord.y*(2.0 / 3.0);
			cubedCoord.y = -cubedCoord.x - cubedCoord.z;
			cubedCoord /= hexRadius;
			
			return cubeToHex(cubedCoord);
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		Vector3D<double> hexToCube(Vector2D<double> coord)
		{
			Vector3D<double> cubed;
			cubed.x = coord.x - (coord.y - double(int(coord.y) & 1)) / 2.0;
			cubed.z = coord.y;
			cubed.y = -cubed.x - cubed.z;
			return cubed;
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		Vector2D<double> cubeToHex(Vector3D<double> coord)
		{
			Vector2D<double> hexCoord;
			hexCoord.x = coord.x + (coord.z - double(int(coord.z) & 1)) / 2.0;
			hexCoord.y = coord.z;
			return hexCoord;
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		double getAngle(NeighbourDirection neighbourDirection)
		{
			switch (neighbourDirection)
			{
			case NeighbourDirection::East:
				return 2.0*M_PI * 0.0 / 12.0;
			case NeighbourDirection::West:
				return 2.0*M_PI * 6.0 / 12.0;
			case NeighbourDirection::SouthEast:
				return 2.0*M_PI * 2.0 / 12.0;
			case NeighbourDirection::NorthEast:
				return -2.0*M_PI * 2.0 / 12.0;
			case NeighbourDirection::SouthWest:
				return 2.0*M_PI * 4.0 / 12.0;
			case NeighbourDirection::NorthWest:
				return -2.0*M_PI * 4.0 / 12.0;

			default:
				assert(false);
				return 0.0;
			}
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		double getAngle(DiagonalDirection diagonalDirection)
		{
			switch (diagonalDirection)
			{
			case DiagonalDirection::SouthEast:
				return 2.0*M_PI * 1.0 / 12.0;
			case DiagonalDirection::South:
				return 2.0*M_PI * 3.0 / 12.0;
			case DiagonalDirection::SouthWest:
				return 2.0*M_PI * 5.0 / 12.0;
			case DiagonalDirection::NorthEast:
				return -2.0*M_PI * 1.0 / 12.0;
			case DiagonalDirection::North:
				return -2.0*M_PI * 3.0 / 12.0;
			case DiagonalDirection::NorthWest:
				return -2.0*M_PI * 5.0 / 12.0;

			default:
				assert(false);
				return 0.0;
			}
		}
	}
}