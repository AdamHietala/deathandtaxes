#pragma once
#include "LoadsOnce.h"
#include "Recipe.h"
#include <map>
#include <vector>
#include "IDType.h"
#include <string>
namespace dat
{
	class ModuleLoader;
	class ResourceTypeLoader;
	class TechTreeLoader;
	class InfrastructureTypeLoader;
	class RecipeLoader : public LoadsOnce 
	{
		std::map<std::string, IDType<Recipe>> stringIDToID; //Maps the Recipe string ID to the RecipeID
		std::vector<std::string> idToStringID;
		std::vector<Recipe *> idToRecipe;
		void addRecipe(std::string stringID, Recipe *recipe); //Adds the Recipe to the datastructure.
	public:
		RecipeLoader();
		~RecipeLoader();

		void load(ModuleLoader &moduleLoader, ResourceTypeLoader const &resourceTypeloader, TechTreeLoader const &techTreeLoader, InfrastructureTypeLoader const &infrastructureTypeLoader);
		IDType<Recipe> getID(std::string stringID) const; //Returns the tileset ID.
		std::string getStringID(IDType<Recipe> id) const; //Returns the stringID so that it can be referred to in a savefile.
		Recipe *getRecipe(IDType<Recipe> id) const; //Returns the resource.
		int getAmountOfRecipies() const;//returns the amount of Recipies that there are.
	};

}