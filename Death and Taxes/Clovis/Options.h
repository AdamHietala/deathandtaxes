#pragma once
#include "VectorXD.h"
namespace dat
{
	class GameMain;
	class Options
	{
	private:
	public:
		Options();
		~Options();
		enum class WindowMode {Full, Windowed, Borderless};

		void load(); //Loads the options from the options file. Throws ParsingException if the options could not be parsed. Throws KeyNotFoundException if options are missing from the options file.
		void save(); //Saves the options to the options file

		Vector2D<int> screenSize;
		bool verticalSync;
		int frameRateLimit, nrOfThreads;
		WindowMode windowMode;


		void updateOptions(GameMain &gameMain);
	};

}