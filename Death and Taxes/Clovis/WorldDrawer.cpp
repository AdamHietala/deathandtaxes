#include "WorldDrawer.h"
#include "AnimationLoader.h"
#include "AnimationInstance.h"
#include "GameMain.h"
#include "TerrainType.h"
#include "GameWorld.h"
#include "AnimationLoader.h"
#include <iostream>
#include "Tileset.h"
#include "WorldDrawerDefines.h"
namespace dat
{
	Vector2D<int> WorldDrawer::getTilesInSight(sf::RenderTarget &renderTarget)
	{
		
		Vector2D<float> tilesInSight;
		tilesInSight.x = float(renderTarget.getSize().x) / (Camera::HEX_RADIUS * float(hex::SHORT_RADIUS_FACTOR)) / 2.0f;
		tilesInSight.y = (float(renderTarget.getSize().y) - 2.0f * Camera::HEX_RADIUS + Camera::HEX_RADIUS*float(hex::VERTICAL_OFFSET_FACTOR)) / (Camera::HEX_RADIUS*float(hex::VERTICAL_OFFSET_FACTOR));
		
		tilesInSight.x = camera.getRealSize(tilesInSight.x);
		tilesInSight.y = camera.getRealSize(tilesInSight.y);

		tilesInSight += 5; //for extra margin

		return tilesInSight.typecast<int>();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void WorldDrawer::drawTerrain(sf::RenderTarget &renderTarget, sf::Time time)
	{
		sf::RectangleShape rect;
		rect.setSize(sf::Vector2f(float(renderTarget.getSize().x), float(renderTarget.getSize().y)));

		rect.setFillColor(sf::Color(0, 0, 0, 0));
		renderTarget.draw(rect, sf::BlendMode(sf::BlendMode::Zero, sf::BlendMode::One, sf::BlendMode::Add, sf::BlendMode::One, sf::BlendMode::Zero, sf::BlendMode::Add));//makes sure that the renderTarget is transparrent.


		Vector2D<int> tilesInSight = getTilesInSight(renderTarget);

		for (int priority = tilesetLoader.getHighestPriority(); priority >= 1; --priority)
		{
			for (int i = -tilesInSight.x / 2; i < tilesInSight.x / 2; ++i)
			{
				for (int q = -tilesInSight.y / 2; q < tilesInSight.y / 2; ++q)
				{
					Vector2D<int> coord = hex::coordToHexCoord(camera.getFocus().typecast<double>(), Camera::HEX_RADIUS) + Vector2D<int>(i, q);
					if (hex::withinMap(coord, gameWorld.getWorldDimensions(), gameWorld.getWrapType()))
					{
						drawTerrainTileFirstMask(renderTarget, time, coord, priority);
					}
				}
			}
		}

		for (int i = -tilesInSight.x / 2; i < tilesInSight.x / 2; ++i)//this order is important, up->down left->right
		{
			for (int q = -tilesInSight.y / 2; q < tilesInSight.y / 2; ++q)
			{
				Vector2D<int> coord = hex::coordToHexCoord(camera.getFocus().typecast<double>(), Camera::HEX_RADIUS) + Vector2D<int>(i, q);
				if (hex::withinMap(coord, gameWorld.getWorldDimensions(), gameWorld.getWrapType()))
				{
					drawTerrainTileSecondMask(renderTarget, time, coord);
				}
			}
		}
		//rect.setFillColor(sf::Color(0, 0, 0, 255));
		//renderTarget.draw(rect, sf::BlendMode(sf::BlendMode::Zero, sf::BlendMode::One, sf::BlendMode::Add, sf::BlendMode::One, sf::BlendMode::Zero, sf::BlendMode::Add));//makes sure that the renderTarget is.
	
		


	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void WorldDrawer::drawTerrainTileFirstMask(sf::RenderTarget &renderTarget, sf::Time time, Vector2D<int> coord, int priorityNumber)
	{
		TerrainType *terrainType = gameWorld.getTerrainType(coord);
		Tileset *tileSet = tilesetLoader.getTileset(terrainType);
		if (tileSet->priority != priorityNumber)
		{
			return;
		}
		

		Vector2D<int> wrappedCoord = hex::wrapCoord(coord, gameWorld.getWorldDimensions(), gameWorld.getWrapType());
		tileChooser.setSeed(wrappedCoord.x);
		tileChooser.addSeedNumber(wrappedCoord.y);



		Vector2D<float> screenPos = camera.getScreenPosition(hex::hexCoordToCoord(coord, Camera::HEX_RADIUS).typecast<float>());



		int textureIndex = tileChooser.getNextNumber() % tileSet->nrOfVariations;
		AnimationInstance baseTexture(*animationLoader.getAnimation(tileSet->baseTextures[textureIndex]));
		AnimationInstance firstMask(*animationLoader.getAnimation(tileSet->firstMasks[textureIndex]));
		baseTexture.scale(Vector2D<float>(//times 3 to make one texture cover 9 entire hexes, so that blending looks good, and so that the mask is entirely masked away
			camera.getScreenSize(Camera::HEX_RADIUS * TILE_TO_LONG_RADIUS_RATIO) / baseTexture.getSize().x,
			camera.getScreenSize(Camera::HEX_RADIUS * TILE_TO_LONG_RADIUS_RATIO) / baseTexture.getSize().y
			)); //assumes the texture is a square
		firstMask.scale(Vector2D<float>(//times 3
			camera.getScreenSize(Camera::HEX_RADIUS * TILE_TO_LONG_RADIUS_RATIO) / firstMask.getSize().x,
			camera.getScreenSize(Camera::HEX_RADIUS * TILE_TO_LONG_RADIUS_RATIO) / firstMask.getSize().y
			)); //assumes the texture is a square
		baseTexture.setPosition(screenPos);
		firstMask.setPosition(screenPos);

		firstMask.draw(renderTarget, time, sf::BlendMode(sf::BlendMode::Zero, sf::BlendMode::One, sf::BlendMode::Add, sf::BlendMode::One, sf::BlendMode::Zero, sf::BlendMode::Add));
		baseTexture.draw(renderTarget, time, sf::BlendMode(sf::BlendMode::DstAlpha, sf::BlendMode::OneMinusDstAlpha, sf::BlendMode::Add, sf::BlendMode::Zero, sf::BlendMode::One, sf::BlendMode::Add));
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void WorldDrawer::drawTerrainTileSecondMask(sf::RenderTarget &renderTarget, sf::Time time, Vector2D<int> coord)
	{

		TerrainType *terrainType = gameWorld.getTerrainType(coord);
		Tileset *tileSet = tilesetLoader.getTileset(terrainType);


		Vector2D<int> wrappedCoord = hex::wrapCoord(coord, gameWorld.getWorldDimensions(), gameWorld.getWrapType());
		tileChooser.setSeed(wrappedCoord.x);
		tileChooser.addSeedNumber(wrappedCoord.y);



		Vector2D<float> screenPos = camera.getScreenPosition(hex::hexCoordToCoord(coord, Camera::HEX_RADIUS).typecast<float>());



		int textureIndex = tileChooser.getNextNumber() % tileSet->nrOfVariations;
		AnimationInstance baseTexture(*animationLoader.getAnimation(tileSet->baseTextures[textureIndex]));
		AnimationInstance secondMask(*animationLoader.getAnimation(tileSet->secondMasks[textureIndex]));
		baseTexture.scale(Vector2D<float>(
			camera.getScreenSize(Camera::HEX_RADIUS * TILE_TO_LONG_RADIUS_RATIO) / baseTexture.getSize().x,
			camera.getScreenSize(Camera::HEX_RADIUS * TILE_TO_LONG_RADIUS_RATIO) / baseTexture.getSize().y
			)); //assumes the texture is a square
		secondMask.scale(Vector2D<float>(
			camera.getScreenSize(Camera::HEX_RADIUS * TILE_TO_LONG_RADIUS_RATIO) / secondMask.getSize().x,
			camera.getScreenSize(Camera::HEX_RADIUS * TILE_TO_LONG_RADIUS_RATIO) / secondMask.getSize().y
			)); //assumes the texture is a square
		baseTexture.setPosition(screenPos);
		secondMask.setPosition(screenPos);

		secondMask.draw(renderTarget, time, sf::BlendMode(sf::BlendMode::Zero, sf::BlendMode::One, sf::BlendMode::Add, sf::BlendMode::One, sf::BlendMode::Zero, sf::BlendMode::Add));
		baseTexture.draw(renderTarget, time, sf::BlendMode(sf::BlendMode::DstAlpha, sf::BlendMode::OneMinusDstAlpha, sf::BlendMode::Add, sf::BlendMode::Zero, sf::BlendMode::One, sf::BlendMode::Add));




	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void WorldDrawer::drawCities(sf::RenderTarget &renderTarget, sf::Time time)
	{
		Vector2D<int> tilesInSight = getTilesInSight(renderTarget);
		for (int i = -tilesInSight.x / 2; i < tilesInSight.x / 2; ++i)//this order is important, up->down left->right
		{
			for (int q = -tilesInSight.y / 2; q < tilesInSight.y / 2; ++q)
			{
				Vector2D<int> coord = hex::coordToHexCoord(camera.getFocus().typecast<double>(), Camera::HEX_RADIUS) + Vector2D<int>(i, q);
				if (gameWorld.cityExists(coord))
				{
					Vector2D<int> wrappedCoord = hex::wrapCoord(coord, gameWorld.getWorldDimensions(), gameWorld.getWrapType());
					tileChooser.setSeed(wrappedCoord.x);
					tileChooser.addSeedNumber(wrappedCoord.y);
					AnimationInstance citySprite(*citySpriteLoader.getAnimation(*gameWorld.getCity(coord), tileChooser.getNextNumber()));
					Vector2D<float> screenPos = camera.getScreenPosition(hex::hexCoordToCoord(coord, Camera::HEX_RADIUS).typecast<float>());
					citySprite.setPosition(screenPos);
					citySprite.scale(Vector2D<float>(
						camera.getScreenSize(Camera::HEX_RADIUS * TILE_TO_LONG_RADIUS_RATIO) / citySprite.getSize().x,
						camera.getScreenSize(Camera::HEX_RADIUS * TILE_TO_LONG_RADIUS_RATIO) / citySprite.getSize().y
						)); //assumes the sprite is a square
					citySprite.draw(renderTarget, time);
				}
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	WorldDrawer::WorldDrawer(AnimationLoader &animationLoader, TilesetLoader &tilesetLoader, CitySpriteLoader &citySpriteLoader, GameWorld &gameWorld, Camera &camera) :
		animationLoader(animationLoader), tilesetLoader(tilesetLoader), citySpriteLoader(citySpriteLoader), gameWorld(gameWorld), camera(camera), tileChooser(140900761, 11741730, 127036277)
	{
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	WorldDrawer::~WorldDrawer()
	{
		
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void WorldDrawer::draw(sf::RenderTarget &renderTarget, sf::Time time)
	{
		if (!gameWorld.gameIsRunning())
		{
			return;
		}



		drawTerrain(renderTarget, time);
		drawCities(renderTarget, time);

	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void WorldDrawer::newGame()
	{
		camera.reset();
		camera.setFocus(gameWorld.getWorldDimensions().typecast<float>() / 2.0f);

	}

}