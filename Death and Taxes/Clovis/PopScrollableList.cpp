#include "PopScrollableList.h"
#include "PlayerControlState.h"
#include "City.h"
#include "TypeImageLoader.h"
#include "AnimationInstance.h"
#include "Localisation.h"
#include "PopTypeLoader.h"
#include "Pop.h"
#include "PopType.h"
#include "LocalisationDefines.h"
#include "StringManipulation.h"
#include "GeneralFunctions.h"
namespace dat
{

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	PopScrollableList::PopInfo::PopInfo(PopScrollableList &popScrollableList, Pop &pop) :
		popScrollableList(popScrollableList), pop(pop)
	{

	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	PopScrollableList::PopInfo::~PopInfo()
	{

	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void PopScrollableList::PopInfo::draw(sf::RenderTarget &renderTarget, sf::Time time, bool respectAlpha)
	{
		if (!isActive())
		{
			return;
		}


		popScrollableList.prepareItemsDrawingArea(renderTarget, respectAlpha);

		Animation *popTypeAnimation = popScrollableList.typeImageLoader.getPopTypeImageLoader().getAnimation(pop.getPopType().id);
		sf::Color textColour = popScrollableList.playerControlState.getSelectedPop() == &pop ? popScrollableList.getSelectedTextColour() : popScrollableList.getTextColour();
		


		AnimationInstance popTypeAnimationInstance(*popTypeAnimation);
		Rectangle<float> imageRect = getBoundingBox().typecast<double>().translate(popScrollableList.professionImageBox).typecast<float>(); 
		Vector2D<float> imageDimensions(min(imageRect.dimensions.x, imageRect.dimensions.y), min(imageRect.dimensions.x, imageRect.dimensions.y));//assumes the poptype iamge is a square
		popTypeAnimationInstance.scale(imageDimensions / popTypeAnimationInstance.getSize());
		popTypeAnimationInstance.setPosition(imageRect.topLeft + imageRect.dimensions / 2.0f);
		popTypeAnimationInstance.draw(renderTarget, time, renderStatesFilterAlphaThroughAlpha);
		popTypeAnimationInstance.draw(renderTarget, time, renderStatesFilterThroughAlpha);

		popScrollableList.textBox.setTextColour(textColour);
		std::string textString;
		Rectangle<int> textBoundingBox;
		textString = popScrollableList.localisation.getString(popScrollableList.popTypeLoader.getStringID(pop.getPopType().id));
		popScrollableList.textBox.setText(textString);
		textBoundingBox = getBoundingBox().typecast<double>().translate(popScrollableList.professionBox).typecast<int>();
		popScrollableList.textBox.setBoundingBox(textBoundingBox);
		popScrollableList.textBox.draw(renderTarget, time, true);

		textString = toString(pop.getPopSize(), 0);
		popScrollableList.textBox.setText(textString);
		textBoundingBox = getBoundingBox().typecast<double>().translate(popScrollableList.populationBox).typecast<int>();
		popScrollableList.textBox.setBoundingBox(textBoundingBox);
		popScrollableList.textBox.draw(renderTarget, time, true);

		textString = toString(pop.getIdeology().getStance(Ideology::IdeologyScale::Economic), 2);
		popScrollableList.textBox.setText(textString);
		textBoundingBox = getBoundingBox().typecast<double>().translate(popScrollableList.politicsBox).translate(popScrollableList.economicPoliticsBox).typecast<int>();
		popScrollableList.textBox.setBoundingBox(textBoundingBox);
		popScrollableList.textBox.draw(renderTarget, time, true);

		textString = toString(pop.getIdeology().getStance(Ideology::IdeologyScale::Governmental), 2);
		popScrollableList.textBox.setText(textString);
		textBoundingBox = getBoundingBox().typecast<double>().translate(popScrollableList.politicsBox).translate(popScrollableList.governmentalPoliticsBox).typecast<int>();
		popScrollableList.textBox.setBoundingBox(textBoundingBox);
		popScrollableList.textBox.draw(renderTarget, time, true);

		textString = toString(pop.getIdeology().getStance(Ideology::IdeologyScale::Personal), 2);
		popScrollableList.textBox.setText(textString);
		textBoundingBox = getBoundingBox().typecast<double>().translate(popScrollableList.politicsBox).translate(popScrollableList.personalPoliticsBox).typecast<int>();
		popScrollableList.textBox.setBoundingBox(textBoundingBox);
		popScrollableList.textBox.draw(renderTarget, time, true);

		if (popScrollableList.playerControlState.isThisPopSelected(&pop))
		{
			popScrollableList.prepareItemsDrawingArea(renderTarget, respectAlpha);
			AnimationInstance selectionOverlayAnimationInstance(*popScrollableList.selectionOverlay);
			Rectangle<float> imageRect = getBoundingBox().typecast<float>();
			selectionOverlayAnimationInstance.scale(imageRect.dimensions / selectionOverlayAnimationInstance.getSize());
			selectionOverlayAnimationInstance.setPosition(imageRect.topLeft + imageRect.dimensions / 2.0f);
			selectionOverlayAnimationInstance.draw(renderTarget, time, renderStatesFilterAlphaThroughAlpha);
			selectionOverlayAnimationInstance.draw(renderTarget, time, renderStatesFilterThroughAlpha);
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void PopScrollableList::PopInfo::mouseButtonPressed(bool &eventClaimed, Vector2D<int> mousePos, sf::Mouse::Button button)
	{
		if (eventClaimed)
		{
			return;
		}
		if (getBoundingBox().within(mousePos) && popScrollableList.getListBoundingBox().within(mousePos))
		{
			eventClaimed = true;
			popScrollableList.playerControlState.selectPop(&pop);
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	bool PopScrollableList::menuOpen() const
	{
		return isActive() && playerControlState.isCitySelected();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	PopScrollableList::PopScrollableList(Localisation &localisation, PopTypeLoader &popTypeLoader, PlayerControlState &playerControlState, TypeImageLoader &typeImageLoader) :
		localisation(localisation), popTypeLoader(popTypeLoader), playerControlState(playerControlState), typeImageLoader(typeImageLoader)
	{
		selectionOverlay = nullptr;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	PopScrollableList::~PopScrollableList()
	{
		
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void PopScrollableList::draw(sf::RenderTarget &renderTarget, sf::Time time, bool respectAlpha)
	{
		if (!menuOpen())
		{
			return;
		}

		clearList();
		assert(playerControlState.isCitySelected());
		City *city = playerControlState.getSelectedCity();
		for (auto popTypeGroup : city->getPops())
		{
			for (auto pop : popTypeGroup)
			{
				PopInfo *newPopInfo = new PopInfo(*this, *pop);
				listItems.push_back(newPopInfo);
			}
		}
		ScrollableList::draw(renderTarget, time, respectAlpha);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void PopScrollableList::drawHeader(sf::RenderTarget &renderTarget, sf::Time time, bool respectAlpha)
	{
		prepareHeaderDrawingArea(renderTarget, respectAlpha);
		textBox.setTextColour(getTextColour());
		std::string textString;
		Rectangle<int> textBoundingBox;



		textString = localisation.getString(loc::GUI::POP_LIST_POP_TYPE_HEADER);
		textBox.setText(textString);
		textBoundingBox = getHeaderBoundingBox().typecast<double>().translate(professionBox).typecast<int>();
		textBox.setBoundingBox(textBoundingBox);
		textBox.draw(renderTarget, time, true);


		textString = localisation.getString(loc::GUI::POP_LIST_POPULATION_HEADER);
		textBox.setText(textString);
		textBoundingBox = getHeaderBoundingBox().typecast<double>().translate(populationBox).typecast<int>();
		textBox.setBoundingBox(textBoundingBox);
		textBox.draw(renderTarget, time, true);



		Rectangle<double> modifiedPoliticsBox = politicsBox;
		modifiedPoliticsBox.dimensions.y *= 0.5;
		textString = localisation.getString(loc::GUI::POP_LIST_POLITICS_HEADER);
		textBox.setText(textString);
		textBoundingBox = getHeaderBoundingBox().typecast<double>().translate(modifiedPoliticsBox).typecast<int>();
		textBox.setBoundingBox(textBoundingBox);
		textBox.draw(renderTarget, time, true);



		modifiedPoliticsBox.topLeft.y += modifiedPoliticsBox.dimensions.y;

		textString = localisation.getString(loc::GUI::POP_LIST_POLITICS_ECONOMICS_HEADER);
		textBox.setText(textString);
		textBoundingBox = getHeaderBoundingBox().typecast<double>().translate(modifiedPoliticsBox).translate(economicPoliticsBox).typecast<int>();
		textBox.setBoundingBox(textBoundingBox);
		textBox.draw(renderTarget, time, true);

		textString = localisation.getString(loc::GUI::POP_LIST_POLITICS_GOVERNMENT_HEADER);
		textBox.setText(textString);
		textBoundingBox = getHeaderBoundingBox().typecast<double>().translate(modifiedPoliticsBox).translate(governmentalPoliticsBox).typecast<int>();
		textBox.setBoundingBox(textBoundingBox);
		textBox.draw(renderTarget, time, true);

		textString = localisation.getString(loc::GUI::POP_LIST_POLITICS_PERSONAL_HEADER);
		textBox.setText(textString);
		textBoundingBox = getHeaderBoundingBox().typecast<double>().translate(modifiedPoliticsBox).translate(personalPoliticsBox).typecast<int>();
		textBox.setBoundingBox(textBoundingBox);
		textBox.draw(renderTarget, time, true);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void PopScrollableList::setProfessionImageBox(Rectangle<double> box)
	{
		professionImageBox = box;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void PopScrollableList::setProfessionBox(Rectangle<double> box)
	{
		professionBox = box;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void PopScrollableList::setPopulationBox(Rectangle<double> box)
	{
		populationBox = box;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void PopScrollableList::setPoliticsBox(Rectangle<double> box)
	{
		politicsBox = box;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void PopScrollableList::setEconomicPoliticsBox(Rectangle<double> box)
	{
		economicPoliticsBox = box;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void PopScrollableList::setGovernmentalPoliticsBox(Rectangle<double> box)
	{
		governmentalPoliticsBox = box;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void PopScrollableList::setPersonalPoliticsBox(Rectangle<double> box)
	{
		personalPoliticsBox = box;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void PopScrollableList::setAnimations(Animation &newBackground, Animation &scrollBar, Animation &scrollIndicator, Animation &selectionOverlay)
	{
		ScrollableList::setAnimations(newBackground, scrollBar, scrollIndicator);
		delete PopScrollableList::selectionOverlay;
		PopScrollableList::selectionOverlay = new AnimationInstance(selectionOverlay);
	}
}