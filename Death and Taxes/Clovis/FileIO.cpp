#include "FileIO.h"
#include <boost\filesystem.hpp>
#include <fstream>
#include "StringManipulation.h"
#include "Exceptions.h"
namespace dat
{
	bool fileExists(std::string filePath)
	{
		boost::filesystem::path file(filePath);
		return boost::filesystem::exists(file) && boost::filesystem::is_regular_file(file);
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void writeToFile(std::string lines, std::string filePath)
	{
		std::ofstream file;
		file.exceptions(std::ofstream::failbit | std::ofstream::badbit);
		try {
			file.open(filePath);
			file << lines;
		}
		catch (std::ofstream::failure e) {
			throw(IOException("File \"" + filePath + "\" could not be written to."));
		}
		file.close();
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void writeToFile(std::vector<std::string> &lines, std::string filePath)
	{
		writeToFile(join(lines, "\n"), filePath);
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::string getFileContent(std::string filePath, bool cleanNewline)
	{
		std::ifstream in(filePath, std::ios::in | std::ios::binary);
		if (in)
		{
			std::string retval = std::string((std::istreambuf_iterator<char>(in)), std::istreambuf_iterator<char>());
			if (cleanNewline)
			{
				retval = replaceStr(retval, "\r\n", "\n");
				retval = replaceChar(retval, '\r', '\n');
			}
			return retval;
		}
		throw(IOException("File \"" + filePath + "\" could not be opened."));
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::vector<std::string> getFileLines(std::string filePath, bool cleanNewline)
	{
		std::string allText = getFileContent(filePath, cleanNewline);
		return split(allText, "\n");
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	bool folderExists(std::string folderPath)
	{
		boost::filesystem::path folder(folderPath);
		return boost::filesystem::exists(folder) && boost::filesystem::is_directory(folder);
	}


	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::vector<std::string> listFilesInFolder(std::string folderPath, std::string extension)
	{
		std::vector<std::string> files;
		boost::filesystem::path path(folderPath);

		boost::filesystem::directory_iterator end_itr;

		// cycle through the directory
		for (boost::filesystem::directory_iterator itr(path); itr != end_itr; ++itr)
		{
			// If it's not a directory, list it. If you want to list directories too, just remove this check.
			if (is_regular_file(itr->path())) {
				files.push_back(itr->path().filename().string());
			}
		}

		return files;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::string getFileType(std::string fileName)
	{
		std::string extention;
		auto i = fileName.size() - 1;
		while (i >= 0)
		{
			if (fileName[i] == '.')
			{
				return extention;
			}
			else
			{
				extention = fileName[i] + extention;
			}
			--i;
		}
		return "";
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	bool goesThroughFolder(std::string folderPath, std::string subfilePath)
	{
		folderPath = standardiseFilePath(folderPath);
		subfilePath = standardiseFilePath(subfilePath);

		return getSubString(subfilePath, 0, folderPath.size()) == subfilePath;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::string standardiseFilePath(std::string path) //will expand all dots and double dots and make all slashes be backslashes '\'
	{
		std::vector<std::string> splitPath = split(path = replaceChar(path, '/', '\\'), "//");
		std::vector<std::string> standardisedPath;
		for (auto step : splitPath)
		{
			if (step == "..")
			{
				testException(standardisedPath.size() != 0, ParsingException("Could not standardise filePath due to too many \"..\" outside the knowledge of this function."));
				standardisedPath.pop_back();
			}
			else if (step != ".")
			{
				standardisedPath.push_back(step);
			}
		}
		return join(standardisedPath, "\\");
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}