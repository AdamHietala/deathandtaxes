#pragma once
#include "HexGrid.h"
namespace dat
{
	const float TILE_TO_LONG_RADIUS_RATIO = 2.0f * (float)hex::HORIZONTAL_OFFSET_FACTOR; //just enough to reach the centre of its neighbours
}