#pragma once
#include <vector>
#include "IDType.h"
namespace dat
{
	class TechTreeLoader;
	class Technology;
	class TechType;
	class TechLevel //describes what technology level a pop is at
	{
	private:
		IDType<Technology> nextUnlockedTech; //When a new tech is unlocked or locked away again the next unlocked tech is recalculated. It's a random tech among the then unlockable techs. If there are no unlockable tech next this variable is invalid.
		bool nextUnlockedTechIsValid;//tells you whether the 'nextUnlockedTech' variable is valid or not.
		std::vector<bool> techUnlocked; //is the size of all existing technologies. The technologies' ids are used for index.
		TechTreeLoader &techTreeLoader;//holds within it the information of all technologies
		std::vector<double> techTypeLevels; //holds the tech level for each tchnology type. The techTypes' ids are used for index.
		void updateNextUnlockedTech(); //makes sure that the nextUnlockedTech is still valid and if not, whether from before or just now, checks if it can be remedied.
	public:
		TechLevel(TechTreeLoader &techTreeLoader);
		~TechLevel();
		bool isTechUnlocked(IDType<Technology> techId) const;//returns true if the technology is unlocked.
		bool hasUnlockableTech() const; //tells whether there is a new tech that can be unlocked.
		void unlockNextTech(); //Unlocks a random unlockable tech. Assumes that there is one.
		double getTechTypeLevel(IDType<TechType> techTypeId) const;//tech level for the technology with techId 'techTypeId'.
		void changeTechLevel(IDType<TechType> techType, double amount);//changes the techlevel for this tech. If negative numbers are used some unlocked techs may become locked again.
	};
}