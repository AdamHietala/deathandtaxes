#pragma once
#include <SFML\Graphics.hpp>
#include <map>
#include "VectorXD.h"
#include "IDType.h"
#include <list>
#include "RNGen.h"
namespace dat
{
	class AnimationInstance;
	class WorldSpace;
	class AnimationLoader;
	class GameWorld;
	class TilesetLoader;
	class Animation;
	class Tileset;
	class Camera;
	class CitySpriteLoader;
	class WorldDrawer
	{
	private:
		GameWorld &gameWorld;
		AnimationLoader &animationLoader;
		TilesetLoader &tilesetLoader;
		CitySpriteLoader &citySpriteLoader;
		Camera &camera;
		Vector2D<int> getTilesInSight(sf::RenderTarget &renderTarget);//Returns the amount of tiles both vertically and horizontally that needs to be rendered, depends on camera.

		void drawTerrain(sf::RenderTarget &renderTarget, sf::Time time); //draws the map terrain.
		void drawTerrainTileFirstMask(sf::RenderTarget &renderTarget, sf::Time time, Vector2D<int> coord, int priorityNumber); //Draws this terrain tile using its first mask, if the tileset for hte terrain of the tile has priority 'priorityNumber'.
		void drawTerrainTileSecondMask(sf::RenderTarget &renderTarget, sf::Time time, Vector2D<int> coord); //Draws this terrain tile using its own second sask

		void drawCities(sf::RenderTarget &renderTarget, sf::Time time);//draws the cities in view.
		RNGen tileChooser;
	public:
		WorldDrawer(AnimationLoader &animationLoader, TilesetLoader &tilesetLoader, CitySpriteLoader &citySpriteLoader, GameWorld &gameWorld, Camera &camera);
		~WorldDrawer();
		void draw(sf::RenderTarget &renderTarget, sf::Time time); //Draws the world to the rendertarget.
		void newGame(); //Should be called when a new game is generated. Deletes all currently running animations
	};

}