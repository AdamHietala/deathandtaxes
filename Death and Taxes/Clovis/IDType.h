#pragma once
#include <assert.h>
namespace dat
{
	template<typename t>
	class IDType //A class that only exists so there can be more strongly typed ID's that won't get mixed in the code
	{
	private:
		bool usable; //Variable to make sure the getID() is never used together with an invalid id.
		int id;
	public:
		IDType() : usable(false) {} //Should never be used. Enabled becuase it's used by copy the constructor.
		IDType(int id) : id(id), usable(true) {}
		~IDType(){}
		inline int getID() const
		{
			assert(usable);
			return id;
		}
		inline bool operator==(IDType<t> const& other) const
		{
			return other.id == id;
		}
		inline bool operator!=(IDType<t> const& other) const
		{
			return !(other == *this);
		}
	};
}