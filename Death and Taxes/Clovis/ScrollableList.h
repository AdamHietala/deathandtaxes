#pragma once
#include "Widget.h"
#include "ScrollBar.h"
#include <vector>
#include "Rectangle.h"
#include "TextBox.h"
namespace dat
{
	class Animation;
	class AnimationInstance;
	class ScrollableList : public Widget
	{
	protected:
		class ScrollableListItem : public Widget
		{
		private:
			Rectangle<int> boundingBox;
		protected:
			Rectangle<int> getBoundingBox() const;
			ScrollableListItem(Rectangle<int> boundingBox);
			ScrollableListItem();
		public:
			virtual ~ScrollableListItem() override;
			void setBoundingBox(Rectangle<int> boundingBox);
		};
	private:
		Rectangle<int> boundingBox;
		double scrollerWidthPercentage;
		void recalcListScroller();//recalculates the list scrollers position and size
		AnimationInstance *background;
		int rowHeight, headerRowHeight;
		void constructor();//does the part common for both the constructors.
		bool scrollerSideLeft;
		sf::Color textColour, selectedTextColour;
		int getItemOffsetY(int listPosition)  const;
	protected:
		TextBox textBox;
		ScrollBar listScroller;
		std::vector<ScrollableListItem*> listItems; 
		bool mousePressed;

		ScrollableList(Rectangle<int> boundingBox, int scrollerWidth, Animation &background, Animation &scrollBar, Animation &scrollIndicator);
		ScrollableList();
		virtual bool menuOpen() const = 0; //returns true if the menu should be open. Used to detect whether input should be listened to and the menu should be drawn at all.
		void clearList();//deletes all items in the list from the heap and clears the list.
	public:
		void prepareHeaderDrawingArea(sf::RenderTarget &renderTarget, bool respectAlpha)  const; //must be called before drawing in the drawing area to make sure nothing is drawn outside of the area. Must sometimes be done again if more than one image with alpha channels will overlap.
		void prepareItemsDrawingArea(sf::RenderTarget &renderTarget, bool respectAlpha)  const; //must be called before drawing in the drawing area to make sure nothing is drawn outside of the area. Must sometimes be done again if more than one image with alpha channels will overlap.
		void prepareEntireDrawingArea(sf::RenderTarget &renderTarget, bool respectAlpha)  const; //must be called before drawing in the drawing area to make sure nothing is drawn outside of the area. Must sometimes be done again if more than one image with alpha channels will overlap.


		virtual ~ScrollableList() override;
		bool withinScrollableList(Vector2D<int> pos)  const;//returns true if 'pos' is over the scrollable list.

		virtual void mouseMoved(bool &eventClaimed, Vector2D<int> mousePos) override;
		virtual void mouseButtonPressed(bool &eventClaimed, Vector2D<int> mousePos, sf::Mouse::Button button) override;
		virtual void mouseButtonReleased(bool &eventClaimed, Vector2D<int> mousePos, sf::Mouse::Button button) override;
		virtual void mouseScrolled(bool &eventClaimed, Vector2D<int> mousePos, int delta) override;
		virtual void keyPressed(bool &eventClaimed, sf::Keyboard::Key key) override;
		virtual void keyReleased(bool &eventClaimed, sf::Keyboard::Key key) override;
		virtual void draw(sf::RenderTarget &renderTarget, sf::Time time, bool respectAlpha = false) override;
		virtual void drawHeader(sf::RenderTarget &renderTarget, sf::Time time, bool respectAlpha) = 0;

		void setBoundingBox(Rectangle<int> box); //Sets the bounding box for the entire scrollable list.
		void setTopLeft(Vector2D<int> newTopLeft); //Sets the top left of the bounding box for the scrollable list.
		Rectangle<int> getListBoundingBox() const; //Returns the bounding box for the list with all the items, without the header.
		Rectangle<int> getHeaderBoundingBox() const; //Returns the bounding box for the header.
		void setSize(Vector2D<int> newSize); //Sets the size of the bounding box for the scrollable list.
		void setScrollerWidth(double newWidthPercentage); //Percentage of the list width.
		int getScrollerWidth() const; //Returns the width of the list scroller.

		void setAnimations(Animation &newBackground, Animation &scrollBar, Animation &scrollIndicator); //Sets the animations of the list.
		void setRowHeight(double height); //Sets the height of each row in the list as a factor of the bounding box height. Size should be > 0.0 and <= 1.0.
		void setHeaderRowHeight(double height); //Sets the height of the header row in the list as a factor of the bounding box height. Size should be > 0.0 and <= 1.0.
		void setScrollerSide(bool left); //Sets the scroller to the left side if true, otherwise to the right side.

		void setFont(sf::Font &font); //Sets the font of the text of the list.
		void setTextColour(sf::Color colour); //sets the colour of the text.
		void setSelectedTextColour(sf::Color colour); //sets the colour of the text when an item is selected.
		void setTextScale(float scale); //scales the text over the list.
		sf::Color getTextColour() const; //returns the default text colour.
		sf::Color getSelectedTextColour() const; //returns the text colour of selected items.
	};

}