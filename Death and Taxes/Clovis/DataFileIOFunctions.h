#pragma once
#include <SFML\Graphics.hpp>
#include "Shapes.h"
#include "DataFileIO.h"
namespace dat
{
	Vector2D<int> get2DVectorInt(DataFileIO &dataFileIO, std::string key);
	void store2DVectorInt(DataFileIO &dataFileIO, std::string key, Vector2D<int> value);
	Vector2D<double> get2DVectorDouble(DataFileIO &dataFileIO, std::string key);
	void store2DVectorDouble(DataFileIO &dataFileIO, std::string key, Vector2D<double> value);
	Vector3D<int> get3DVectorInt(DataFileIO &dataFileIO, std::string key);
	void store3DVectorInt(DataFileIO &dataFileIO, std::string key, Vector3D<int> value);
	Vector3D<double> get3DVectorDouble(DataFileIO &dataFileIO, std::string key);
	void store3DVectorDouble(DataFileIO &dataFileIO, std::string key, Vector3D<double> value);
	Rectangle<int> getRectangleInt(DataFileIO &dataFileIO, std::string key);
	void storeRectangleInt(DataFileIO &dataFileIO, std::string key, Rectangle<int> rect);
	Rectangle<double> getRectangleDouble(DataFileIO &dataFileIO, std::string key);
	void storeRectangleDouble(DataFileIO &dataFileIO, std::string key, Rectangle<double> rect);
	sf::Color getColor(DataFileIO &dataFileIO, std::string key);
	void storeColor(DataFileIO &dataFileIO, std::string key, sf::Color color);
	bool getBoolean(DataFileIO &dataFileIO, std::string key);
	void storeBoolean(DataFileIO &dataFileIO, std::string key, bool boolean);
	int getInt(DataFileIO &dataFileIO, std::string key);
	void storeInt(DataFileIO &dataFileIO, std::string key, int value);
	double getDouble(DataFileIO &dataFileIO, std::string key);
	void storeDouble(DataFileIO &dataFileIO, std::string key, double value);
}