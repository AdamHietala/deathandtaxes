#include "GameMain.h"
#include <SFML/Graphics.hpp>
#include "DataFileIO.h"
#include "GlobalDefines.h"
namespace dat
{
	GameMain::GameMain() : gui(*this), gameWorld(*this), camera(renderWindow, gameWorld), inputEventHandler(*this), renderer(renderWindow, gui, worldDrawer), moduleLoader(moduleOrder), 
		textureLoader(moduleLoader), fontLoader(moduleLoader), worldDrawer(animationLoader, tilesetLoader, citySpriteLoader, gameWorld, camera), playerControlState(inputEventHandler, gameWorld, camera)
	{
		programRunning = true;


	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	GameMain::~GameMain()
	{
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void GameMain::run()
	{
#ifdef FIXED_RANDOM_SEED
		srand(0);
#else
		srand((unsigned)time(0));
#endif
		GameLoader gameLoader;
		gameLoader.load(*this);
		sf::Clock clock;


		while (programRunning)
		{

			inputEventHandler.handleEvents();
			camera.calc();
			if (gameWorld.gameIsRunning())
			{
				if (clock.getElapsedTime().asSeconds() >= 0.1)
				{
					gameWorld.calcTick();
					clock.restart();
				}
			}
			renderer.draw();

		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void GameMain::closeGame()
	{
		programRunning = false;
	}
}