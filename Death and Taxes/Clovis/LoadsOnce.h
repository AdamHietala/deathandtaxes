#pragma once
#include "NonCopyable.h"

namespace dat
{
	class LoadsOnce : NonCopyable
	{
	private:
		bool loaded;

	protected:
		void load();
		LoadsOnce(void);
		~LoadsOnce(void);
	public:
		bool hasLoaded() const;
	};

}