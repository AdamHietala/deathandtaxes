#include "Recipe.h"

namespace dat
{
	Recipe::Recipe(IDType<Recipe> id, std::vector<std::pair<IDType<ResourceType>, double>> neededResources, std::vector<std::pair<IDType<ResourceType>, double>> yieldedResources, std::vector<std::pair<IDType<TechType>, double>> minimumTechLevel, std::vector<std::pair<IDType<TechType>, double>> maximumTechLevel, std::vector<IDType<Technology>> neededTechs, std::vector<IDType<Technology>> forbiddenTechs, std::vector<std::pair<IDType<InfrastructureType>, double>> minimumInfrastructureLevel, std::vector<std::pair<IDType<InfrastructureType>, double>> maximumInfrastructureLevel) :
		id(id), neededResources(neededResources), yieldedResources(yieldedResources), minimumTechLevel(minimumTechLevel), maximumTechLevel(maximumTechLevel), neededTechs(neededTechs), forbiddenTechs(forbiddenTechs), minimumInfrastructureLevel(minimumInfrastructureLevel), maximumInfrastructureLevel(maximumInfrastructureLevel)
	{
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Recipe::~Recipe()
	{
	}
}