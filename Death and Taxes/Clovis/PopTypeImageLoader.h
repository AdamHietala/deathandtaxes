#pragma once
#include "LoadsOnce.h"
#include "IDType.h"
#include <map>
#include <vector>
namespace dat
{
	class ModuleLoader;
	class PopTypeLoader;
	class PopType;
	class AnimationLoader;
	class Animation;
	class PopTypeImageLoader : public LoadsOnce
	{
	private:

		std::vector<Animation *> idToAnimation;
	public:
		PopTypeImageLoader();
		~PopTypeImageLoader();

		void load(ModuleLoader &moduleLoader, AnimationLoader const &animationLoader, PopTypeLoader const &popTypeLoader);
		Animation *getAnimation(IDType<PopType> id) const; //Returns the resource.
	};

}