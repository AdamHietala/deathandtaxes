#pragma once
#include <exception>
#include <string>
namespace dat
{
	
	class Dat_Exception : public std::exception
	{
	private:
		std::string info;
	public:
		Dat_Exception(std::string errorInfo = ""):info(errorInfo){}
		~Dat_Exception(void){}
		virtual const char* what() const throw()
		{
			return info.c_str();
		}
	};

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	class CriticalError : public Dat_Exception									
	{																			
	public:																		
		CriticalError(std::string errorInfo) : Dat_Exception("This error should never happen and if it does it should be reported, since it means there is something wrong with the exe. No normal mod can cause this. Errorinfo: " + errorInfo) {}		
	};

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define ADD_EXCEPTION_TYPE(exceptionName)											\
	class exceptionName : public Dat_Exception										\
	{																				\
	public:																			\
		exceptionName(std::string errorInfo = "") : Dat_Exception(errorInfo) {}		\
	};


	ADD_EXCEPTION_TYPE(AlreadyAtTopLevelException);
	ADD_EXCEPTION_TYPE(ParsingException);
	ADD_EXCEPTION_TYPE(IOException);
	ADD_EXCEPTION_TYPE(FileMissingException);
	ADD_EXCEPTION_TYPE(ItemNotFoundException);
	ADD_EXCEPTION_TYPE(IndexOutOfRangeException);
	ADD_EXCEPTION_TYPE(CouldNotReserveMemoryException);
	ADD_EXCEPTION_TYPE(KeyAlreadyExistsException);
	ADD_EXCEPTION_TYPE(KeyNotFoundException);
	ADD_EXCEPTION_TYPE(DifferentSizeException);
	ADD_EXCEPTION_TYPE(NoFittingTerrainException);
	ADD_EXCEPTION_TYPE(NegativeNeedException);
	ADD_EXCEPTION_TYPE(TechAlreadyAddedException);
	ADD_EXCEPTION_TYPE(NoMatchingSpriteExcepetion);
	ADD_EXCEPTION_TYPE(EndOfStringException);
	ADD_EXCEPTION_TYPE(ExecutingDeadCodeException);


#undef ADD_EXCEPTION_TYPE

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define testException(test, exception)			\
	if(!( test ))								\
	{											\
		throw exception;						\
	}

#define ASSERT_DEAD_CODE assert(false); throw ExecutingDeadCodeException("The code should never get here.");


}