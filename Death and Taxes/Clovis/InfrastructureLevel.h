#pragma once
#include <vector>
#include "IDType.h"
namespace dat
{
	class InfrastructureTypeLoader;
	class InfrastructureType;
	class InfrastructureLevel
	{
	private:
		std::vector<double> infrastrctureTypeLevels;
	public:
		InfrastructureLevel(InfrastructureTypeLoader &infrastructureTypeLoader);
		~InfrastructureLevel();

		void setInfrastructureLevel(IDType<InfrastructureType> infrastructureType, double amount);
		void addInfrastructure(IDType<InfrastructureType> infrastructureType, double amount);//if amount is negative the user must have checked that there is this much amount to take away
		double getInfrastructure(IDType<InfrastructureType> infrastructureType); //returns the infrastructure level of this type.
	};

}