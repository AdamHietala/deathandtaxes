#pragma once
#include <vector>
#include <string>
namespace dat
{
	class ModuleOrder
	{
	private:
		std::vector<std::string> loadOrder;
	public:
		ModuleOrder();
		~ModuleOrder();
		std::vector<std::string> getLoadOrder() const; //Returns the current loadorder;
		void setLoadorder(std::vector<std::string> newOrder); //Changes the loadorder to the new one.
		void load(); //Loads the loadorder from the load order file.
		void save(); //Saves the current loadorder to the load order file.
		
	};
}