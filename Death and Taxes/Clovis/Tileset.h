#pragma once
#include "IDType.h"
#include <vector>
#include "NonCopyable.h"
namespace dat
{
	class TerrainType;
	class Animation;
	class Tileset : NonCopyable
	{
	private:

	public:
		Tileset(IDType<Tileset> id, IDType<TerrainType> terrainTypeID, int priority, std::vector<IDType<Animation>> baseTextures, std::vector<IDType<Animation>> firstMasks , std::vector<IDType<Animation>> secondMasks);
		~Tileset();
		const int priority;
		const std::vector<IDType<Animation>> baseTextures, firstMasks, secondMasks;
		const IDType<Tileset> id;
		const IDType<TerrainType> terrainTypeID;
		const int nrOfVariations; //the size of baseTextures, firstMasks and secondMaks, all of which should be the same.
	};

}