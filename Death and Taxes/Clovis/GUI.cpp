#include "GUI.h"
#include <assert.h>
#include "FileStructure.h"
#include "DataFileIOFunctions.h"
#include "RectButton.h"
#include "GameMain.h"
#include "GUIMenu.h"
#include "LocalisationDefines.h"
#include "PopScrollableList.h"
#include "StringManipulation.h"
#include "ResourcePackScrollableList.h"
#include "PlayerControlState.h"
#include "Pop.h"
namespace dat
{

	GUI::GUI(GameMain &gameMain) : gameMain(gameMain)
	{
		ingameMainMenu = nullptr;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	GUI::~GUI()
	{
		for (int i = 0; i < (int)widgets.size(); ++i)
		{
			delete widgets[i];
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void GUI::addWidget(Widget *widget)
	{
		widgets.push_back(widget);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void GUI::removeWidget(Widget *widget)
	{
		
		for (int i = 0; i < (int)widgets.size(); ++i)
		{
			if (widgets[i] == widget)
			{
				delete widgets[i];
				widgets.erase(widgets.begin() + i);
				return;
			}
		}
		ASSERT_DEAD_CODE
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	RectButton *GUI::loadRectButton(GameMain &gameMain, DataFileIO &dataFileIO, std::string buttonKey, GUIMenu* menu, std::string textKey) //Loads the rectbutton and adds it to the widgetlist. Returns so the on event functions can be added.
	{
		dataFileIO.openGroup(buttonKey); //a1

		RectButton *rectButton = new RectButton;

		Vector2D<int> size = gameMain.options.screenSize;

		sf::Font &font = gameMain.fontLoader.loadFont(dataFileIO.getString("font"));
		rectButton->setFont(font);

		sf::Color colour = getColor(dataFileIO, "text colour");
		rectButton->setTextColour(colour);

		rectButton->setTextScale((float) getDouble(dataFileIO, "text scale"));

		Animation &normal = *gameMain.animationLoader.getAnimation(gameMain.animationLoader.getID(dataFileIO.getString("normal")));
		Animation &highlighted = *gameMain.animationLoader.getAnimation(gameMain.animationLoader.getID(dataFileIO.getString("highlighted")));
		Animation &clicked = *gameMain.animationLoader.getAnimation(gameMain.animationLoader.getID(dataFileIO.getString("clicked")));
		Animation &clickedHighlighted = *gameMain.animationLoader.getAnimation(gameMain.animationLoader.getID(dataFileIO.getString("clicked highlighted")));
		Animation &disabled = *gameMain.animationLoader.getAnimation(gameMain.animationLoader.getID(dataFileIO.getString("disabled")));


		rectButton->setAnimationInstances(AnimationInstance(normal), AnimationInstance(highlighted), AnimationInstance(clicked), AnimationInstance(clickedHighlighted), AnimationInstance(disabled));


		Vector2D<double> positionPercent = get2DVectorDouble(dataFileIO, "position");
		Vector2D<double> buttonSizePercent = get2DVectorDouble(dataFileIO, "size");
		
		Vector2D<int> position(int(double(size.x) * positionPercent.x), int(double(size.y) * positionPercent.y));
		Vector2D<int> buttonSize(int(double(size.x) * buttonSizePercent.x), int(double(size.y) * buttonSizePercent.y));


		rectButton->setSize(buttonSize);
		rectButton->setPos(position);

		rectButton->setHeightAlignmentString(dataFileIO.getString("text height alignment string"));

		dataFileIO.closeGroup(); //1a




		menu->addWidget(rectButton);
		rectButton->setText(gameMain.localisation.getString(textKey));

		return rectButton;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void GUI::eventHappened(sf::Event &evnt, bool &eventClaimed)
	{
		if (evnt.type == sf::Event::KeyPressed)
		{
			if (evnt.key.code == sf::Keyboard::Escape)
			{
				if (gameMain.gameWorld.gameIsRunning() && !ingameMainMenu->isActive())
				{
					ingameMainMenu->setActive(true);
					eventClaimed = true;
				}
			}
		}
		for (auto widget : widgets)
		{
			switch (evnt.type)
			{
			case sf::Event::KeyPressed:
				widget->keyPressed(eventClaimed, evnt.key.code);
				break;

			case sf::Event::KeyReleased:
				widget->keyReleased(eventClaimed, evnt.key.code);
				break;

			case sf::Event::MouseButtonPressed:
				widget->mouseButtonPressed(eventClaimed, Vector2D<int>(evnt.mouseButton.x, evnt.mouseButton.y), evnt.mouseButton.button);
				break;

			case sf::Event::MouseButtonReleased:
				widget->mouseButtonReleased(eventClaimed, Vector2D<int>(evnt.mouseButton.x, evnt.mouseButton.y), evnt.mouseButton.button);
				break;

			case sf::Event::MouseMoved:
				widget->mouseMoved(eventClaimed, Vector2D<int>(evnt.mouseMove.x, evnt.mouseMove.y));
				break;

			case sf::Event::MouseWheelScrolled:
				widget->mouseScrolled(eventClaimed, Vector2D<int>(evnt.mouseWheel.x, evnt.mouseWheel.y), evnt.mouseWheel.delta);
				break;

			default: return;
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void GUI::draw(sf::RenderTarget &renderTarget, sf::Time time, bool respectAlpha)
	{
		//setView
		sf::View oldView = renderTarget.getView();
		sf::Vector2f size(float(renderTarget.getSize().x), float(renderTarget.getSize().y));
		renderTarget.setView(sf::View(sf::Vector2f(size.x/2, size.y/2), size));


		for (auto widget : widgets)
		{
			widget->draw(renderTarget, time, respectAlpha);
		}

		renderTarget.setView(oldView);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void GUI::load()
	{
		assert(gameMain.localisation.hasLoaded());
		LoadsOnce::load();
		gameMain.inputEventHandler.addListener(this, std::numeric_limits<unsigned>::min());


		DataFileIO &dataFileIO = gameMain.moduleLoader.loadDataFileIO(filestructure::GUI_CONFIG_FOLDER_PATH);

		GUIMenu *mainMenu = new GUIMenu(gameMain.options.screenSize.typecast<float>(), Vector2D<float>(0.0f, 0.0f));
		addWidget(mainMenu);
		ingameMainMenu = new GUIMenu(gameMain.options.screenSize.typecast<float>(), Vector2D<float>(0.0f, 0.0f));
		GUIMenu *ingameMainMenu = GUI::ingameMainMenu;//declares it here as well so it can be added to the lambdas
		addWidget(ingameMainMenu);
		GUIMenu *optionsMenu = new GUIMenu(gameMain.options.screenSize.typecast<float>(), Vector2D<float>(0.0f, 0.0f));
		addWidget(optionsMenu);
		GUIMenu *newGameMenu = new GUIMenu(gameMain.options.screenSize.typecast<float>(), Vector2D<float>(0.0f, 0.0f));
		addWidget(newGameMenu);


		mainMenu->setActive(true);

		const std::string BACKGROUND_KEY = "background";
		const std::string BUTTONS_KEY = "buttons";
		RectButton *newButton;

		dataFileIO.openGroup("main menu"); //a1
		mainMenu->setBackGround(gameMain.animationLoader.getAnimation(gameMain.animationLoader.getID(dataFileIO.getString(BACKGROUND_KEY))));
		dataFileIO.openGroup(BUTTONS_KEY); //b1

		GameMain *gameMainPtr = &gameMain; //used to send gameMain to the lambdas

		newButton = loadRectButton(gameMain, dataFileIO, "quit", mainMenu, loc::GUI::QUIT_BUTTON);
		newButton->setDoOnMouseButtonReleased([gameMainPtr]() { gameMainPtr->closeGame(); });

		newButton = loadRectButton(gameMain, dataFileIO, "options", mainMenu, loc::GUI::OPTIONS_BUTTON);
		newButton->setDoOnMouseButtonReleased([mainMenu, optionsMenu, gameMainPtr]() 
		{
			mainMenu->setActive(false); 
			optionsMenu->setActive(true);
			bool eventClaimed = false;
			optionsMenu->mouseMoved(eventClaimed, Vector2D<int>(sf::Mouse::getPosition(gameMainPtr->renderWindow).x, sf::Mouse::getPosition(gameMainPtr->renderWindow).y)); //a fake event to make the menu react to the mouse being there
		});

		newButton = loadRectButton(gameMain, dataFileIO, "multiplayer", mainMenu, loc::GUI::JOIN_BUTTON);
		//newButton->setDoOnMouseButtonReleased([]() {});

		newButton = loadRectButton(gameMain, dataFileIO, "singleplayer", mainMenu, loc::GUI::NEW_GAME_BUTTON);
		newButton->setDoOnMouseButtonReleased([mainMenu, newGameMenu, gameMainPtr]() 
		{
			mainMenu->setActive(false); 
			newGameMenu->setActive(true);
			bool eventClaimed = false;
			newGameMenu->mouseMoved(eventClaimed, Vector2D<int>(sf::Mouse::getPosition(gameMainPtr->renderWindow).x, sf::Mouse::getPosition(gameMainPtr->renderWindow).y)); //a fake event to make the menu react to the mouse being there
		});


		dataFileIO.closeGroup(); //b1
		dataFileIO.closeGroup(); //a1



		dataFileIO.openGroup("options menu"); //a2
		optionsMenu->setBackGround(gameMain.animationLoader.getAnimation(gameMain.animationLoader.getID(dataFileIO.getString(BACKGROUND_KEY))));
		dataFileIO.openGroup(BUTTONS_KEY); //b2


		newButton = loadRectButton(gameMain, dataFileIO, "back", optionsMenu, loc::GUI::BACK_BUTTON);
		newButton->setDoOnMouseButtonReleased([mainMenu, optionsMenu, gameMainPtr]() 
		{
			mainMenu->setActive(true); 
			optionsMenu->setActive(false);
			bool eventClaimed = false;
			mainMenu->mouseMoved(eventClaimed, Vector2D<int>(sf::Mouse::getPosition(gameMainPtr->renderWindow).x, sf::Mouse::getPosition(gameMainPtr->renderWindow).y)); //a fake event to make the menu react to the mouse being there
		});

		newButton = loadRectButton(gameMain, dataFileIO, "graphics", optionsMenu, loc::GUI::GRAPHICS_BUTTON);
		//newButton->setDoOnMouseButtonReleased([]() {});

		newButton = loadRectButton(gameMain, dataFileIO, "sound", optionsMenu, loc::GUI::SOUND_BUTTON);
		//newButton->setDoOnMouseButtonReleased([]() {});

		newButton = loadRectButton(gameMain, dataFileIO, "gameplay", optionsMenu, loc::GUI::GAMEPLAY_BUTTON);
		//newButton->setDoOnMouseButtonReleased([]() {});

		newButton = loadRectButton(gameMain, dataFileIO, "advanced", optionsMenu, loc::GUI::ADVANCED_BUTTON);
		//newButton->setDoOnMouseButtonReleased([]() {});


		dataFileIO.closeGroup(); //b2
		dataFileIO.closeGroup(); //a2



		dataFileIO.openGroup("new game menu"); //a3
		newGameMenu->setBackGround(gameMain.animationLoader.getAnimation(gameMain.animationLoader.getID(dataFileIO.getString(BACKGROUND_KEY))));
		dataFileIO.openGroup(BUTTONS_KEY); //b3


		newButton = loadRectButton(gameMain, dataFileIO, "back", newGameMenu, loc::GUI::BACK_BUTTON);
		newButton->setDoOnMouseButtonReleased([mainMenu, newGameMenu, gameMainPtr]() 
		{
			mainMenu->setActive(true); 
			newGameMenu->setActive(false);
			bool eventClaimed = false;
			mainMenu->mouseMoved(eventClaimed, Vector2D<int>(sf::Mouse::getPosition(gameMainPtr->renderWindow).x, sf::Mouse::getPosition(gameMainPtr->renderWindow).y));//a fake event to make the menu react to the mouse being there
		});

		newButton = loadRectButton(gameMain, dataFileIO, "start", newGameMenu, loc::GUI::START_GAME_BUTTON);
		newButton->setDoOnMouseButtonReleased([newGameMenu, gameMainPtr](){
			newGameMenu->setActive(false);

			//debug //There should be another menu for generating the new game
			//gameMainPtr->gameWorld.generateNewWorld(Vector2D<int>(480, 300), hex::WrapType::Cylinder);
			//gameMainPtr->gameWorld.generateNewWorld(Vector2D<int>(240, 150), hex::WrapType::Cylinder);
			gameMainPtr->gameWorld.generateNewWorld(Vector2D<int>(48, 30), hex::WrapType::Flat);
			gameMainPtr->worldDrawer.newGame();
			//end debug

		});

		dataFileIO.closeGroup(); //b3
		dataFileIO.closeGroup(); //a3



		dataFileIO.openGroup("ingame main menu"); //a4
		ingameMainMenu->setBackGround(gameMain.animationLoader.getAnimation(gameMain.animationLoader.getID(dataFileIO.getString(BACKGROUND_KEY))));
		dataFileIO.openGroup(BUTTONS_KEY); //b4


		newButton = loadRectButton(gameMain, dataFileIO, "quit", ingameMainMenu, loc::GUI::QUIT_BUTTON);
		newButton->setDoOnMouseButtonReleased([gameMainPtr]() {gameMainPtr->closeGame(); });

		newButton = loadRectButton(gameMain, dataFileIO, "end game", ingameMainMenu, loc::GUI::END_GAME_BUTTON);
		newButton->setDoOnMouseButtonReleased([mainMenu, ingameMainMenu, gameMainPtr]() 
		{
			mainMenu->setActive(true); 
			ingameMainMenu->setActive(false); 
			gameMainPtr->gameWorld.dumpWorld();
			bool eventClaimed = false;
			mainMenu->mouseMoved(eventClaimed, Vector2D<int>(sf::Mouse::getPosition(gameMainPtr->renderWindow).x, sf::Mouse::getPosition(gameMainPtr->renderWindow).y)); //a fake event to make the menu react to the mouse being there
		});

		newButton = loadRectButton(gameMain, dataFileIO, "back", ingameMainMenu, loc::GUI::BACK_BUTTON);
		newButton->setDoOnMouseButtonReleased([ingameMainMenu, gameMainPtr]() {ingameMainMenu->setActive(false); });

		dataFileIO.closeGroup(); //b4
		dataFileIO.closeGroup(); //a4



		dataFileIO.openGroup("pop list"); //a5
		PopScrollableList *popScrollableList = new PopScrollableList(gameMain.localisation, gameMain.popTypeLoader, gameMain.playerControlState, gameMain.typeImageLoader);
		Vector2D<double> screenSize = gameMain.options.screenSize.typecast<double>();
		popScrollableList->setSize((screenSize*get2DVectorDouble(dataFileIO, "size") + 0.5).typecast<int>());
		popScrollableList->setTopLeft((screenSize*get2DVectorDouble(dataFileIO, "position") + 0.5).typecast<int>());
		popScrollableList->setTextColour(getColor(dataFileIO, "text colour"));
		popScrollableList->setSelectedTextColour(getColor(dataFileIO, "selected text colour"));
		popScrollableList->setTextScale((float)getDouble(dataFileIO, "text scale"));
		popScrollableList->setFont(gameMain.fontLoader.loadFont(dataFileIO.getString("font")));
		popScrollableList->setScrollerWidth(getDouble(dataFileIO, "scroller width"));
		popScrollableList->setRowHeight(getDouble(dataFileIO, "item size"));
		popScrollableList->setHeaderRowHeight(getDouble(dataFileIO, "header size"));
		popScrollableList->setAnimations(
			*gameMain.animationLoader.getAnimation(gameMain.animationLoader.getID(dataFileIO.getString(BACKGROUND_KEY))),
			*gameMain.animationLoader.getAnimation(gameMain.animationLoader.getID(dataFileIO.getString("scroll bar"))),
			*gameMain.animationLoader.getAnimation(gameMain.animationLoader.getID(dataFileIO.getString("indicator"))),
			*gameMain.animationLoader.getAnimation(gameMain.animationLoader.getID(dataFileIO.getString("selection overlay")))
			);
		popScrollableList->setScrollerSide(lower(dataFileIO.getString("scroller side")) == "left");
		dataFileIO.openGroup("columns"); //b1
		dataFileIO.openGroup("order"); //c1
		std::vector<std::string> orderKeys = sortStrings(dataFileIO.getStringKeys());
		std::vector<std::string> columnKeys;
		for (auto key : orderKeys)
		{
			columnKeys.push_back(dataFileIO.getString(key));
		}
		dataFileIO.closeGroup(); //c1
		double usedSizeX = 0.0;
		for (std::string key : columnKeys)
		{
			dataFileIO.openGroup(key); //c2
			Vector2D<double> size = get2DVectorDouble(dataFileIO, "size");
			if (key == "profession image")
			{
				popScrollableList->setProfessionImageBox(Rectangle<double>(Vector2D<double>(usedSizeX, 0), size));
			}
			else if (key == "profession")
			{
				popScrollableList->setProfessionBox(Rectangle<double>(Vector2D<double>(usedSizeX, 0), size));
			}
			else if (key == "population size")
			{
				popScrollableList->setPopulationBox(Rectangle<double>(Vector2D<double>(usedSizeX, 0), size));
			}
			else if (key == "politics")
			{
				popScrollableList->setPoliticsBox(Rectangle<double>(Vector2D<double>(usedSizeX, 0), size));
			}
			else
			{
				assert(false);//all alternatives should have been taken care of. Ignores any key that will not be used, when not in debug mode.
			}
			usedSizeX += size.x;
			dataFileIO.closeGroup(); //c2
		}

		dataFileIO.openGroup("politics"); //c3
		dataFileIO.openGroup("order"); //d1
		orderKeys = sortStrings(dataFileIO.getStringKeys());
		columnKeys.clear();
		for (auto key : orderKeys)
		{
			columnKeys.push_back(dataFileIO.getString(key));
		}
		dataFileIO.closeGroup(); //d1
		usedSizeX = 0.0;
		for (std::string key : columnKeys)
		{
			dataFileIO.openGroup(key); //d2
			Vector2D<double> size = get2DVectorDouble(dataFileIO, "size");
			if (key == "economic")
			{
				popScrollableList->setEconomicPoliticsBox(Rectangle<double>(Vector2D<double>(usedSizeX, 0), size));
			}
			else if (key == "governmental")
			{
				popScrollableList->setGovernmentalPoliticsBox(Rectangle<double>(Vector2D<double>(usedSizeX, 0), size));
			}
			else if (key == "personal")
			{
				popScrollableList->setPersonalPoliticsBox(Rectangle<double>(Vector2D<double>(usedSizeX, 0), size));
			}
			else
			{
				assert(false);//all alternatives should have been taken care of. Ignores any key that will not be used, when not in debug mode.
			}
			usedSizeX += size.x;
			dataFileIO.closeGroup(); //d2
		}
		dataFileIO.closeGroup(); //c2
		dataFileIO.closeGroup(); //b1

		dataFileIO.closeGroup(); //a5
		addWidget(popScrollableList);





		auto getResourcePack = [this]()->ResourcePack *
		{
			if (!gameMain.playerControlState.isPopSelected())
			{
				return nullptr;
			}
			return &gameMain.playerControlState.getSelectedPop()->getResourcePack();
		};
		ResourcePackScrollableList *resourcePackScrollableList = new ResourcePackScrollableList(gameMain.localisation, gameMain.typeImageLoader, gameMain.resourceTypeloader, gameMain.playerControlState, getResourcePack);
		dataFileIO.openGroup("pop resource list"); //a6
		screenSize = gameMain.options.screenSize.typecast<double>();
		resourcePackScrollableList->setSize((screenSize*get2DVectorDouble(dataFileIO, "size") + 0.5).typecast<int>());
		resourcePackScrollableList->setTopLeft((screenSize*get2DVectorDouble(dataFileIO, "position") + 0.5).typecast<int>());
		resourcePackScrollableList->setTextColour(getColor(dataFileIO, "text colour"));
		resourcePackScrollableList->setSelectedTextColour(getColor(dataFileIO, "selected text colour"));
		resourcePackScrollableList->setTextScale((float)getDouble(dataFileIO, "text scale"));
		resourcePackScrollableList->setFont(gameMain.fontLoader.loadFont(dataFileIO.getString("font")));
		resourcePackScrollableList->setScrollerWidth(getDouble(dataFileIO, "scroller width"));
		resourcePackScrollableList->setRowHeight(getDouble(dataFileIO, "item size"));
		resourcePackScrollableList->setHeaderRowHeight(getDouble(dataFileIO, "header size"));
		resourcePackScrollableList->setAnimations(
			*gameMain.animationLoader.getAnimation(gameMain.animationLoader.getID(dataFileIO.getString(BACKGROUND_KEY))),
			*gameMain.animationLoader.getAnimation(gameMain.animationLoader.getID(dataFileIO.getString("scroll bar"))),
			*gameMain.animationLoader.getAnimation(gameMain.animationLoader.getID(dataFileIO.getString("indicator")))
			);
		resourcePackScrollableList->setScrollerSide(lower(dataFileIO.getString("scroller side")) == "left");

		dataFileIO.openGroup("columns"); //b1
		dataFileIO.openGroup("order"); //c1
		orderKeys = sortStrings(dataFileIO.getStringKeys());
		columnKeys.clear();
		for (auto key : orderKeys)
		{
			columnKeys.push_back(dataFileIO.getString(key));
		}
		dataFileIO.closeGroup(); //c1
		usedSizeX = 0.0;
		for (std::string key : columnKeys)
		{
			dataFileIO.openGroup(key); //c2
			Vector2D<double> size = get2DVectorDouble(dataFileIO, "size");
			if (key == "type image")
			{
				resourcePackScrollableList->setResourceTypeImageBox(Rectangle<double>(Vector2D<double>(usedSizeX, 0), size));
			}
			else if (key == "type")
			{
				resourcePackScrollableList->setResourceTypeBox(Rectangle<double>(Vector2D<double>(usedSizeX, 0), size));
			}
			else if (key == "amount")
			{
				resourcePackScrollableList->setAmountBox(Rectangle<double>(Vector2D<double>(usedSizeX, 0), size));
			}
			else if (key == "price")
			{
				resourcePackScrollableList->setPriceBox(Rectangle<double>(Vector2D<double>(usedSizeX, 0), size));
			}
			else if (key == "up for sale")
			{
				resourcePackScrollableList->setUpForSaleBox(Rectangle<double>(Vector2D<double>(usedSizeX, 0), size));
			}
			else
			{
				assert(false);//all alternatives should have been taken care of. Ignores any key that will not be used, when not in debug mode.
			}
			usedSizeX += size.x;
			dataFileIO.closeGroup(); //c2
		}
		dataFileIO.closeGroup(); //b1
		dataFileIO.closeGroup(); //a6

		addWidget(resourcePackScrollableList);
	}
}