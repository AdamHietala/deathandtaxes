#include "AnimationLoader.h"
#include "ModuleLoader.h"
#include "FileStructure.h"
#include <set>
#include "DataFileIOFunctions.h"
#include "Animation.h"
#include "Exceptions.h"
#include "StringManipulation.h"
#include "TextureLoader.h"
namespace dat
{
	void AnimationLoader::addAnimation(std::string stringID, Animation *animation)
	{
		testException(stringIDToID.count(stringID) == 0, KeyAlreadyExistsException("The type with string id \"" + stringID + "\" was added twice."));
		assert(stringIDToID.size() == animation->id.getID());
		stringIDToID[stringID] = animation->id;
		iDToAnimation.push_back(animation);
		idToStringID.push_back(stringID);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	AnimationLoader::AnimationLoader()
	{
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	AnimationLoader::~AnimationLoader()
	{
		for (auto animation : iDToAnimation)
		{
			delete animation;
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void AnimationLoader::load(ModuleLoader &moduleLoader, TextureLoader &textureLoader)
	{
		LoadsOnce::load();

		DataFileIO &animationsFile = moduleLoader.loadDataFileIO(filestructure::ANIMATION_FOLDER_PATH);


		int freeID = 0;

		for (auto animationKey : sortStrings(animationsFile.getGroupKeys())) ////Sort them to make sure the number id's are always the same so that they can be sent between different clients without problem.
		{
			animationsFile.openGroup(animationKey); //a1

			int newID = freeID++;
			int frameLength = getInt(animationsFile, "framelength");

			Animation *anim = new Animation(IDType<Animation>(newID), frameLength);
			addAnimation(animationKey, anim);


			for (auto frameKey : sortStrings(animationsFile.getGroupKeys()))//Sort the frames so that they are loaded in the correct order
			{
				animationsFile.openGroup(frameKey); //a2
				std::string imagePath = animationsFile.getString("image");
				Vector2D<float> position = get2DVectorInt(animationsFile, "position").typecast<float>();
				sf::Texture &texture = textureLoader.loadTexture(imagePath);
				anim->addFrame(sf::Sprite(texture), position);



				animationsFile.closeGroup(); //a2
			}

			animationsFile.closeGroup(); //a1
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	IDType<Animation> AnimationLoader::getID(std::string stringID) const
	{
		testException(stringIDToID.count(stringID) > 0, KeyNotFoundException("The animation ID \"" + stringID + "\" could not be found."));
		return stringIDToID.at(stringID);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Animation *AnimationLoader::getAnimation(IDType<Animation> id) const
	{
		return iDToAnimation[id.getID()];
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::string AnimationLoader::getStringID(IDType<Animation> id) const
	{
		return idToStringID[id.getID()];
	}
}