#pragma once
#include <vector>
#include "LoadsOnce.h"
#include "SFML\Graphics.hpp"
namespace sf
{
	class RenderWindow;
	class Sprite;
	class Text;
}
namespace dat
{
	class GUI;
	class WorldDrawer;
	class SFML_Renderer
	{
	private:
		sf::RenderWindow &window;
		GUI &gui;
		WorldDrawer &worldDrawer;
		sf::Clock clock;
	public:
		SFML_Renderer(sf::RenderWindow &window, GUI &gui, WorldDrawer &worldDrawer);
		~SFML_Renderer();
		void draw(); //Renders the image on the screen.
		void drawLoadingScreen(sf::Sprite &background, sf::Text &text);
	};
}