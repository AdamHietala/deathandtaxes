#pragma once
#include "IDType.h"
#include <vector>
#include "NonCopyable.h"
namespace dat
{
	class Animation;
	class TerrainType : NonCopyable
	{
	private:
	public:
		TerrainType(IDType<TerrainType> id, bool water, bool unreachable, bool habitable, double minTemp, double maxTemp, double minHumidity, double maxHumidity, double minWaterHeight, double maxWaterHeight, double minHeight, double maxHeight, double minHeightDifference, double maxHeightDifference);
		~TerrainType();
		const IDType<TerrainType> id;
		const bool water;
		const bool unreachable;
		const bool habitable;
		const double minTemp;
		const double maxTemp;
		const double minHumidity;
		const double maxHumidity;
		const double minWaterHeight;
		const double maxWaterHeight;
		const double minHeight;
		const double maxHeight;
		const double minHeightDifference;
		const double maxHeightDifference;
	};
}