#include "Options.h"
#include "GameMain.h"
#include "FileStructure.h"
#include "Exceptions.h"
#include "DataFileIOFunctions.h"
namespace clv
{

	Options::Options()
	{
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Options::~Options()
	{

	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void Options::load()
	{
		DataFileIO optionsFile;
		optionsFile.loadFromFile(filestructure::OPTIONS_FILE_PATH);

		optionsFile.openGroup("graphics"); //a1
		screenSize = get2DVectorInt(optionsFile, "screen size");
		verticalSync = getBoolean(optionsFile, "vertical sync");
		frameRateLimit = getInt(optionsFile, "frame rate limit");
		std::string windowModeStr = optionsFile.getString("window mode");
		if (windowModeStr == "full")
			windowMode = WindowMode::Full;
		else if (windowModeStr == "windowed")
			windowMode = WindowMode::Windowed;
		else if (windowModeStr == "borderless")
			windowMode = WindowMode::Borderless;
		else
			throw ParsingException("\"" + windowModeStr + "\" could not be parsed into a window mode.");
		optionsFile.closeGroup(); //a1

		optionsFile.openGroup("sound"); //b1
		optionsFile.closeGroup(); //b1

		optionsFile.openGroup("gameplay"); //c1
		optionsFile.closeGroup(); //c1
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void Options::save()
	{
		DataFileIO optionsFile;
		optionsFile.addGroup("graphics"); //a1
		store2DVectorInt(optionsFile, "screen size", screenSize);
		storeBoolean(optionsFile, "vertical sync", verticalSync);
		storeInt(optionsFile, "frame rate limit", frameRateLimit);
		switch (windowMode)
		{
		case WindowMode::Borderless: optionsFile.addString("window mode", "borderless"); break;
		case WindowMode::Full: optionsFile.addString("window mode", "full"); break;
		case WindowMode::Windowed: optionsFile.addString("window mode", "windowed"); break;
		}
		optionsFile.closeGroup(); //a1

		optionsFile.addGroup("sound"); //b1
		optionsFile.closeGroup(); //b1

		optionsFile.addGroup("gameplay"); //c1
		optionsFile.closeGroup(); //c1

		optionsFile.saveToFile(filestructure::OPTIONS_FILE_PATH);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void Options::updateOptions(GameMain &gameMain)
	{
		sf::Style style;
		style = sf::Style::Close;

		gameMain.renderWindow.close();
		gameMain.renderWindow.create(sf::VideoMode(screenSize.x, screenSize.y, 32), gameMain.localisation.getString("game name"), sf::Style::Fullscreen);
		gameMain.renderWindow.setFramerateLimit(frameRateLimit);
		
	}
}