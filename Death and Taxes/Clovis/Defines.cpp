#include "Defines.h"
#include "ModuleLoader.h"
#include "FileStructure.h"
#include "DataFileIOFunctions.h"
namespace dat
{
	Defines::Defines()
	{
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Defines::~Defines()
	{
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void Defines::load(ModuleLoader &moduleLoader)
	{
		LoadsOnce::load();

		DataFileIO &definesFile = moduleLoader.loadDataFileIO(filestructure::DEFINES_FOLDER_PATH);
		definesFile.openGroup("pops"); //a1
		popFearLossFactor = getDouble(definesFile, "fear loss factor");
		popSatisfactionHappinessFactor = getDouble(definesFile, "satisfaction happiness factor");
		popBuyExtraFactor = getDouble(definesFile, "buy extra factor");
		definesFile.closeGroup();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	double Defines::getPopFearLossFactor() const
	{
		return popFearLossFactor;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	double Defines::getPopSatisfactionHappinessFactor() const
	{
		return popSatisfactionHappinessFactor;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	double Defines::getPopBuyExtraFactor() const
	{
		return popBuyExtraFactor;
	}
}