#include "TextureLoader.h"
#include "ModuleLoader.h"
#include "FileStructure.h"
#include "FileIO.h"
namespace dat
{
	TextureLoader::TextureLoader(ModuleLoader &moduleLoader) : moduleLoader(moduleLoader)
	{

	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	TextureLoader::~TextureLoader()
	{
		for (auto im : alreadyLoadedImages)
		{
			delete im.second;
		}
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	sf::Texture &TextureLoader::loadTexture(std::string defaultPath)
	{
		defaultPath = filestructure::IMAGES_FOLDER_PATH + "\\" + standardiseFilePath(defaultPath);
		if (alreadyLoadedImages.count(defaultPath) == 0)
		{
			std::string truePath = moduleLoader.getModFilePath(defaultPath);
			sf::Texture *texture = new sf::Texture();
			try
			{
				texture->loadFromFile(truePath);
				texture->setSmooth(true);
				
			}
			catch (...)
			{
				delete texture;
				throw;
			}
			alreadyLoadedImages[defaultPath] = texture;
		}
		return *alreadyLoadedImages.at(defaultPath);
		

	}
}