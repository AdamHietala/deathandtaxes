#include "ResourceTypeLoader.h"
#include "Exceptions.h"
#include "FileStructure.h"
#include "StringManipulation.h"
#include "DataFileIOFunctions.h"
#include "ResourceDefines.h"
#include "ModuleLoader.h"
namespace dat
{
	IDType<ResourceType> const ResourceTypeLoader::CURRENCY_ID = IDType<ResourceType>(0);

	void ResourceTypeLoader::addResourceType(std::string stringID, ResourceType *resourceType)
	{
		testException(stringIDToID.count(stringID) == 0, KeyAlreadyExistsException("The resourceType with string id \"" + stringID + "\" was added twice."));
		assert(stringIDToID.size() == resourceType->id.getID());
		stringIDToID[stringID] = resourceType->id;
		idToStringID.push_back(stringID);
		idToResourceType.push_back(resourceType);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	ResourceTypeLoader::ResourceTypeLoader()
	{

	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	ResourceTypeLoader::~ResourceTypeLoader()
	{
		for (auto resType : idToResourceType)
		{
			delete resType;
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void ResourceTypeLoader::load(ModuleLoader &moduleLoader)
	{
		LoadsOnce::load();


		int freeID = 1;

		
		addResourceType(CURRENCY_RESOURCE_STRING_ID, new ResourceType(CURRENCY_ID, false));//hard code in currency


		DataFileIO &resourceTypesFile = moduleLoader.loadDataFileIO(filestructure::RESOURCE_TYPES_FOLDER_PATH);


		for (auto resourceTypeKey : sortStrings(resourceTypesFile.getGroupKeys())) //Sort them to make sure the number id's are always the same so that they can be sent between different clients without problem.
		{
			resourceTypesFile.openGroup(resourceTypeKey);//a1
			bool service = getBoolean(resourceTypesFile, "service");
			IDType<ResourceType> id(freeID++);
			addResourceType(resourceTypeKey, new ResourceType(id, service));
			resourceTypesFile.closeGroup();//a1
		}


	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	IDType<ResourceType> ResourceTypeLoader::getID(std::string stringID) const
	{
		return stringIDToID.at(stringID);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::string ResourceTypeLoader::getStringID(IDType<ResourceType> id) const
	{
		return idToStringID.at(id.getID());
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	ResourceType *ResourceTypeLoader::getResource(IDType<ResourceType> id) const
	{
		return idToResourceType[id.getID()];
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	int ResourceTypeLoader::getNumberOfResourceTypes() const
	{
		return (int)stringIDToID.size();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	IDType<ResourceType> ResourceTypeLoader::getCurrencyID()
	{
		return CURRENCY_ID;
	}
}
