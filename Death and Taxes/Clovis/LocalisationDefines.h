#pragma once
#include <string>
namespace dat
{
	namespace loc
	{
		namespace Window
		{
			const std::string GAME_NAME = "game name";
		}
		namespace GUI
		{
			const std::string QUIT_BUTTON = "button quit";
			const std::string END_GAME_BUTTON = "button end game";
			const std::string OPTIONS_BUTTON = "button options";
			const std::string JOIN_BUTTON = "button join";
			const std::string NEW_GAME_BUTTON = "button new game";
			const std::string BACK_BUTTON = "button back";
			const std::string GRAPHICS_BUTTON = "button graphics";
			const std::string SOUND_BUTTON = "button sound";
			const std::string GAMEPLAY_BUTTON = "button gameplay";
			const std::string ADVANCED_BUTTON = "button advanced";
			const std::string START_GAME_BUTTON = "button start game";



			const std::string POP_LIST_POP_TYPE_HEADER = "pop list pop type header";
			const std::string POP_LIST_POPULATION_HEADER = "pop list population header";
			const std::string POP_LIST_POLITICS_HEADER = "pop list politics header";
			const std::string POP_LIST_POLITICS_GOVERNMENT_HEADER = "pop list politics government header";
			const std::string POP_LIST_POLITICS_ECONOMICS_HEADER = "pop list politics economics header";
			const std::string POP_LIST_POLITICS_PERSONAL_HEADER = "pop list politics personal header";



			const std::string RESOURCE_PACK_LIST_BUYING_KEYWORD = "resource pack buying";
			const std::string RESOURCE_PACK_LIST_SELLING_KEYWORD = "resource pack selling";
			const std::string RESOURCE_LIST_RESOURCE_NAME_HEADER = "resource list name header";
			const std::string RESOURCE_LIST_AMOUNT_HEADER = "resource list amount header";
			const std::string RESOURCE_LIST_PRICE_HEADER = "resource list price header";
			const std::string RESOURCE_LIST_UP_FOR_SALE_HEADER = "resource list up for sale header";
		}
		namespace Ideology
		{
			const std::string IDEOLOGY = "ideology";
			const std::string ECONOMIC = "economic";
			const std::string GOVERNMENTAL = "governmental";
			const std::string PERSONAL = "personal";
		}
	}
}