#include "ResourcePackScrollableList.h"
#include <assert.h>
#include "PlayerControlState.h"
#include "City.h"
#include "ResourcePack.h"
#include "TypeImageLoader.h"
#include "AnimationInstance.h"
#include "GeneralFunctions.h"
#include "ResourceTypeLoader.h"
#include "Localisation.h"
#include "StringManipulation.h"
#include "LocalisationDefines.h"
namespace dat
{
	ResourcePackScrollableList::ResourceInfo::ResourceInfo(ResourcePackScrollableList &resourcePackScrollableList, IDType<ResourceType> resourceID) :
		resourcePackScrollableList(resourcePackScrollableList), resourceID(resourceID)
	{
		
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	ResourcePackScrollableList::ResourceInfo::~ResourceInfo()
	{

	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void ResourcePackScrollableList::ResourceInfo::draw(sf::RenderTarget &renderTarget, sf::Time time, bool respectAlpha)
	{

		if (!isActive())
		{
			return;
		}

		resourcePackScrollableList.prepareItemsDrawingArea(renderTarget, respectAlpha);

		assert(resourcePackScrollableList.getResourcePack() != nullptr);
		ResourcePack &resourcePack = *resourcePackScrollableList.getResourcePack();

		Animation *resourceTypeAnimation = resourcePackScrollableList.typeImageLoader.getResourceTypeImageLoader().getAnimation(resourceID);
		sf::Color textColour = resourcePackScrollableList.getTextColour();

		AnimationInstance resourceTypeAnimationInstance(*resourceTypeAnimation);
		Rectangle<float> imageRect = getBoundingBox().typecast<double>().translate(resourcePackScrollableList.resourceTypeImageBox).typecast<float>();
		Vector2D<float> imageDimensions(min(imageRect.dimensions.x, imageRect.dimensions.y), min(imageRect.dimensions.x, imageRect.dimensions.y));//assumes the image is a square
		resourceTypeAnimationInstance.scale(imageDimensions / resourceTypeAnimationInstance.getSize());
		resourceTypeAnimationInstance.setPosition(imageRect.topLeft + imageRect.dimensions / 2.0f);
		resourceTypeAnimationInstance.draw(renderTarget, time, renderStatesFilterAlphaThroughAlpha);
		resourceTypeAnimationInstance.draw(renderTarget, time, renderStatesFilterThroughAlpha);



		resourcePackScrollableList.textBox.setTextColour(textColour);
		std::string textString;
		Rectangle<int> textBoundingBox;

		textString = resourcePackScrollableList.localisation.getString(resourcePackScrollableList.resourceTypeLoader.getStringID(resourceID));
		resourcePackScrollableList.textBox.setText(textString);
		textBoundingBox = getBoundingBox().typecast<double>().translate(resourcePackScrollableList.resourceTypeBox).typecast<int>();
		resourcePackScrollableList.textBox.setBoundingBox(textBoundingBox);
		resourcePackScrollableList.textBox.draw(renderTarget, time, true);

		textString = toString(resourcePack.getResourceAmount(resourceID), 0);
		resourcePackScrollableList.textBox.setText(textString);
		textBoundingBox = getBoundingBox().typecast<double>().translate(resourcePackScrollableList.amountBox).typecast<int>();
		resourcePackScrollableList.textBox.setBoundingBox(textBoundingBox);
		resourcePackScrollableList.textBox.draw(renderTarget, time, true);

		if (resourceID == resourcePackScrollableList.resourceTypeLoader.getCurrencyID())
		{
			textString = "-";
		}
		else
		{
			textString = toString(abs(resourcePack.getPrice(resourceID)), 0);
		}
		resourcePackScrollableList.textBox.setText(textString);
		textBoundingBox = getBoundingBox().typecast<double>().translate(resourcePackScrollableList.priceBox).typecast<int>();
		resourcePackScrollableList.textBox.setBoundingBox(textBoundingBox);
		resourcePackScrollableList.textBox.draw(renderTarget, time, true);

		if (resourceID == resourcePackScrollableList.resourceTypeLoader.getCurrencyID())
		{
			textString = "-";
		}
		else
		{
			textString = resourcePackScrollableList.localisation.getString((resourcePack.getUpForSale(resourceID) < 0) ? loc::GUI::RESOURCE_PACK_LIST_BUYING_KEYWORD : loc::GUI::RESOURCE_PACK_LIST_SELLING_KEYWORD);
			textString += ": ";
			textString += toString(resourcePack.getUpForSale(resourceID), 0);
		}
		resourcePackScrollableList.textBox.setText(textString);
		textBoundingBox = getBoundingBox().typecast<double>().translate(resourcePackScrollableList.upForSaleBox).typecast<int>();
		resourcePackScrollableList.textBox.setBoundingBox(textBoundingBox);
		resourcePackScrollableList.textBox.draw(renderTarget, time, true);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void ResourcePackScrollableList::drawHeader(sf::RenderTarget &renderTarget, sf::Time time, bool respectAlpha)
	{
		prepareHeaderDrawingArea(renderTarget, respectAlpha);
		assert(getResourcePack() != nullptr);


		textBox.setTextColour(getTextColour());
		std::string textString;
		Rectangle<int> textBoundingBox;

		textString = localisation.getString(loc::GUI::RESOURCE_LIST_RESOURCE_NAME_HEADER);
		textBox.setText(textString);
		textBoundingBox = getHeaderBoundingBox().typecast<double>().translate(resourceTypeBox).typecast<int>();
		textBox.setBoundingBox(textBoundingBox);
		textBox.draw(renderTarget, time, true);

		textString = localisation.getString(loc::GUI::RESOURCE_LIST_AMOUNT_HEADER);
		textBox.setText(textString);
		textBoundingBox = getHeaderBoundingBox().typecast<double>().translate(amountBox).typecast<int>();
		textBox.setBoundingBox(textBoundingBox);
		textBox.draw(renderTarget, time, true);

		textString = localisation.getString(loc::GUI::RESOURCE_LIST_PRICE_HEADER);
		textBox.setText(textString);
		textBoundingBox = getHeaderBoundingBox().typecast<double>().translate(priceBox).typecast<int>();
		textBox.setBoundingBox(textBoundingBox);
		textBox.draw(renderTarget, time, true);

		textString = localisation.getString(loc::GUI::RESOURCE_LIST_UP_FOR_SALE_HEADER);
		textBox.setText(textString);
		textBoundingBox = getHeaderBoundingBox().typecast<double>().translate(upForSaleBox).typecast<int>();
		textBox.setBoundingBox(textBoundingBox);
		textBox.draw(renderTarget, time, true);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	bool ResourcePackScrollableList::menuOpen() const
	{
		return isActive() && (getResourcePack() != nullptr);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	ResourcePackScrollableList::ResourcePackScrollableList(Localisation &localisation, TypeImageLoader &typeImageLoader, ResourceTypeLoader &resourceTypeLoader, PlayerControlState &playerControlState, std::function<ResourcePack*(void)> getResourcePack) :
		localisation(localisation), typeImageLoader(typeImageLoader), playerControlState(playerControlState), resourceTypeLoader(resourceTypeLoader), getResourcePack(getResourcePack)
	{
		
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	ResourcePackScrollableList::~ResourcePackScrollableList()
	{

	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void ResourcePackScrollableList::draw(sf::RenderTarget &renderTarget, sf::Time time, bool respectAlpha)
	{
		if (!menuOpen())
		{
			return;
		}

		clearList();
		ResourcePack* resourcePack = getResourcePack();
		for (int i = 0; i <resourcePack->getNrOfResourceTypes(); ++i)
		{
			ResourceInfo *newResourceInfo = new ResourceInfo(*this, IDType<ResourceType>(i));
			listItems.push_back(newResourceInfo);
		}
		ScrollableList::draw(renderTarget, time, respectAlpha);
	}

	void ResourcePackScrollableList::setResourceTypeImageBox(Rectangle<double> box)
	{
		resourceTypeImageBox = box;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void ResourcePackScrollableList::setResourceTypeBox(Rectangle<double> box)
	{
		resourceTypeBox = box;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void ResourcePackScrollableList::setAmountBox(Rectangle<double> box)
	{
		amountBox = box;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void ResourcePackScrollableList::setPriceBox(Rectangle<double> box)
	{
		priceBox = box;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void ResourcePackScrollableList::setUpForSaleBox(Rectangle<double> box)
	{
		upForSaleBox = box;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}