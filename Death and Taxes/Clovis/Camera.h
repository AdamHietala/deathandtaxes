#pragma once
#include "VectorXD.h"
#include <SFML/Graphics.hpp>
namespace sf
{
	class RenderWindow;
}
namespace dat
{
	class GameWorld;
	class Camera
	{
	private:
		float zoom, zoomSpeed;
		Vector2D<float> focus, speed;
		static float getNewSpeed(float currentspeed, float frictionFactor, float frameTime);
		sf::RenderWindow &renderWindow;
		GameWorld &gameWorld;
		void limitCamera(); //Makes sure the camera focus does not go to far so that the player can't find their way back or the numbers get too high and changes get rounded away.
		sf::Clock clock;
	public:
		Camera(sf::RenderWindow &renderWindow, GameWorld &gameWorld);
		~Camera();
		static const float HEX_RADIUS;

		Vector2D<float> getFocus() const; //Returns the current camera focus.
		void reset(); //Resets the camera focus and zoom factor.
		void move(Vector2D<float> newSpeed); //Accelerates the camera to newSpeed.
		void setFocus(Vector2D<float> newFocus); //Moves the camera instantly to the new focus.
		void setZoomSpeed(float newSpeed); //Accelerates the zoom speed to newSpeed.
		void accelerateZoomSpeed(float amount); //Accelerates the zoom speed with amount.
		void setZoom(float newZoom); //Sets the zoom factor instantly to newZoom.
		void calc(); //Calculates the new zoom level and focus of the camera depending on what speed they're moving with.
		Vector2D <float> getScreenPosition(Vector2D<float> pos) const; //Calculates where something should be on the screen depending on the camera's focus and zoom.
		Vector2D <float> getRealPosition(Vector2D<float> screenPos) const; //Calculates where something is from where it is on the screen.
		float getScreenSize(float size) const; //Calculates the screen size of somethnig depending on the cameras zoom.
		float getRealSize(float screenSize) const; //Calculates the size of something if it has size screenSize on the screen now.
	};

}