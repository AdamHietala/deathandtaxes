#pragma once
#include <SFML\Graphics.hpp>
#include "NonCopyable.h"
#include "ModuleLoader.h"
namespace dat
{
	class FontLoader : NonCopyable
	{
	private:
		ModuleLoader &moduleLoader;
		std::map<std::string, sf::Font *> alreadyLoadedFonts;
	public:
		FontLoader(ModuleLoader &moduleLoader);
		~FontLoader();
		sf::Font &loadFont(std::string defaultPath); //Loads this font if it's not already loaded.
	};
}