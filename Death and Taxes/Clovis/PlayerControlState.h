#pragma once
#include "InputEventListener.h"
#include "NonCopyable.h"
#include "VectorXD.h"
#include "IDType.h"
namespace dat
{
	class Pop;
	class PopType;
	class City;
	class GameWorld;
	class InputEventHandler;
	class Camera;
	class PlayerControlState : public InputEventListener, public NonCopyable
	{
	public:
		class SelectedPop//contains all the data unique for the currently selected pop, so that the getSelectedPop function can find it in the datastructure and check if the pointer is still valid, since it may be deleted at any moment
		{
		public: //debug missing culture and religion
			SelectedPop();
			~SelectedPop();
			Pop *pop;
			IDType<PopType> popTypeID;
		};
	private:
		Camera &camera;
		GameWorld &gameWorld;

		bool tileSelected;
		Vector2D<int> selectedTile;

		SelectedPop selectedPop;
		
	public:
		PlayerControlState(InputEventHandler &inputEventHandler, GameWorld &gameWorld, Camera &camera);
		~PlayerControlState();
		virtual void eventHappened(sf::Event &evnt, bool &eventClaimed) override; //assumes it was added last, and is last to get all events.
		void reset();
		void unselectTile(); //Sets the selected tile to no tile.
		bool isTileSelected() const; //returns true if there is a tile selected.
		Vector2D<int> getSelectedTile() const; //returns the coordinates of the selected tile. Undefined when no tile is selected.

		bool isCitySelected() const; //returns true if a city is currently selected.
		City *getSelectedCity() const; //returns the selected city. If no city is selected it returns a nullptr.

		void selectPop(Pop *pop); //Selects a specific pop.
		void unselectPop(); //Sets the selected pop to no pop.
		bool isPopSelected(); //returns true if the player has a pop selected.
		bool isThisPopSelected(Pop *pop) const; //returns true if the pop selected by the player is this pop. Returns false if not or no pop is selected.
		Pop *getSelectedPop(); //returns the pop currently selected by the player. If no pop is selected it returns a nullptr.
	};

}