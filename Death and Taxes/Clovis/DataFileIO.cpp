#include "DataFileIO.h"
#include <assert.h>
#include <functional>
#include "Exceptions.h"
#include "FileIO.h"
#include "StringManipulation.h"
#include "DataFileIODefines.h"
#include "ThreadScheduler.h"
#include <iostream>
namespace dat
{
	DataFileIO::Group::Group(Group *up) : up(up)
	{
		subGroupsLoading = 0;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	DataFileIO::Group::~Group()
	{
		assert(subGroupsLoading == 0);


		for (auto subgroup : subGroups)
		{
			delete subgroup.second;
		}
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::string DataFileIO::toString(std::string option, Group *group, std::string indentation)
	{
		if (option != "")
		{
			option += " ";
		}
		std::string output;
		for (std::pair<std::string, std::string> str : group->strings)
		{
			output += indentation + option + TYPE_STRING + " " + escapeSpecialCharacters(str.first) + " = {"
				+ escapeSpecialCharacters(str.second);
			output += "}\n";
		}

		for (std::pair<std::string, Group *> sub : group->subGroups)
		{
			std::string subStr = toString(option, sub.second, indentation + "	");
			if (subStr != "")
			{
				subStr = "\n" + subStr + indentation;
			}
			output += indentation + option + TYPE_GROUP + " " + escapeSpecialCharacters(sub.first) + " = {" + subStr + "}\n";
		}
		return output;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::getNextCommandType(std::string& str, int& i, int to, Option& option, Type& type, std::string& keyword)
	{
		std::string optionStr, typeStr;
		getNextWord(str, optionStr, i, to, false);
		if (optionStr == "")
		{
			throw EndOfStringException();
		}
		try {
			option = stringToOption(optionStr);
			getNextWord(str, typeStr, i, to, false);
		}
		catch (ParsingException)
		{
			option = Option::StrongAdd;
			typeStr = optionStr;
		}
		testException(typeStr != "", ParsingException("Type keyword was interpreted as empty."));
		testException(typeStr == TYPE_GROUP || typeStr == TYPE_STRING, ParsingException("\"" + typeStr + "\" could not be interpreted as a type."));

		getNextWord(str, keyword, i, to, true);
		testException(keyword != "", ParsingException("Key was interpreted as empty."));

		type = stringToType(typeStr);
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::parseValue(std::string& str, int& i, int& endBracket, int to)
	{
		std::string nextWord;
		getNextWord(str, nextWord, i, to, false);
		testException(nextWord == "=", ParsingException("Command followed by \"" + nextWord + "\" instead of \"={\""));
		getNextWord(str, nextWord, i, to, false);
		testException(nextWord == "{", ParsingException("Command followed by \"" + nextWord + "\" instead of \"={\""));





		endBracket = findEndBracket(str, i - 1, to);

		assert(str[i - 1] == '{');
		assert(str[endBracket] == '}');
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::addStringCommand(std::string& str, Group* group, int& i, Option option, std::string &keyword, int endBracket)
	{
		std::lock_guard<std::mutex> lock(group->loadingMutex);

		if (option == Option::StrongAdd || group->strings.count(keyword) == 0)
		{
			group->strings[keyword] = parseEscapeSequences(getSubString(str, i, endBracket));
		}
		i = endBracket + 1;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::addGroupCommand(std::string& str, Group* group, int &i, Option option, std::string& keyword, int endBracket, ThreadScheduler* threadScheduler)
	{
		std::lock_guard<std::mutex> lock(group->loadingMutex);

		Group* subGroup = nullptr;

		bool load;
		if (group->subGroups.count(keyword) == 0)
		{
			subGroup = new Group(group);
			group->subGroups[keyword] = subGroup;
			load = true;
		}
		else if (option == Option::StrongAdd)
		{
			subGroup = group->subGroups.at(keyword);
			load = true;
		}
		else
		{
			load = false;
		}


		if (load)
		{
			if (threadScheduler != nullptr)
			{
				group->subgroupsLoadingMutex.lock();
				group->subGroupsLoading += 1;
				group->subgroupsLoadingMutex.unlock();

				threadScheduler->scheduleWork([this, &str, group, subGroup, i, endBracket, threadScheduler](void* dummy)
				{
					loadFromString(str, subGroup, i, endBracket, threadScheduler);

					group->subgroupsLoadingMutex.lock();
					group->subGroupsLoading -= 1;
					group->subgroupsLoadingMutex.unlock();
				}, nullptr);
			}
			else
			{
				loadFromString(str, subGroup, i, endBracket, threadScheduler);
			}
		}

		i = endBracket + 1;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::clearStringCommand(Group* group, std::string &keyword)
	{
		std::lock_guard<std::mutex> lock(group->loadingMutex);

		testException(group->strings.count(keyword) == 1, KeyNotFoundException("The string \"" + keyword + "\" could not be cleared since it doesn't exist."));
		group->strings[keyword] = "";
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::clearGroupCommand(Group* group, std::string &keyword, ThreadScheduler* threadScheduler)
	{
		threadScheduler->waitWhileCondition([group]()->bool
		{
			std::lock_guard<std::mutex> lock(group->loadingMutex);
			return group->subGroupsLoading > 0;
		}, true);

		std::lock_guard<std::mutex> lock(group->loadingMutex);

		testException(group->subGroups.count(keyword) == 1, KeyNotFoundException("The subgroup \"" + keyword + "\" could not be cleared since it doesn't exist."));
		group->subGroups[keyword]->strings.clear();
		for (auto sub : group->subGroups[keyword]->subGroups)
		{
			delete sub.second;
		}
		group->subGroups[keyword]->subGroups.clear();
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::deleteStringCommand(Group* group, std::string& keyword)
	{
		std::lock_guard<std::mutex> lock(group->loadingMutex);

		testException(group->strings.count(keyword) == 1, KeyNotFoundException("The string \"" + keyword + "\" could not be deleted since it doesn't exist."));
		group->strings.erase(keyword);
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::deleteGroupCommand(Group *group, std::string& keyword, ThreadScheduler* threadScheduler)
	{
		threadScheduler->waitWhileCondition([group]()->bool
		{
			std::lock_guard<std::mutex> lock(group->loadingMutex);
			return group->subGroupsLoading > 0;
		}, true);

		std::lock_guard<std::mutex> lock(group->loadingMutex);

		testException(group->subGroups.count(keyword) == 1, KeyNotFoundException("The subgroup \"" + keyword + "\" could not be deleted since it doesn't exist."));
		group->subGroups[keyword]->strings.clear();
		delete group->subGroups.at(keyword);
		group->subGroups.erase(keyword);
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::loadFromString(std::string &str, Group *group, int from, int to, ThreadScheduler *threadScheduler) //Loads this group from a string and all subgroups recursively.
	{
		assert(to <= (int) str.size());
		int i = from;

		try
		{
			while (i < to)
			{

				std::string keyword;
				Option option;
				Type type;
				getNextCommandType(str, i, to, option, type, keyword);


				if (option == Option::StrongAdd || option == Option::WeakAdd)
				{
					int endBracket;
					parseValue(str, i, endBracket, to);

					if (type == Type::String)
					{
						addStringCommand(str, group, i, option, keyword, endBracket);
					}
					else
					{
						assert(type == Type::Group);
						addGroupCommand(str, group, i, option, keyword, endBracket, threadScheduler);
					}
				}
				else if (option == Option::Clear)
				{
					if (type == Type::String)
					{
						clearStringCommand(group, keyword);
					}
					else
					{
						assert(type == Type::Group);
						clearGroupCommand(group, keyword, threadScheduler);
					}
				}
				else
				{
					assert(option == Option::Delete);
					if (type == Type::String)
					{
						deleteStringCommand(group, keyword);
					}
					else
					{
						assert(type == Type::Group);
						deleteGroupCommand(group, keyword, threadScheduler);
					}
				}
			}
		}
		catch(EndOfStringException e)
		{
			//sub functions will throw this to break the main parsing loop.
		}


	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	int DataFileIO::findEndBracket(std::string &str, int from, int to) 
	{
		assert(str[from] == '{');
		int count = 1;
		bool escaped = false, commentedOut = false;
		for (int i = from + 1; i < to; ++i)
		{
			assert(!(escaped && commentedOut));
			if (escaped)
			{
				escaped = false;
			}
			else if (commentedOut)
			{
				if (str[i] == '\n')
				{
					commentedOut = false;
				}
			}
			else
			{
				if (str[i] == '#')
				{
					commentedOut = true;
				}
				else if (str[i] == '\\')
				{
					escaped = true;
				}
				else
				{
					if (str[i] == '{')
					{
						++count;
					}
					else if (str[i] == '}')
					{
						--count;
						if (count == 0)
						{
							return i;
						}
					}
				}
			}
		}
		throw(ParsingException("No endbracket could be found."));
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::getNextWord(std::string &str, std::string &word, int &from, int to, bool key) //Returns the next word while parsing a load from string. Will make whatever variable put in "from" to tick towards to.
	{
		word = "";
		while (true)//Search for next word, first by skipping whitespaces.
		{
			if (from >= to) //End of str
			{
				return;
			}
			else if (!isSpace(str[from]))
			{
				if (str[from] == '#')
				{
					from = findFirstOccurrence(str, "\n", from);
					assert(from >= to || str[from] == '\n');
					if (from >= to) //End of str
					{
						return;
					}
				}
				else
				{
					break;
				}
			}
			else
			{
				++from;
			}
		}
		if (from >= to) //End of str
		{
			throw(ParsingException("Unexpected end of string."));
		}
		else if (str[from] == '=' || str[from] == '{')
		{
			word += str[from];
			++from;
			return;
		}

		
		while (true) //Read the word
		{
			if (from == to) //End of str
			{
				return;
			}

			if ((!isSpace(str[from]) && str[from] != '=' )|| (key && isSpace(str[from]) && str[from] != '\n'))
			{
				if (str[from] == '\\')
				{
					std::string character = parseEscapeSequences(getSubString(str, from, from + 2));
					word += character;
					from += 2;
				}
				else
				{
					word += str[from];
					++from;
				}
			}
			else
			{
				if (key)
				{
					word = trimWhiteSpaces(word);
				}
				return;
			}
		}
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::string DataFileIO::escapeSpecialCharacters(std::string &str)
	{
		std::string newStr;
		for (auto chr : str)
		{
			switch (chr)
			{
			case '\n':
				newStr += "\\n";
				break;

			case '\\':
			case '{':
			case '}':
			case '#':
				newStr += "\\";
			default: 
				newStr += chr;
			}
		}
		return newStr;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::string DataFileIO::parseEscapeSequences(std::string &str)
	{
		std::string newStr;
		bool escaped = false;
		for (auto chr : str)
		{
			if (escaped)
			{
				switch (chr)
				{
				case 'n':
					newStr += "\n";
					break;

				case '\\':
				case '{':
				case '}':
				case '#':
					newStr += chr;
					break;

				default: 
					newStr += "\\" + chr;
				}
				escaped = false;
			}
			else if (chr == '\\')
			{
				escaped = true;
			}
			else
			{
				newStr += chr;
			}
		}
		return newStr;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::loadFromStringCompressed(std::string str, ThreadScheduler *threadScheduler)
	{

		assert(anchor.strings.size() == 0 && anchor.subGroups.size() == 0);//this does not support loading several files

		std::string newStr;
		for (int i = 0; i < (int)str.size(); ++i)
		{
			if (str[i] == '\\')
			{
				i += 1;
				assert(i < (int)str.size());
				if (str[i] == 'n')
				{
					newStr += '\n';
				}
				else
				{
					assert(str[i] == '\\');
					newStr += '\\';
				}
			}
			else
			{
				newStr += str[i];
			}
		}
		loadFromStringCompressed(newStr, &anchor, 0, threadScheduler);
		if (threadScheduler != nullptr)
		{
			threadScheduler->waitForWorkToFinish();
		}
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::string DataFileIO::toCompressedString(Group *group)
	{
		std::string str;
		
		std::vector<std::string> stringLines;
		std::vector<std::string> groupLines;

		auto escapeChars = [](std::string str)
		{
			return replaceStr(replaceStr(str, "\\", "\\\\"), "\n", "\\n");
		};

		for (auto pair : group->strings)
		{
			stringLines.push_back(escapeChars(pair.first));
			stringLines.push_back(escapeChars(pair.second));
		}

		for (auto pair : group->subGroups)
		{
			groupLines.push_back(escapeChars(pair.first));
			std::string subGroupString = toCompressedString(pair.second);
			assert(subGroupString[subGroupString.size() - 1] == '\n');
			subGroupString.pop_back();
			groupLines.push_back(std::to_string(subGroupString.size()));
			groupLines.push_back(subGroupString);
		}
		assert(stringLines.size() % 2 == 0);
		str += std::to_string(stringLines.size() / 2) + "\n";
		assert(groupLines.size() % 3 == 0);
		str += std::to_string(groupLines.size() / 3) + "\n";

		for (std::string &line : stringLines)
		{
			str += line + "\n";
		}

		for (std::string &line : groupLines)
		{
			str += line + "\n";
		}
		return str;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::loadFromStringCompressed(std::string &str, Group *group, int from, ThreadScheduler *threadScheduler)
	{
		std::vector<Group *> groupsToBeLoaded;
		std::vector<int> groupsFrom;
		std::vector<int> groupsTo;
		groupsToBeLoaded.push_back(group);
		groupsFrom.push_back(from);

		for (int i = 0; i < (int)groupsToBeLoaded.size(); ++i)
		{
			Group *thisGroup = groupsToBeLoaded[i];
			int index = groupsFrom[i];

			int endOfNextWord = findFirstOccurrence(str, "\n", index);
			int numberOfStrings = atoi(getSubString(str, index, endOfNextWord).c_str());
			index = endOfNextWord + 1;

			endOfNextWord = findFirstOccurrence(str, "\n", index);
			int numberOfGroups = atoi(getSubString(str, index, endOfNextWord).c_str());
			index = endOfNextWord + 1;

			for (int q = 0; q < numberOfStrings; ++q) //load strings
			{
				endOfNextWord = findFirstOccurrence(str, "\n", index);
				std::string key = getSubString(str, index, endOfNextWord);
				index = endOfNextWord + 1;

				endOfNextWord = findFirstOccurrence(str, "\n", index);
				std::string string = getSubString(str, index, endOfNextWord);
				index = endOfNextWord + 1;


				thisGroup->strings[key] = string; //strong add
			}

			for (int q = 0; q < numberOfGroups; ++q) //load groups
			{
				endOfNextWord = findFirstOccurrence(str, "\n", index);
				std::string key = getSubString(str, index, endOfNextWord);
				index = endOfNextWord + 1;

				endOfNextWord = findFirstOccurrence(str, "\n", index);
				int groupStringSize = atoi(getSubString(str, index, endOfNextWord).c_str());
				index = endOfNextWord + 1;

				Group *subGroup;
				if (thisGroup->subGroups.count(key) == 0) //strong add
				{
					subGroup = new Group(thisGroup);
					thisGroup->subGroups[key] = subGroup;
				}
				else
				{
					subGroup = thisGroup->subGroups[key];
				}

				if (threadScheduler != nullptr && threadScheduler->getWorkSize() <= threadScheduler->getNumberOfThreads() * 2)
				{
					auto loadGroup = [this, &str, subGroup, index, groupStringSize, threadScheduler](void *ptr)
					{
						loadFromStringCompressed(str, subGroup, index, threadScheduler);
					};
					threadScheduler->scheduleWork(loadGroup, nullptr);
				}
				else
				{
					groupsToBeLoaded.push_back(subGroup);
					groupsFrom.push_back(index);
					groupsTo.push_back(index);
				}
				index += groupStringSize + 1;
			}
		}

	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	DataFileIO::DataFileIO() : anchor(nullptr) //This is the top Group and has nothing above it.
	{
		openedGroup = &anchor;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	DataFileIO::~DataFileIO()
	{

	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::string DataFileIO::optionToString(Option option)
	{
		switch (option)
		{
		case Option::StrongAdd: return OPTION_STRONG_ADD;
		case Option::Clear: return OPTION_CLEAR;
		case Option::Delete: return OPTION_DELETE;
		case Option::WeakAdd: return OPTION_WEAK_ADD;
		}

		ASSERT_DEAD_CODE
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	DataFileIO::Option DataFileIO::stringToOption(std::string& str)
	{
		auto ops = { Option::StrongAdd, Option::Clear, Option::Delete, Option::WeakAdd };
		for (auto op : ops)
		{
			if (str == optionToString(op))
			{
				return op;
			}
		}
		throw (ParsingException("\"" + str + "\" could not be parsed into an option."));
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::string DataFileIO::typeToString(Type type)
	{
		switch (type)
		{
		case Type::Group: return TYPE_GROUP;
		case Type::String: return TYPE_STRING;
		}

		ASSERT_DEAD_CODE
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	DataFileIO::Type DataFileIO::stringToType(std::string& str)
	{
		auto types = { Type::String, Type::Group };
		for (auto type : types)
		{
			if (str == typeToString(type))
			{
				return type;
			}
		}
		throw (ParsingException("\"" + str + "\" could not be parsed into an option."));
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::addGroup(std::string key)
	{
		testException(!existsGroup(key), KeyAlreadyExistsException("Group key: " + key));
		openedGroup->subGroups[key] = new Group(openedGroup);
		openedGroup = openedGroup->subGroups[key];
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::addString(std::string key, std::string content)
	{
		testException(!existsString(key), KeyAlreadyExistsException("String key: " + key));
		openedGroup->strings[key] = content;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::removeGroup(std::string key)
	{
		testException(existsGroup(key), KeyNotFoundException("Group key: " + key));
		openedGroup->subGroups.erase(key);
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::removeString(std::string key) //Removes the string with the name key. Throws KeyNotFoundException if the key does not exist.
	{
		testException(existsString(key), KeyNotFoundException("String key: " + key));
		openedGroup->strings.erase(key);
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::clearGroup(std::string key)
	{
		testException(existsGroup(key), KeyNotFoundException("Group key: " + key));

		Group *subgroup = openedGroup->subGroups[key];
		subgroup->strings.clear();
		subgroup->subGroups.clear();
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::clearString(std::string key)
	{
		testException(existsString(key), KeyNotFoundException("String key: " + key));
		openedGroup->strings[key] = "";
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	bool DataFileIO::existsGroup(std::string key)
	{
		return openedGroup->subGroups.count(key) != 0;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	bool DataFileIO::existsString(std::string key)
	{
		return openedGroup->strings.count(key) != 0;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	void DataFileIO::openGroup(std::string key)
	{
		testException(existsGroup(key), KeyNotFoundException("Group key '" + key + "' not found."));
		openedGroup = openedGroup->subGroups.at(key);
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::closeGroup()
	{

		testException(openedGroup->up != nullptr, AlreadyAtTopLevelException());
		openedGroup = openedGroup->up;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::string DataFileIO::getString(std::string key)
	{
		testException(existsString(key), KeyNotFoundException("String key '" + key + "' not found."));
		return openedGroup->strings.at(key);
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::vector<std::string> DataFileIO::getStringKeys()
	{
		std::vector<std::string> keys;
		for (auto group : openedGroup->strings)
		{
			keys.push_back(group.first);
		}
		return keys;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::vector<std::string> DataFileIO::getGroupKeys()
	{
		std::vector<std::string> keys;
		for (auto group : openedGroup->subGroups)
		{
			keys.push_back(group.first);
		}
		return keys;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::loadFromString(std::string str, ThreadScheduler *threadScheduler)
	{
		loadFromString(str, &anchor, 0, str.size(), threadScheduler);
		if (threadScheduler != nullptr)
		{
			threadScheduler->waitForWorkToFinish();
		}
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::string DataFileIO::toString(bool strong) //Creates a string representing this DataFileIO from the data it contains to be loaded in another dataFileIO or saved in a file.
	{
		return trimWhiteSpaces(toString(strong ? "" : optionToString(Option::WeakAdd), &anchor));
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::string DataFileIO::toCompressedString()
	{
		return toCompressedString(&anchor);
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::loadFromFile(std::string filePath, ThreadScheduler *threadScheduler)
	{
		std::string fileContent = trimWhiteSpaces(getFileContent(filePath));
		loadFromString(fileContent, threadScheduler);
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::loadFromFileCompressed(std::string filePath, ThreadScheduler *threadScheduler)
	{
		std::string fileContent = trimWhiteSpaces(getFileContent(filePath));
		loadFromStringCompressed(fileContent, threadScheduler);
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::saveToFile(std::string filePath, bool strong)
	{
		writeToFile(toString(strong), filePath);
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::saveToFileCompressed(std::string filePath)
	{
		writeToFile(toCompressedString(), filePath);
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::clearData()
	{
		openedGroup = &anchor;
		anchor.strings.clear();
		anchor.subGroups.clear();
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::closeAllGroups()
	{
		openedGroup = &anchor;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	bool DataFileIO::anyGroupOpened()
	{
		return openedGroup != &anchor;
	}
}