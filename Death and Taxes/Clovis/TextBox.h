#pragma once
#include "Widget.h"
#include "Shapes.h"
namespace dat
{
	class TextBox : public Widget
	{
	private:
		std::string text;
		bool writeable;
		sf::Font *font;
		sf::Color textColour;
		Rectangle<int> boundingBox;
		float textScale;
		std::string heightAlignmentString;
	public:
		TextBox(Rectangle<int> boundingBox = Rectangle<int>(Vector2D<int>(0, 0), Vector2D<int>(0, 0)), float textScale = 1.0f, sf::Color textColour = sf::Color(0, 0, 0), sf::Font *font = nullptr, bool writeable = false, std::string text = "", std::string heightAlignmentString = "A");
		~TextBox();
		virtual void mouseMoved(bool &eventClaimed, Vector2D<int> mousePos) override;
		virtual void mouseButtonPressed(bool &eventClaimed, Vector2D<int> mousePos, sf::Mouse::Button button) override;
		virtual void mouseButtonReleased(bool &eventClaimed, Vector2D<int> mousePos, sf::Mouse::Button button) override;
		virtual void mouseScrolled(bool &eventClaimed, Vector2D<int> mousePos, int delta) override;
		virtual void keyPressed(bool &eventClaimed, sf::Keyboard::Key key) override;
		virtual void keyReleased(bool &eventClaimed, sf::Keyboard::Key key) override;
		virtual void draw(sf::RenderTarget &renderTarget, sf::Time time, bool respectAlpha = false) override;
		void setWriteable(bool writeable); //if the textBox is writeable it can be selected and typed in.
		void setText(std::string text); //sets the text string of the textBox.
		void setFont(sf::Font *font); //sets the font of the textBox.
		void setTextColour(sf::Color colour); //sets the text colour.
		void setBoundingBox(Rectangle<int> boundingBox); //Sets the bounding box of the text. The text cannot go outside of this box.
		Rectangle<int> getBoundingBox() const; //returns the bounding box of the text. The text cannot go outside of this box.
		void setTextScale(float textScale);
		void setHeightAlignmentString(std::string string);//Since different characters have different height, one cannot use an arbitrary string to find where the centre of a text is. The height alignment string should therefore in the latin alphabet contain a letter that is taller than most lower case letters, but doesn't go below the line.
	};
}