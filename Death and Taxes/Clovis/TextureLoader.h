#pragma once
#include <SFML\Graphics.hpp>
#include <string>
#include <map>
#include "NonCopyable.h"
namespace dat
{
	class ModuleLoader;
	class TextureLoader : NonCopyable
	{
	private:
		ModuleLoader &moduleLoader;
		std::map<std::string, sf::Texture *> alreadyLoadedImages;
	public:
		TextureLoader(ModuleLoader &moduleLoader);
		~TextureLoader();
		sf::Texture &loadTexture(std::string defaultPath); //Loads this texture if it's not already loaded.
	};

}