#pragma once
#include <vector>
#include "LoadsOnce.h"
#include "IDType.h"
namespace dat
{
	class Animation;
	class ModuleLoader;
	class AnimationLoader;
	class ResourceTypeLoader;
	class ResourceType;
	class ResourceTypeImageLoader : public LoadsOnce
	{
	private:
		std::vector<Animation *> idToAnimation;
	public:
		ResourceTypeImageLoader();
		~ResourceTypeImageLoader();

		void load(ModuleLoader &moduleLoader, AnimationLoader const &animationLoader, ResourceTypeLoader const &resourceTypeLoader);
		Animation *getAnimation(IDType<ResourceType> id) const; //Returns the resource.
	};

}