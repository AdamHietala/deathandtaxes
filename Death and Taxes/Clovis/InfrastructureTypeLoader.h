#pragma once
#include "LoadsOnce.h"
#include "InfrastructureType.h"
#include <map>
#include <vector>
#include <string>
namespace dat
{
	class ModuleLoader;
	class TechTreeLoader;
	class ResourceTypeLoader;
	class InfrastructureTypeLoader : public LoadsOnce
	{
	private:
		std::map<std::string, IDType<InfrastructureType>> stringIDToID; //Maps the InfrastructureType string ID to the InfrastructureTypeID
		std::vector<std::string> idToStringID;
		void addInfrastructureType(std::string stringID, InfrastructureType *infrastructureType); //Adds the ResourceType to the datastructure.
		std::vector<InfrastructureType *> idToInfrastructureType;
	public:
		InfrastructureTypeLoader();
		~InfrastructureTypeLoader();

		void load(ModuleLoader &moduleLoader, TechTreeLoader const &techTreeLoader, ResourceTypeLoader const &resourceTypeloader); //needs techTreeLoader, resourceTypeloader to be loaded.
		IDType<InfrastructureType> getID(std::string stringID) const; //Returns the infrastructureType ID.
		std::string getStringID(IDType<InfrastructureType> id) const; //Returns the stringID so that it can be referred to in a savefile.
		int getAmountOfInfrastructureTypes() const;//returns the amount of resource types that there are.

	};

}