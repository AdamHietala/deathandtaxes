#pragma once
#include <string>
#include <map>
#include "NonCopyable.h"
#include "DataFileIO.h"
namespace dat
{
	class ModuleOrder;

	class ModuleLoader : NonCopyable
	{
	private:
		ModuleOrder &moduleOrder;
		std::map<std::string, DataFileIO *> loadedDataFileIOs;
	public:
		ModuleLoader(ModuleOrder &moduleOrder);
		~ModuleLoader();
		std::string getModFilePath(std::string defaultFilePath) const; //Checks through the load order to find the right file to load. This should not be used for DataFileIOs being loaded.
		DataFileIO &loadDataFileIO (std::string path); //Loads the DataFileIO from the different files with this default path that they have. If the path is a folder it will load all files within into the same DataFileIO prioritising the files in alphabetical order.
	};
}