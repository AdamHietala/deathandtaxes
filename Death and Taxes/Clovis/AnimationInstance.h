#pragma once
#include "Animation.h"
#include <SFML\System.hpp>
#include "IDType.h"
namespace dat
{
	class AnimationAttachmentPoint;
	class AnimationInstance 
	{
	private:
		Animation &animation;
		Vector2D<float> imageScale, position;
		float rotation;
	public:
		AnimationInstance(Animation &animation);
		~AnimationInstance();
		void setPosition(float x, float y); //Sets the position of the animation in pixels.
		void setPosition(Vector2D<float> position); //Sets the position of the animation in pixels.
		Vector2D<float> getPosition() const; //Returns the position of the animationInstance.
		void setScale(float scaleX, float scaleY); //Scales the animation.
		void setScale(Vector2D<float> newScale); //Scales the animation.
		Vector2D<float> getScale() const; //Returns the scale of the animation.
		void scale(float factorX, float factorY); //Sets the scale the animation.
		void scale(Vector2D<float> factor); //Sets the scale the animation.
		void rotate(float rotation); //Rotates the animation by rotation.
		void setRotation(float newRotation); //sets the rotation to newRotation.
		float getRotation() const; //returns the rotation of the animation.

		void draw(sf::RenderTarget &renderTarget, sf::Time time, sf::RenderStates renderStates = sf::RenderStates::Default) const; //Draws the animation to the renderTarget //debug this class draw does not respect alpha, it should
		Vector2D<float> getSize() const; //Returns the height and width of the animation after tranforming it.
	};
}