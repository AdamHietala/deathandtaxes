#pragma once
#include "IDType.h"
#include "NonCopyable.h"
namespace dat
{
	class ResourceType : NonCopyable
	{
	public:
		ResourceType(IDType<ResourceType> id, bool service);
		~ResourceType();
		const IDType<ResourceType> id;
		const bool service;
	};

}