#pragma once
#include "VectorXD.h"
namespace dat
{
	template <typename type> class Rectangle
	{
	private:
	public:
		Vector2D<type> topLeft, dimensions;

		Rectangle()
		{

		}
		Rectangle(Vector2D<type> topLeft, Vector2D<type> dimensions)
		{
			Rectangle::topLeft = topLeft;
			Rectangle::dimensions = dimensions;
		}
		~Rectangle()
		{

		}
		bool operator==(const Rectangle<type> &otherRect) const
		{
			return topLeft == otherRect.topLeft && dimensions == otherRect.dimensions;
		}
		bool operator!=(const Rectangle<type> &otherRect) const
		{
			return !(*this == otherRect);
		}
		bool operator<(const Rectangle<type> &otherRect) const
		{
			return dimensions < otherRect.dimensions;
		}
		bool operator<=(const Rectangle<type> &otherRect) const
		{
			return dimensions <= otherRect.dimensions;
		}
		bool operator>(const Rectangle<type> &otherRect) const
		{
			return dimensions > otherRect.dimensions;
		}
		bool operator>=(const Rectangle<type> &otherRect) const
		{
			return dimensions >= otherRect.dimensions;
		}
		//multiplication
		Rectangle& operator*=(const Rectangle<type> &otherRect)
		{
			topLeft *= otherRect.topLeft;
			dimensions *= otherRect.dimensions;
			return *this;
		}
		Rectangle& operator*=(const Vector2D<type> &vect) //changes the dimensions of the rectangle, not the position.
		{
			dimensions *= vect;
			return *this;
		}
		Rectangle& operator*=(type otherValue) //changes the dimensions of the rectangle, not the position.
		{
			dimensions *= otherValue;
			return *this;
		}
		Rectangle operator*(const Rectangle<type> &otherRect) const
		{
			Rectangle<type> newRect(*this);
			newRect *= otherRect;
			return newRect;
		}
		Rectangle operator*(const Vector2D<type> &vect) const
		{
			Rectangle<type> newRect(*this);
			newRect *= vect;
			return newRect;
		}
		Rectangle operator*(type otherValue) const
		{
			Rectangle<type> newRect(*this);
			newRect *= otherValue;
			return newRect;
		}

		//division
		Rectangle& operator/=(const Rectangle<type> &otherRect)
		{
			topLeft /= otherRect.topLeft;
			dimensions /= otherRect.dimensions;
			return *this;
		}
		Rectangle& operator/=(const Vector2D<type> &vect) //changes the dimensions of the rectangle, not the position.
		{
			dimensions /= vect;
			return *this;
		}
		Rectangle& operator/=(type otherValue) //changes the dimensions of the rectangle, not the position.
		{
			dimensions /= otherValue;
			return *this;
		}
		Rectangle operator/(const Rectangle<type> &otherRect) const
		{
			Rectangle<type> newRect(*this);
			newRect /= otherRect;
			return newRect;
		}
		Rectangle operator/(const Vector2D<type> &vect) const
		{
			Rectangle<type> newRect(*this);
			newRect /= vect;
			return newRect;
		}
		Rectangle operator/(type otherValue) const
		{
			Rectangle<type> newRect(*this);
			newRect /= otherValue;
			return newRect;
		}
		//addition
		Rectangle& operator+=(const Rectangle<type> &otherRect)
		{
			topLeft += otherRect.topLeft;
			dimensions += otherRect.dimensions;
			return *this;
		}
		Rectangle& operator+=(const Vector2D<type> &vect) //changes the dimensions of the rectangle, not the position.
		{
			dimensions += vect;
			return *this;
		}
		Rectangle& operator+=(type otherValue) //changes the dimensions of the rectangle, not the position.
		{
			dimensions += otherValue;
			return *this;
		}
		Rectangle operator+(const Rectangle<type> &otherRect) const
		{
			Rectangle<type> newRect(*this);
			newRect += otherRect;
			return newRect;
		}
		Rectangle operator+(const Vector2D<type> &vect) const
		{
			Rectangle<type> newRect(*this);
			newRect += vect;
			return newRect;
		}
		Rectangle operator+(type otherValue) const
		{
			Rectangle<type> newRect(*this);
			newRect += otherValue;
			return newRect;
		}

		//subtraction
		Rectangle& operator-=(const Rectangle<type> &otherRect)
		{
			topLeft -= otherRect.topLeft;
			dimensions -= otherRect.dimensions;
			return *this;
		}
		Rectangle& operator-=(const Vector2D<type> &vect) //changes the dimensions of the rectangle, not the position.
		{
			dimensions -= vect;
			return *this;
		}
		Rectangle& operator-=(type otherValue) //changes the dimensions of the rectangle, not the position.
		{
			dimensions -= otherValue;
			return *this;
		}
		Rectangle operator-(const Rectangle<type> &otherRect) const
		{
			Rectangle<type> newRect(*this);
			newRect -= otherRect;
			return newRect;
		}
		Rectangle operator-(const Vector2D<type> &vect) const
		{
			Rectangle<type> newRect(*this);
			newRect -= vect;
			return newRect;
		}
		Rectangle operator-(type otherValue) const
		{
			Rectangle<type> newRect(*this);
			newRect -= otherValue;
			return newRect;
		}
		template<typename type2> Rectangle<type2> typecast() const
		{
			return Rectangle<type2>(topLeft.typecast<type2>(), dimensions.typecast<type2>());
		}
		Rectangle translate(Rectangle<type> otherWorld)  const
		{
			return Rectangle<type>(otherWorld.topLeft*dimensions + topLeft, dimensions*otherWorld.dimensions);
		}
		bool within(Vector2D<type> point) const
		{
			return topLeft.x < point.x && point.x < topLeft.x + dimensions.x && topLeft.y < point.y && point.y < topLeft.y + dimensions.y;
		}
	};
}