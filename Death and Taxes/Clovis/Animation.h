#pragma once
#include <SFML\Graphics.hpp>
#include "VectorXD.h"
#include "IDType.h"
#include "NonCopyable.h"
namespace dat
{
	class AnimationAttachmentPoint;
	class Animation : NonCopyable
	{
	public:
		struct AttachmentPoint
		{
			float rotation;
			Vector2D<float> offset;
			bool behind;
		};
	private:
		int frameLength;
		std::vector<sf::Sprite> sprites;
		std::vector<Vector2D<float>> positionComps;
	public:
		Animation(const IDType<Animation> id, int frameLength);
		~Animation();

		void addFrame(sf::Sprite sprite, Vector2D<float> positionCompensation); //time in miliseconds. Throws an DifferentSizeException if the frame images don't have the same sizes.

		const IDType<Animation> id;

		int getFrameLength() const; //Returns the frame length of a frame
		sf::Sprite getSprite(int frameIndex) const; //Returns a copy of the sprite for this frame
		Vector2D<float> getPositionCompensation(int frameIndex) const; //Returns the position compensation this frame's sprite needs.
		int getNumberOfFrames() const; //returns the number of frames in this animation.
		int getRevolutionTime() const; //Returns the time it takes for the animation to go through all the frames.
	};

}