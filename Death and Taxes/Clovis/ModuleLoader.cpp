#include "ModuleLoader.h"
#include <set>
#include <algorithm>
#include "ModuleOrder.h"
#include "FileIO.h"
#include "Exceptions.h"
#include <assert.h>
namespace dat
{
	ModuleLoader::ModuleLoader(ModuleOrder &moduleOrder) : moduleOrder(moduleOrder)
	{

	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	ModuleLoader::~ModuleLoader()
	{
		for (auto file : loadedDataFileIOs)
		{
			delete file.second;
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::string ModuleLoader::getModFilePath(std::string defaultFilePath) const
	{
		std::vector<std::string> &order = moduleOrder.getLoadOrder();
		for (int i = order.size() - 1; i >= 0; --i)
		{
			std::string path = order[i] + "\\" + defaultFilePath;
			if (fileExists(path))
			{
				return path;
			}
		}
		throw(FileMissingException("No mod has a file in the path " + defaultFilePath));
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	DataFileIO & ModuleLoader::loadDataFileIO(std::string path)
	{
		if (loadedDataFileIOs.count(path) == 0)
		{
			DataFileIO *newFileIO = new DataFileIO;

			try
			{
				for (auto modPath : moduleOrder.getLoadOrder())
				{
					std::string thisPath = modPath + "\\" + path;

					if (folderExists(thisPath))
					{
						std::vector<std::string> files = listFilesInFolder(thisPath);

						std::set<std::string> sortedFiles; //sorts the strings alphabetically
						for (auto file : files)
						{
							sortedFiles.insert(file);
						}

						for (auto file : sortedFiles)
						{
							newFileIO->loadFromFile(thisPath + "\\" + file);
						}
					}
					else if (fileExists(thisPath))
					{
						newFileIO->loadFromFile(thisPath);
					}
				}
			}
			catch (...)
			{
				delete newFileIO;
				throw;
			}


			loadedDataFileIOs[path] = newFileIO;
		}


		DataFileIO *dataFileIO = loadedDataFileIOs.at(path);
		assert(!dataFileIO->anyGroupOpened()); //No user should have left it opened.
		return *dataFileIO;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}