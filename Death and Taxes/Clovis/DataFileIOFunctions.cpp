#include "DataFileIOFunctions.h"
#include "StringManipulation.h"
#include <assert.h>
#include "Exceptions.h"
namespace dat
{
	Vector2D<int> get2DVectorInt(DataFileIO &dataFileIO, std::string key)
	{
		dataFileIO.openGroup(key);
		Vector2D<int> retVal(getInt(dataFileIO, "x"), getInt(dataFileIO, "y"));
		dataFileIO.closeGroup();

		return retVal;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void store2DVectorInt(DataFileIO &dataFileIO, std::string key, Vector2D<int> value)
	{
		assert(dataFileIO.existsGroup(key) == false);
		dataFileIO.addGroup(key);
		storeInt(dataFileIO, "x", value.x);
		storeInt(dataFileIO, "y", value.y);
		dataFileIO.closeGroup();
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Vector2D<double> get2DVectorDouble(DataFileIO &dataFileIO, std::string key)
	{
		dataFileIO.openGroup(key);
		Vector2D<double> retVal(getDouble(dataFileIO, "x"), getDouble(dataFileIO, "y"));
		dataFileIO.closeGroup();

		return retVal;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void store2DVectorDouble(DataFileIO &dataFileIO, std::string key, Vector2D<double> value)
	{
		assert(dataFileIO.existsGroup(key) == false);
		dataFileIO.addGroup(key);
		storeDouble(dataFileIO, "x", value.x);
		storeDouble(dataFileIO, "y", value.y);
		dataFileIO.closeGroup();
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Vector3D<int> get3DVectorInt(DataFileIO &dataFileIO, std::string key)
	{
		dataFileIO.openGroup(key);
		Vector3D<int> retVal(getInt(dataFileIO, "x"), getInt(dataFileIO, "y"), getInt(dataFileIO, "z"));
		dataFileIO.closeGroup();

		return retVal;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void store3DVectorInt(DataFileIO &dataFileIO, std::string key, Vector3D<int> value)
	{
		assert(dataFileIO.existsGroup(key) == false);
		dataFileIO.addGroup(key);
		storeInt(dataFileIO, "x", value.x);
		storeInt(dataFileIO, "y", value.y);
		storeInt(dataFileIO, "z", value.z);
		dataFileIO.closeGroup();
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Vector3D<double> get3DVectorDouble(DataFileIO &dataFileIO, std::string key)
	{
		dataFileIO.openGroup(key);
		Vector3D<double> retVal(getDouble(dataFileIO, "x"), getDouble(dataFileIO, "y"), getDouble(dataFileIO, "z"));
		dataFileIO.closeGroup();

		return retVal;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void store3DVectorDouble(DataFileIO &dataFileIO, std::string key, Vector3D<double> value)
	{
		assert(dataFileIO.existsGroup(key) == false);
		dataFileIO.addGroup(key);
		storeDouble(dataFileIO, "x", value.x);
		storeDouble(dataFileIO, "y", value.y);
		storeDouble(dataFileIO, "z", value.z);
		dataFileIO.closeGroup();
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Rectangle<int> getRectangleInt(DataFileIO &dataFileIO, std::string key)
	{
		return Rectangle<int>(get2DVectorInt(dataFileIO, "dimensions"), get2DVectorInt(dataFileIO, "top left"));
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void storeRectangleInt(DataFileIO &dataFileIO, std::string key, Rectangle<int> rect)
	{
		assert(dataFileIO.existsGroup(key) == false);
		dataFileIO.addGroup(key);
		store2DVectorInt(dataFileIO, "top left", rect.topLeft);
		store2DVectorInt(dataFileIO, "dimensions", rect.dimensions);
		dataFileIO.closeGroup();
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Rectangle<double> getRectangleDouble(DataFileIO &dataFileIO, std::string key)
	{
		return Rectangle<double>(get2DVectorDouble(dataFileIO, "dimensions"), get2DVectorDouble(dataFileIO, "top left"));
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void storeRectangleDouble(DataFileIO &dataFileIO, std::string key, Rectangle<double> rect)
	{
		assert(dataFileIO.existsGroup(key) == false);
		dataFileIO.addGroup(key);
		store2DVectorDouble(dataFileIO, "top left", rect.topLeft);
		store2DVectorDouble(dataFileIO, "dimensions", rect.dimensions);
		dataFileIO.closeGroup();
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	sf::Color getColor(DataFileIO &dataFileIO, std::string key)
	{
		dataFileIO.openGroup(key);
		sf::Color color(
			sf::Uint8(255.0 * getDouble(dataFileIO, "red")),
			sf::Uint8(255.0 * getDouble(dataFileIO, "green")),
			sf::Uint8(255.0 * getDouble(dataFileIO, "blue")),
			sf::Uint8(255.0 * getDouble(dataFileIO, "alpha"))
			);
		dataFileIO.closeGroup();
		return color;

	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void storeColor(DataFileIO &dataFileIO, std::string key, sf::Color color)
	{
		assert(dataFileIO.existsGroup(key) == false);
		dataFileIO.addGroup(key);
		storeDouble(dataFileIO, "red", color.r);
		storeDouble(dataFileIO, "blue", color.g);
		storeDouble(dataFileIO, "green", color.b);
		storeDouble(dataFileIO, "alpha", color.a);
		dataFileIO.closeGroup();
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	bool getBoolean(DataFileIO &dataFileIO, std::string key)
	{
		if (dataFileIO.getString(key) == "yes")
		{
			return true;
		}
		else if (dataFileIO.getString(key) == "no")
		{
			return false;
		}
		else
		{
			throw ParsingException("The string " + key + " could not be interpreted as a boolean. The correct values are \"yes\" and \"no\".");
		}
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void storeBoolean(DataFileIO &dataFileIO, std::string key, bool boolean)
	{
		dataFileIO.addString(key, boolean ? "yes" : "no");
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	int getInt(DataFileIO &dataFileIO, std::string key)
	{
		std::string number = dataFileIO.getString(key);
		if (number == "max")
		{
			return std::numeric_limits<int>::max();
		}
		else if (number == "min")
		{
			return std::numeric_limits<int>::min();
		}
		else
		{
			testException(isInt(number), ParsingException("The string " + key + " could not be interpreted as an integer."));
			return atoi(number.c_str());
		}
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void storeInt(DataFileIO &dataFileIO, std::string key, int value)
	{
		dataFileIO.addString(key, std::to_string(value));
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	double getDouble(DataFileIO &dataFileIO, std::string key)
	{
		std::string number = dataFileIO.getString(key);
		if (number == "max")
		{
			return std::numeric_limits<double>::max();
		}
		else if (number == "min")
		{
			return std::numeric_limits<double>::min();
		}
		else
		{
			testException(isDouble(number), ParsingException("The string " + key + " could not be interpreted as a real number."));
			return atof(number.c_str());
		}
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void storeDouble(DataFileIO &dataFileIO, std::string key, double value)
	{
		dataFileIO.addString(key, std::to_string(value));
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}