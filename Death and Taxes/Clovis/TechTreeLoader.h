#pragma once
#include "LoadsOnce.h"
#include <vector>
#include "IDType.h"
#include "Technology.h"
#include "TechType.h"
#include <map>
#include <string>
namespace dat
{
	class ModuleLoader;
	class TechTreeLoader : public LoadsOnce
	{
	private:
		std::vector<Technology *> iDToTechnology; //this is the owner of the Technologies on the heap
		std::map<std::string, IDType<Technology>> technologyStringIDToID; //Maps the Technology string ID to the TechnologyID
		std::vector<std::string> technologyIDToStringID;
		void TechTreeLoader::addTechnology(std::string stringID, Technology *technology);//Adds Technology to the datastructure.

		std::map<std::string, IDType<TechType>> techTypeStringIDToID; //Maps the TechType string ID to the TechTypeID
		std::vector<std::string> techTypeIDToStringID;
		void addTechTypeID(std::string stringID, IDType<TechType> id);//Adds the techtypes's id to the datastructure.
		void loadTechTypes(ModuleLoader &moduleLoader);
		void loadTechnologies(ModuleLoader &moduleLoader);
	public:
		TechTreeLoader();
		~TechTreeLoader();
		void load(ModuleLoader &moduleLoader);
		int getNrOfTechTypes() const;
		int getNrOfTechnologies() const;
		Technology* getTechnology(IDType<Technology> techID) const; //returns the technology with this id.
		IDType<TechType> getTechTypeID(std::string stringID) const;
		std::string getTechTypeStringID(IDType<TechType> id) const;
		IDType<Technology> getTechnologyID(std::string stringID) const;
		std::string getTechnologyStringID(IDType<Technology> id) const;
	};

}