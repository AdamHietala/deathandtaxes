#include "Technology.h"
#include "TechType.h"
#include "Exceptions.h"
namespace dat
{
	Technology::Technology(IDType<Technology> id, int nrOfTechTypes) : id(id)
	{
		neededTechTypeLevels.resize(nrOfTechTypes, 0.0);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Technology::~Technology()
	{
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	IDType<Technology> Technology::getID() const
	{
		return id;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	bool Technology::isUnlockable(std::vector<double> techTypeLevels, std::vector<bool> techUnlocked) const
	{
		assert(techTypeLevels.size() == neededTechTypeLevels.size());
		for (int i = 0; i < int(neededTechTypeLevels.size()); ++i)
		{
			if (techTypeLevels[i] < neededTechTypeLevels[i])
			{
				return false;
			}
		}
		for (int i = 0; i < int(neededTechs.size()); ++i)
		{
			if (techUnlocked[neededTechs[i].getID()] == false)
			{
				return false;
			}
		}
		return true;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void Technology::addTechnologyDependency(IDType<Technology> id)
	{

		for (int i = 0; i < (int)neededTechs.size(); ++i)
		{
			testException(neededTechs[i] != id, TechAlreadyAddedException("A technology dependency was added twice for the same technology."));
		}

		neededTechs.push_back(id);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void Technology::addTechTypeLevelNeed(IDType<TechType> id, double level)
	{
		neededTechTypeLevels[id.getID()] = level;
	}
}