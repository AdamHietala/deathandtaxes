#pragma once
#include <string>
#include "ClickButton.h"
#include "Shapes.h"
#include "AnimationInstance.h"
#include "TextBox.h"
namespace dat
{
	class RectButton : public ClickButton
	{
	private:
		virtual bool within(Vector2D<int> pos) override; //Tells wether a point is above this button or not.
		Rectangle<int> boundingBox;
		AnimationInstance *normal, *highlighted, *clicked, *clickedHighlighted, *disabled;
		TextBox textBox;
		void updateAnimationSizeAndPos(); 
		void deleteAnimationInstances();
	public:
		RectButton();
		virtual ~RectButton() override;

		virtual void draw(sf::RenderTarget &renderTarget, sf::Time time, bool respectAlpha = false) override; //Draws the rectbutton to the renderTarget.
		void setBoundingBox(Rectangle<int> boundingBox); //sets the position and size of the button.
		void setPos(Vector2D<int> topLeft); //Sets the button's upper left corner's position.
		void setSize(Vector2D<int> size); //Sets the size of the button.
		void setAnimationInstances(AnimationInstance normal, AnimationInstance highlighted, AnimationInstance clicked, AnimationInstance clickedHighlighted, AnimationInstance disabled); //Will copy the sprites, no need to keep them afterwards.
		void setText(std::string str); //Sets the text to be written over the button.
		void setFont(sf::Font &font); //Sets the font of the text of the button.
		void setTextColour(sf::Color colour); //sets the colour of the text.
		void setTextScale(float scale); //scales the text over the button. scale 1,1 would make the text cover the entire button
		void setHeightAlignmentString(std::string str); //See TextBox::setHeightAlignmentString
	};
}