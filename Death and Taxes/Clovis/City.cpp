#include "City.h"
#include <assert.h>
#include "Pop.h"
#include "PopTypeLoader.h"
#include "IDType.h"
#include "GameWorld.h"
namespace dat
{
	void City::deletePop(Pop *pop)
	{
		assert(&pop->getHomeCity() == this);
		std::vector<Pop *> &popTypeVect = pops[pop->getPopType().id.getID()];

		for (int i = 0; i < (int)popTypeVect.size(); ++i)
		{
			if (pop == popTypeVect[i])
			{
				popTypeVect.erase(popTypeVect.begin() + i);
				numberOfPops--;
				return;
			}
		}
		gameWorld.getPopList().deleteItem(pop);

		assert(false);//this should not be called if there isn't such a pop in this city.
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void City::createPopGroup(ResourceTypeLoader &resourceTypeloader, PopType const &popType, double popSize)
	{
		Pop *newPop = gameWorld.getPopList().instantiate(Pop(defines, resourceTypeLoader, gameWorld.getPopList().getNextFreeID(), popType, *this, popSize));
		pops[popType.id.getID()].push_back(newPop);
		numberOfPops += 1;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	City::City(Defines &defines, GameWorld &gameWorld, ResourceTypeLoader &resourceTypeLoader, PopTypeLoader &popTypeLoader, InfrastructureTypeLoader &infrastructureTypeLoader, IDType<City> id, Vector2D<int> coordinate) :
		defines(defines), gameWorld(gameWorld), resourceTypeLoader(resourceTypeLoader), popTypeLoader(popTypeLoader), infrastructureTypeLoader(infrastructureTypeLoader), ID(id), coordinate(coordinate), infrastructureLevel(infrastructureTypeLoader)
	{
		numberOfPops = 0;
		pops.resize(popTypeLoader.getNrOfPopTypes(), std::vector<Pop*>());


		//debug, these first pops should probably be generated more intelligently.
		for (int i = 0; i < popTypeLoader.getNrOfPopTypes(); ++i)
		{
			createPopGroup(resourceTypeLoader, popTypeLoader.getPopType(IDType<PopType>(i)), 1000.0);
		}
		//end debug
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	City::City(Defines &defines, GameWorld &gameWorld, ResourceTypeLoader &resourceTypeLoader, PopTypeLoader &popTypeLoader, InfrastructureTypeLoader &infrastructureTypeLoader, IDType<City> id, Vector2D<int> coordinate, Pop &foundingPop, double movingPercentage) :
		defines(defines), gameWorld(gameWorld), resourceTypeLoader(resourceTypeLoader), popTypeLoader(popTypeLoader), infrastructureTypeLoader(infrastructureTypeLoader), ID(id), coordinate(coordinate), infrastructureLevel(infrastructureTypeLoader)
	{
		numberOfPops = 0;
		pops.resize(popTypeLoader.getNrOfPopTypes(), std::vector<Pop*>());


		createPopGroup(resourceTypeLoader, foundingPop.getPopType(), 0.0);
		foundingPop.transferFrom(foundingPop, foundingPop.getPopSize()*movingPercentage);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	City::~City()
	{
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	const std::vector<std::vector<Pop *>> &City::getPops() const
	{
		return pops;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	const std::vector<Pop *> &City::getPops(IDType<PopType> &popType) const
	{
		return pops[popType.getID()];
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	const std::vector<City *> &City::getNeighbouringCities() const
	{
		return neighbouringCities;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Vector2D<int> City::getCoordinate() const
	{
		return coordinate;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void City::addCityNeighbour(City *otherCity)
	{
		neighbouringCities.push_back(otherCity);
		otherCity->neighbouringCities.push_back(this);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void City::removeCityNeighbour(City *otherCity)
	{
		for (int i = 0; i < (int)neighbouringCities.size(); ++i)
		{
			if (neighbouringCities[i] == otherCity)
			{
				neighbouringCities.erase(neighbouringCities.begin() + i);
				break;
			}
		}

		for (int i = 0; i < (int)otherCity->neighbouringCities.size(); ++i)
		{
			if (otherCity->neighbouringCities[i] == this)
			{
				otherCity->neighbouringCities.erase(otherCity->neighbouringCities.begin() + i);
				break;
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	bool City::inhabited() const
	{
		return numberOfPops > 0;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void City::calcTick(int tickTime)
	{
		for (auto popType : pops)
		{
			for (auto pop : popType)
			{
				//pop->calcTick(tickTime); debug temporarily disabled.
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	double City::getPopulation() const
	{
		double population = 0.0;
		for (auto popType : pops)
		{
			for (auto pop : popType)
			{
				population += pop->getPopSize();
			}
		}
		return population;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	IDType<City> City::getID() const
	{
		return ID;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void City::destroyCity()
	{
		for (auto vect : pops)
		{
			for (auto pop : vect)
			{
				gameWorld.getPopList().deleteItem(pop);
				numberOfPops -= 1;
			}
		}
		assert(numberOfPops == 0);

		for (auto otherCity : neighbouringCities)
		{
			for (int i = 0; i < (int)otherCity->neighbouringCities.size(); ++i)
			{
				if (otherCity->neighbouringCities[i] == this)
				{
					otherCity->neighbouringCities.erase(otherCity->neighbouringCities.begin() + i);
					break;
				}
			}
		}
	}
}