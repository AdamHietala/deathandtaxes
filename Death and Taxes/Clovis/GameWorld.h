#pragma once
#include <vector>
#include "VectorXD.h"
#include "HexGrid.h"
#include "IDDealingList.h"
namespace dat
{
	class City;
	class TerrainType;
	class GameMain;
	class Pop;
	class Defines;
	class GameWorld
	{
	private:
		class WorldTile
		{
		public:
			WorldTile();
			~WorldTile();
			TerrainType *terrain;
			City *city;
		};
		std::vector<std::vector<WorldTile>> worldTiles;


		bool gameRunning;
		Vector2D<int> worldDimensions;
		hex::WrapType wrapType;
		GameMain &gameMain;
		IDDealingList<Pop> popList;
		IDDealingList<City> cityList;
		int tickTime; //tickTime is a number incremented by one each tick, which is then sent to all other tick functions. It helps with doing things every x amount of ticks rather than doing all things every tick.
	public:
		GameWorld(GameMain &gameMain);
		~GameWorld();

		void calcTick(); //calculates the next turn for the world clausewitz style.
		void dumpWorld(); //Dumps this world.
		void generateNewWorld(Vector2D<int> worldDimensions, hex::WrapType wrapType); //Generates a new world with terrain, buildings and people.
		bool gameIsRunning() const; //returns true if a game is runnnig right now.
		TerrainType *getTerrainType(Vector2D<int> globalCoord); //Returns the terrain type of the coordinates. Returns nullptr if no terrain is defined for that coordinate.
		Vector2D<int> getWorldDimensions(); //returns the coordinates of this corner of the world, to show the size of the world 
		hex::WrapType getWrapType(); //returns the current wraptype.
		bool cityExists(Vector2D<int> coord) const;
		City *getCity(Vector2D<int> coord) const;
		void foundCity(Vector2D<int> coord);
		void removeCity(Vector2D<int> coord);
		IDDealingList<Pop> &getPopList();
		IDDealingList<City> &getCityList();
	};

}