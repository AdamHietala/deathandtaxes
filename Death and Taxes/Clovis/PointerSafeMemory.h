#pragma once
#include <vector>
#include <assert.h>
#include "Exceptions.h"
#include <stdlib.h>
#include <new>
#include "NonCopyable.h"
namespace dat
{
	template <typename type>
	class PointerSafeMemory : NonCopyable //The point of this is to make a replacement for using Vector<type *> so that as much memory as possible is still aligned without risk of copying the items to new adresses after having added them to the PoitnerSafeVector.
	{
	private:

		struct MemoryPiece
		{
			type * first;
			int size;
			std::vector<int> freeSubIndices;

		};

		std::vector<MemoryPiece> memoryPieces;
		int totalSize;
		int increaseBySize;

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		void reserveMoreMemory()
		{

			memoryPieces.push_back(MemoryPiece());
			MemoryPiece &newMemoryPiece = memoryPieces[memoryPieces.size() - 1];

			do
			{
				testException(increaseBySize > 0, CouldNotReserveMemoryException("It seems you've run out of memory."));

				newMemoryPiece.first = (type *)malloc(sizeof(type)* increaseBySize);
				newMemoryPiece.size = increaseBySize;
				increaseBySize /= 2;
			} while (newMemoryPiece.first == nullptr);
			increaseBySize *= 4; //the size increase is under normal circumstances now twice as large as it was when the function was entered.

			newMemoryPiece.freeSubIndices.resize(newMemoryPiece.size);
			for (int i = 0; i < newMemoryPiece.size; ++i)
			{
				newMemoryPiece.freeSubIndices[i] = i;
			}

		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		void releaseMemory(int memoryPieceIndex)
		{
			assert(memoryPieces[memoryPieceIndex].freeSubIndices.size() == memoryPieces[memoryPieceIndex].size);
			free(memoryPieces[memoryPieceIndex].first);
			memoryPieces.erase(memoryPieces.begin() + memoryPieceIndex);
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		int findMemoryPeiceIndex(type *pointer) const
		{
			for (int i = 0; i < (int)memoryPieces.size(); ++i)
			{
				if (memoryPieces[i].first <= pointer && pointer < memoryPieces[i].first + memoryPieces[i].size)
				{
					return i;
				}
			}
			throw ItemNotFoundException("Tried to find where a pointer is stored when it was not a part of this PointerSafeVector.");
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		type * findFreeMemorySlot()
		{
		tryAgain:

			for (int i = 0; i < (int)memoryPieces.size(); ++i)
			{
				MemoryPiece& memoryPiece = memoryPieces[i];
				if (memoryPiece.freeSubIndices.size() > 0)
				{
					for (int q = 0; q < (int)memoryPiece.freeSubIndices.size(); ++q)
					{
						type* pointer = memoryPiece.first + memoryPiece.freeSubIndices[memoryPiece.freeSubIndices.size() - 1];
						memoryPiece.freeSubIndices.pop_back();
						return pointer;
					}
				}
			}

			//The code only gets here if no memory slot was found
			reserveMoreMemory();
			goto tryAgain; //it will only loop once as it will find a free slot the second time when more memory has been reserved.
		}

	public:
		PointerSafeMemory(int expectedMaxSize)
		{
			increaseBySize = expectedMaxSize;
			clear();
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		~PointerSafeMemory()
		{
			clear();
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		void deletePointer(type *pointer)
		{
			int index = findMemoryPeiceIndex(pointer);
			int pointerIndex = pointer - memoryPieces[index].first;
			memoryPieces[index].freeSubIndices.push_back(pointerIndex);

			pointer->~type();
			if ((int)memoryPieces[index].freeSubIndices.size() == memoryPieces[index].size)
			{
				releaseMemory(index);
			}
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		type * instantiate(type const &copyFrom)
		{
			type *newPointer = findFreeMemorySlot();
			new(newPointer) type(copyFrom);//This is not allocating new memory, it is just calling the contructor here.
			totalSize += 1;
			return newPointer;
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		int size() const
		{
			return size;
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		void clear()
		{
			for (int i = 0; i < (int)memoryPieces.size(); ++i)
			{
				MemoryPiece &memoryPiece = memoryPieces[i];
				for (int q = 0; q < memoryPiece.size; ++q)
				{
					bool callDestructor = true;
					for (int j = 0; j < (int)memoryPiece.freeSubIndices.size(); ++j)
					{
						if (memoryPiece.freeSubIndices[j] == q)
						{
							callDestructor = false;
							break;
						}
					}
					if (callDestructor)
					{
						(memoryPiece.first + q)->~type();
					}
				}
				free(memoryPiece.first);
			}
			memoryPieces.clear();
			totalSize = 0;
		}
	};
}