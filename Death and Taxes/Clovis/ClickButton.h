#pragma once
#include <functional>
#include <initializer_list>
#include "Widget.h"
namespace dat
{
	class ClickButton : public Widget
	{
	protected:
		enum class State{ Normal, Highlighted, Clicked, ClickedHighlighted, Inactive };
		State state;
		std::function<void(void)> onMouseEntered, onMouseButtonPressed, onMouseButtonReleased, onMouseExit, onMouseButtonCancelled;
		virtual bool within(Vector2D<int> pos) = 0;
		bool mouseHasEntered, disabled;
	public:
		ClickButton();
		virtual ~ClickButton() override;

		virtual void mouseMoved(bool &eventClaimed, Vector2D<int> mousePos) override;
		virtual void mouseButtonPressed(bool &eventClaimed, Vector2D<int> mousePos, sf::Mouse::Button button) override;
		virtual void mouseButtonReleased(bool &eventClaimed, Vector2D<int> mousePos, sf::Mouse::Button button) override;


		void setDoOnMouseEntered(std::function<void(void)> funct); //Tells the widget what function to be run if the mouse enters above the button.
		void setDoOnMouseExit(std::function<void(void)> funct); //Tells the widget what function to be run if the mouse stops hovering above the button.
		void setDoOnMouseButtonPressed(std::function<void(void)> funct); //Tells the widget what function to be run on MouseButtonPressed.
		void setDoOnMouseButtonReleased(std::function<void(void)> funct); //Tells the widget what function to be run on MouseButtonReleased, meaning what to do when the button is pressed down and the mosue is released above the button.
		void setDoOnMouseButtonCancelled(std::function<void(void)> funct); //Tells the widget what function to be run if the button is pressed down and then released with the mosue outside the button.

		virtual void setActive(bool active) override; //Tells wether to enable or disable this button. Disabled buttons can't be clicked.
		void setDisabled(bool disable); //Tells wether this widget should be drawn or not. Hidden widgets shouldn't work.
	};

}