#pragma once
#include "LoadsOnce.h"
#include "PopType.h"
#include <string>
#include <vector>
#include <map>
#include "IDType.h"


namespace dat
{
	class InfrastructureTypeLoader;
	class ModuleLoader;
	class ResourceTypeLoader;
	class TechTreeLoader;
	class RecipeLoader;

	class PopTypeLoader : public LoadsOnce
	{
	private:
		std::map<std::string, IDType<PopType>> stringIDToID; //Maps the PopType string ID to the PopTypeID
		std::vector<PopType *> iDToPopType; //this is the owner of the PopTypes on the heap
		std::vector<std::string> idToStringID;
		void addPopType(PopType *popType); //adds the pop type to the datastructure
		void addPopTypeID(std::string stringID, IDType<PopType> id);//Adds the PopType's ids to the datastructure.
	public:
		PopTypeLoader();
		~PopTypeLoader();

		void load(ModuleLoader &moduleLoader, ResourceTypeLoader const &resourceTypeloader, TechTreeLoader const &techTreeLoader, RecipeLoader const &recipeLoader, InfrastructureTypeLoader const &infrastructureTypeLoader);
		IDType<PopType> getID(std::string stringID) const; //Returns the tileset ID.
		const PopType &getPopType(IDType<PopType> id) const; //Returns a reference of the loaded PopType with the particular ID.
		std::string getStringID(IDType<PopType> id) const; //Returns the stringID so that it can be referred to in a savefile.
		int getNrOfPopTypes() const;
	};

}