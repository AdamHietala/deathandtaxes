#include "TerrainType.h"


namespace dat
{
	TerrainType::TerrainType(IDType<TerrainType> id, bool water, bool unreachable, bool habitable, double minTemp, double maxTemp, double minHumidity, double maxHumidity, double minWaterHeight, double maxWaterHeight, double minHeight, double maxHeight, double minHeightDifference, double maxHeightDifference) :
		id(id), water(water), unreachable(unreachable), habitable(habitable), minTemp(minTemp), maxTemp(maxTemp), minHumidity(minHumidity), maxHumidity(maxHumidity), minWaterHeight(minWaterHeight), maxWaterHeight(maxWaterHeight), minHeight(minHeight), maxHeight(maxHeight), minHeightDifference(minHeightDifference), maxHeightDifference(maxHeightDifference)
	{
		
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	TerrainType::~TerrainType()
	{

	}
}