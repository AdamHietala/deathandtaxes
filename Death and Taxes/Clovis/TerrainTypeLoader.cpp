#include "TerrainTypeLoader.h"
#include "ModuleLoader.h"
#include "Exceptions.h"
#include "TerrainType.h"
#include "DataFileIOFunctions.h"
#include "FileStructure.h"
#include <set>
namespace dat
{
	void TerrainTypeLoader::addType(std::string stringID, TerrainType *terrainType)
	{
		testException(stringIDToID.count(stringID) == 0, KeyAlreadyExistsException("The type with string id \"" + stringID + "\" was added twice."));
		assert(stringIDToID.size() == terrainType->id.getID());
		stringIDToID[stringID] = terrainType->id;
		iDToTerrainType.push_back(terrainType);
		idToStringID.push_back(stringID);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	TerrainTypeLoader::TerrainTypeLoader()
	{
		idGiver = 0;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	TerrainTypeLoader::~TerrainTypeLoader()
	{
		for (auto terrainType : iDToTerrainType)
		{
			delete terrainType;
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void TerrainTypeLoader::load(ModuleLoader &moduleLoader)
	{
		LoadsOnce::load();


		DataFileIO &terrainTypesFile = moduleLoader.loadDataFileIO(filestructure::TERRAIN_TYPES_FOLDER_PATH);

		std::set<std::string> sortedKeys; //Makes sure the nubmer id's are always the same so that they can be sent between different clients without problem.
		for (auto terrainTypeID : terrainTypesFile.getGroupKeys())
		{
			sortedKeys.insert(terrainTypeID);
		}

		for (auto terrainTypeID : sortedKeys)
		{
			terrainTypesFile.openGroup(terrainTypeID); //a1


			bool water = getBoolean(terrainTypesFile, "water");

			bool unreachable = getBoolean(terrainTypesFile, "unreachable");

			bool habitable = getBoolean(terrainTypesFile, "habitable");

			terrainTypesFile.openGroup("temperature"); //b1
			double minTemp = getDouble(terrainTypesFile, "minimum");
			double maxTemp = getDouble(terrainTypesFile, "maximum");
			terrainTypesFile.closeGroup(); //b1

			terrainTypesFile.openGroup("humidity"); //b2
			double minHumidity = getDouble(terrainTypesFile, "minimum");
			double maxHumidity = getDouble(terrainTypesFile, "maximum");
			terrainTypesFile.closeGroup(); //b2

			terrainTypesFile.openGroup("water height"); //b3
			double minWaterHeight = getDouble(terrainTypesFile, "minimum");
			double maxWaterHeight = getDouble(terrainTypesFile, "maximum");
			terrainTypesFile.closeGroup(); //b3

			terrainTypesFile.openGroup("height"); //b4
			double minHeight = getDouble(terrainTypesFile, "minimum");
			double maxHeight = getDouble(terrainTypesFile, "maximum");
			terrainTypesFile.closeGroup(); //b4

			terrainTypesFile.openGroup("height difference"); //b5
			double minHeightDifference = getDouble(terrainTypesFile, "minimum");
			double maxHeightDifference = getDouble(terrainTypesFile, "maximum");
			terrainTypesFile.closeGroup(); //b5


			terrainTypesFile.closeGroup(); //a1



			addType(terrainTypeID, new TerrainType(IDType<TerrainType>(idGiver++), water, unreachable, habitable, minTemp, maxTemp, minHumidity, maxHumidity, minWaterHeight, maxWaterHeight, minHeight, maxHeight, minHeightDifference, maxHeightDifference));
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	IDType<TerrainType> TerrainTypeLoader::getID(std::string stringID) const
	{
		return stringIDToID.at(stringID);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	TerrainType *TerrainTypeLoader::getTerrainType(IDType<TerrainType> id) const
	{
		return iDToTerrainType[id.getID()];
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::string TerrainTypeLoader::getStringID(IDType<TerrainType> id) const
	{
		return idToStringID[id.getID()];
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	int TerrainTypeLoader::getNumberOfTerrainTypes() const
	{
		return (int) iDToTerrainType.size();
	}
}