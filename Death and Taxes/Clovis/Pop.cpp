#include "Pop.h"
#include <assert.h>
#include "City.h"
#include "GeneralFunctions.h"
#include "Defines.h"
namespace dat
{

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void Pop::setPricesAndUpForSale()
	{
		assert(resourceTypeloader.getCurrencyID().getID() == 0);
		for (int i = 1; i < (int)resourcePack.getNrOfResourceTypes(); ++i)
		{
			IDType<ResourceType> resourceID(i);
			resourcePack.setUpForSale(resourceID, resourcePack.getResourceAmount(resourceID));
		}
		double missingResourceSum = 0.0;
		const std::vector<std::pair<IDType<ResourceType>, double>> *needs;
		switch (highestFulfillableNeed)
		{
		case NeedType::LuxuryNeeds:
			needs = &popType.luxuryNeeds;
			break;

		case NeedType::EveryDayNeeds:
			needs = &popType.everyDayNeeds;
			break;

		case NeedType::BasicNeeds:
			needs = &popType.basicNeeds;
			break;

		case NeedType::LifeNeeds:
		case NeedType::None:
			needs = &popType.lifeNeeds;
			break;

		default:
			needs = nullptr; //this is done so the compiler won't complain about unitialized variable being used later.
			assert(false);//the function should never have get arguments that lead here.
			break;

		}

		for (auto needs : *needs)
		{
			IDType<ResourceType> resourceID = needs.first;
			double neededAmount = needs.second*popSize * defines.getPopBuyExtraFactor();
			double upForSale = resourcePack.getResourceAmount(resourceID) - neededAmount;
			resourcePack.setUpForSale(resourceID, upForSale);
			if (upForSale < 0.0)
			{
				missingResourceSum += -upForSale;
			}
		}

		double money = resourcePack.getResourceAmount(resourceTypeloader.getCurrencyID());
		assert(resourceTypeloader.getCurrencyID().getID() == 0); //the for loop starts at one because we won't try to buy or sell the currency resource.
		for (int i = 1; i < (int)resourcePack.getNrOfResourceTypes(); ++i)
		{
			IDType<ResourceType> resourceID(i);
			double upForSale = resourcePack.getUpForSale(resourceID);
			if (upForSale < 0.0) //there is a need to buy this resource, not sell
			{
				assert(missingResourceSum > 0.0);//if there is a need to buy something missingResourceSum should be greater than zero
				resourcePack.setPrice(resourceID, money * (-upForSale) / missingResourceSum);
			}
			else //the pop will try to sell this resource.
			{
				double ownedAmount = resourcePack.getResourceAmount(resourceID);
				assert(ownedAmount >= 0.0);
				if (ownedAmount > 0.0)
				{
					if (missingResourceSum >= 1.0) //first I checked if it was higher than zero, but then realised that it makes no sense that they would increasing the price the closer to zero they are after 1.0.
					{
						resourcePack.setPrice(resourceID, money / ownedAmount / missingResourceSum);//the resource is less valuable the more there is of it and the more they need other resources.
					}
					else
					{
						resourcePack.setPrice(resourceID, money / ownedAmount);
					}
				}
				else
				{
					resourcePack.setPrice(resourceID, std::numeric_limits<double>::max());
				}
			}
		}

	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void Pop::checkForBestPrice(City &city, IDType<ResourceType> resourceType, Pop *&bestSeller)
	{
		double bestPrice;
		if (bestSeller == nullptr)
		{
			bestPrice = std::numeric_limits<double>::max();
		}
		else
		{
			bestPrice = bestSeller->getResourcePack().getPrice(resourceType);
		}
		
		for (auto popTypeGroup : city.getPops())
		{
			if (popTypeGroup.size() > 0 && (popTypeGroup[0]->getPopType().merchant || popType.merchant))
			{
				for (auto pop : popTypeGroup)
				{
					if (pop->resourcePack.getUpForSale(resourceType) > 0.0 && pop->resourcePack.getPrice(resourceType) < bestPrice)
					{
						bestPrice = pop->resourcePack.getPrice(resourceType);
						bestSeller = pop;
					}
				}
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void Pop::buyResources()
	{
		assert(resourceTypeloader.getCurrencyID().getID() == 0);
		for (int i = 1; i < resourceTypeloader.getNumberOfResourceTypes(); ++i)
		{
			IDType<ResourceType> id(i);
			double upForSale = resourcePack.getUpForSale(id);
			if (upForSale < 0.0)
			{
				double neededAmount = -upForSale;
				double boughtSum = 0.0;
				do
				{
					Pop *bestSeller = nullptr;

					checkForBestPrice(homeCity, id, bestSeller);
					if (popType.merchant)
					{
						for (auto city : homeCity.getNeighbouringCities())
						{
							checkForBestPrice(homeCity, id, bestSeller);
						}
					}
					if (bestSeller == nullptr || bestSeller->resourcePack.getPrice(id) > resourcePack.getPrice(id))
					{
						break;
					}
					else
					{
						double availableForSale = bestSeller->resourcePack.getResourceAmount(id);
						double boughtNow = min(neededAmount - boughtSum, availableForSale);
						double price = boughtNow*bestSeller->resourcePack.getPrice(id);
						boughtSum += boughtNow;
						resourcePack.transferFromSeller(bestSeller->resourcePack, id, boughtNow, price);
						assert(price > 0.0);
					}

				} while (boughtSum < neededAmount);
			}
		}



		if (popType.merchant)
		{
			for (int i = 1; i < resourceTypeloader.getNumberOfResourceTypes(); ++i)
			{
				IDType<ResourceType> id(i);
				while (true)
				{
					Pop *bestSeller = nullptr;

					checkForBestPrice(homeCity, id, bestSeller);
					for (auto city : homeCity.getNeighbouringCities())
					{
						checkForBestPrice(homeCity, id, bestSeller);
					}
					if (bestSeller == nullptr || bestSeller->resourcePack.getPrice(id) >= resourcePack.getPrice(id))
					{
						break;
					}
					else
					{
						double myMoney = resourcePack.getResourceAmount(resourceTypeloader.getCurrencyID());
						double availableForSale = bestSeller->resourcePack.getResourceAmount(id);
						double pricePerUnit = bestSeller->resourcePack.getPrice(id);
						double spentMoney = min(pricePerUnit*availableForSale, myMoney);
						double boughtNow = spentMoney / pricePerUnit;
						resourcePack.transferFromSeller(bestSeller->resourcePack, id, boughtNow, spentMoney, true);
					}

				}
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void Pop::checkHighestFulfillableNeed()
	{
		bool fulfilled = true;
		for (auto needs : popType.luxuryNeeds)
		{
			if (resourcePack.getResourceAmount(needs.first) < needs.second)
			{
				fulfilled = false;
			}
		}
		if (fulfilled)
		{
			highestFulfillableNeed = NeedType::LuxuryNeeds;
			return;
		}

		fulfilled = true;
		for (auto needs : popType.everyDayNeeds)
		{
			if (resourcePack.getResourceAmount(needs.first) < needs.second)
			{
				fulfilled = false;
			}
		}
		if (fulfilled)
		{
			highestFulfillableNeed = NeedType::EveryDayNeeds;
			return;
		}

		fulfilled = true;
		for (auto needs : popType.basicNeeds)
		{
			if (resourcePack.getResourceAmount(needs.first) < needs.second)
			{
				fulfilled = false;
			}
		}
		if (fulfilled)
		{
			highestFulfillableNeed = NeedType::BasicNeeds;
			return;
		}

		fulfilled = true;
		for (auto needs : popType.lifeNeeds)
		{
			if (resourcePack.getResourceAmount(needs.first) < needs.second)
			{
				fulfilled = false;
			}
		}
		if (fulfilled)
		{
			highestFulfillableNeed = NeedType::LifeNeeds;
			return;
		}

		highestFulfillableNeed = NeedType::None;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void Pop::tryFulfillingNeeds()
	{


		double needsSatisfaction;
		double growthFactor;
		switch (highestFulfillableNeed)
		{
		default:
			assert(false);//all cases are already taken care of.
			break;

		case NeedType::LuxuryNeeds:
			for (auto needs : popType.luxuryNeeds)
			{
				resourcePack.addResourceAmount(needs.first, -needs.second*popSize);
			}
			growthFactor = popType.fulfilledLuxuryNeedsGrowth;
			needsSatisfaction = 1.25;
			break;


		case NeedType::EveryDayNeeds:
			for (auto needs : popType.everyDayNeeds)
			{
				resourcePack.addResourceAmount(needs.first, -needs.second*popSize);
			}
			growthFactor = popType.fulfilledEveryDayNeedsGrowth;
			needsSatisfaction = 1.0;
			break;


		case NeedType::BasicNeeds:
			for (auto needs : popType.basicNeeds)
			{
				resourcePack.addResourceAmount(needs.first, -needs.second*popSize);
			}
			growthFactor = popType.fulfilledBasicNeedsGrowth;
			needsSatisfaction = 0.75;
			break;


		case NeedType::LifeNeeds:
			for (auto needs : popType.lifeNeeds)
			{
				resourcePack.addResourceAmount(needs.first, -needs.second*popSize);
			}
			growthFactor = popType.fulfilledLifeNeedsGrowth;
			needsSatisfaction = 0.5;
			break;

		case NeedType::None:
			double needsNeeded = 0.0, needsSatisfied = 0.0;
			for (auto needs : popType.lifeNeeds) //fulfill the need to the extent that it is possible.
			{
				needsNeeded += needs.second*popSize;
				double consumedAmount = min(resourcePack.getResourceAmount(needs.first), needs.second*popSize);
				needsSatisfied += consumedAmount;
				resourcePack.addResourceAmount(needs.first, -consumedAmount);
			}
			double satisfactionFactor;
			if (needsNeeded != 0.0)
			{
				satisfactionFactor = needsSatisfied / needsNeeded;
			}
			else
			{
				satisfactionFactor = 1.0;
			}
			needsSatisfaction = 0.5*satisfactionFactor;
			growthFactor = popType.fulfilledNoNeedsGrowth * satisfactionFactor;
			break;
		}

		assert(0.0 <= growthFactor);
		popSize *= growthFactor;

		
		happiness = (happiness + needsSatisfaction*defines.getPopSatisfactionHappinessFactor()) / (1.0 + defines.getPopSatisfactionHappinessFactor());
		happiness = max(0.0, min(1.0, happiness));
		fear *= defines.getPopFearLossFactor();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Pop::Pop(Defines &defines, ResourceTypeLoader const &resourceTypeloader, IDType<Pop> id, PopType const &popType, City &homeCity, double popSize, double happiness, double fear) :
		defines(defines), resourceTypeloader(resourceTypeloader), ID(id), popType(popType), homeCity(homeCity), resourcePack(resourceTypeloader)
	{
		Pop::popSize = popSize;
		Pop::happiness = happiness;
		Pop::fear = fear;

		//debug
		for (int i = 0; i < resourceTypeloader.getNumberOfResourceTypes(); ++i)
		{
			resourcePack.addResourceAmount(i, 100000.0);
		}
		//end debug


		highestFulfillableNeed = NeedType::None;
		setPricesAndUpForSale();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Pop::~Pop()
	{
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	double Pop::getPopSize() const
	{
		return popSize;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	IDType<Pop> Pop::getID() const
	{
		return ID;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void Pop::transferFrom(Pop &fromPop, double amount)
	{
		assert(amount > 0.0);
		assert(fromPop.getPopSize() >= amount);
		assert(fromPop.getPopSize() > 0.0);
		double movedPercentage = amount / fromPop.getPopSize();
		for (int i = 0; i < resourcePack.getNrOfResourceTypes(); ++i)
		{
			IDType<ResourceType> resourceType(i);
			double movedResources = fromPop.resourcePack.getResourceAmount(resourceType)*movedPercentage;
			fromPop.resourcePack.addResourceAmount(resourceType, - movedResources);
			resourcePack.addResourceAmount(resourceType, movedResources);
		}

		double newMigrantPercentage = amount / (getPopSize() + amount);

		const Ideology::IdeologyScale IDEOLOGY_SCALES[3] = { Ideology::IdeologyScale::Economic, Ideology::IdeologyScale::Governmental, Ideology::IdeologyScale::Personal};//the other's avarage does not change
		for (auto ideologyScale : IDEOLOGY_SCALES)
		{
			double newStance = ideology.getStance(ideologyScale)*(1.0 - newMigrantPercentage) + fromPop.ideology.getStance(ideologyScale)*newMigrantPercentage;
			ideology.setIdeologyStance(newStance, ideologyScale);
		}

		happiness = happiness*(1.0 - newMigrantPercentage) + fromPop.happiness*newMigrantPercentage;
		fear = fear*(1.0 - newMigrantPercentage) + fromPop.fear*newMigrantPercentage;
		assert(happiness >= 0.0 && happiness <= 1.0);
		assert(fear >= 0.0 && fear <= 1.0);

		
		popSize += amount;
		fromPop.popSize -= amount;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	City &Pop::getHomeCity()
	{
		return homeCity;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	PopType const &Pop::getPopType() const
	{
		return popType;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void Pop::createCitizens(double amount)
	{
		popSize += amount;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	ResourcePack &Pop::getResourcePack()
	{
		return resourcePack;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Ideology &Pop::getIdeology()
	{
		return ideology;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Pop::NeedType Pop::getHighestFulfillableNeed() const
	{
		return highestFulfillableNeed;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void Pop::calcTick(int tickTime)
	{
		if (tickTime % 20 == 0)
		{
			tryFulfillingNeeds();
			checkHighestFulfillableNeed();
		}

		setPricesAndUpForSale();
		buyResources();
	}
}