#include "GeneralFunctions.h"

#include <stdlib.h>
#include <cmath>

namespace dat
{

	int random(int max, int min)
	{
		return rand() % (max - min) + min;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	double randomAngle()
	{
		double sign = double(1 - (rand() % 2) * 2);
		return double(rand() % 1000) / 1000.0 * acos(0.0) * 2.0*sign;
	}
}