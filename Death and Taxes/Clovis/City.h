#pragma once
#include <vector>
#include "VectorXD.h"
#include "IDType.h"
#include "InfrastructureLevel.h"
#include "IDDealingList.h"
namespace dat
{
	class InfrastructureTypeLoader;
	class PopTypeLoader;
	class ResourceTypeLoader;
	class PopType;
	class Pop;
	class GameWorld;
	class Defines;
	class City //debug missing techlevel and infrastructurelevel
	{
	private:
		Defines &defines;
		GameWorld &gameWorld;
		InfrastructureLevel infrastructureLevel;
		unsigned numberOfPops; //used to tell if the city is inhabited.
		std::vector<std::vector<Pop *>> pops;//for the first index use IDType<PopType>, for the second one you'll just have to search through
		std::vector<City *> neighbouringCities;
		const Vector2D<int> coordinate;
		void deletePop(Pop *pop);
		void createPopGroup(ResourceTypeLoader &resourceTypeloader, PopType const &popType, double popSize); //creates the pop and adds it to the poplist of the world, should only be used when there is no group that already matches this pop.
		InfrastructureTypeLoader &infrastructureTypeLoader;
		ResourceTypeLoader &resourceTypeLoader;
		PopTypeLoader &popTypeLoader;
		const IDType<City> ID;
	public:
		City(Defines &defines, GameWorld &gameWorld, ResourceTypeLoader &resourceTypeLoader, PopTypeLoader &popTypeLoader, InfrastructureTypeLoader &infrastructureTypeLoader, IDType<City> id, Vector2D<int> coordinate); //this is to be used when the game is generating cities at the beginning.
		City(Defines &defines, GameWorld &gameWorld, ResourceTypeLoader &resourceTypeLoader, PopTypeLoader &popTypeLoader, InfrastructureTypeLoader &infrastructureTypeLoader, IDType<City> id, Vector2D<int> coordinate, Pop &foundingPop, double movingPercentage); //The foundingPop is the pop the foudners come from, the percentage is how many of them that move.
		~City();//deletes all pops and removes 'this' from all neighbouringCities' 'neighbouringCities'

		const std::vector<std::vector<Pop *>> &getPops() const;//for the first index use IDType<PopType>, for the second one you'll just have to search through
		const std::vector<Pop *> &getPops(IDType<PopType> &popType) const;
		const std::vector<City *> &getNeighbouringCities() const;
		Vector2D<int> getCoordinate() const; //returns the position of the city.
		void addCityNeighbour(City *otherCity); //adds 'otherCity' as a neighbour to 'this', and 'this' as a neighbour to 'otherCity'.
		void removeCityNeighbour(City *otherCity);//removes 'otherCity' as a neighbour to 'this', and 'this' as a neighbour to 'otherCity'.
		bool inhabited() const;//return true if the city has any pop.
		void calcTick(int tickTime);//calcs all the pops' stat changes for the next tick.
		double getPopulation() const; //calculates and returns the amount of people in this city.
		IDType<City> getID() const; 
		void destroyCity(); //destroys the city and all pops within it. Called before removing a city from the game.
	};

}