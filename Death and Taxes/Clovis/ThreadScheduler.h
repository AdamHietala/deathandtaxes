#pragma once
#include <vector>
#include <functional>
#include <list>
#include <thread>
#include <mutex>
#include <atomic>
#include <condition_variable>

//#define SINGLE_THREADED
namespace dat
{
	class ThreadScheduler
	{
	public:
		class WaitCondition
		{
		private:
			int worksToFinnish;
		public:
			WaitCondition(int worksToFinnish);
			~WaitCondition();
			bool shouldWait(); //returns false 
			void reportFinnishedWork(); //this should only be called by threads when they finnished doing work.
		};
	private:
		std::vector<std::thread *> threads;
		std::list<std::function<void(void)>> workList; //The list of functions that need to be run. No infinite loops are supposed to be in there since theese threads do all the loose work.
		std::atomic<bool> runThreads; //Tells the threads whether to continue to run or not.
		std::mutex workMutex; //The mutex protecting the workList
		std::condition_variable workToDoCondition, waitForWorkToFinishCondition;
		int worksInProgress;
		void helpWithWork(); //Makes the calling thread help with the work list. Will return when the worklist is empty.
		void helpWithWorkOnce(); //Makes the calling thread help with the work list. Will return after having done one piece of work.
		int getWorkSizeUnprotected(); //returns the number of works in worklist plus worksInProgress. Assumes the workMutex is already locked.
		void addToWorkList(std::function<void(void)> work); //Adds a work function to the worklist.
	public:
		ThreadScheduler();
		~ThreadScheduler();

		std::shared_ptr<WaitCondition> scheduleWork(std::function<void(void* p)> function, void* argument); //Adds a function to the workList. Returns a waitcondition to keep track of when the work is done.
		std::shared_ptr<WaitCondition> scheduleWork(std::pair<std::function<void(void* p)>, void*> work); //Adds a function to the workList.  Returns a waitcondition to keep track of when the work is done.
		std::shared_ptr<WaitCondition> scheduleWork(std::vector<std::pair<std::function<void(void* p)>, void*>> work); //Adds a function to the workList.  Returns a waitcondition to keep track of when the work is done.
		void setNumberOfThreads(int numberOfThreads); //Sets the number of threads working on the workList. Can be changed runtime at any point but will probably cause a lagspike since all threads are shut down before new ones are created.
		int getNumberOfThreads(); //returns the number of threads this threadScheduler keeps track of. 
		bool threadsAreWorking(); //Returns false if no threads are working
		void waitForWorkToFinish(); //waits until all work is finised. This thread will help out with work until it is.
		void waitWhileCondition(std::function<bool()> shouldIWait, bool doWorkWhileWaiting = false); //This function should only be called if this ThreadScheduler actually has some work listed that this thread wants to wait for. Makes this thread wait until condition returns false. The thread will be inactive if doWorkWhileWaiting is false, otherwise it will be used to do work on the queue of scheduled work until the work it's waiting for is done. But this means it might be stuck doing other work, even though the work it was waiting for might have finished.
		int getWorkSize(); //returns the number of works in worklist plus worksInProgress
	};
}