#pragma once
#include "PopTypeImageLoader.h"
#include "ResourceTypeImageLoader.h"
namespace dat
{
	class ResourceTypeLoader;
	class TypeImageLoader : public LoadsOnce
	{
		PopTypeImageLoader popTypeImageLoader;
		ResourceTypeImageLoader resourceTypeImageLoader;
	public:
		TypeImageLoader();
		~TypeImageLoader();

		void load(ModuleLoader  &moduleLoader, AnimationLoader const &animationLoader, PopTypeLoader const &popTypeLoader, ResourceTypeLoader const &resourceTypeLoader);
		PopTypeImageLoader const &getPopTypeImageLoader() const;
		ResourceTypeImageLoader const &getResourceTypeImageLoader() const;
	};

}