#pragma once
#include "IDType.h"
#include <vector>
#include "ResourceType.h"
#include "TechType.h"
#include "Technology.h"
#include "InfrastructureType.h"
namespace dat
{
	class Recipe : NonCopyable
	{
	private:
	public:
		Recipe(IDType<Recipe> id, std::vector<std::pair<IDType<ResourceType>, double>> neededResources, std::vector<std::pair<IDType<ResourceType>, double>> yieldedResources, std::vector<std::pair<IDType<TechType>, double>> minimumTechLevel, std::vector<std::pair<IDType<TechType>, double>> maximumTechLevel, std::vector<IDType<Technology>> neededTechs, std::vector<IDType<Technology>> forbiddenTechs, std::vector<std::pair<IDType<InfrastructureType>, double>> minimumInfrastructureLevel, std::vector<std::pair<IDType<InfrastructureType>, double>> maximumInfrastructureLevel);
		~Recipe();

		const IDType<Recipe> id;

		const std::vector<std::pair<IDType<ResourceType>, double>> neededResources;
		const std::vector<std::pair<IDType<ResourceType>, double>> yieldedResources;

		const std::vector<std::pair<IDType<TechType>, double>> minimumTechLevel;
		const std::vector<std::pair<IDType<TechType>, double>> maximumTechLevel;

		const std::vector<IDType<Technology>> neededTechs;
		const std::vector<IDType<Technology>> forbiddenTechs;

		const std::vector<std::pair<IDType<InfrastructureType>, double>> minimumInfrastructureLevel;
		const std::vector<std::pair<IDType<InfrastructureType>, double>> maximumInfrastructureLevel;
	};

}