#pragma once
#include <SFML\Graphics.hpp>
#include "NonCopyable.h"
#include "Shapes.h"
namespace dat
{
	class GUI;
	class Widget : NonCopyable
	{
	private:
		bool active;
	protected:
		static const sf::RenderStates renderStatesFilterAlphaThroughAlpha;
		static const sf::RenderStates renderStatesFilterThroughAlpha;
		static void resetDrawingAreaAlpha(sf::RenderTarget &renderTarget);
		static void prepareDrawingAreaAlpha(sf::RenderTarget &renderTarget, Rectangle<float> drawingArea, bool respectAlpha = false); //must be called before drawing in the drawing area to make sure nothing is drawn outside of the area. Must sometimes be done again if more than one image with alpha channels will overlap.
		Widget();
	public:
		virtual ~Widget();
		bool isActive() const;

		//It's up to the widget to determine if the event was relevant for it or not.
		virtual void mouseMoved(bool &eventClaimed, Vector2D<int> mousePos){}
		virtual void mouseButtonPressed(bool &eventClaimed, Vector2D<int> mousePos, sf::Mouse::Button button){}
		virtual void mouseButtonReleased(bool &eventClaimed, Vector2D<int> mousePos, sf::Mouse::Button button){}
		virtual void mouseScrolled(bool &eventClaimed, Vector2D<int> mousePos, int delta){}
		virtual void keyPressed(bool &eventClaimed, sf::Keyboard::Key key){}
		virtual void keyReleased(bool &eventClaimed, sf::Keyboard::Key key){}
		virtual void draw(sf::RenderTarget &renderTarget, sf::Time time, bool respectAlpha = false) = 0;
		virtual void setActive(bool active);
	};

}