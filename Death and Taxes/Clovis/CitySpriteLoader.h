#pragma once
#include "LoadsOnce.h"
#include <vector>
#include "IDType.h"
namespace dat
{
	class ModuleLoader;
	class AnimationLoader;
	class TechTreeLoader;
	class InfrastructureTypeLoader;
	class InfrastructureType;
	class TechType;
	class Technology;
	class City;
	class Animation;
	class CitySpriteLoader : public LoadsOnce
	{
	private:
		struct CitySprite
		{
			double minSize;
			double maxSize;
			std::vector<Animation *> animations;
			std::vector<std::pair<IDType<InfrastructureType>, double>> infrastructureNeed, infrastructureForbidden;
			std::vector<std::pair<IDType<TechType>, double>> techTypeNeed, techTypeForbidden;
			std::vector<IDType<Technology>> technologiesNeeded, technologiesForbidden;
		};
		std::vector<CitySprite *> citySprites;
	public:
		CitySpriteLoader();
		~CitySpriteLoader();

		void load(ModuleLoader &moduleLoader, AnimationLoader const &animationLoader, TechTreeLoader const &techTreeLoader, InfrastructureTypeLoader const &infrastructureTypeLoader); //relies on animationLoader, techTreeLoader, infrastructureTypeLoader

		Animation *getAnimation(City const &city, int choice) const; //choice will be used to select among several animations. No value is invalid.
	};

}