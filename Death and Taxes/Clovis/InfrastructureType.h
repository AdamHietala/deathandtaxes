#pragma once
#include "IDType.h"
#include <vector>
#include "TechType.h"
#include "Technology.h"
#include "ResourceType.h"
namespace dat
{
	class InfrastructureType : NonCopyable
	{
	private:

	public:
		InfrastructureType(IDType<InfrastructureType> id, std::vector<std::pair<IDType<TechType>, double>> neededTechLevel, std::vector<std::pair<IDType<ResourceType>, double>> resourceCosts, std::vector<std::pair<IDType<Technology>, double>> technologyLimits);
		~InfrastructureType();
		const IDType<InfrastructureType> id;
		const std::vector<std::pair<IDType<TechType>, double>> neededTechLevel; //the tech level needed to increase infrastructure by 1.0. Infrastructurelevel cannot grow if it is >= neededTechLevel[i].second
		const std::vector<std::pair<IDType<ResourceType>, double>> resourceCosts; //the resources needed to increase infrastructure by 1.0. Payed for by the state or interested pops.
		const std::vector<std::pair<IDType<Technology>, double>> technologyLimits; //if the city or the building pop lacks this technology the infrastructure can't go higher than technologyLimiters[i].second
	};

}