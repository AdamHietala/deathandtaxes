#pragma once
#include <vector>
#include "IDType.h"
#include "NonCopyable.h"
namespace dat
{
	class TechType;
	class Technology : NonCopyable
	{
	private:
		std::vector<IDType<Technology>> neededTechs; 
		std::vector<double> neededTechTypeLevels; //Stores the minimum tech level needed for this tech of each tech type. The techTypes' ids are used for index.
		const IDType<Technology> id;
	public:
		Technology(IDType<Technology> id, int nrOfTechTypes);
		~Technology();
		IDType<Technology> getID() const;//returns this technology's id.
		bool isUnlockable(std::vector<double> techTypeLevels, std::vector<bool> techUnlocked) const;//returns true if this technology is unlockable.
		void addTechnologyDependency(IDType<Technology> id);
		void addTechTypeLevelNeed(IDType<TechType> id, double level);
	};
}