#pragma once
namespace dat
{



#define DEFAULT_FUNCTIONS							\
	SIZE_OPERATOR(< );								\
	SIZE_OPERATOR(<= );								\
	SIZE_OPERATOR(> );								\
	SIZE_OPERATOR(>= );								\
													\
													\
	MATHS_OPERATOR(*, EQUALS_VERSION);				\
	MATHS_OPERATOR(/ , EQUALS_VERSION);				\
	MATHS_OPERATOR(+, EQUALS_VERSION);				\
	MATHS_OPERATOR(-, EQUALS_VERSION);				\
	MATHS_OPERATOR(<< , EQUALS_VERSION);			\
	MATHS_OPERATOR(>> , EQUALS_VERSION);			\
	MATHS_OPERATOR(| , EQUALS_VERSION);				\
	MATHS_OPERATOR(^, EQUALS_VERSION);				\
	MATHS_OPERATOR(&, EQUALS_VERSION);				\
	MATHS_OPERATOR(%, EQUALS_VERSION);				\
													\
													\
	COMPARISON_OPERATOR(== , &&);					\
	COMPARISON_OPERATOR(&&, &&);					\
	COMPARISON_OPERATOR(|| , || );					\
													\
	ASSIGNING_OPERATOR(= );							\
													\
	PLUSPLUS_OPERATOR(++);							\
	PLUSPLUS_OPERATOR(--);		

	///////////////////////////////
	//definitions for 2d version///
	///////////////////////////////
#define COMPARISON_OPERATOR(OPERATOR, BINDING_OPERATOR)								\
	bool operator ## OPERATOR(const Vector2D<type>& otherVect) const				\
	{																				\
		return	(x OPERATOR otherVect.x) BINDING_OPERATOR							\
				(y OPERATOR otherVect.y);											\
	}

#define SIZE_OPERATOR(OPERATOR)														\
	bool operator OPERATOR (const Vector2D<type> &otherVect) const					\
	{																				\
		return sqrMagnitude() OPERATOR otherVect.sqrMagnitude()						\
	}

#define MATHS_EQUAL_OPERATOR(OPERATOR)												\
	Vector2D& operator OPERATOR ## = (const Vector2D<type> &otherVect)				\
	{																				\
		x OPERATOR ## = otherVect.x;												\
		y OPERATOR ## = otherVect.y;												\
		return *this;																\
	}																				\
	Vector2D& operator OPERATOR ## = (type otherValue)								\
	{																				\
		x OPERATOR ## = otherValue;													\
		y OPERATOR ## = otherValue;													\
		return *this;																\
	}																		

#define MATHS_CALCULATION_OPERATOR(OPERATOR)										\
		Vector2D operator OPERATOR (const Vector2D<type> &otherVect) const			\
		{																			\
			return Vector2D(														\
				x OPERATOR otherVect.x,												\
				y OPERATOR otherVect.y												\
			);																		\
		}																			\
		Vector2D operator OPERATOR (type otherValue) const							\
		{																			\
			return Vector2D(														\
				x OPERATOR otherValue, 												\
				y OPERATOR otherValue												\
			);																		\
		}

#define EQUALS_VERSION(OPERATOR) MATHS_EQUAL_OPERATOR(OPERATOR)
#define NO_EQUALS_VERSION(OPERATOR)
#define MATHS_OPERATOR(OPERATOR, EQUALS_VERSION) MATHS_CALCULATION_OPERATOR(OPERATOR) EQUALS_VERSION(OPERATOR)

#define ASSIGNING_OPERATOR(OPERATOR)												\
	Vector2D & operator OPERATOR (const Vector2D<type> & otherVect)					\
	{																				\
		x OPERATOR otherVect.x;														\
		y OPERATOR otherVect.y;														\
		return *this;																\
	}																				\
	Vector2D& operator OPERATOR (type otherValue)									\
	{																				\
		x OPERATOR otherValue;														\
		y OPERATOR otherValue;														\
		return *this;																\
	}	


#define PLUSPLUS_OPERATOR(OPERATOR)													\
	Vector2D& operator ## OPERATOR()												\
	{																				\
		OPERATOR ## x;																\
		OPERATOR ## y;																\
		return *this;																\
	}																				\
	Vector2D operator ## OPERATOR(int)												\
	{																				\
		Vector2D currentVal(*this);													\
		operator ## OPERATOR();														\
		return currentVal;															\
	}


					


	template <typename type> class Vector2D
	{
	public:
		type x, y;
		Vector2D(type x, type y) : x(x), y(y) {}
		Vector2D(void) {}
		~Vector2D(void) {}

		DEFAULT_FUNCTIONS

		bool operator!=(const Vector2D<type>& otherVect) const
		{
			return !(*this == otherVect);
		}
		Vector2D operator-() const
		{
			return Vector2D(-x, -y);
		}
		template<typename type2> Vector2D<type2> typecast() const
		{
			return Vector2D<type2>(type2(x), type2(y));
		}
		type manhattanMagnitude() const
		{
			return x + y;
		}
		type sqrMagnitude() const
		{
			return x * x + y * y;
		}
		type magnitude() const
		{
			return sqrt(sqrMagnitude());
		}
		Vector2D<type> abs() const
		{
			return Vector2D<type>(x > 0 ? x : -x, y > 0 ? y : -y);
		}
		type area() const
		{
			return x*y;
		}


		static Vector2D<type> rotatePoint(Vector2D<type> pivot, Vector2D<type> point, type angle)
		{
			// translate point back to origin:
			Vector2D<type> translatedPoint = point - pivot;

			// rotate point
			type s = sin(angle);
			type c = cos(angle);
			Vector2D<type> adjustedPoint;
			adjustedPoint.x = translatedPoint.x * c - translatedPoint.y * s;
			adjustedPoint.y = translatedPoint.x * s + translatedPoint.y * c;


			return adjustedPoint + pivot;
		}
	};




	///////////////////////////////
	//redefinitions for 3d version///
	///////////////////////////////

#undef COMPARISON_OPERATOR
#define COMPARISON_OPERATOR(OPERATOR, BINDING_OPERATOR)								\
	bool operator ## OPERATOR(const Vector3D<type>& otherVect) const				\
	{																				\
		return	(x OPERATOR otherVect.x) BINDING_OPERATOR							\
				(y OPERATOR otherVect.y) BINDING_OPERATOR							\
				(z OPERATOR otherVect.z);											\
	}

	
#undef MATHS_EQUAL_OPERATOR
#define MATHS_EQUAL_OPERATOR(OPERATOR)												\
	Vector3D& operator OPERATOR ## = (const Vector3D<type> &otherVect)				\
	{																				\
		x OPERATOR ## = otherVect.x;												\
		y OPERATOR ## = otherVect.y;												\
		z OPERATOR ## = otherVect.z;												\
		return *this;																\
	}																				\
	Vector3D& operator OPERATOR ## = (type otherValue)								\
	{																				\
		x OPERATOR ## = otherValue;													\
		y OPERATOR ## = otherValue;													\
		z OPERATOR ## = otherValue;													\
		return *this;																\
	}		

#undef MATHS_CALCULATION_OPERATOR
#define MATHS_CALCULATION_OPERATOR(OPERATOR)										\
		Vector3D operator OPERATOR (const Vector3D<type> &otherVect) const			\
		{																			\
			return Vector3D(														\
				x OPERATOR otherVect.x,												\
				y OPERATOR otherVect.y,												\
				z OPERATOR otherVect.z												\
			);																		\
		}																			\
		Vector3D operator OPERATOR (type otherValue) const							\
		{																			\
			return Vector3D(														\
				x OPERATOR otherValue, 												\
				y OPERATOR otherValue,												\
				z OPERATOR otherValue												\
			);																		\
		}

#undef ASSIGNING_OPERATOR
#define ASSIGNING_OPERATOR(OPERATOR)												\
	Vector3D & operator OPERATOR (const Vector3D<type> & otherVect)					\
	{																				\
		x OPERATOR otherVect.x;														\
		y OPERATOR otherVect.y;														\
		z OPERATOR otherVect.z;														\
		return *this;																\
	}																				\
	Vector3D& operator OPERATOR (type otherValue)									\
	{																				\
		x OPERATOR otherValue;														\
		y OPERATOR otherValue;														\
		z OPERATOR otherValue;														\
		return *this;																\
	}	

#undef PLUSPLUS_OPERATOR
#define PLUSPLUS_OPERATOR(OPERATOR)													\
	Vector3D& operator ## OPERATOR()												\
	{																				\
		OPERATOR ## x;																\
		OPERATOR ## y;																\
		OPERATOR ## z;																\
		return *this;																\
	}																				\
	Vector3D operator ## OPERATOR(int)												\
	{																				\
		Vector3D currentVal(*this);													\
		operator ## OPERATOR();														\
		return currentVal;															\
	}


	template <typename type> class Vector3D
	{
	public:
		type x, y, z;
		Vector3D(type x, type y, type z):x(x), y(y), z(z) {}
		Vector3D(void){}
		~Vector3D(void){}

		DEFAULT_FUNCTIONS


		bool operator!=(const Vector3D<type>& otherVect) const
		{
			return !(*this == otherVect);
		}
		Vector3D operator-() const
		{
			return Vector3D(-x, -y, -z);
		}
		template<typename type2> Vector3D<type2> typecast() const
		{
			return Vector3D<type2>(type2(x), type2(y), type2(z));
		}
		type manhattanMagnitude() const
		{
			return x + y + z;
		}
		type sqrMagnitude() const
		{
			return x * x + y * y + z * z;
		}
		type magnitude() const
		{
			return sqrt(sqrMagnitude());
		}
		Vector3D<type> abs() const
		{
			return Vector3D<type>(x > 0 ? x : -x, y > 0 ? y : -y, z > 0 ? z : -z);
		}
		type volume() const
		{
			return x * y * z;
		}
	};

}

//cleanup

#undef DEFAULT_FUNCTIONS

#undef EQUALS_VERSION
#undef NO_EQUALS_VERSION
#undef MATHS_OPERATOR

#undef COMPARISON_OPERATOR
#undef SIZE_OPERATOR
#undef MATHS_EQUAL_OPERATOR
#undef MATHS_CALCULATION_OPERATOR
#undef ASSIGNING_OPERATOR
#undef PLUSPLUS_OPERATOR



