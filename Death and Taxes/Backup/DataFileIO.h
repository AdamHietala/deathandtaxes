#pragma once
#include <map>
#include <string>
#include <vector>
#include "NonCopyable.h"
namespace dat
{
	class DataFileIO : NonCopyable
	{
	private:
		class Group
		{
		public:
			Group *const up;
			std::map<std::string, std::string> strings; //All strings within this Group.
			std::map<std::string, Group *> subGroups; //All direct subGroups within this group.
			Group(Group *up);
			~Group();
		};
		std::string toString(std::string option, Group *group, std::string indentation = ""); //Creates a string representing currentGroup. Will go through subgroups recursively.
		void loadFromString(std::string &str, Group *group, int from, int to); //Loads this group from a string and all subgroups recursively. Variables from and to tells what parts of the string to read. Throws ParsingException if there is an error in the string and KeyNotFoundException if the string is trying to delete or clear a group or string that does not exist.
		void getNextWord(std::string &str, std::string &word, int &from, int to, bool key); //Returns the next word while parsing a load from string. Will make whatever variable put in "from" to tick towards to. If key is true the function won't search for a whitespace to end the word, but a '='.
		int findEndBracket(std::string &str, int from, int to); //Will return the position of the endbracket to the bracket str[from]. That str[from] == '{' is asserted. Will throw ParsingException if to is reached before an endbracket.
		std::string escapeSpecialCharacters(std::string &str); //Will escape the special characters '\\', '}' and '#'.
		std::string parseEscapeSequences(std::string &str);	//Will turn the escaped characters to normal ones. Any unknown combination e.g. "\a" will be interpreted as two separate chars '\\' and 'a'.

		Group anchor; //The highest level group there is.
		Group *openedGroup; //The group currently opened.
	public:
		DataFileIO();
		~DataFileIO();
		enum class Option { StrongAdd, WeakAdd, Delete, Clear }; //Options to write in front of each command in the file.
		std::string optionToString(Option option); //Gives the official string representation of the enums used in the string representation of the DataFileIO.
		Option stringToOption(std::string str); //Converts a string representation of an option to the option enum. Throws ParsingException if the string could not be interpreted as an option.


		void addGroup(std::string key); //Creates a subgroup within the opened Group. Throws KeyAlreadyExistsException if the group already exists. Opens the group after creating it.
		void addString(std::string key, std::string content = ""); //Creates a string within the opened Group. Throws KeyAlreadyExistsException if the string key already exists.
		void removeGroup(std::string key); //Removes the group with the name key. Throws KeyNotFoundException if the key does not exist.
		void removeString(std::string key); //Removes the string with the name key. Throws KeyNotFoundException if the key does not exist.
		void clearGroup(std::string key); //Clears the group with the name key of all subgroups and strings. Throws KeyNotFoundException if the key does not exist.
		void clearString(std::string key); //Clears the string with the name key and makes it into an empty string. Throws KeyNotFoundException if the key does not exist.
		bool existsGroup(std::string key); //Returns true if the group key exists. Otherwise false.
		bool existsString(std::string key); //Returns true if the string key exists. Otherwise false.

		void openGroup(std::string key); //Opens the subgroup key. Opens the newly created group. Throws KeyNotFoundException if key doesn't exist.
		void closeGroup(); //Jumps one step up in the subgroups. Throws AlreadyAtTopLevelException if the current opened group is already the topgroup.
		std::string getString(std::string key); //Gets the string content of the string key. Throws KeyNotFoundException if key doesn't exist.
		std::vector<std::string> getStringKeys(); //Returns all keys in the opened group refering to strings.
		std::vector<std::string> getGroupKeys(); //Returns all the keys to all the subgroups of the opened group.

		void loadFromString(std::string str); //Sets up the DataFileIO from a string taken from a file or another DataFileIO. Throws ParsingException if the string could not be parsed and a KeyNotFoundException if the string is trying to delete or clear a group or string that does not exist.
		std::string toString(bool strong = true); //Creates a string representing this DataFileIO from the data it contains to be loaded in another dataFileIO or saved in a file. If strong is true add will be used, otherwise wadd.
		void loadFromFile(std::string filePath); //Loads this DataFileIO from filePath. Throws ParsingException if the string could not be parsed, IOException if the file could not be opened and KeyNotFoundException if the file is trying to delete or clear a group or string that does not exist.
		void saveToFile(std::string filePath, bool strong = true); //Saves this DataFileIO to filePath. Throws IOException if the file could not be saved. If strong is true add will be used, otherwise wadd.
		void clearData(); //Clears all data written to this DataFileIO.

		void closeAllGroups(); //Closes all opened groups.
		bool anyGroupOpened(); //return true if the openedGroup is not anchor.
	};
}
