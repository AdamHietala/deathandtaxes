#pragma once
#include <string>
#include <vector>
namespace dat
{
	std::string multiplyString(std::string str, int times); //Concatinates str "times" times after itself and returns it as a new string
	bool isSpace(std::string str);
	bool isSpace(char character);
	std::string join(std::vector<std::string> &lines, std::string glue = "");
	std::string trimWhiteSpaces(std::string str);
	std::vector<std::string> trimWhiteSpaces(std::vector<std::string> &lines);
	std::string getSubString(std::string str, unsigned from, unsigned to); //From and including "from" and to and excluding "to".
	bool isInt(char chr);
	bool isInt(std::string str);
	bool isDouble(std::string str);
	std::vector<std::string> split(std::string str, std::string splitter);
	std::wstring stringToWstring(std::string str);
	std::string replaceChar(std::string string, char oldChar, char newChar); //searches through the string and replaces all instances of oldChar with newChar.
	std::string replaceStr(std::string string, std::string oldstr, std::string newStr); //searches through string and replaces all instances of oldstr with newStr.
	std::string lower(std::string str); //Converts all upper case chars to lower case chars.
	std::string upper(std::string str); //Converts all lower case chars to upper case chars.
	bool charInStr(std::string str, char chr); //Tells you wether a string contains at least one instance of chr.
	int findFirstOccurrence(std::string str, std::string subStr, int start = 0); //Will return the first occurance of the string subStr inside "str". Start defines where to begin the search. The function will never search left ot start. If no occurance was found the length of the string will be returned. 
	std::vector<std::string> sortStrings(std::vector<std::string> strings); //Sorts the string alphabetically and numerically.
	std::string toString(double number, int decimals);
}