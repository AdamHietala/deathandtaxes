#pragma once
#include <vector>
#include <functional>
#include <list>
#include <thread>
#include <mutex>
namespace dat
{
	class ThreadScheduler
	{
	private:
		std::vector<std::thread *> threads;
		std::list<std::function<void(void *p)>> workList; //The list of functions that need to be run. No infinate loops are supposed to be in there since theese threads do all the loose work.
		std::list<void *> arguments;
		bool runThreads; //Tells the threads whether to continue to run or not.
		std::mutex workMutex; //The mutex protecting the workList
		int worksInProgress;
		void helpWithWork(); //Makes the calling thread help with the work list. Will return when the worklist is empty.
	public:
		ThreadScheduler();
		~ThreadScheduler();

		void scheduleWork(std::function<void(void *p)> work, void *argument); //Adds a function to the workList
		void setNumberOfThreads(int numberOfThreads); //Sets the number of threads working on the workList. Can be changed runtime at any point but will probably cause a lagspike since all threads are shut down before new ones are created.
		int getNumberOfThreads();
		bool threadsAreWorking(); //Returns true if no threads are working
		void waitForWorkToFinish();
	};
}