#pragma once
namespace dat
{
	template <typename type> class Vector2D
	{
	public:
		type x, y;
		Vector2D(type x, type y) : x(x), y(y) {}
		Vector2D(void){}
		~Vector2D(void){}

		bool operator==(const Vector2D<type> &otherVect) const
		{
			return x == otherVect.x && y == otherVect.y;
		}
		bool operator!=(const Vector2D<type> &otherVect) const
		{
			return !(*this == otherVect);
		}
		bool operator<(const Vector2D<type> &otherVect) const
		{
			if (x == otherVect.x)
			{
				return y < otherVect.y;
			}
			else
			{
				return x < otherVect.x;
			}
		}
		bool operator<=(const Vector2D<type> &otherVect) const
		{
			if (x == otherVect.x)
			{
				return y <= otherVect.y;
			}
			else
			{
				return x <= otherVect.x;
			}
		}
		bool operator>(const Vector2D<type> &otherVect) const
		{
			if (x == otherVect.x)
			{
				return y > otherVect.y;
			}
			else
			{
				return x > otherVect.x;
			}
		}
		bool operator>=(const Vector2D<type> &otherVect) const
		{
			if (x == otherVect.x)
			{
				return y >= otherVect.y;
			}
			else
			{
				return x >= otherVect.x;
			}
		}
		//multiplication
		Vector2D& operator*=(const Vector2D<type> &otherVect)
		{
			x *= otherVect.x;
			y *= otherVect.y;
			return *this;
		}
		Vector2D& operator*=(type otherValue)
		{
			x *= otherValue;
			y *= otherValue;
			return *this;
		}
		Vector2D operator*(const Vector2D<type> &otherVect) const
		{
			return Vector2D(x * otherVect.x, y * otherVect.y);
		}
		Vector2D operator*(type otherValue) const
		{
			return Vector2D(x * otherValue, y * otherValue);
		}

		//division
		Vector2D& operator/=(const Vector2D<type> &otherVect)
		{
			x /= otherVect.x;
			y /= otherVect.y;
			return *this;
		}
		Vector2D& operator/=(type otherValue)
		{
			x /= otherValue;
			y /= otherValue;
			return *this;
		}
		Vector2D operator/(const Vector2D<type> &otherVect) const
		{
			return Vector2D(x / otherVect.x, y / otherVect.y);
		}
		Vector2D operator/(type otherValue) const
		{
			return Vector2D(x / otherValue, y / otherValue);
		}

		//addition
		Vector2D& operator+=(const Vector2D<type> &otherVect)
		{
			x += otherVect.x;
			y += otherVect.y;
			return *this;
		}
		Vector2D& operator+=(type otherValue)
		{
			x += otherValue;
			y += otherValue;
			return *this;
		}
		Vector2D operator+(const Vector2D<type> &otherVect) const
		{
			return Vector2D(x + otherVect.x, y + otherVect.y);
		}
		Vector2D operator+(type otherValue) const
		{
			return Vector2D(x + otherValue, y + otherValue);
		}
		Vector2D& operator++()
		{
			++x;
			++y;
			return *this;
		}
		Vector2D operator++(int)
		{
			Vector2D currentVal(*this);
			operator++();
			return currentVal;
		}

		//subtraction
		Vector2D operator-() const
		{
			return Vector2D(-x, -y);
		}
		Vector2D operator-(const Vector2D<type> &otherVect) const
		{
			return Vector2D(x - otherVect.x, y - otherVect.y);
		}
		Vector2D operator-(type otherValue) const
		{
			return Vector2D(x - otherValue, y - otherValue);
		}
		Vector2D& operator-=(const Vector2D<type> &otherVect)
		{
			x -= otherVect.x;
			y -= otherVect.y;
			return *this;
		}
		Vector2D& operator-=(type otherValue)
		{
			x -= otherValue;
			y -= otherValue;
			return *this;
		}
		Vector2D& operator--()
		{
			--x;
			--y;
			return *this;
		}
		Vector2D operator--(int)
		{
			Vector2D currentVal(*this);
			operator--();
			return currentVal;
		}
		template<typename type2> Vector2D<type2> typecast() const
		{
			return Vector2D<type2>(type2(x), type2(y));
		}
	};
	template <typename type> class Vector3D
	{
	public:
		type x, y, z;
		Vector3D(type x, type y, type z):x(x), y(y), z(z) {}
		Vector3D(void){}
		~Vector3D(void){}
		bool operator==(const Vector3D<type> &otherVect) const
		{
			return x == otherVect.x && y == otherVect.y && z == otherVect.z;
		}
		bool operator!=(const Vector3D<type> &otherVect) const
		{
			return !(*this == otherVect);
		}
		bool operator<(const Vector3D<type> &otherVect) const
		{

			if (x == otherVect.x)
			{
				if (y == otherVect.y)
				{
					return z < otherVect.z;
				}
				else
				{
					return y < otherVect.y;
				}
			}
			else
			{
				return x < otherVect.x;
			}
		}
		bool operator<=(const Vector3D<type> &otherVect) const
		{
			if (x == otherVect.x)
			{
				if (y == otherVect.y)
				{
					return z <= otherVect.z;
				}
				else
				{
					return y <= otherVect.y;
				}
			}
			else
			{
				return x <= otherVect.x;
			}
		}
		bool operator>(const Vector3D<type> &otherVect) const
		{
			if (x == otherVect.x)
			{
				if (y == otherVect.y)
				{
					return z > otherVect.z;
				}
				else
				{
					return y > otherVect.y;
				}
			}
			else
			{
				return x > otherVect.x;
			}
		}
		bool operator>=(const Vector3D<type> &otherVect) const
		{
			if (x == otherVect.x)
			{
				if (y == otherVect.y)
				{
					return z >= otherVect.z;
				}
				else
				{
					return y >= otherVect.y;
				}
			}
			else
			{
				return x >= otherVect.x;
			}
		}
		Vector3D& operator*=(const Vector3D<type> &otherVect)
		{
			x *= otherVect.x;
			y *= otherVect.y;
			z *= otherVect.z;
			return *this;
		}
		Vector3D& operator*=(type otherValue)
		{
			x *= otherValue;
			y *= otherValue;
			z *= otherValue;
			return *this;
		}
		Vector3D operator*(const Vector3D<type> &otherVect) const
		{
			return Vector3D(x * otherVect.x, y * otherVect.y, z * otherVect.z);
		}
		Vector3D operator*(type otherValue) const
		{
			return Vector3D(x * otherValue, y * otherValue, z * otherValue);
		}

		//division
		Vector3D& operator/=(const Vector3D<type> &otherVect)
		{
			x /= otherVect.x;
			y /= otherVect.y;
			z /= otherVect.z;
			return *this;
		}
		Vector3D& operator/=(type otherValue)
		{
			x /= otherValue;
			y /= otherValue;
			z /= otherValue;
			return *this;
		}
		Vector3D operator/(const Vector3D<type> &otherVect) const
		{
			return Vector3D(x / otherVect.x, y / otherVect.y, z / otherVect.z);
		}
		Vector3D operator/(type otherValue) const
		{
			return Vector3D(x / otherValue, y / otherValue, z / otherValue);
		}

		//addition
		Vector3D& operator+=(const Vector3D<type> &otherVect)
		{
			x += otherVect.x;
			y += otherVect.y;
			z += otherVect.z;
			return *this;
		}
		Vector3D& operator+=(type otherValue)
		{
			x += otherValue;
			y += otherValue;
			z += otherValue;
			return *this;
		}
		Vector3D operator+(const Vector3D<type> &otherVect) const
		{
			return Vector3D(x + otherVect.x, y + otherVect.y, z + otherVect.z);
		}
		Vector3D operator+(type otherValue) const
		{
			return Vector3D(x + otherValue, y + otherValue, z + otherValue);
		}
		Vector3D& operator++()
		{
			++x;
			++y;
			++z;
			return *this;
		}
		Vector3D operator++(int)
		{
			Vector3D currentVal(*this);
			operator++();
			return currentVal;
		}

		//subtraction
		
		Vector3D operator-() const
		{
			return Vector3D(-x, -y, -z);
		}
		Vector3D operator-(const Vector3D<type> &otherVect) const
		{
			return Vector3D(x - otherVect.x, y - otherVect.y, z - otherVect.z);
		}
		Vector3D operator-(type otherValue) const
		{
			return Vector3D(x - otherValue, y - otherValue, z - otherValue);
		}
		Vector3D& operator-=(const Vector3D<type> &otherVect)
		{
			x -= otherVect.x;
			y -= otherVect.y;
			z -= otherVect.z;
			return *this;
		}
		Vector3D& operator-=(type otherValue)
		{
			x -= otherValue;
			y -= otherValue;
			z -= otherValue;
			return *this;
		}
		Vector3D& operator--()
		{
			--x;
			--y;
			--z;
			return *this;
		}
		Vector3D operator--(int)
		{
			Vector3D currentVal(*this);
			operator--();
			return currentVal;
		}
		template<typename type2> Vector3D<type2> typecast() const
		{
			return Vector3D<type2>(type2(x), type2(y), type2(z));
		}
	};

}