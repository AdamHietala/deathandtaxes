#include "ThreadScheduler.h"
#include <chrono>
#include <thread>
#include "GlobalDefines.h"
namespace dat
{

	ThreadScheduler::ThreadScheduler()
	{
		worksInProgress = 0;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	ThreadScheduler::~ThreadScheduler()
	{
		setNumberOfThreads(0);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void ThreadScheduler::scheduleWork(std::function<void(void *p)> work, void *argument)
	{
		workMutex.lock();
		workList.push_back(work);
		arguments.push_back(argument);
		workMutex.unlock();

#ifdef SINGLE_THREADED
		waitForWorkToFinish();
#endif
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void ThreadScheduler::setNumberOfThreads(int numberOfThreads)
	{
#ifdef SINGLE_THREADED
		numberOfThreads = 0;
#endif


		//Shut down all other threads. They will first finish the current workload though.
		runThreads = false;
		for (auto thread : threads)
		{
			thread->join();
			delete thread;
		}
		threads.clear();

		//create new helper threads
		runThreads = true;

		for (int i = 0; i < numberOfThreads; ++i)
		{
			threads.push_back(new std::thread(
				[&]()
			{
				while (runThreads)
				{
					helpWithWork(); //Will return when the workList is empty.
					std::this_thread::sleep_for(std::chrono::milliseconds(10)); //The worklist is now empty and we sleep for a few miliseconds to see if there is more work later.
				}
			}
			));
		}

	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	int ThreadScheduler::getNumberOfThreads()
	{
#ifdef SINGLE_THREADED
		return 1; //so that it reports to others that it has at least one worker thread
#endif

		return (int)threads.size(); //last thread is the thraid waiting
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void ThreadScheduler::helpWithWork()
	{
		while (true)
		{
			workMutex.lock();
			if (workList.size() > 0)
			{
				auto f = workList.front();
				workList.pop_front();
				void *argument = arguments.front();
				arguments.pop_front();
				worksInProgress++;
				workMutex.unlock();
				f(argument);
				workMutex.lock();
				worksInProgress--;
				workMutex.unlock();
			}
			else
			{
				workMutex.unlock();
				return;
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	bool ThreadScheduler::threadsAreWorking()
	{
		workMutex.lock();
		int works = (int)workList.size() + worksInProgress;
		workMutex.unlock();
		return works > 0;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void ThreadScheduler::waitForWorkToFinish()
	{
		while (threadsAreWorking())
		{
			helpWithWork(); //Will return when the workList is empty.
			std::this_thread::sleep_for(std::chrono::milliseconds(10)); //The worklist is now empty and we sleep for a few miliseconds to see if there is more work later.
		}
	}
}