#include "DataFileIO.h"
#include <assert.h>
#include <functional>
#include "Exceptions.h"
#include "FileIO.h"
#include "StringManipulation.h"
#include "DataFileIODefines.h"
namespace dat
{
	DataFileIO::Group::Group(Group *up) : up(up)
	{

	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	DataFileIO::Group::~Group()
	{
		for (auto subgroup : subGroups)
		{
			delete subgroup.second;
		}
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::string DataFileIO::toString(std::string option, Group *group, std::string indentation)
	{
		if (option != "")
		{
			option += " ";
		}
		std::string output;
		for (auto str : group->strings)
		{
			output += indentation + option + TYPE_STRING + " " + str.first + " = {"
				+ escapeSpecialCharacters(str.second);
			output += "}\n";
		}

		for (auto sub : group->subGroups)
		{
			std::string subStr = toString(option, sub.second, indentation + "	");
			if (subStr != "")
			{
				subStr = "\n" + subStr + indentation;
			}
			output += indentation + option + TYPE_GROUP + " " + sub.first + " = {" + subStr + "}\n";
		}
		return output;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::loadFromString(std::string &str, Group *group, int from, int to) //Loads this group from a string and all subgroups recursively.
	{

		assert(to <= (int) str.size());
		int i = from;

		while (i < to)
		{
			std::string optionStr, typeStr, keyword;
			getNextWord(str, optionStr, i, to, false);
			if (optionStr == "")
			{
				return;
			}
			Option option;
			try{
				option = stringToOption(optionStr);
				getNextWord(str, typeStr, i, to, false);
			}
			catch (ParsingException)
			{
				option = Option::StrongAdd;
				typeStr = optionStr;
			}
			testException(typeStr != "", ParsingException("Type keyword was interpreted as empty."));
			testException(typeStr == TYPE_GROUP || typeStr == TYPE_STRING, ParsingException("\"" + typeStr + "\" could not be interpreted as a type."));

			getNextWord(str, keyword, i, to, true);
			testException(keyword != "", ParsingException("Key was interpreted as empty."));


			if (option == Option::StrongAdd || option == Option::WeakAdd)
			{
				std::string nextWord;
				getNextWord(str, nextWord, i, to, false);
				testException(nextWord == "=", ParsingException("Command followed by \"" + nextWord + "\" instead of \"={\""));
				getNextWord(str, nextWord, i, to, false);
				testException(nextWord == "{", ParsingException("Command followed by \"" + nextWord + "\" instead of \"={\""));





				int endBracket = findEndBracket(str, i - 1, to);

				assert(str[i - 1] == '{');
				assert(str[endBracket] == '}');
				if (typeStr == TYPE_STRING)
				{
					if (option == Option::StrongAdd || group->strings.count(keyword) == 0)
					{
						group->strings[keyword] = parseEscapeSequences(getSubString(str, i, endBracket));
					}
					i = endBracket + 1;
				}
				else
				{
					assert(typeStr == TYPE_GROUP);
					if (group->subGroups.count(keyword) == 0)
					{
						Group *newGroup = new Group(group);
						group->subGroups[keyword] = newGroup;
						loadFromString(str, newGroup, i, endBracket);
					}
					else if (option == Option::StrongAdd)
					{
						loadFromString(str, group->subGroups.at(keyword), i, endBracket);
					}
					i = endBracket + 1;
				}
			}
			else if (option == Option::Clear)
			{
				if (typeStr == TYPE_STRING)
				{
					testException(group->strings.count(keyword) == 1, KeyNotFoundException("The string \"" + keyword + "\" could not be cleared since it doesn't exist."));
					group->strings[keyword] = "";
				}
				else
				{
					assert(typeStr == TYPE_GROUP);
					testException(group->subGroups.count(keyword) == 1, KeyNotFoundException("The subgroup \"" + keyword + "\" could not be cleared since it doesn't exist."));
					group->subGroups[keyword]->strings.clear();
					for (auto sub : group->subGroups[keyword]->subGroups)
					{
						delete sub.second;
					}
					group->subGroups[keyword]->subGroups.clear();
				}
			}
			else
			{
				assert(option == Option::Delete);
				if (typeStr == TYPE_STRING)
				{
					testException(group->strings.count(keyword) == 1, KeyNotFoundException("The string \"" + keyword + "\" could not be deleted since it doesn't exist."));
					group->strings.erase(keyword);
				}
				else
				{
					assert(typeStr == TYPE_GROUP);
					testException(group->subGroups.count(keyword) == 1, KeyNotFoundException("The subgroup \"" + keyword + "\" could not be deleted since it doesn't exist."));
					group->subGroups[keyword]->strings.clear();
					delete group->subGroups.at(keyword);
					group->subGroups.erase(keyword);
				}
			}
		}
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	int DataFileIO::findEndBracket(std::string &str, int from, int to) 
	{
		assert(str[from] == '{');
		int count = 1;
		bool escaped = false, commentedOut = false;
		for (int i = from + 1; i < to; ++i)
		{
			assert(!(escaped && commentedOut));
			if (escaped)
			{
				escaped = false;
			}
			else if (commentedOut)
			{
				if (str[i] == '\n')
				{
					commentedOut = false;
				}
			}
			else
			{
				if (str[i] == '#')
				{
					commentedOut = true;
				}
				else if (str[i] == '\\')
				{
					escaped = true;
				}
				else
				{
					if (str[i] == '{')
					{
						++count;
					}
					else if (str[i] == '}')
					{
						--count;
						if (count == 0)
						{
							return i;
						}
					}
				}
			}
		}
		throw(ParsingException("No endbracket could be found."));
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::getNextWord(std::string &str, std::string &word, int &from, int to, bool key) //Returns the next word while parsing a load from string. Will make whatever variable put in "from" to tick towards to.
	{
		word = "";
		while (true)//Search for next word, skipping whitespaces.
		{
			if (from >= to) //End of str
			{
				return;
			}
			else if (!isSpace(str[from]))
			{
				if (str[from] == '#')
				{
					from = findFirstOccurrence(str, "\n", from);
					assert(str[from] == '\n' || from >= to);
					if (from >= to) //End of str
					{
						return;
					}
				}
				else
				{
					break;
				}
			}
			else
			{
				++from;
			}
		}
		if (from >= to) //End of str
		{
			throw(ParsingException("Unexpected end of string."));
		}
		else if (str[from] == '=' || str[from] == '{')
		{
			word += str[from];
			++from;
			return;
		}

		
		while (true) //Read the word into option
		{
			if (from == to) //End of str
			{
				return;
			}

			if ((!isSpace(str[from]) && str[from] != '=' )|| (key && isSpace(str[from]) && str[from] != '\n'))
			{
				word += str[from];
				++from;
			}
			else
			{
				if (key)
				{
					word = trimWhiteSpaces(word);
				}
				return;
			}
		}
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::string DataFileIO::escapeSpecialCharacters(std::string &str)
	{
		std::string newStr;
		for (auto chr : str)
		{
			switch (chr)
			{
			case '\n':
				newStr += "\\n";
				break;

			case '\\':
			case '{':
			case '}':
			case '#':
				newStr += "\\";
			default: 
				newStr += chr;
			}
		}
		return newStr;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::string DataFileIO::parseEscapeSequences(std::string &str)
	{
		std::string newStr;
		bool escaped = false;
		for (auto chr : str)
		{
			if (escaped)
			{
				switch (chr)
				{
				case 'n':
					newStr += "\n";
					break;

				case '\\':
				case '{':
				case '}':
				case '#':
					newStr += chr;
					break;

				default: 
					newStr += "\\" + chr;
				}
				escaped = false;
			}
			else if (chr == '\\')
			{
				escaped = true;
			}
			else
			{
				newStr += chr;
			}
		}
		return newStr;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	DataFileIO::DataFileIO() : anchor(nullptr) //This is the top Group and has nothing above it.
	{
		openedGroup = &anchor;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	DataFileIO::~DataFileIO()
	{

	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::string DataFileIO::optionToString(Option option)
	{
		switch (option)
		{
		case Option::StrongAdd: return OPTION_STRONG_ADD;
		case Option::Clear: return OPTION_CLEAR;
		case Option::Delete: return OPTION_DELETE;
		case Option::WeakAdd: return OPTION_WEAK_ADD;
		}
		assert(false); //The program should never get here.
		return OPTION_STRONG_ADD; //This is to get rid of a warning that this function doesn't always return a value.
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	DataFileIO::Option DataFileIO::stringToOption(std::string str)
	{
		auto ops = { Option::StrongAdd, Option::Clear, Option::Delete, Option::WeakAdd };
		for (auto op : ops)
		{
			if (str == optionToString(op))
			{
				return op;
			}
		}
		throw (ParsingException("\"" + str + "\" could not be parsed into an option."));
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::addGroup(std::string key)
	{
		testException(!existsGroup(key), KeyAlreadyExistsException("Group key: " + key));
		openedGroup->subGroups[key] = new Group(openedGroup);
		openedGroup = openedGroup->subGroups[key];
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::addString(std::string key, std::string content)
	{
		testException(!existsString(key), KeyAlreadyExistsException("String key: " + key));
		openedGroup->strings[key] = content;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::removeGroup(std::string key)
	{
		testException(existsGroup(key), KeyNotFoundException("Group key: " + key));
		openedGroup->subGroups.erase(key);
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::removeString(std::string key) //Removes the string with the name key. Throws KeyNotFoundException if the key does not exist.
	{
		testException(existsString(key), KeyNotFoundException("String key: " + key));
		openedGroup->strings.erase(key);
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::clearGroup(std::string key)
	{
		testException(existsGroup(key), KeyNotFoundException("Group key: " + key));

		Group *subgroup = openedGroup->subGroups[key];
		subgroup->strings.clear();
		subgroup->subGroups.clear();
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::clearString(std::string key)
	{
		testException(existsString(key), KeyNotFoundException("String key: " + key));
		openedGroup->strings[key] = "";
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	bool DataFileIO::existsGroup(std::string key)
	{
		return openedGroup->subGroups.count(key) != 0;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	bool DataFileIO::existsString(std::string key)
	{
		return openedGroup->strings.count(key) != 0;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	void DataFileIO::openGroup(std::string key)
	{
		testException(existsGroup(key), KeyNotFoundException("Group key '" + key + "' not found."));
		openedGroup = openedGroup->subGroups.at(key);
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::closeGroup()
	{

		testException(openedGroup->up != nullptr, AlreadyAtTopLevelException());
		openedGroup = openedGroup->up;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::string DataFileIO::getString(std::string key)
	{
		testException(existsString(key), KeyNotFoundException("String key '" + key + "' not found."));
		return openedGroup->strings.at(key);
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::vector<std::string> DataFileIO::getStringKeys()
	{
		std::vector<std::string> keys;
		for (auto group : openedGroup->strings)
		{
			keys.push_back(group.first);
		}
		return keys;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::vector<std::string> DataFileIO::getGroupKeys()
	{
		std::vector<std::string> keys;
		for (auto group : openedGroup->subGroups)
		{
			keys.push_back(group.first);
		}
		return keys;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::loadFromString(std::string str)
	{
		loadFromString(str, &anchor, 0, str.size());
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::string DataFileIO::toString(bool strong) //Creates a string representing this DataFileIO from the data it contains to be loaded in another dataFileIO or saved in a file.
	{
		return trimWhiteSpaces(toString(strong ? "" : optionToString(Option::WeakAdd), &anchor));
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::loadFromFile(std::string filePath)
	{
		loadFromString(trimWhiteSpaces(getFileContent(filePath)));
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::saveToFile(std::string filePath, bool strong)
	{
		writeToFile(toString(strong), filePath);
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::clearData()
	{
		openedGroup = &anchor;
		anchor.strings.clear();
		anchor.subGroups.clear();
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void DataFileIO::closeAllGroups()
	{
		openedGroup = &anchor;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	bool DataFileIO::anyGroupOpened()
	{
		return openedGroup != &anchor;
	}
}